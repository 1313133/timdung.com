let canvas;
let click_button;
let videocamera;

$(document).ready(function() {
    canvas = document.querySelector("#mainCanvas");
    videocamera = document.querySelector("#video");

    var categoryTypeCurrent = sessionStorage.getItem('categoryIdGlobal');
    if (categoryTypeCurrent == null || categoryTypeCurrent == "") {
        return;
    }

    click_button = document.querySelector("#click-photo");

    click_button.addEventListener('click', function() {

        var ctx = canvas.getContext('2d');
        ctx.translate(360, 0);
        ctx.scale(-1, 1);
        ctx.drawImage(videocamera, 0, 0, 360, 480);

        let image_data_url = canvas.toDataURL('image/jpeg');

        let imagesrc = document.getElementById("resultImage2");
        let fileOpen = document.getElementById("fileOpen");


        imagesrc.src = image_data_url;
        fileOpen.src = canvas.toDataURL('image/jpeg');
        showResultImage(fileOpen);


        // data url of the image
        console.log(image_data_url);
    });

    getAllColorByCategroy(categoryTypeCurrent);
});







function loadProductByColor(colorCode = "#ee7b7e") {


    var companyId = "610e497b852dc227fcce0a5c";
    var companyData = window.sessionStorage.getItem('companyData');
    if (companyData == null) {


    } else {
        companyData = JSON.parse(companyData);
        companyId = "6129ad26e6ad9b5abfbacb8f";
    }

    $.ajax({
        url: "https://api-soida.applamdep.com/api/get-makeup-data",
        type: "post",
        data: {
            hex: colorCode,
            company_id: companyData

        },

        success: function(response) {

            loadProductAndBrand(response.data);
        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });


}

function loadProductAndBrand(data) {

    $(".resultHeader").show();

    var listProduct = data.list_product;
    var htmltemplate = "";
    var listbrand = data.list_brand;

    $("#recomend-product").empty();
    $("#makeup-brand").empty();
    $("#recomend-product").removeClass("owl-loaded owl-drag ");
    $("#makeup-brand").removeClass("slick-initialized slick-slider");

    reinstallSlide();
    htmltemplate += '<div class="productcontainer owl-carousel">';
    for (var i = 0; i < listProduct.length; i++) {


        var product_id = listProduct[i];
        var imagelink = product_id.image;

        if (product_id.image_link) {
            if (product_id.image_link != "") {
                imagelink = "https://api-soida.applamdep.com/public/image_makeup/" + product_id.image_link;
            }
        }


        htmltemplate += '<div class="productItem ">\
        <div class="productImage ">\
            <a href="javascript:void(0)">\
                <img src=' + '"' + imagelink + '"' + ' />\
            </a>\
        </div>\
        <div class="productTitle ">\
            <a href=' + '"' + product_id.href + '"' + '> ' + product_id.name + ' </a>\
        </div>\
        <div class="btnSeemore ">\
            <a href=' + '"' + product_id.href + '"' + '>Mua hàng</a>\
        </div>\
        </div>';

    }
    htmltemplate += '</div>';

    $("#recomendTitle").show();



    $("#containerProduct").html(htmltemplate);
    // $("#makeup-brand").html(htmlbrqandTemplate);
    $("#containerProduct").show();

    $(".productcontainer").owlCarousel();
    // alert(listProduct.length);
    // return;
    // debugger;

    setTimeout(() => {
        $(".loading").removeClass("loadingcameraShow");

    }, 1000);


    if (listProduct.length == 1) {

        $('#recomend-product').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }

    if (listProduct.length == 2) {
        return;
        $('#recomend-product').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2


        });
    }

    if (listProduct.length > 2) {

        return;
        $('#recomend-product').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    }

}

function reinstallSlide() {
    var containerSlidecheck = $("#recomend-product");
    if (containerSlidecheck.hasClass("owl-loaded owl-drag ")) {
        containerSlidecheck.owlCarousel({
            touchDrag: false,
            mouseDrag: false
        });
        containerSlidecheck.data('owlCarousel').destroy();
        containerSlidecheck.removeClass('owl-carousel owl-loaded');
        containerSlidecheck.find('.owl-stage-outer').children().unwrap();
        containerSlidecheck.removeData();
    }
}