! function(t, e) { "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.MTAR = e() : t.MTAR = e() }(window, function() {
    return i = {}, r.m = n = {
        "+SFK": function(t, e, n) { n("AUvm"), n("wgeU"), n("adOz"), n("dl0q"), t.exports = n("WEpk").Symbol },
        "+oT+": function(t, e, n) {
            var c = n("eVuF");

            function l(t, e, n, i, r, a, o) {
                try {
                    var s = t[a](o),
                        l = s.value
                } catch (t) { return void n(t) }
                s.done ? e(l) : c.resolve(l).then(i, r)
            }
            t.exports = function(s) {
                return function() {
                    var t = this,
                        o = arguments;
                    return new c(function(e, n) {
                        var i = s.apply(t, o);

                        function r(t) { l(i, e, n, r, a, "next", t) }

                        function a(t) { l(i, e, n, r, a, "throw", t) }
                        r(void 0)
                    })
                }
            }
        },
        "+plK": function(t, e, n) { n("ApPD"), t.exports = n("WEpk").Object.getPrototypeOf },
        "/+P4": function(e, t, n) {
            var i = n("Bhuq"),
                r = n("TRZx");

            function a(t) { return e.exports = a = r ? i : function(t) { return t.__proto__ || i(t) }, a(t) }
            e.exports = a
        },
        "/HRN": function(t, e) { t.exports = function(t, e) { if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function") } },
        "/eQG": function(t, e, n) {
            n("v5Dd");
            var i = n("WEpk").Object;
            t.exports = function(t, e) { return i.getOwnPropertyDescriptor(t, e) }
        },
        "0tVQ": function(t, e, n) { n("FlQf"), n("VJsP"), t.exports = n("WEpk").Array.from },
        "1amR": function(t, e, n) {
            var i = n("/+P4");
            t.exports = function(t, e) { for (; !Object.prototype.hasOwnProperty.call(t, e) && null !== (t = i(t));); return t }
        },
        "29s/": function(t, e, n) {
            var i = n("WEpk"),
                r = n("5T2Y"),
                a = "__core-js_shared__",
                o = r[a] || (r[a] = {});
            (t.exports = function(t, e) { return o[t] || (o[t] = void 0 !== e ? e : {}) })("versions", []).push({ version: i.version, mode: n("uOPS") ? "pure" : "global", copyright: "© 2019 Denis Pushkarev (zloirock.ru)" })
        },
        "2Eek": function(t, e, n) { t.exports = n("W7oM") },
        "2GTP": function(t, e, n) {
            var a = n("eaoh");
            t.exports = function(i, r, t) {
                if (a(i), void 0 === r) return i;
                switch (t) {
                    case 1:
                        return function(t) { return i.call(r, t) };
                    case 2:
                        return function(t, e) { return i.call(r, t, e) };
                    case 3:
                        return function(t, e, n) { return i.call(r, t, e, n) }
                }
                return function() { return i.apply(r, arguments) }
            }
        },
        "2Nb0": function(t, e, n) { n("FlQf"), n("bBy9"), t.exports = n("zLkG").f("iterator") },
        "2SVd": function(t, e, n) {
            "use strict";
            t.exports = function(t) { return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(t) }
        },
        "2faE": function(t, e, n) {
            var i = n("5K7Z"),
                r = n("eUtF"),
                a = n("G8Mo"),
                o = Object.defineProperty;
            e.f = n("jmDH") ? Object.defineProperty : function(t, e, n) {
                if (i(t), e = a(e, !0), i(n), r) try { return o(t, e, n) } catch (t) {}
                if ("get" in n || "set" in n) throw TypeError("Accessors not supported!");
                return "value" in n && (t[e] = n.value), t
            }
        },
        "3GJH": function(t, e, n) {
            n("lCc8");
            var i = n("WEpk").Object;
            t.exports = function(t, e) { return i.create(t, e) }
        },
        "3UD+": function(t, e) {
            t.exports = function(t) {
                if (!t.webpackPolyfill) {
                    var e = Object.create(t);
                    e.children || (e.children = []), Object.defineProperty(e, "loaded", { enumerable: !0, get: function() { return e.l } }), Object.defineProperty(e, "id", { enumerable: !0, get: function() { return e.i } }), Object.defineProperty(e, "exports", { enumerable: !0 }), e.webpackPolyfill = 1
                }
                return e
            }
        },
        "49sm": function(t, e) {
            var n = {}.toString;
            t.exports = Array.isArray || function(t) { return "[object Array]" == n.call(t) }
        },
        "4mXO": function(t, e, n) { t.exports = n("7TPF") },
        "5K7Z": function(t, e, n) {
            var i = n("93I4");
            t.exports = function(t) { if (!i(t)) throw TypeError(t + " is not an object!"); return t }
        },
        "5T2Y": function(t, e) { var n = t.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(); "number" == typeof __g && (__g = n) },
        "5biZ": function(t, e, n) {
            n("EnHN");
            var i = n("WEpk").Object;
            t.exports = function(t) { return i.getOwnPropertyNames(t) }
        },
        "5oMp": function(t, e, n) {
            "use strict";
            t.exports = function(t, e) { return e ? t.replace(/\/+$/, "") + "/" + e.replace(/^\/+/, "") : t }
        },
        "5pKv": function(t, e) { t.exports = "\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff" },
        "5vMV": function(t, e, n) {
            var o = n("B+OT"),
                s = n("NsO/"),
                l = n("W070")(!1),
                c = n("VVlx")("IE_PROTO");
            t.exports = function(t, e) {
                var n, i = s(t),
                    r = 0,
                    a = [];
                for (n in i) n != c && o(i, n) && a.push(n);
                for (; e.length > r;) o(i, n = e[r++]) && (~l(a, n) || a.push(n));
                return a
            }
        },
        "6/1s": function(t, e, n) {
            function i(t) { s(t, r, { value: { i: "O" + ++l, w: {} } }) }
            var r = n("YqAc")("meta"),
                a = n("93I4"),
                o = n("B+OT"),
                s = n("2faE").f,
                l = 0,
                c = Object.isExtensible || function() { return !0 },
                u = !n("KUxP")(function() { return c(Object.preventExtensions({})) }),
                h = t.exports = {
                    KEY: r,
                    NEED: !1,
                    fastKey: function(t, e) {
                        if (!a(t)) return "symbol" == typeof t ? t : ("string" == typeof t ? "S" : "P") + t;
                        if (!o(t, r)) {
                            if (!c(t)) return "F";
                            if (!e) return "E";
                            i(t)
                        }
                        return t[r].i
                    },
                    getWeak: function(t, e) {
                        if (!o(t, r)) {
                            if (!c(t)) return !0;
                            if (!e) return !1;
                            i(t)
                        }
                        return t[r].w
                    },
                    onFreeze: function(t) { return u && h.NEED && c(t) && !o(t, r) && i(t), t }
                }
        },
        "6BQ9": function(t, e, n) { t.exports = n("uekQ") },
        "6CmU": function(t, e, n) { n("LzdP"), t.exports = n("WEpk").Date.now },
        "6tYh": function(t, e, r) {
            function a(t, e) { if (i(t), !n(e) && null !== e) throw TypeError(e + ": can't set as prototype!") }
            var n = r("93I4"),
                i = r("5K7Z");
            t.exports = {
                set: Object.setPrototypeOf || ("__proto__" in {} ? function(t, n, i) {
                    try {
                        (i = r("2GTP")(Function.call, r("vwuL").f(Object.prototype, "__proto__").set, 2))(t, []), n = !(t instanceof Array)
                    } catch (t) { n = !0 }
                    return function(t, e) { return a(t, e), n ? t.__proto__ = e : i(t, e), t }
                }({}, !1) : void 0),
                check: a
            }
        },
        "7TPF": function(t, e, n) { n("AUvm"), t.exports = n("WEpk").Object.getOwnPropertySymbols },
        "7m0m": function(t, e, n) {
            var i = n("Y7ZC"),
                l = n("uplh"),
                c = n("NsO/"),
                u = n("vwuL"),
                h = n("IP1Z");
            i(i.S, "Object", { getOwnPropertyDescriptors: function(t) { for (var e, n, i = c(t), r = u.f, a = l(i), o = {}, s = 0; a.length > s;) void 0 !== (n = r(i, e = a[s++])) && h(o, e, n); return o } })
        },
        "8+Nu": function(t, e, n) {
            var i = n("8bdy"),
                r = n("fprZ"),
                a = n("QN8s"),
                o = n("Bh1o");
            t.exports = function(t, e) { return i(t) || r(t, e) || a(t, e) || o() }
        },
        "8bdy": function(t, e, n) {
            var i = n("p0XB");
            t.exports = function(t) { if (i(t)) return t }
        },
        "8gHz": function(t, e, n) {
            var r = n("5K7Z"),
                a = n("eaoh"),
                o = n("UWiX")("species");
            t.exports = function(t, e) { var n, i = r(t).constructor; return void 0 === i || null == (n = r(i)[o]) ? e : a(n) }
        },
        "8oxB": function(t, e) {
            var n, i, r = t.exports = {};

            function a() { throw new Error("setTimeout has not been defined") }

            function o() { throw new Error("clearTimeout has not been defined") }

            function s(e) { if (n === setTimeout) return setTimeout(e, 0); if ((n === a || !n) && setTimeout) return n = setTimeout, setTimeout(e, 0); try { return n(e, 0) } catch (t) { try { return n.call(null, e, 0) } catch (t) { return n.call(this, e, 0) } } }! function() { try { n = "function" == typeof setTimeout ? setTimeout : a } catch (t) { n = a } try { i = "function" == typeof clearTimeout ? clearTimeout : o } catch (t) { i = o } }();
            var l, c = [],
                u = !1,
                h = -1;

            function d() { u && l && (u = !1, l.length ? c = l.concat(c) : h = -1, c.length && f()) }

            function f() {
                if (!u) {
                    var t = s(d);
                    u = !0;
                    for (var e = c.length; e;) {
                        for (l = c, c = []; ++h < e;) l && l[h].run();
                        h = -1, e = c.length
                    }
                    l = null, u = !1,
                        function(e) { if (i === clearTimeout) return clearTimeout(e); if ((i === o || !i) && clearTimeout) return i = clearTimeout, clearTimeout(e); try { i(e) } catch (t) { try { return i.call(null, e) } catch (t) { return i.call(this, e) } } }(t)
                }
            }

            function m(t, e) { this.fun = t, this.array = e }

            function p() {}
            r.nextTick = function(t) {
                var e = new Array(arguments.length - 1);
                if (1 < arguments.length)
                    for (var n = 1; n < arguments.length; n++) e[n - 1] = arguments[n];
                c.push(new m(t, e)), 1 !== c.length || u || s(f)
            }, m.prototype.run = function() { this.fun.apply(null, this.array) }, r.title = "browser", r.browser = !0, r.env = {}, r.argv = [], r.version = "", r.versions = {}, r.on = p, r.addListener = p, r.once = p, r.off = p, r.removeListener = p, r.removeAllListeners = p, r.emit = p, r.prependListener = p, r.prependOnceListener = p, r.listeners = function(t) { return [] }, r.binding = function(t) { throw new Error("process.binding is not supported") }, r.cwd = function() { return "/" }, r.chdir = function(t) { throw new Error("process.chdir is not supported") }, r.umask = function() { return 0 }
        },
        "93I4": function(t, e) { t.exports = function(t) { return "object" == typeof t ? null !== t : "function" == typeof t } },
        "9BDd": function(t, e, n) { n("GvbO"), t.exports = n("WEpk").Array.isArray },
        "9Jkg": function(t, e, n) { t.exports = n("oh+g") },
        "9rSQ": function(t, e, n) {
            "use strict";
            var i = n("xTJ+");

            function r() { this.handlers = [] }
            r.prototype.use = function(t, e) { return this.handlers.push({ fulfilled: t, rejected: e }), this.handlers.length - 1 }, r.prototype.eject = function(t) { this.handlers[t] && (this.handlers[t] = null) }, r.prototype.forEach = function(e) { i.forEach(this.handlers, function(t) { null !== t && e(t) }) }, t.exports = r
        },
        "9tPo": function(t, e) {
            t.exports = function(t) {
                var e = "undefined" != typeof window && window.location;
                if (!e) throw new Error("fixUrls requires window.location");
                if (!t || "string" != typeof t) return t;
                var r = e.protocol + "//" + e.host,
                    a = r + e.pathname.replace(/\/[^\/]*$/, "/");
                return t.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(t, e) { var n, i = e.trim().replace(/^"(.*)"$/, function(t, e) { return e }).replace(/^'(.*)'$/, function(t, e) { return e }); return /^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(i) ? t : (n = 0 === i.indexOf("//") ? i : 0 === i.indexOf("/") ? r + i : a + i.replace(/^\.\//, ""), "url(" + JSON.stringify(n) + ")") })
            }
        },
        A5Xg: function(t, e, n) {
            var i = n("NsO/"),
                r = n("ar/p").f,
                a = {}.toString,
                o = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];
            t.exports.f = function(t) { return o && "[object Window]" == a.call(t) ? function(t) { try { return r(t) } catch (t) { return o.slice() } }(t) : r(i(t)) }
        },
        A8zu: function(t, e, n) { t.exports = n("5biZ") },
        AUvm: function(t, e, n) {
            "use strict";

            function i(t) { var e = Y[t] = M(W[j]); return e._k = t, e }

            function r(t, e) { x(t); for (var n, i = k(e = S(e)), r = 0, a = i.length; r < a;) et(t, n = i[r++], e[n]); return t }

            function a(t) { var e = Z.call(this, t = F(t, !0)); return !(this === Q && u(Y, t) && !u(V, t)) && (!(e || !u(this, t) || !u(Y, t) || u(this, N) && this[N][t]) || e) }

            function o(t, e) { if (t = S(t), e = F(e, !0), t !== Q || !u(Y, e) || u(V, e)) { var n = L(t, e); return !n || !u(Y, e) || u(t, N) && t[N][e] || (n.enumerable = !0), n } }

            function s(t) { for (var e, n = O(S(t)), i = [], r = 0; n.length > r;) u(Y, e = n[r++]) || e == N || e == m || i.push(e); return i }

            function l(t) { for (var e, n = t === Q, i = O(n ? V : S(t)), r = [], a = 0; i.length > a;) !u(Y, e = i[a++]) || n && !u(Q, e) || r.push(Y[e]); return r }
            var c = n("5T2Y"),
                u = n("B+OT"),
                h = n("jmDH"),
                d = n("Y7ZC"),
                f = n("kTiW"),
                m = n("6/1s").KEY,
                p = n("KUxP"),
                v = n("29s/"),
                _ = n("RfKB"),
                g = n("YqAc"),
                w = n("UWiX"),
                b = n("zLkG"),
                y = n("Zxgi"),
                k = n("R+7+"),
                I = n("kAMH"),
                x = n("5K7Z"),
                A = n("93I4"),
                E = n("JB68"),
                S = n("NsO/"),
                F = n("G8Mo"),
                R = n("rr1i"),
                M = n("oVml"),
                C = n("A5Xg"),
                P = n("vwuL"),
                D = n("mqlF"),
                T = n("2faE"),
                z = n("w6GO"),
                L = P.f,
                B = T.f,
                O = C.f,
                W = c.Symbol,
                H = c.JSON,
                U = H && H.stringify,
                j = "prototype",
                N = w("_hidden"),
                K = w("toPrimitive"),
                Z = {}.propertyIsEnumerable,
                G = v("symbol-registry"),
                Y = v("symbols"),
                V = v("op-symbols"),
                Q = Object[j],
                J = "function" == typeof W && !!D.f,
                X = c.QObject,
                q = !X || !X[j] || !X[j].findChild,
                $ = h && p(function() { return 7 != M(B({}, "a", { get: function() { return B(this, "a", { value: 7 }).a } })).a }) ? function(t, e, n) {
                    var i = L(Q, e);
                    i && delete Q[e], B(t, e, n), i && t !== Q && B(Q, e, i)
                } : B,
                tt = J && "symbol" == typeof W.iterator ? function(t) { return "symbol" == typeof t } : function(t) { return t instanceof W },
                et = function(t, e, n) { return t === Q && et(V, e, n), x(t), e = F(e, !0), x(n), u(Y, e) ? (n.enumerable ? (u(t, N) && t[N][e] && (t[N][e] = !1), n = M(n, { enumerable: R(0, !1) })) : (u(t, N) || B(t, N, R(1, {})), t[N][e] = !0), $(t, e, n)) : B(t, e, n) };
            J || (f((W = function(t) {
                if (this instanceof W) throw TypeError("Symbol is not a constructor!");
                var e = g(0 < arguments.length ? t : void 0),
                    n = function(t) { this === Q && n.call(V, t), u(this, N) && u(this[N], e) && (this[N][e] = !1), $(this, e, R(1, t)) };
                return h && q && $(Q, e, { configurable: !0, set: n }), i(e)
            })[j], "toString", function() { return this._k }), P.f = o, T.f = et, n("ar/p").f = C.f = s, n("NV0k").f = a, D.f = l, h && !n("uOPS") && f(Q, "propertyIsEnumerable", a, !0), b.f = function(t) { return i(w(t)) }), d(d.G + d.W + d.F * !J, { Symbol: W });
            for (var nt = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), it = 0; nt.length > it;) w(nt[it++]);
            for (var rt = z(w.store), at = 0; rt.length > at;) y(rt[at++]);
            d(d.S + d.F * !J, "Symbol", {
                for: function(t) { return u(G, t += "") ? G[t] : G[t] = W(t) },
                keyFor: function(t) {
                    if (!tt(t)) throw TypeError(t + " is not a symbol!");
                    for (var e in G)
                        if (G[e] === t) return e
                },
                useSetter: function() { q = !0 },
                useSimple: function() { q = !1 }
            }), d(d.S + d.F * !J, "Object", { create: function(t, e) { return void 0 === e ? M(t) : r(M(t), e) }, defineProperty: et, defineProperties: r, getOwnPropertyDescriptor: o, getOwnPropertyNames: s, getOwnPropertySymbols: l });
            var ot = p(function() { D.f(1) });
            d(d.S + d.F * ot, "Object", { getOwnPropertySymbols: function(t) { return D.f(E(t)) } }), H && d(d.S + d.F * (!J || p(function() { var t = W(); return "[null]" != U([t]) || "{}" != U({ a: t }) || "{}" != U(Object(t)) })), "JSON", { stringify: function(t) { for (var e, n, i = [t], r = 1; r < arguments.length;) i.push(arguments[r++]); if (n = e = i[1], (A(e) || void 0 !== t) && !tt(t)) return I(e) || (e = function(t, e) { if ("function" == typeof n && (e = n.call(this, t, e)), !tt(e)) return e }), i[1] = e, U.apply(H, i) } }), W[j][K] || n("NegM")(W[j], K, W[j].valueOf), _(W, "Symbol"), _(Math, "Math", !0), _(c.JSON, "JSON", !0)
        },
        ApPD: function(t, e, n) {
            var i = n("JB68"),
                r = n("U+KD");
            n("zn7N")("getPrototypeOf", function() { return function(t) { return r(i(t)) } })
        },
        "B+OT": function(t, e) {
            var n = {}.hasOwnProperty;
            t.exports = function(t, e) { return n.call(t, e) }
        },
        Bh1o: function(t, e) { t.exports = function() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.") } },
        Bhuq: function(t, e, n) { t.exports = n("+plK") },
        C6ey: function(t, e, n) {
            (t.exports = n("JPst")(!1)).push([t.i, 'body,html{-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;margin:0;padding:0}body{font-family:Helvetica Neue,Tahoma,Arial,PingFangSC-Regular,Hiragino Sans GB,Microsoft Yahei,sans-serif}a{-webkit-tap-highlight-color:transparent}html{-webkit-box-sizing:border-box;box-sizing:border-box;color:#333}.mtar{word-break:break-all;-webkit-tap-highlight-color:transparent}.mtar a{text-decoration:none}.mtar__hide{display:none!important}.mtar__flex{-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.mtar__flex,.mtar__flex-y{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center}.mtar__center{position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.mtar__elli{white-space:nowrap}.mtar__elli,.mtar__elli-2{overflow:hidden;text-overflow:ellipsis}.mtar__elli-2{white-space:normal;word-wrap:break-word;display:-webkit-box;-webkit-box-orient:vertical;-webkit-line-clamp:2}.mtar__zindex-max{z-index:99999!important}.mtar__img-wrap{-webkit-box-sizing:border-box;box-sizing:border-box;border:1px solid transparent;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;margin-bottom:4px;-webkit-transition:all .3s;transition:all .3s}.mtar__file{opacity:.01;font-size:0;cursor:pointer}.mtar__preview-wrap{-ms-flex-negative:0;flex-shrink:0;font-size:16px;overflow:hidden;position:relative;-webkit-box-shadow:0 11px 40px 0 rgba(0,0,0,.06);box-shadow:0 11px 40px 0 rgba(0,0,0,.06);background-color:#000;color:#000}.mtar__pos-a,.mtar__pos-all{width:100%;height:100%;position:absolute;top:0;left:0}.mtar__pos-all{right:0;bottom:0}.mtar__video-wrap .video{position:absolute;top:150%;left:200%}.mtar__detect-photo-result,.mtar__result-content{background:#747474;visibility:hidden;opacity:0}.mtar__detect-photo-result img,.mtar__result-content img{max-width:100%;max-height:100%;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);z-index:10}.mtar__result-content img{-webkit-transform:translate(-50%,-57%);transform:translate(-50%,-57%)}.mtar__detect-photo-result{z-index:50}.mtar__detect-photo-result .mtar__cir_btn{position:absolute;top:20px;right:20px;z-index:10}.mtar__detect-photo-result.mtar__show .mtar__cir_btn{visibility:visible;opacity:1}.mtar__landscape{z-index:100;font-size:30px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.mtar__browser-tip,.mtar__landscape{background-color:#747474;color:#fff;visibility:hidden;opacity:0}.mtar__browser-tip{z-index:300;-webkit-box-sizing:border-box;box-sizing:border-box;font-size:18px;padding:20px 10px;text-align:center;position:fixed}.mtar__modal{opacity:0;visibility:hidden;-webkit-transition:all .3s;transition:all .3s;z-index:80}.mtar__modal.mtar__show{visibility:visible;opacity:1}.mtar__modal.mtar__show .modalBox{-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.mtar__modal .mask{background-color:rgba(0,0,0,.5)}.mtar__modal .modalBox{width:242px;border-radius:4px;-webkit-box-shadow:0 0 16px 0 rgba(0,0,0,.25);box-shadow:0 0 16px 0 rgba(0,0,0,.25);background-color:#fff;padding:50px 20px;-webkit-box-sizing:border-box;box-sizing:border-box;text-align:center;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}.mtar__modal .modalBox .icon-cam{width:32px;height:32px;margin-bottom:20px}.mtar__modal .modalBox .text{font-size:15px;line-height:22px}.mtar__modal .modalBox .icon-close-wrap{cursor:pointer;width:24px;height:24px;position:absolute;top:10px;right:10px}.mtar__modal .modalBox .icon-close{width:100%;height:100%}.mtar__sel{background-color:#fff;z-index:50;visibility:hidden;opacity:0}.mtar__sel .bg-rt{background:url(https://makeup-magic.zone1.meitudata.com/webar/images/webar_sdk/modal_rt.png) no-repeat 100% 0;background-size:100%;right:0;top:0}.mtar__sel .bg-lb,.mtar__sel .bg-rt{width:75%;height:50%;position:absolute}.mtar__sel .bg-lb{background:url(https://makeup-magic.zone1.meitudata.com/webar/images/webar_sdk/modal_lb.png) no-repeat 0 100%;background-size:100%;left:-2px;bottom:-2px}.mtar__sel-tit{font-size:18px;font-weight:500;line-height:25px;height:50px;position:relative;text-align:center;margin-bottom:50px}.mtar__sel-tit:after{content:"";width:30px;height:2px;background-color:#000;position:absolute;left:50%;bottom:0;-webkit-transform:translate(-50%);transform:translate(-50%)}.mtar__sel-btn{width:320px;height:48px;font-size:14px;color:#fff;background-color:#000;cursor:pointer;margin-bottom:30px;position:relative}.mtar__sel-btn .i{width:14px;height:14px;margin-right:10px}.mtar__sel-btn.gray{margin-bottom:20px}.mtar__sel-btn:active{opacity:.6}.mtar__sel-detect-wrap.disabled .mtar__sel-btn{cursor:not-allowed;background-color:rgba(0,0,0,.2)}.mtar__sel-detect-wrap.disabled .mtar__sel-tip{display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:start;-ms-flex-align:start;align-items:flex-start}.mtar__sel-tip{font-size:12px;font-weight:500;color:#fc3d3d;text-align:center;display:none}.mtar__sel-tip .i{margin-right:5px}.mtar__switch-sel{width:100%;height:32px;position:absolute;left:0;bottom:0;z-index:20;background-color:rgba(0,0,0,.7);cursor:pointer;color:#fff;font-size:14px}.mtar__switch-sel .i{margin-right:6px}.mtar__rt_btns{top:20px}.mtar__rb_btns,.mtar__rt_btns{position:absolute;right:20px;z-index:40}.mtar__rb_btns{bottom:52px;-webkit-transition:bottom .5s;transition:bottom .5s}.mtar__rb_btns .mtar__cir_btn:last-child{margin-bottom:0}.mtar__lb_btns{position:absolute;left:20px;bottom:52px;z-index:40;-webkit-transition:bottom .5s;transition:bottom .5s}.mtar__lb_btns .mtar__cir_btn:last-child{margin-bottom:0}.mtar__cir_btn{background:rgba(0,0,0,.5);width:40px;height:40px;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;margin-bottom:20px;border-radius:100%;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;position:relative;border:none;visibility:hidden;opacity:0}.mtar__cir_btn:active,.mtar__cir_btn:hover{opacity:.8}.mtar__cir_btn.disabled{opacity:.5;cursor:not-allowed}.mtar__cir_btn.active{background:#e31937}.mtar__cir_btn .icon{display:block;width:20px;height:20px;margin:auto;cursor:pointer}.mtar__cir_btn .i3{width:20px;height:18px}.mtar__cir_btn .i4{width:16px;height:18px}.mtar__cir_btn.material-switch{-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mtar .button-download{position:absolute;right:20px;bottom:20px;z-index:55;visibility:hidden;opacity:0}.mtar .button-download.mtar__show{opacity:1;visibility:visible}.mtar__loading{z-index:90;background:rgba(0,0,0,.5);display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-ms-flex-direction:column;flex-direction:column;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;visibility:hidden;opacity:0;-webkit-transition:all .3s;transition:all .3s}.mtar__loading .text{color:#fff;font-size:16px;margin-top:10px}.mtar__loading .lds-eclipse{position:relative;width:60px;height:60px}.mtar__loading .lds-eclipse div{-webkit-animation:lds-eclipse 1.5s linear infinite;animation:lds-eclipse 1.5s linear infinite;width:60px;height:60px;position:absolute;top:0;left:0;border-radius:50%;-webkit-box-shadow:0 2px 0 0 #fff;box-shadow:0 2px 0 0 #fff}@-webkit-keyframes lds-eclipse{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}@keyframes lds-eclipse{0%{-webkit-transform:rotate(0deg);transform:rotate(0deg)}to{-webkit-transform:rotate(1turn);transform:rotate(1turn)}}.mtar__show{visibility:visible;opacity:1}.mtar .title-line{display:inline-block;font-size:30px;line-height:42px;padding-bottom:10px;border-bottom:1px solid #000;margin-bottom:20px}.mtar__wrap{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__product-wrap{width:100%;-ms-flex-negative:0;flex-shrink:0;color:#000;padding-left:60px;width:520px;-webkit-box-sizing:border-box;box-sizing:border-box}.mtar__sku-name{font-size:16px;font-weight:300;line-height:22px;margin-bottom:20px}.mtar__sku-list{-ms-flex-wrap:wrap;flex-wrap:wrap;margin-bottom:27px}.mtar__sku-list,.mtar__sku-list-li{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__sku-list-li{margin-right:25px;margin-bottom:18px;cursor:pointer;width:36px;height:36px;-webkit-box-sizing:border-box;box-sizing:border-box;border:1px solid transparent;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-transition:all .3s;transition:all .3s}.mtar__sku-list-li .img{width:32px;height:32px}.mtar__sku-list-li.active,.mtar__sku-list-li:hover{border:1px solid #000}.mtar__sku-list-li.active .img,.mtar__sku-list-li:hover .img{width:30px;height:30px}.mtar__skus-wrap{width:100%}.mtar__tab{height:100px}.mtar__tab,.mtar__tab-list{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__tab-list{width:390px;overflow:hidden;position:relative}.mtar__tab-click-l,.mtar__tab-click-r{width:10px;height:64px;cursor:pointer;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center}.mtar__tab-click-l .icon,.mtar__tab-click-r .icon{width:10px;height:10px}.mtar__tab-click-l.disabled,.mtar__tab-click-r.disabled{cursor:not-allowed}.mtar__tab-click-l.disabled .icon,.mtar__tab-click-r.disabled .icon{opacity:.4}.mtar__tab-click-l{margin-right:20px}.mtar__tab-click-r{margin-left:20px}.mtar__tab-item{width:60px;margin-right:22px;cursor:pointer}.mtar__tab-item.active .mtar__tab-img-wrap,.mtar__tab-item:hover .mtar__tab-img-wrap{border:1px solid #000}.mtar__tab-item.active .mtar__tab-img,.mtar__tab-item:hover .mtar__tab-img{width:54px;height:54px}.mtar__tab-img,.mtar__tab-img-wrap{width:60px;height:60px}.mtar__tab-text{font-size:12px;line-height:14px;text-align:center}.mtar__tab-look{margin-bottom:30px}.mtar__look-sku-tab{width:450px;margin-bottom:45px;display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__look-sku-tab .mtar__tab-click-l,.mtar__look-sku-tab .mtar__tab-click-r{height:80px}.mtar__look-sku-tab .mtar__tab-click-l{margin-right:10px}.mtar__look-sku-tab .mtar__tab-click-r{margin-left:10px}.mtar__look-sku-tab-list{width:410px;overflow:hidden}.mtar__look-sku-tab-item,.mtar__look-sku-tab-list{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__look-sku-tab-item{-ms-flex-negative:0;flex-shrink:0;width:200px;height:80px;margin-right:10px;font-size:12px;line-height:15px;cursor:pointer}.mtar__look-sku-tab-item.active .mtar__look-sku-tab-img-wrap,.mtar__look-sku-tab-item:hover .mtar__look-sku-tab-img-wrap{border:1px solid #000}.mtar__look-sku-tab-item.active .mtar__look-sku-tab-img,.mtar__look-sku-tab-item:hover .mtar__look-sku-tab-img{width:74px;height:74px}.mtar__look-sku-tab-img-wrap{width:80px;height:80px;margin-right:10px;-ms-flex-negative:0;flex-shrink:0}.mtar__look-sku-tab-img{width:80px;height:80px}.mtar__look-sku-tab-id{color:#666;margin-bottom:4px}.mtar__look-sku-tab-tit{height:30px;font-weight:700}.mtar__look-sku-tab-text{height:30px;font-weight:300;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:end;-ms-flex-align:end;align-items:flex-end}.mtar__look-sku-tab .no-text{height:80px}.mtar .btn-wrap{-webkit-box-pack:justify;-ms-flex-pack:justify;justify-content:space-between}.mtar .btn,.mtar .btn-wrap{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar .btn{-webkit-box-sizing:border-box;box-sizing:border-box;width:204px;height:48px;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;-webkit-box-shadow:0 -6px 10px 0 rgba(0,0,0,.04);box-shadow:0 -6px 10px 0 rgba(0,0,0,.04);font-size:14px;cursor:pointer}.mtar .btn:active{opacity:.6}.mtar .btn .icon{margin-right:6px}.mtar__product-buy{color:#fff;background-color:#000}.mtar__product-car{border:2px solid #000}.mtar__header{width:100%;height:78px;position:relative}.mtar__header-car{width:54px;height:36px;position:absolute;right:0;top:21px}.mtar__header-car .icon{width:22px;height:22px;margin-right:14px}.mtar__header-car .car-num{font-size:12px;width:18px;height:18px;line-height:18px;text-align:center;background-color:#000;color:#fff;border-radius:50%}.mtar.is-mobile .mtar__cir_btn.zoom-in,.mtar.is-mobile .mtar__cir_btn.zoom-out,.mtar.is-mobile .mtar__header,.mtar.is-mobile .mtar__look-wrap,.mtar.is-mobile .mtar__skus-wrap,.mtar.is-mobile .mtar__switch-sel,.mtar.is-pc .mtar__cir_btn.car,.mtar.is-pc .mtar__look-wrap-mobile,.mtar.is-pc .mtar__skus-wrap-mobile,.mtar.is-pc .mtar__switch-sel-mobile,.mtar__in-look .mtar__skus-wrap,.mtar__in-look .mtar__skus-wrap-mobile,.mtar__in-sku .mtar__look-wrap,.mtar__in-sku .mtar__look-wrap-mobile,.mtar__no-sku-look .mtar__look-wrap,.mtar__no-sku-look .mtar__look-wrap-mobile,.mtar__no-sku-look .mtar__skus-wrap,.mtar__no-sku-look .mtar__skus-wrap-mobile{display:none}.mtar__no-sku-look .mtar__no-pro{display:-webkit-box;display:-ms-flexbox;display:flex}.mtar__update{padding:30px 20px;background-color:#fff;font-size:15px;-webkit-box-sizing:border-box;box-sizing:border-box;z-index:300;overflow-y:auto;display:none}.mtar__update.mtar__show{display:block}.mtar__update-tit{font-size:30px;font-weight:500}.mtar__update-sub-tit{padding:15px 0 40px}.mtar__update-tag,.mtar__update-text{margin-bottom:10px}.mtar__update-tag{background-color:#000;color:#fff;display:inline-block;padding:5px}.mtar__no-pro{width:100%;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-ms-flex-pack:center;justify-content:center;display:none}#mtar-custom-wrap{position:relative}#mtar-custom-wrap #mtar-custom-switch-page{position:absolute;top:0;left:0;background:#fff;z-index:50}#mt-toast{pointer-events:none;position:absolute;z-index:301;top:50%;left:50%;-webkit-transform:translate(-50%,-30%);transform:translate(-50%,-30%);max-width:80%;padding:10px 20px;font-size:14px;word-break:break-word;word-wrap:break-word;border-radius:10px;background-color:rgba(0,0,0,.7);color:#fff;opacity:0;-webkit-transition:all .5s;transition:all .5s}#mt-toast.mtar__show{opacity:1;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%)}#mt-toast.pc-toast{position:fixed}', ""])
        },
        Cg2A: function(t, e, n) { t.exports = n("6CmU") },
        CgaS: function(t, e, n) {
            "use strict";
            var r = n("xTJ+"),
                i = n("MLWZ"),
                a = n("9rSQ"),
                o = n("UnBK"),
                s = n("SntB");

            function l(t) { this.defaults = t, this.interceptors = { request: new a, response: new a } }
            l.prototype.request = function(t, e) {
                "string" == typeof t ? (t = e || {}).url = arguments[0] : t = t || {}, (t = s(this.defaults, t)).method ? t.method = t.method.toLowerCase() : this.defaults.method ? t.method = this.defaults.method.toLowerCase() : t.method = "get";
                var n = [o, void 0],
                    i = Promise.resolve(t);
                for (this.interceptors.request.forEach(function(t) { n.unshift(t.fulfilled, t.rejected) }), this.interceptors.response.forEach(function(t) { n.push(t.fulfilled, t.rejected) }); n.length;) i = i.then(n.shift(), n.shift());
                return i
            }, l.prototype.getUri = function(t) { return t = s(this.defaults, t), i(t.url, t.params, t.paramsSerializer).replace(/^\?/, "") }, r.forEach(["delete", "get", "head", "options"], function(n) { l.prototype[n] = function(t, e) { return this.request(r.merge(e || {}, { method: n, url: t })) } }), r.forEach(["post", "put", "patch"], function(i) { l.prototype[i] = function(t, e, n) { return this.request(r.merge(n || {}, { method: i, url: t, data: e })) } }), t.exports = l
        },
        D8kY: function(t, e, n) {
            var i = n("Ojgd"),
                r = Math.max,
                a = Math.min;
            t.exports = function(t, e) { return (t = i(t)) < 0 ? r(t + e, 0) : a(t, e) }
        },
        DfZB: function(t, e, n) {
            "use strict";
            t.exports = function(e) { return function(t) { return e.apply(null, t) } }
        },
        EXMj: function(t, e) { t.exports = function(t, e, n, i) { if (!(t instanceof e) || void 0 !== i && i in t) throw TypeError(n + ": incorrect invocation!"); return t } },
        EnHN: function(t, e, n) { n("zn7N")("getOwnPropertyNames", function() { return n("A5Xg").f }) },
        FlQf: function(t, e, n) {
            "use strict";
            var i = n("ccE7")(!0);
            n("MPFp")(String, "String", function(t) { this._t = String(t), this._i = 0 }, function() {
                var t, e = this._t,
                    n = this._i;
                return n >= e.length ? { value: void 0, done: !0 } : (t = i(e, n), this._i += t.length, { value: t, done: !1 })
            })
        },
        FpHa: function(t, e) { t.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",") },
        G8Mo: function(t, e, n) {
            var r = n("93I4");
            t.exports = function(t, e) { if (!r(t)) return t; var n, i; if (e && "function" == typeof(n = t.toString) && !r(i = n.call(t))) return i; if ("function" == typeof(n = t.valueOf) && !r(i = n.call(t))) return i; if (!e && "function" == typeof(n = t.toString) && !r(i = n.call(t))) return i; throw TypeError("Can't convert object to primitive value") }
        },
        GvbO: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S, "Array", { isArray: n("kAMH") })
        },
        H7XF: function(t, e, n) {
            "use strict";
            e.byteLength = function(t) {
                var e = h(t),
                    n = e[0],
                    i = e[1];
                return 3 * (n + i) / 4 - i
            }, e.toByteArray = function(t) {
                var e, n, i = h(t),
                    r = i[0],
                    a = i[1],
                    o = new u(function(t, e) { return 3 * (t + e) / 4 - e }(r, a)),
                    s = 0,
                    l = 0 < a ? r - 4 : r;
                for (n = 0; n < l; n += 4) e = c[t.charCodeAt(n)] << 18 | c[t.charCodeAt(n + 1)] << 12 | c[t.charCodeAt(n + 2)] << 6 | c[t.charCodeAt(n + 3)], o[s++] = e >> 16 & 255, o[s++] = e >> 8 & 255, o[s++] = 255 & e;
                2 === a && (e = c[t.charCodeAt(n)] << 2 | c[t.charCodeAt(n + 1)] >> 4, o[s++] = 255 & e);
                1 === a && (e = c[t.charCodeAt(n)] << 10 | c[t.charCodeAt(n + 1)] << 4 | c[t.charCodeAt(n + 2)] >> 2, o[s++] = e >> 8 & 255, o[s++] = 255 & e);
                return o
            }, e.fromByteArray = function(t) {
                for (var e, n = t.length, i = n % 3, r = [], a = 0, o = n - i; a < o; a += 16383) r.push(l(t, a, o < a + 16383 ? o : a + 16383));
                1 == i ? (e = t[n - 1], r.push(s[e >> 2] + s[e << 4 & 63] + "==")) : 2 == i && (e = (t[n - 2] << 8) + t[n - 1], r.push(s[e >> 10] + s[e >> 4 & 63] + s[e << 2 & 63] + "="));
                return r.join("")
            };
            for (var s = [], c = [], u = "undefined" != typeof Uint8Array ? Uint8Array : Array, i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", r = 0, a = i.length; r < a; ++r) s[r] = i[r], c[i.charCodeAt(r)] = r;

            function h(t) { var e = t.length; if (0 < e % 4) throw new Error("Invalid string. Length must be a multiple of 4"); var n = t.indexOf("="); return -1 === n && (n = e), [n, n === e ? 0 : 4 - n % 4] }

            function l(t, e, n) { for (var i, r, a = [], o = e; o < n; o += 3) i = (t[o] << 16 & 16711680) + (t[o + 1] << 8 & 65280) + (255 & t[o + 2]), a.push(s[(r = i) >> 18 & 63] + s[r >> 12 & 63] + s[r >> 6 & 63] + s[63 & r]); return a.join("") }
            c["-".charCodeAt(0)] = 62, c["_".charCodeAt(0)] = 63
        },
        HLdI: function(t, e, n) {
            var o = n("vwuL"),
                s = n("U+KD"),
                l = n("B+OT"),
                i = n("Y7ZC"),
                c = n("93I4"),
                u = n("5K7Z");
            i(i.S, "Reflect", { get: function t(e, n) { var i, r, a = arguments.length < 3 ? e : arguments[2]; return u(e) === a ? e[n] : (i = o.f(e, n)) ? l(i, "value") ? i.value : void 0 !== i.get ? i.get.call(a) : void 0 : c(r = s(e)) ? t(r, n, a) : void 0 } })
        },
        HSsa: function(t, e, n) {
            "use strict";
            t.exports = function(n, i) { return function() { for (var t = new Array(arguments.length), e = 0; e < t.length; e++) t[e] = arguments[e]; return n.apply(i, t) } }
        },
        Hfiw: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S, "Object", { setPrototypeOf: n("6tYh").set })
        },
        HoUH: function(t, e) {
            t.exports = function(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i
            }
        },
        Hsns: function(t, e, n) {
            var i = n("93I4"),
                r = n("5T2Y").document,
                a = i(r) && i(r.createElement);
            t.exports = function(t) { return a ? r.createElement(t) : {} }
        },
        IP1Z: function(t, e, n) {
            "use strict";
            var i = n("2faE"),
                r = n("rr1i");
            t.exports = function(t, e, n) { e in t ? i.f(t, e, r(0, n)) : t[e] = n }
        },
        JB68: function(t, e, n) {
            var i = n("Jes0");
            t.exports = function(t) { return Object(i(t)) }
        },
        JEQr: function(s, t, l) {
            "use strict";
            (function(t) {
                var n = l("xTJ+"),
                    i = l("yK9s"),
                    e = { "Content-Type": "application/x-www-form-urlencoded" };

                function r(t, e) {!n.isUndefined(t) && n.isUndefined(t["Content-Type"]) && (t["Content-Type"] = e) }
                var a, o = {
                    adapter: (("undefined" != typeof XMLHttpRequest || void 0 !== t && "[object process]" === Object.prototype.toString.call(t)) && (a = l("tQ2B")), a),
                    transformRequest: [function(t, e) { return i(e, "Accept"), i(e, "Content-Type"), n.isFormData(t) || n.isArrayBuffer(t) || n.isBuffer(t) || n.isStream(t) || n.isFile(t) || n.isBlob(t) ? t : n.isArrayBufferView(t) ? t.buffer : n.isURLSearchParams(t) ? (r(e, "application/x-www-form-urlencoded;charset=utf-8"), t.toString()) : n.isObject(t) ? (r(e, "application/json;charset=utf-8"), JSON.stringify(t)) : t }],
                    transformResponse: [function(t) {
                        if ("string" == typeof t) try { t = JSON.parse(t) } catch (t) {}
                        return t
                    }],
                    timeout: 0,
                    xsrfCookieName: "XSRF-TOKEN",
                    xsrfHeaderName: "X-XSRF-TOKEN",
                    maxContentLength: -1,
                    validateStatus: function(t) { return 200 <= t && t < 300 }
                };
                o.headers = { common: { Accept: "application/json, text/plain, */*" } }, n.forEach(["delete", "get", "head"], function(t) { o.headers[t] = {} }), n.forEach(["post", "put", "patch"], function(t) { o.headers[t] = n.merge(e) }), s.exports = o
            }).call(this, l("8oxB"))
        },
        JKvY: function(t, e, r) {
            "use strict";
            (function(t, e) {
                var n, i = r("iZP3");
                n = function() {
                    return function i(r, a, o) {
                        function s(n, t) {
                            if (!a[n]) {
                                if (!r[n]) { if (0, l) return l(n, !0); throw new Error("Cannot find module '" + n + "'") }
                                var e = a[n] = { exports: {} };
                                r[n][0].call(e.exports, function(t) { var e = r[n][1][t]; return s(e || t) }, e, e.exports, i, r, a, o)
                            }
                            return a[n].exports
                        }
                        for (var l = !1, t = 0; t < o.length; t++) s(o[t]);
                        return s
                    }({
                        1: [function(t, e, n) {
                            var o = {};

                            function i() { try { return new window.XMLHttpRequest } catch (t) {} }
                            o._getBinaryFromXHR = function(t) { return t.response || t.responseText };
                            var s = window.ActiveXObject ? function() { return i() || function() { try { return new window.ActiveXObject("Microsoft.XMLHTTP") } catch (t) {} }() } : i;
                            o.getBinaryContent = function(i, r) {
                                try {
                                    var a = s();
                                    a.open("GET", i, !0), "responseType" in a && (a.responseType = "arraybuffer"), a.overrideMimeType && a.overrideMimeType("text/plain; charset=x-user-defined"), a.onreadystatechange = function(t) {
                                        var e, n;
                                        if (4 === a.readyState)
                                            if (200 === a.status || 0 === a.status) {
                                                n = e = null;
                                                try { e = o._getBinaryFromXHR(a) } catch (t) { n = new Error(t) }
                                                r(n, e)
                                            } else r(new Error("Ajax error for " + i + " : " + this.status + " " + this.statusText), null)
                                    }, a.send()
                                } catch (t) { r(new Error(t), null) }
                            }, e.exports = o
                        }, {}]
                    }, {}, [1])(1)
                }, "object" == ("undefined" == typeof exports ? "undefined" : r.n(i)()(exports)) ? t.exports = n() : "function" == typeof define && r("PDX0") ? define(n) : "undefined" != typeof window ? window.JSZipUtils = n() : void 0 !== e ? e.JSZipUtils = n() : "undefined" != typeof self && (self.JSZipUtils = n())
            }).call(this, r("3UD+")(t), r("yLpj"))
        },
        "JMW+": function(t, e, n) {
            "use strict";

            function i() {}

            function h(t) { var e; return !(!v(t) || "function" != typeof(e = t.then)) && e }

            function r(u, n) {
                if (!u._n) {
                    u._n = !0;
                    var i = u._c;
                    k(function() {
                        for (var l = u._v, c = 1 == u._s, t = 0, e = function(t) {
                                var e, n, i, r = c ? t.ok : t.fail,
                                    a = t.resolve,
                                    o = t.reject,
                                    s = t.domain;
                                try { r ? (c || (2 == u._h && O(u), u._h = 1), !0 === r ? e = l : (s && s.enter(), e = r(l), s && (s.exit(), i = !0)), e === t.promise ? o(F("Promise-chain cycle")) : (n = h(e)) ? n.call(e, a, o) : a(e)) : o(l) } catch (t) { s && !i && s.exit(), o(t) }
                            }; i.length > t;) e(i[t++]);
                        u._c = [], u._n = !1, n && !u._h && L(u)
                    })
                }
            }

            function a(t) {
                var e = this;
                e._d || (e._d = !0, (e = e._w || e)._v = t, e._s = 2, e._a || (e._a = e._c.slice()), r(e, !0))
            }
            var o, s, l, c, u = n("uOPS"),
                d = n("5T2Y"),
                f = n("2GTP"),
                m = n("QMMT"),
                p = n("Y7ZC"),
                v = n("93I4"),
                _ = n("eaoh"),
                g = n("EXMj"),
                w = n("oioR"),
                b = n("8gHz"),
                y = n("QXhf").set,
                k = n("q6LJ")(),
                I = n("ZW5q"),
                x = n("RDmV"),
                A = n("vBP9"),
                E = n("zXhZ"),
                S = "Promise",
                F = d.TypeError,
                R = d.process,
                M = R && R.versions,
                C = M && M.v8 || "",
                P = d[S],
                D = "process" == m(R),
                T = s = I.f,
                z = !! function() {
                    try {
                        var t = P.resolve(1),
                            e = (t.constructor = {})[n("UWiX")("species")] = function(t) { t(i, i) };
                        return (D || "function" == typeof PromiseRejectionEvent) && t.then(i) instanceof e && 0 !== C.indexOf("6.6") && -1 === A.indexOf("Chrome/66")
                    } catch (t) {}
                }(),
                L = function(a) {
                    y.call(d, function() {
                        var t, e, n, i = a._v,
                            r = B(a);
                        if (r && (t = x(function() { D ? R.emit("unhandledRejection", i, a) : (e = d.onunhandledrejection) ? e({ promise: a, reason: i }) : (n = d.console) && n.error && n.error("Unhandled promise rejection", i) }), a._h = D || B(a) ? 2 : 1), a._a = void 0, r && t.e) throw t.v
                    })
                },
                B = function(t) { return 1 !== t._h && 0 === (t._a || t._c).length },
                O = function(e) {
                    y.call(d, function() {
                        var t;
                        D ? R.emit("rejectionHandled", e) : (t = d.onrejectionhandled) && t({ promise: e, reason: e._v })
                    })
                },
                W = function(t) {
                    var n, i = this;
                    if (!i._d) {
                        i._d = !0, i = i._w || i;
                        try {
                            if (i === t) throw F("Promise can't be resolved itself");
                            (n = h(t)) ? k(function() { var e = { _w: i, _d: !1 }; try { n.call(t, f(W, e, 1), f(a, e, 1)) } catch (t) { a.call(e, t) } }): (i._v = t, i._s = 1, r(i, !1))
                        } catch (t) { a.call({ _w: i, _d: !1 }, t) }
                    }
                };
            z || (P = function(t) { g(this, P, S, "_h"), _(t), o.call(this); try { t(f(W, this, 1), f(a, this, 1)) } catch (t) { a.call(this, t) } }, (o = function() { this._c = [], this._a = void 0, this._s = 0, this._d = !1, this._v = void 0, this._h = 0, this._n = !1 }).prototype = n("XJU/")(P.prototype, { then: function(t, e) { var n = T(b(this, P)); return n.ok = "function" != typeof t || t, n.fail = "function" == typeof e && e, n.domain = D ? R.domain : void 0, this._c.push(n), this._a && this._a.push(n), this._s && r(this, !1), n.promise }, catch: function(t) { return this.then(void 0, t) } }), l = function() {
                var t = new o;
                this.promise = t, this.resolve = f(W, t, 1), this.reject = f(a, t, 1)
            }, I.f = T = function(t) { return t === P || t === c ? new l : s(t) }), p(p.G + p.W + p.F * !z, { Promise: P }), n("RfKB")(P, S), n("TJWN")(S), c = n("WEpk")[S], p(p.S + p.F * !z, S, { reject: function(t) { var e = T(this); return (0, e.reject)(t), e.promise } }), p(p.S + p.F * (u || !z), S, { resolve: function(t) { return E(u && this === c ? P : this, t) } }), p(p.S + p.F * !(z && n("TuGD")(function(t) { P.all(t).catch(i) })), S, {
                all: function(t) {
                    var o = this,
                        e = T(o),
                        s = e.resolve,
                        l = e.reject,
                        n = x(function() {
                            var i = [],
                                r = 0,
                                a = 1;
                            w(t, !1, function(t) {
                                var e = r++,
                                    n = !1;
                                i.push(void 0), a++, o.resolve(t).then(function(t) { n || (n = !0, i[e] = t, --a || s(i)) }, l)
                            }), --a || s(i)
                        });
                    return n.e && l(n.v), e.promise
                },
                race: function(t) {
                    var e = this,
                        n = T(e),
                        i = n.reject,
                        r = x(function() { w(t, !1, function(t) { e.resolve(t).then(n.resolve, i) }) });
                    return r.e && i(r.v), n.promise
                }
            })
        },
        JPst: function(t, e, n) {
            "use strict";
            t.exports = function(n) {
                var o = [];
                return o.toString = function() {
                    return this.map(function(t) {
                        var e = function(t, e) {
                            var n = t[1] || "",
                                i = t[3];
                            if (!i) return n;
                            if (e && "function" == typeof btoa) {
                                var r = function(t) { return "/*# sourceMappingURL=data:application/json;charset=utf-8;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(t)))) + " */" }(i),
                                    a = i.sources.map(function(t) { return "/*# sourceURL=" + i.sourceRoot + t + " */" });
                                return [n].concat(a).concat([r]).join("\n")
                            }
                            return [n].join("\n")
                        }(t, n);
                        return t[2] ? "@media " + t[2] + "{" + e + "}" : e
                    }).join("")
                }, o.i = function(t, e) {
                    "string" == typeof t && (t = [
                        [null, t, ""]
                    ]);
                    for (var n = {}, i = 0; i < this.length; i++) {
                        var r = this[i][0];
                        null != r && (n[r] = !0)
                    }
                    for (i = 0; i < t.length; i++) {
                        var a = t[i];
                        null != a[0] && n[a[0]] || (e && !a[2] ? a[2] = e : e && (a[2] = "(" + a[2] + ") and (" + e + ")"), o.push(a))
                    }
                }, o
            }
        },
        JQAc: function(t, e, n) {
            var i = { "./en.json": "u2kb", "./tw.json": "JkEd", "./zh.json": "tXnD" };

            function r(t) { var e = a(t); return n(e) }

            function a(t) { if (n.o(i, t)) return i[t]; var e = new Error("Cannot find module '" + t + "'"); throw e.code = "MODULE_NOT_FOUND", e }
            r.keys = function() { return Object.keys(i) }, r.resolve = a, (t.exports = r).id = "JQAc"
        },
        JbBM: function(t, e, n) { n("Hfiw"), t.exports = n("WEpk").Object.setPrototypeOf },
        Jes0: function(t, e) { t.exports = function(t) { if (null == t) throw TypeError("Can't call method on  " + t); return t } },
        JkEd: function(t) { t.exports = JSON.parse('{"vertical_tip":"為了更好地體驗，請直向瀏覽","detect":"即時試色","import_img":"匯入照片","long_press_img":"長按儲存圖片","loading_hard":"拚命載入中…","faceErrorText":{"1000":"臉部辨識失敗，請上傳清晰的正面照。","1003":"最多兩張臉"},"file_limit":"文檔僅支援{0}格式","save_image_tip":"長按或截圖儲存圖片","buy":"立即購買","add_to_car":"加入購物車","no_sku_tip":"商品/妝容不存在","custome_btn_tip":"由商家自定義","product_detail":"買同款","update_page":{"sorry":"抱歉","tip":"當前功能無法使用，請更新系統後重試。","advice":"建議系統規格：","suggest_list":{"1":{"tag":"iPhone","text":"iOS V11以上"},"2":{"tag":"Android","text":"Android OS 6.0、Chrome V60.0以上"},"3":{"tag":"Windows","text":"Chrome V60.0、Firefox V70、QQ瀏覽器10.5.1(3824)、 Opera V64、Edge V44以上"},"4":{"tag":"macOS","text":"Chrome V60.0、Safari V11、Firefox V70、Opera V64以上"}}},"no_action_tip":"無法執行此操作","select_modal":{"select":"選擇試妝方式","import":"匯入照片","detect":"即時試色","tip":"即時試色無法使用，如需試妝請匯入照片。"},"switch_select":"切換試妝方式","no_camera_tip":"鏡頭無法使用，請檢查瀏覽器設定，確保已開啟鏡頭。","no_sup_camera_tip":"設備瀏覽器不支持相機，請更換瀏覽器或設備","select_skus_tip":"請先選擇商品","server_error":"伺服器錯誤","horizontal_refresh":"請勿在橫向畫面下操作相機權限，如需繼續體驗，請重新整理。","error_tip":"發生錯誤，請檢查網路連線後重新整理。","err_page_tip":"顯示異常，請重新開啟Safari 後重新掃描。","no_product":"暫無商品","browser_tip":{"1":"請點選右上方“...”-“在Safari中開啟”","2":"請點選右上方“...”-“在Chrome中開啟”"},"error_msg":{"1":"現正處於選擇模式，無法上妝。","2":"SKU ID不存在","3":"妝容ID不存在","4":"請先取消對比模式","5":"商品/妝容不存在","6":"無法執行此操作","7":"授權未完成，請稍後再試。","100":"未知錯誤","101":"其他錯誤","300":"License已過期，請聯絡美圖。"}}') },
        "Jo+v": function(t, e, n) { t.exports = n("/eQG") },
        K47E: function(t, e) { t.exports = function(t) { if (void 0 === t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); return t } },
        KUxP: function(t, e) { t.exports = function(t) { try { return !!t() } catch (t) { return !0 } } },
        KhLd: function(i, t, e) {
            var a = e("Jo+v"),
                r = e("j+vE"),
                o = e("1amR");

            function s(t, e, n) { return "undefined" != typeof Reflect && r ? i.exports = s = r : i.exports = s = function(t, e, n) { var i = o(t, e); if (i) { var r = a(i, e); return r.get ? r.get.call(n) : r.value } }, s(t, e, n || t) }
            i.exports = s
        },
        LG64: function(t, e, l) {
            "use strict";
            (function(d, n, e) {
                var t = l("br1p"),
                    i = l.n(t),
                    r = l("6BQ9"),
                    w = l.n(r),
                    a = l("eVuF"),
                    o = l.n(a),
                    s = l("iZP3"),
                    p = l.n(s);
                /*!

                JSZip v3.2.1 - A JavaScript class for generating and reading zip files
                <http://stuartk.com/jszip>

                (c) 2009-2016 Stuart Knightley <stuart [at] stuartk.com>
                Dual licenced under the MIT license or GPLv3. See https://raw.github.com/Stuk/jszip/master/LICENSE.markdown.

                JSZip uses the library pako released under the MIT license :
                https://github.com/nodeca/pako/blob/master/LICENSE
                */
                ! function(t) {
                    if ("object" === ("undefined" == typeof exports ? "undefined" : p()(exports)) && void 0 !== e) e.exports = t();
                    else if ("function" == typeof define && l("PDX0")) define([], t);
                    else {
                        ("undefined" != typeof window ? window : void 0 !== n ? n : "undefined" != typeof self ? self : this).JSZip = t()
                    }
                }(function() {
                    return function r(a, o, s) {
                        function l(n, t) {
                            if (!o[n]) {
                                if (!a[n]) { if (0, c) return c(n, !0); var e = new Error("Cannot find module '" + n + "'"); throw e.code = "MODULE_NOT_FOUND", e }
                                var i = o[n] = { exports: {} };
                                a[n][0].call(i.exports, function(t) { var e = a[n][1][t]; return l(e || t) }, i, i.exports, r, a, o, s)
                            }
                            return o[n].exports
                        }
                        for (var c = !1, t = 0; t < s.length; t++) l(s[t]);
                        return l
                    }({
                        1: [function(t, e, n) {
                            var f = t("./utils"),
                                h = t("./support"),
                                m = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
                            n.encode = function(t) { for (var e, n, i, r, a, o, s, l = [], c = 0, u = t.length, h = u, d = "string" !== f.getTypeOf(t); c < t.length;) h = u - c, i = d ? (e = t[c++], n = c < u ? t[c++] : 0, c < u ? t[c++] : 0) : (e = t.charCodeAt(c++), n = c < u ? t.charCodeAt(c++) : 0, c < u ? t.charCodeAt(c++) : 0), r = e >> 2, a = (3 & e) << 4 | n >> 4, o = 1 < h ? (15 & n) << 2 | i >> 6 : 64, s = 2 < h ? 63 & i : 64, l.push(m.charAt(r) + m.charAt(a) + m.charAt(o) + m.charAt(s)); return l.join("") }, n.decode = function(t) {
                                var e, n, i, r, a, o, s = 0,
                                    l = 0;
                                if ("data:" === t.substr(0, "data:".length)) throw new Error("Invalid base64 input, it looks like a data url.");
                                var c, u = 3 * (t = t.replace(/[^A-Za-z0-9\+\/\=]/g, "")).length / 4;
                                if (t.charAt(t.length - 1) === m.charAt(64) && u--, t.charAt(t.length - 2) === m.charAt(64) && u--, u % 1 != 0) throw new Error("Invalid base64 input, bad content length.");
                                for (c = new(h.uint8array ? Uint8Array : Array)(0 | u); s < t.length;) e = m.indexOf(t.charAt(s++)) << 2 | (r = m.indexOf(t.charAt(s++))) >> 4, n = (15 & r) << 4 | (a = m.indexOf(t.charAt(s++))) >> 2, i = (3 & a) << 6 | (o = m.indexOf(t.charAt(s++))), c[l++] = e, 64 !== a && (c[l++] = n), 64 !== o && (c[l++] = i);
                                return c
                            }
                        }, { "./support": 30, "./utils": 32 }],
                        2: [function(t, e, n) {
                            var i = t("./external"),
                                r = t("./stream/DataWorker"),
                                a = t("./stream/DataLengthProbe"),
                                o = t("./stream/Crc32Probe");
                            a = t("./stream/DataLengthProbe");

                            function s(t, e, n, i, r) { this.compressedSize = t, this.uncompressedSize = e, this.crc32 = n, this.compression = i, this.compressedContent = r }
                            s.prototype = {
                                getContentWorker: function() {
                                    var t = new r(i.Promise.resolve(this.compressedContent)).pipe(this.compression.uncompressWorker()).pipe(new a("data_length")),
                                        e = this;
                                    return t.on("end", function() { if (this.streamInfo.data_length !== e.uncompressedSize) throw new Error("Bug : uncompressed data size mismatch") }), t
                                },
                                getCompressedWorker: function() { return new r(i.Promise.resolve(this.compressedContent)).withStreamInfo("compressedSize", this.compressedSize).withStreamInfo("uncompressedSize", this.uncompressedSize).withStreamInfo("crc32", this.crc32).withStreamInfo("compression", this.compression) }
                            }, s.createWorkerFrom = function(t, e, n) { return t.pipe(new o).pipe(new a("uncompressedSize")).pipe(e.compressWorker(n)).pipe(new a("compressedSize")).withStreamInfo("compression", e) }, e.exports = s
                        }, { "./external": 6, "./stream/Crc32Probe": 25, "./stream/DataLengthProbe": 26, "./stream/DataWorker": 27 }],
                        3: [function(t, e, n) {
                            var i = t("./stream/GenericWorker");
                            n.STORE = { magic: "\0\0", compressWorker: function() { return new i("STORE compression") }, uncompressWorker: function() { return new i("STORE decompression") } }, n.DEFLATE = t("./flate")
                        }, { "./flate": 7, "./stream/GenericWorker": 28 }],
                        4: [function(t, e, n) {
                            var i = t("./utils");
                            var s = function() {
                                for (var t, e = [], n = 0; n < 256; n++) {
                                    t = n;
                                    for (var i = 0; i < 8; i++) t = 1 & t ? 3988292384 ^ t >>> 1 : t >>> 1;
                                    e[n] = t
                                }
                                return e
                            }();
                            e.exports = function(t, e) {
                                return void 0 !== t && t.length ? ("string" !== i.getTypeOf(t) ? function(t, e, n, i) {
                                    var r = s,
                                        a = i + n;
                                    t ^= -1;
                                    for (var o = i; o < a; o++) t = t >>> 8 ^ r[255 & (t ^ e[o])];
                                    return -1 ^ t
                                } : function(t, e, n, i) {
                                    var r = s,
                                        a = i + n;
                                    t ^= -1;
                                    for (var o = i; o < a; o++) t = t >>> 8 ^ r[255 & (t ^ e.charCodeAt(o))];
                                    return -1 ^ t
                                })(0 | e, t, t.length, 0) : 0
                            }
                        }, { "./utils": 32 }],
                        5: [function(t, e, n) { n.base64 = !1, n.binary = !1, n.dir = !1, n.createFolders = !0, n.date = null, n.compression = null, n.compressionOptions = null, n.comment = null, n.unixPermissions = null, n.dosPermissions = null }, {}],
                        6: [function(t, e, n) {
                            var i = null;
                            i = void 0 !== o.a ? o.a : t("lie"), e.exports = { Promise: i }
                        }, { lie: 37 }],
                        7: [function(t, e, n) {
                            var i = "undefined" != typeof Uint8Array && "undefined" != typeof Uint16Array && "undefined" != typeof Uint32Array,
                                r = t("pako"),
                                a = t("./utils"),
                                o = t("./stream/GenericWorker"),
                                s = i ? "uint8array" : "array";

                            function l(t, e) { o.call(this, "FlateWorker/" + t), this._pako = null, this._pakoAction = t, this._pakoOptions = e, this.meta = {} }
                            n.magic = "\b\0", a.inherits(l, o), l.prototype.processChunk = function(t) { this.meta = t.meta, null === this._pako && this._createPako(), this._pako.push(a.transformTo(s, t.data), !1) }, l.prototype.flush = function() { o.prototype.flush.call(this), null === this._pako && this._createPako(), this._pako.push([], !0) }, l.prototype.cleanUp = function() { o.prototype.cleanUp.call(this), this._pako = null }, l.prototype._createPako = function() {
                                this._pako = new r[this._pakoAction]({ raw: !0, level: this._pakoOptions.level || -1 });
                                var e = this;
                                this._pako.onData = function(t) { e.push({ data: t, meta: e.meta }) }
                            }, n.compressWorker = function(t) { return new l("Deflate", t) }, n.uncompressWorker = function() { return new l("Inflate", {}) }
                        }, { "./stream/GenericWorker": 28, "./utils": 32, pako: 38 }],
                        8: [function(t, e, n) {
                            function C(t, e) { var n, i = ""; for (n = 0; n < e; n++) i += String.fromCharCode(255 & t), t >>>= 8; return i }

                            function r(t, e, n, i, r, a) {
                                var o, s, l = t.file,
                                    c = t.compression,
                                    u = a !== D.utf8encode,
                                    h = P.transformTo("string", a(l.name)),
                                    d = P.transformTo("string", D.utf8encode(l.name)),
                                    f = l.comment,
                                    m = P.transformTo("string", a(f)),
                                    p = P.transformTo("string", D.utf8encode(f)),
                                    v = d.length !== l.name.length,
                                    _ = p.length !== f.length,
                                    g = "",
                                    w = "",
                                    b = "",
                                    y = l.dir,
                                    k = l.date,
                                    I = { crc32: 0, compressedSize: 0, uncompressedSize: 0 };
                                e && !n || (I.crc32 = t.crc32, I.compressedSize = t.compressedSize, I.uncompressedSize = t.uncompressedSize);
                                var x = 0;
                                e && (x |= 8), u || !v && !_ || (x |= 2048);
                                var A, E, S, F = 0,
                                    R = 0;
                                y && (F |= 16), "UNIX" === r ? (R = 798, F |= (A = l.unixPermissions, E = y, (S = A) || (S = E ? 16893 : 33204), (65535 & S) << 16)) : (R = 20, F |= 63 & (l.dosPermissions || 0)), o = k.getUTCHours(), o <<= 6, o |= k.getUTCMinutes(), o <<= 5, o |= k.getUTCSeconds() / 2, s = k.getUTCFullYear() - 1980, s <<= 4, s |= k.getUTCMonth() + 1, s <<= 5, s |= k.getUTCDate(), v && (w = C(1, 1) + C(T(h), 4) + d, g += "up" + C(w.length, 2) + w), _ && (b = C(1, 1) + C(T(m), 4) + p, g += "uc" + C(b.length, 2) + b);
                                var M = "";
                                return M += "\n\0", M += C(x, 2), M += c.magic, M += C(o, 2), M += C(s, 2), M += C(I.crc32, 4), M += C(I.compressedSize, 4), M += C(I.uncompressedSize, 4), M += C(h.length, 2), M += C(g.length, 2), { fileRecord: z.LOCAL_FILE_HEADER + M + h + g, dirRecord: z.CENTRAL_FILE_HEADER + C(R, 2) + M + C(m.length, 2) + "\0\0\0\0" + C(F, 4) + C(i, 4) + h + g + m }
                            }
                            var P = t("../utils"),
                                a = t("../stream/GenericWorker"),
                                D = t("../utf8"),
                                T = t("../crc32"),
                                z = t("../signature");

                            function i(t, e, n, i) { a.call(this, "ZipFileWorker"), this.bytesWritten = 0, this.zipComment = e, this.zipPlatform = n, this.encodeFileName = i, this.streamFiles = t, this.accumulate = !1, this.contentBuffer = [], this.dirRecords = [], this.currentSourceOffset = 0, this.entriesCount = 0, this.currentFile = null, this._sources = [] }
                            P.inherits(i, a), i.prototype.push = function(t) {
                                var e = t.meta.percent || 0,
                                    n = this.entriesCount,
                                    i = this._sources.length;
                                this.accumulate ? this.contentBuffer.push(t) : (this.bytesWritten += t.data.length, a.prototype.push.call(this, { data: t.data, meta: { currentFile: this.currentFile, percent: n ? (e + 100 * (n - i - 1)) / n : 100 } }))
                            }, i.prototype.openedSource = function(t) {
                                this.currentSourceOffset = this.bytesWritten, this.currentFile = t.file.name;
                                var e = this.streamFiles && !t.file.dir;
                                if (e) {
                                    var n = r(t, e, !1, this.currentSourceOffset, this.zipPlatform, this.encodeFileName);
                                    this.push({ data: n.fileRecord, meta: { percent: 0 } })
                                } else this.accumulate = !0
                            }, i.prototype.closedSource = function(t) {
                                this.accumulate = !1;
                                var e, n = this.streamFiles && !t.file.dir,
                                    i = r(t, n, !0, this.currentSourceOffset, this.zipPlatform, this.encodeFileName);
                                if (this.dirRecords.push(i.dirRecord), n) this.push({ data: (e = t, z.DATA_DESCRIPTOR + C(e.crc32, 4) + C(e.compressedSize, 4) + C(e.uncompressedSize, 4)), meta: { percent: 100 } });
                                else
                                    for (this.push({ data: i.fileRecord, meta: { percent: 0 } }); this.contentBuffer.length;) this.push(this.contentBuffer.shift());
                                this.currentFile = null
                            }, i.prototype.flush = function() {
                                for (var t = this.bytesWritten, e = 0; e < this.dirRecords.length; e++) this.push({ data: this.dirRecords[e], meta: { percent: 100 } });
                                var n, i, r, a, o, s, l = this.bytesWritten - t,
                                    c = (n = this.dirRecords.length, i = l, r = t, a = this.zipComment, o = this.encodeFileName, s = P.transformTo("string", o(a)), z.CENTRAL_DIRECTORY_END + "\0\0\0\0" + C(n, 2) + C(n, 2) + C(i, 4) + C(r, 4) + C(s.length, 2) + s);
                                this.push({ data: c, meta: { percent: 100 } })
                            }, i.prototype.prepareNextSource = function() { this.previous = this._sources.shift(), this.openedSource(this.previous.streamInfo), this.isPaused ? this.previous.pause() : this.previous.resume() }, i.prototype.registerPrevious = function(t) { this._sources.push(t); var e = this; return t.on("data", function(t) { e.processChunk(t) }), t.on("end", function() { e.closedSource(e.previous.streamInfo), e._sources.length ? e.prepareNextSource() : e.end() }), t.on("error", function(t) { e.error(t) }), this }, i.prototype.resume = function() { return !!a.prototype.resume.call(this) && (!this.previous && this._sources.length ? (this.prepareNextSource(), !0) : this.previous || this._sources.length || this.generatedError ? void 0 : (this.end(), !0)) }, i.prototype.error = function(t) {
                                var e = this._sources;
                                if (!a.prototype.error.call(this, t)) return !1;
                                for (var n = 0; n < e.length; n++) try { e[n].error(t) } catch (t) {}
                                return !0
                            }, i.prototype.lock = function() { a.prototype.lock.call(this); for (var t = this._sources, e = 0; e < t.length; e++) t[e].lock() }, e.exports = i
                        }, { "../crc32": 4, "../signature": 23, "../stream/GenericWorker": 28, "../utf8": 31, "../utils": 32 }],
                        9: [function(t, e, n) {
                            var c = t("../compressions"),
                                i = t("./ZipFileWorker");
                            n.generateWorker = function(t, o, e) {
                                var s = new i(o.streamFiles, e, o.platform, o.encodeFileName),
                                    l = 0;
                                try {
                                    t.forEach(function(t, e) {
                                        l++;
                                        var n = function(t, e) {
                                                var n = t || e,
                                                    i = c[n];
                                                if (!i) throw new Error(n + " is not a valid compression method !");
                                                return i
                                            }(e.options.compression, o.compression),
                                            i = e.options.compressionOptions || o.compressionOptions || {},
                                            r = e.dir,
                                            a = e.date;
                                        e._compressWorker(n, i).withStreamInfo("file", { name: t, dir: r, date: a, comment: e.comment || "", unixPermissions: e.unixPermissions, dosPermissions: e.dosPermissions }).pipe(s)
                                    }), s.entriesCount = l
                                } catch (t) { s.error(t) }
                                return s
                            }
                        }, { "../compressions": 3, "./ZipFileWorker": 8 }],
                        10: [function(t, e, n) {
                            function i() {
                                if (!(this instanceof i)) return new i;
                                if (arguments.length) throw new Error("The constructor with parameters has been removed in JSZip 3.0, please check the upgrade guide.");
                                this.files = {}, this.comment = null, this.root = "", this.clone = function() { var t = new i; for (var e in this) "function" != typeof this[e] && (t[e] = this[e]); return t }
                            }(i.prototype = t("./object")).loadAsync = t("./load"), i.support = t("./support"), i.defaults = t("./defaults"), i.version = "3.2.0", i.loadAsync = function(t, e) { return (new i).loadAsync(t, e) }, i.external = t("./external"), e.exports = i
                        }, { "./defaults": 5, "./external": 6, "./load": 11, "./object": 15, "./support": 30 }],
                        11: [function(t, e, n) {
                            var i = t("./utils"),
                                r = t("./external"),
                                s = t("./utf8"),
                                l = (i = t("./utils"), t("./zipEntries")),
                                a = t("./stream/Crc32Probe"),
                                c = t("./nodejsUtils");

                            function u(i) {
                                return new r.Promise(function(t, e) {
                                    var n = i.decompressed.getContentWorker().pipe(new a);
                                    n.on("error", function(t) { e(t) }).on("end", function() { n.streamInfo.crc32 !== i.decompressed.crc32 ? e(new Error("Corrupted zip : CRC32 mismatch")) : t() }).resume()
                                })
                            }
                            e.exports = function(t, a) {
                                var o = this;
                                return a = i.extend(a || {}, { base64: !1, checkCRC32: !1, optimizedBinaryString: !1, createFolders: !1, decodeFileName: s.utf8decode }), c.isNode && c.isStream(t) ? r.Promise.reject(new Error("JSZip can't accept a stream when loading a zip file.")) : i.prepareContent("the loaded zip file", t, !0, a.optimizedBinaryString, a.base64).then(function(t) { var e = new l(a); return e.load(t), e }).then(function(t) {
                                    var e = [r.Promise.resolve(t)],
                                        n = t.files;
                                    if (a.checkCRC32)
                                        for (var i = 0; i < n.length; i++) e.push(u(n[i]));
                                    return r.Promise.all(e)
                                }).then(function(t) {
                                    for (var e = t.shift(), n = e.files, i = 0; i < n.length; i++) {
                                        var r = n[i];
                                        o.file(r.fileNameStr, r.decompressed, { binary: !0, optimizedBinaryString: !0, date: r.date, dir: r.dir, comment: r.fileCommentStr.length ? r.fileCommentStr : null, unixPermissions: r.unixPermissions, dosPermissions: r.dosPermissions, createFolders: a.createFolders })
                                    }
                                    return e.zipComment.length && (o.comment = e.zipComment), o
                                })
                            }
                        }, { "./external": 6, "./nodejsUtils": 14, "./stream/Crc32Probe": 25, "./utf8": 31, "./utils": 32, "./zipEntries": 33 }],
                        12: [function(t, e, n) {
                            var i = t("../utils"),
                                r = t("../stream/GenericWorker");

                            function a(t, e) { r.call(this, "Nodejs stream input adapter for " + t), this._upstreamEnded = !1, this._bindStream(e) }
                            i.inherits(a, r), a.prototype._bindStream = function(t) {
                                var e = this;
                                (this._stream = t).pause(), t.on("data", function(t) { e.push({ data: t, meta: { percent: 0 } }) }).on("error", function(t) { e.isPaused ? this.generatedError = t : e.error(t) }).on("end", function() { e.isPaused ? e._upstreamEnded = !0 : e.end() })
                            }, a.prototype.pause = function() { return !!r.prototype.pause.call(this) && (this._stream.pause(), !0) }, a.prototype.resume = function() { return !!r.prototype.resume.call(this) && (this._upstreamEnded ? this.end() : this._stream.resume(), !0) }, e.exports = a
                        }, { "../stream/GenericWorker": 28, "../utils": 32 }],
                        13: [function(t, e, n) {
                            var r = t("readable-stream").Readable;

                            function i(t, e, n) {
                                r.call(this, e), this._helper = t;
                                var i = this;
                                t.on("data", function(t, e) { i.push(t) || i._helper.pause(), n && n(e) }).on("error", function(t) { i.emit("error", t) }).on("end", function() { i.push(null) })
                            }
                            t("../utils").inherits(i, r), i.prototype._read = function() { this._helper.resume() }, e.exports = i
                        }, { "../utils": 32, "readable-stream": 16 }],
                        14: [function(t, e, n) { e.exports = { isNode: void 0 !== d, newBufferFrom: function(t, e) { if (d.from && d.from !== Uint8Array.from) return d.from(t, e); if ("number" == typeof t) throw new Error('The "data" argument must not be a number'); return new d(t, e) }, allocBuffer: function(t) { if (d.alloc) return d.alloc(t); var e = new d(t); return e.fill(0), e }, isBuffer: function(t) { return d.isBuffer(t) }, isStream: function(t) { return t && "function" == typeof t.on && "function" == typeof t.pause && "function" == typeof t.resume } } }, {}],
                        15: [function(t, e, n) {
                            function a(t, e, n) {
                                var i, r = c.getTypeOf(e),
                                    a = c.extend(n || {}, h);
                                a.date = a.date || new Date, null !== a.compression && (a.compression = a.compression.toUpperCase()), "string" == typeof a.unixPermissions && (a.unixPermissions = w()(a.unixPermissions, 8)), a.unixPermissions && 16384 & a.unixPermissions && (a.dir = !0), a.dosPermissions && 16 & a.dosPermissions && (a.dir = !0), a.dir && (t = _(t)), a.createFolders && (i = v(t)) && g.call(this, i, !0);
                                var o = "string" === r && !1 === a.binary && !1 === a.base64;
                                n && void 0 !== n.binary || (a.binary = !o), (e instanceof d && 0 === e.uncompressedSize || a.dir || !e || 0 === e.length) && (a.base64 = !1, a.binary = !0, e = "", a.compression = "STORE", r = "string");
                                var s = null;
                                s = e instanceof d || e instanceof u ? e : m.isNode && m.isStream(e) ? new p(t, e) : c.prepareContent(t, e, a.binary, a.optimizedBinaryString, a.base64);
                                var l = new f(t, s, a);
                                this.files[t] = l
                            }
                            var r = t("./utf8"),
                                c = t("./utils"),
                                u = t("./stream/GenericWorker"),
                                o = t("./stream/StreamHelper"),
                                h = t("./defaults"),
                                d = t("./compressedObject"),
                                f = t("./zipObject"),
                                s = t("./generate"),
                                m = t("./nodejsUtils"),
                                p = t("./nodejs/NodejsStreamInputAdapter"),
                                v = function(t) { "/" === t.slice(-1) && (t = t.substring(0, t.length - 1)); var e = t.lastIndexOf("/"); return 0 < e ? t.substring(0, e) : "" },
                                _ = function(t) { return "/" !== t.slice(-1) && (t += "/"), t },
                                g = function(t, e) { return e = void 0 !== e ? e : h.createFolders, t = _(t), this.files[t] || a.call(this, t, null, { dir: !0, createFolders: e }), this.files[t] };

                            function l(t) { return "[object RegExp]" === Object.prototype.toString.call(t) }
                            var i = {
                                load: function() { throw new Error("This method has been removed in JSZip 3.0, please check the upgrade guide.") },
                                forEach: function(t) { var e, n, i; for (e in this.files) this.files.hasOwnProperty(e) && (i = this.files[e], (n = e.slice(this.root.length, e.length)) && e.slice(0, this.root.length) === this.root && t(n, i)) },
                                filter: function(n) { var i = []; return this.forEach(function(t, e) { n(t, e) && i.push(e) }), i },
                                file: function(t, e, n) { if (1 !== arguments.length) return t = this.root + t, a.call(this, t, e, n), this; if (l(t)) { var i = t; return this.filter(function(t, e) { return !e.dir && i.test(t) }) } var r = this.files[this.root + t]; return r && !r.dir ? r : null },
                                folder: function(n) {
                                    if (!n) return this;
                                    if (l(n)) return this.filter(function(t, e) { return e.dir && n.test(t) });
                                    var t = this.root + n,
                                        e = g.call(this, t),
                                        i = this.clone();
                                    return i.root = e.name, i
                                },
                                remove: function(n) {
                                    n = this.root + n;
                                    var t = this.files[n];
                                    if (t || ("/" !== n.slice(-1) && (n += "/"), t = this.files[n]), t && !t.dir) delete this.files[n];
                                    else
                                        for (var e = this.filter(function(t, e) { return e.name.slice(0, n.length) === n }), i = 0; i < e.length; i++) delete this.files[e[i].name];
                                    return this
                                },
                                generate: function() { throw new Error("This method has been removed in JSZip 3.0, please check the upgrade guide.") },
                                generateInternalStream: function(t) {
                                    var e, n = {};
                                    try {
                                        if ((n = c.extend(t || {}, { streamFiles: !1, compression: "STORE", compressionOptions: null, type: "", platform: "DOS", comment: null, mimeType: "application/zip", encodeFileName: r.utf8encode })).type = n.type.toLowerCase(), n.compression = n.compression.toUpperCase(), "binarystring" === n.type && (n.type = "string"), !n.type) throw new Error("No output type specified.");
                                        c.checkSupport(n.type), "darwin" !== n.platform && "freebsd" !== n.platform && "linux" !== n.platform && "sunos" !== n.platform || (n.platform = "UNIX"), "win32" === n.platform && (n.platform = "DOS");
                                        var i = n.comment || this.comment || "";
                                        e = s.generateWorker(this, n, i)
                                    } catch (t) {
                                        (e = new u("error")).error(t)
                                    }
                                    return new o(e, n.type || "string", n.mimeType)
                                },
                                generateAsync: function(t, e) { return this.generateInternalStream(t).accumulate(e) },
                                generateNodeStream: function(t, e) { return (t = t || {}).type || (t.type = "nodebuffer"), this.generateInternalStream(t).toNodejsStream(e) }
                            };
                            e.exports = i
                        }, { "./compressedObject": 2, "./defaults": 5, "./generate": 9, "./nodejs/NodejsStreamInputAdapter": 12, "./nodejsUtils": 14, "./stream/GenericWorker": 28, "./stream/StreamHelper": 29, "./utf8": 31, "./utils": 32, "./zipObject": 35 }],
                        16: [function(t, e, n) { e.exports = t("stream") }, { stream: void 0 }],
                        17: [function(t, e, n) {
                            var i = t("./DataReader");

                            function r(t) { i.call(this, t); for (var e = 0; e < this.data.length; e++) t[e] = 255 & t[e] }
                            t("../utils").inherits(r, i), r.prototype.byteAt = function(t) { return this.data[this.zero + t] }, r.prototype.lastIndexOfSignature = function(t) {
                                for (var e = t.charCodeAt(0), n = t.charCodeAt(1), i = t.charCodeAt(2), r = t.charCodeAt(3), a = this.length - 4; 0 <= a; --a)
                                    if (this.data[a] === e && this.data[a + 1] === n && this.data[a + 2] === i && this.data[a + 3] === r) return a - this.zero;
                                return -1
                            }, r.prototype.readAndCheckSignature = function(t) {
                                var e = t.charCodeAt(0),
                                    n = t.charCodeAt(1),
                                    i = t.charCodeAt(2),
                                    r = t.charCodeAt(3),
                                    a = this.readData(4);
                                return e === a[0] && n === a[1] && i === a[2] && r === a[3]
                            }, r.prototype.readData = function(t) { if (this.checkOffset(t), 0 === t) return []; var e = this.data.slice(this.zero + this.index, this.zero + this.index + t); return this.index += t, e }, e.exports = r
                        }, { "../utils": 32, "./DataReader": 18 }],
                        18: [function(t, e, n) {
                            var i = t("../utils");

                            function r(t) { this.data = t, this.length = t.length, this.index = 0, this.zero = 0 }
                            r.prototype = { checkOffset: function(t) { this.checkIndex(this.index + t) }, checkIndex: function(t) { if (this.length < this.zero + t || t < 0) throw new Error("End of data reached (data length = " + this.length + ", asked index = " + t + "). Corrupted zip ?") }, setIndex: function(t) { this.checkIndex(t), this.index = t }, skip: function(t) { this.setIndex(this.index + t) }, byteAt: function() {}, readInt: function(t) { var e, n = 0; for (this.checkOffset(t), e = this.index + t - 1; e >= this.index; e--) n = (n << 8) + this.byteAt(e); return this.index += t, n }, readString: function(t) { return i.transformTo("string", this.readData(t)) }, readData: function() {}, lastIndexOfSignature: function() {}, readAndCheckSignature: function() {}, readDate: function() { var t = this.readInt(4); return new Date(Date.UTC(1980 + (t >> 25 & 127), (t >> 21 & 15) - 1, t >> 16 & 31, t >> 11 & 31, t >> 5 & 63, (31 & t) << 1)) } }, e.exports = r
                        }, { "../utils": 32 }],
                        19: [function(t, e, n) {
                            var i = t("./Uint8ArrayReader");

                            function r(t) { i.call(this, t) }
                            t("../utils").inherits(r, i), r.prototype.readData = function(t) { this.checkOffset(t); var e = this.data.slice(this.zero + this.index, this.zero + this.index + t); return this.index += t, e }, e.exports = r
                        }, { "../utils": 32, "./Uint8ArrayReader": 21 }],
                        20: [function(t, e, n) {
                            var i = t("./DataReader");

                            function r(t) { i.call(this, t) }
                            t("../utils").inherits(r, i), r.prototype.byteAt = function(t) { return this.data.charCodeAt(this.zero + t) }, r.prototype.lastIndexOfSignature = function(t) { return this.data.lastIndexOf(t) - this.zero }, r.prototype.readAndCheckSignature = function(t) { return t === this.readData(4) }, r.prototype.readData = function(t) { this.checkOffset(t); var e = this.data.slice(this.zero + this.index, this.zero + this.index + t); return this.index += t, e }, e.exports = r
                        }, { "../utils": 32, "./DataReader": 18 }],
                        21: [function(t, e, n) {
                            var i = t("./ArrayReader");

                            function r(t) { i.call(this, t) }
                            t("../utils").inherits(r, i), r.prototype.readData = function(t) { if (this.checkOffset(t), 0 === t) return new Uint8Array(0); var e = this.data.subarray(this.zero + this.index, this.zero + this.index + t); return this.index += t, e }, e.exports = r
                        }, { "../utils": 32, "./ArrayReader": 17 }],
                        22: [function(t, e, n) {
                            var i = t("../utils"),
                                r = t("../support"),
                                a = t("./ArrayReader"),
                                o = t("./StringReader"),
                                s = t("./NodeBufferReader"),
                                l = t("./Uint8ArrayReader");
                            e.exports = function(t) { var e = i.getTypeOf(t); return i.checkSupport(e), "string" !== e || r.uint8array ? "nodebuffer" === e ? new s(t) : r.uint8array ? new l(i.transformTo("uint8array", t)) : new a(i.transformTo("array", t)) : new o(t) }
                        }, { "../support": 30, "../utils": 32, "./ArrayReader": 17, "./NodeBufferReader": 19, "./StringReader": 20, "./Uint8ArrayReader": 21 }],
                        23: [function(t, e, n) { n.LOCAL_FILE_HEADER = "PK", n.CENTRAL_FILE_HEADER = "PK", n.CENTRAL_DIRECTORY_END = "PK", n.ZIP64_CENTRAL_DIRECTORY_LOCATOR = "PK", n.ZIP64_CENTRAL_DIRECTORY_END = "PK", n.DATA_DESCRIPTOR = "PK\b" }, {}],
                        24: [function(t, e, n) {
                            var i = t("./GenericWorker"),
                                r = t("../utils");

                            function a(t) { i.call(this, "ConvertWorker to " + t), this.destType = t }
                            r.inherits(a, i), a.prototype.processChunk = function(t) { this.push({ data: r.transformTo(this.destType, t.data), meta: t.meta }) }, e.exports = a
                        }, { "../utils": 32, "./GenericWorker": 28 }],
                        25: [function(t, e, n) {
                            var i = t("./GenericWorker"),
                                r = t("../crc32");

                            function a() { i.call(this, "Crc32Probe"), this.withStreamInfo("crc32", 0) }
                            t("../utils").inherits(a, i), a.prototype.processChunk = function(t) { this.streamInfo.crc32 = r(t.data, this.streamInfo.crc32 || 0), this.push(t) }, e.exports = a
                        }, { "../crc32": 4, "../utils": 32, "./GenericWorker": 28 }],
                        26: [function(t, e, n) {
                            var i = t("../utils"),
                                r = t("./GenericWorker");

                            function a(t) { r.call(this, "DataLengthProbe for " + t), this.propName = t, this.withStreamInfo(t, 0) }
                            i.inherits(a, r), a.prototype.processChunk = function(t) {
                                if (t) {
                                    var e = this.streamInfo[this.propName] || 0;
                                    this.streamInfo[this.propName] = e + t.data.length
                                }
                                r.prototype.processChunk.call(this, t)
                            }, e.exports = a
                        }, { "../utils": 32, "./GenericWorker": 28 }],
                        27: [function(t, e, n) {
                            var i = t("../utils"),
                                r = t("./GenericWorker");

                            function a(t) {
                                r.call(this, "DataWorker");
                                var e = this;
                                this.dataIsReady = !1, this.index = 0, this.max = 0, this.data = null, this.type = "", this._tickScheduled = !1, t.then(function(t) { e.dataIsReady = !0, e.data = t, e.max = t && t.length || 0, e.type = i.getTypeOf(t), e.isPaused || e._tickAndRepeat() }, function(t) { e.error(t) })
                            }
                            i.inherits(a, r), a.prototype.cleanUp = function() { r.prototype.cleanUp.call(this), this.data = null }, a.prototype.resume = function() { return !!r.prototype.resume.call(this) && (!this._tickScheduled && this.dataIsReady && (this._tickScheduled = !0, i.delay(this._tickAndRepeat, [], this)), !0) }, a.prototype._tickAndRepeat = function() { this._tickScheduled = !1, this.isPaused || this.isFinished || (this._tick(), this.isFinished || (i.delay(this._tickAndRepeat, [], this), this._tickScheduled = !0)) }, a.prototype._tick = function() {
                                if (this.isPaused || this.isFinished) return !1;
                                var t = null,
                                    e = Math.min(this.max, this.index + 16384);
                                if (this.index >= this.max) return this.end();
                                switch (this.type) {
                                    case "string":
                                        t = this.data.substring(this.index, e);
                                        break;
                                    case "uint8array":
                                        t = this.data.subarray(this.index, e);
                                        break;
                                    case "array":
                                    case "nodebuffer":
                                        t = this.data.slice(this.index, e)
                                }
                                return this.index = e, this.push({ data: t, meta: { percent: this.max ? this.index / this.max * 100 : 0 } })
                            }, e.exports = a
                        }, { "../utils": 32, "./GenericWorker": 28 }],
                        28: [function(t, e, n) {
                            function i(t) { this.name = t || "default", this.streamInfo = {}, this.generatedError = null, this.extraStreamInfo = {}, this.isPaused = !0, this.isFinished = !1, this.isLocked = !1, this._listeners = { data: [], end: [], error: [] }, this.previous = null }
                            i.prototype = {
                                push: function(t) { this.emit("data", t) },
                                end: function() {
                                    if (this.isFinished) return !1;
                                    this.flush();
                                    try { this.emit("end"), this.cleanUp(), this.isFinished = !0 } catch (t) { this.emit("error", t) }
                                    return !0
                                },
                                error: function(t) { return !this.isFinished && (this.isPaused ? this.generatedError = t : (this.isFinished = !0, this.emit("error", t), this.previous && this.previous.error(t), this.cleanUp()), !0) },
                                on: function(t, e) { return this._listeners[t].push(e), this },
                                cleanUp: function() { this.streamInfo = this.generatedError = this.extraStreamInfo = null, this._listeners = [] },
                                emit: function(t, e) {
                                    if (this._listeners[t])
                                        for (var n = 0; n < this._listeners[t].length; n++) this._listeners[t][n].call(this, e)
                                },
                                pipe: function(t) { return t.registerPrevious(this) },
                                registerPrevious: function(t) {
                                    if (this.isLocked) throw new Error("The stream '" + this + "' has already been used.");
                                    this.streamInfo = t.streamInfo, this.mergeStreamInfo(), this.previous = t;
                                    var e = this;
                                    return t.on("data", function(t) { e.processChunk(t) }), t.on("end", function() { e.end() }), t.on("error", function(t) { e.error(t) }), this
                                },
                                pause: function() { return !this.isPaused && !this.isFinished && (this.isPaused = !0, this.previous && this.previous.pause(), !0) },
                                resume: function() { if (!this.isPaused || this.isFinished) return !1; var t = this.isPaused = !1; return this.generatedError && (this.error(this.generatedError), t = !0), this.previous && this.previous.resume(), !t },
                                flush: function() {},
                                processChunk: function(t) { this.push(t) },
                                withStreamInfo: function(t, e) { return this.extraStreamInfo[t] = e, this.mergeStreamInfo(), this },
                                mergeStreamInfo: function() { for (var t in this.extraStreamInfo) this.extraStreamInfo.hasOwnProperty(t) && (this.streamInfo[t] = this.extraStreamInfo[t]) },
                                lock: function() {
                                    if (this.isLocked) throw new Error("The stream '" + this + "' has already been used.");
                                    this.isLocked = !0, this.previous && this.previous.lock()
                                },
                                toString: function() { var t = "Worker " + this.name; return this.previous ? this.previous + " -> " + t : t }
                            }, e.exports = i
                        }, {}],
                        29: [function(t, e, n) {
                            var l = t("../utils"),
                                r = t("./ConvertWorker"),
                                a = t("./GenericWorker"),
                                c = t("../base64"),
                                i = t("../support"),
                                o = t("../external"),
                                s = null;
                            if (i.nodestream) try { s = t("../nodejs/NodejsStreamOutputAdapter") } catch (t) {}

                            function u(t, s) {
                                return new o.Promise(function(e, n) {
                                    var i = [],
                                        r = t._internalType,
                                        a = t._outputType,
                                        o = t._mimeType;
                                    t.on("data", function(t, e) { i.push(t), s && s(e) }).on("error", function(t) { i = [], n(t) }).on("end", function() {
                                        try {
                                            var t = function(t, e, n) {
                                                switch (t) {
                                                    case "blob":
                                                        return l.newBlob(l.transformTo("arraybuffer", e), n);
                                                    case "base64":
                                                        return c.encode(e);
                                                    default:
                                                        return l.transformTo(t, e)
                                                }
                                            }(a, function(t, e) {
                                                var n, i = 0,
                                                    r = null,
                                                    a = 0;
                                                for (n = 0; n < e.length; n++) a += e[n].length;
                                                switch (t) {
                                                    case "string":
                                                        return e.join("");
                                                    case "array":
                                                        return Array.prototype.concat.apply([], e);
                                                    case "uint8array":
                                                        for (r = new Uint8Array(a), n = 0; n < e.length; n++) r.set(e[n], i), i += e[n].length;
                                                        return r;
                                                    case "nodebuffer":
                                                        return d.concat(e);
                                                    default:
                                                        throw new Error("concat : unsupported type '" + t + "'")
                                                }
                                            }(r, i), o);
                                            e(t)
                                        } catch (t) { n(t) }
                                        i = []
                                    }).resume()
                                })
                            }

                            function h(t, e, n) {
                                var i = e;
                                switch (e) {
                                    case "blob":
                                    case "arraybuffer":
                                        i = "uint8array";
                                        break;
                                    case "base64":
                                        i = "string"
                                }
                                try { this._internalType = i, this._outputType = e, this._mimeType = n, l.checkSupport(i), this._worker = t.pipe(new r(i)), t.lock() } catch (t) { this._worker = new a("error"), this._worker.error(t) }
                            }
                            h.prototype = { accumulate: function(t) { return u(this, t) }, on: function(t, e) { var n = this; return "data" === t ? this._worker.on(t, function(t) { e.call(n, t.data, t.meta) }) : this._worker.on(t, function() { l.delay(e, arguments, n) }), this }, resume: function() { return l.delay(this._worker.resume, [], this._worker), this }, pause: function() { return this._worker.pause(), this }, toNodejsStream: function(t) { if (l.checkSupport("nodestream"), "nodebuffer" !== this._outputType) throw new Error(this._outputType + " is not supported by this method"); return new s(this, { objectMode: "nodebuffer" !== this._outputType }, t) } }, e.exports = h
                        }, { "../base64": 1, "../external": 6, "../nodejs/NodejsStreamOutputAdapter": 13, "../support": 30, "../utils": 32, "./ConvertWorker": 24, "./GenericWorker": 28 }],
                        30: [function(t, e, n) {
                            if (n.base64 = !0, n.array = !0, n.string = !0, n.arraybuffer = "undefined" != typeof ArrayBuffer && "undefined" != typeof Uint8Array, n.nodebuffer = void 0 !== d, n.uint8array = "undefined" != typeof Uint8Array, "undefined" == typeof ArrayBuffer) n.blob = !1;
                            else {
                                var i = new ArrayBuffer(0);
                                try { n.blob = 0 === new Blob([i], { type: "application/zip" }).size } catch (t) {
                                    try {
                                        var r = new(self.BlobBuilder || self.WebKitBlobBuilder || self.MozBlobBuilder || self.MSBlobBuilder);
                                        r.append(i), n.blob = 0 === r.getBlob("application/zip").size
                                    } catch (t) { n.blob = !1 }
                                }
                            }
                            try { n.nodestream = !!t("readable-stream").Readable } catch (t) { n.nodestream = !1 }
                        }, { "readable-stream": 16 }],
                        31: [function(t, e, a) {
                            for (var s = t("./utils"), l = t("./support"), n = t("./nodejsUtils"), i = t("./stream/GenericWorker"), c = new Array(256), r = 0; r < 256; r++) c[r] = 252 <= r ? 6 : 248 <= r ? 5 : 240 <= r ? 4 : 224 <= r ? 3 : 192 <= r ? 2 : 1;
                            c[254] = c[254] = 1;

                            function o() { i.call(this, "utf-8 decode"), this.leftOver = null }

                            function u() { i.call(this, "utf-8 encode") }
                            a.utf8encode = function(t) {
                                return l.nodebuffer ? n.newBufferFrom(t, "utf-8") : function(t) {
                                    var e, n, i, r, a, o = t.length,
                                        s = 0;
                                    for (r = 0; r < o; r++) 55296 == (64512 & (n = t.charCodeAt(r))) && r + 1 < o && 56320 == (64512 & (i = t.charCodeAt(r + 1))) && (n = 65536 + (n - 55296 << 10) + (i - 56320), r++), s += n < 128 ? 1 : n < 2048 ? 2 : n < 65536 ? 3 : 4;
                                    for (e = new(l.uint8array ? Uint8Array : Array)(s), r = a = 0; a < s; r++) 55296 == (64512 & (n = t.charCodeAt(r))) && r + 1 < o && 56320 == (64512 & (i = t.charCodeAt(r + 1))) && (n = 65536 + (n - 55296 << 10) + (i - 56320), r++), n < 128 ? e[a++] = n : (n < 2048 ? e[a++] = 192 | n >>> 6 : (n < 65536 ? e[a++] = 224 | n >>> 12 : (e[a++] = 240 | n >>> 18, e[a++] = 128 | n >>> 12 & 63), e[a++] = 128 | n >>> 6 & 63), e[a++] = 128 | 63 & n);
                                    return e
                                }(t)
                            }, a.utf8decode = function(t) {
                                return l.nodebuffer ? s.transformTo("nodebuffer", t).toString("utf-8") : function(t) {
                                    var e, n, i, r, a = t.length,
                                        o = new Array(2 * a);
                                    for (e = n = 0; e < a;)
                                        if ((i = t[e++]) < 128) o[n++] = i;
                                        else if (4 < (r = c[i])) o[n++] = 65533, e += r - 1;
                                    else {
                                        for (i &= 2 === r ? 31 : 3 === r ? 15 : 7; 1 < r && e < a;) i = i << 6 | 63 & t[e++], r--;
                                        1 < r ? o[n++] = 65533 : i < 65536 ? o[n++] = i : (i -= 65536, o[n++] = 55296 | i >> 10 & 1023, o[n++] = 56320 | 1023 & i)
                                    }
                                    return o.length !== n && (o.subarray ? o = o.subarray(0, n) : o.length = n), s.applyFromCharCode(o)
                                }(t = s.transformTo(l.uint8array ? "uint8array" : "array", t))
                            }, s.inherits(o, i), o.prototype.processChunk = function(t) {
                                var e = s.transformTo(l.uint8array ? "uint8array" : "array", t.data);
                                if (this.leftOver && this.leftOver.length) {
                                    if (l.uint8array) {
                                        var n = e;
                                        (e = new Uint8Array(n.length + this.leftOver.length)).set(this.leftOver, 0), e.set(n, this.leftOver.length)
                                    } else e = this.leftOver.concat(e);
                                    this.leftOver = null
                                }
                                var i = function(t, e) { var n; for ((e = e || t.length) > t.length && (e = t.length), n = e - 1; 0 <= n && 128 == (192 & t[n]);) n--; return !(n < 0) && 0 !== n && n + c[t[n]] > e ? n : e }(e),
                                    r = e;
                                i !== e.length && (l.uint8array ? (r = e.subarray(0, i), this.leftOver = e.subarray(i, e.length)) : (r = e.slice(0, i), this.leftOver = e.slice(i, e.length))), this.push({ data: a.utf8decode(r), meta: t.meta })
                            }, o.prototype.flush = function() { this.leftOver && this.leftOver.length && (this.push({ data: a.utf8decode(this.leftOver), meta: {} }), this.leftOver = null) }, a.Utf8DecodeWorker = o, s.inherits(u, i), u.prototype.processChunk = function(t) { this.push({ data: a.utf8encode(t.data), meta: t.meta }) }, a.Utf8EncodeWorker = u
                        }, { "./nodejsUtils": 14, "./stream/GenericWorker": 28, "./support": 30, "./utils": 32 }],
                        32: [function(t, e, s) {
                            var l = t("./support"),
                                c = t("./base64"),
                                n = t("./nodejsUtils"),
                                i = t("set-immediate-shim"),
                                u = t("./external");

                            function r(t) { return t }

                            function h(t, e) { for (var n = 0; n < t.length; ++n) e[n] = 255 & t.charCodeAt(n); return e }
                            s.newBlob = function(e, n) { s.checkSupport("blob"); try { return new Blob([e], { type: n }) } catch (t) { try { var i = new(self.BlobBuilder || self.WebKitBlobBuilder || self.MozBlobBuilder || self.MSBlobBuilder); return i.append(e), i.getBlob(n) } catch (t) { throw new Error("Bug : can't construct the Blob.") } } };
                            var a = {
                                stringifyByChunk: function(t, e, n) {
                                    var i = [],
                                        r = 0,
                                        a = t.length;
                                    if (a <= n) return String.fromCharCode.apply(null, t);
                                    for (; r < a;) "array" === e || "nodebuffer" === e ? i.push(String.fromCharCode.apply(null, t.slice(r, Math.min(r + n, a)))) : i.push(String.fromCharCode.apply(null, t.subarray(r, Math.min(r + n, a)))), r += n;
                                    return i.join("")
                                },
                                stringifyByChar: function(t) { for (var e = "", n = 0; n < t.length; n++) e += String.fromCharCode(t[n]); return e },
                                applyCanBeUsed: { uint8array: function() { try { return l.uint8array && 1 === String.fromCharCode.apply(null, new Uint8Array(1)).length } catch (t) { return !1 } }(), nodebuffer: function() { try { return l.nodebuffer && 1 === String.fromCharCode.apply(null, n.allocBuffer(1)).length } catch (t) { return !1 } }() }
                            };

                            function o(t) {
                                var e = 65536,
                                    n = s.getTypeOf(t),
                                    i = !0;
                                if ("uint8array" === n ? i = a.applyCanBeUsed.uint8array : "nodebuffer" === n && (i = a.applyCanBeUsed.nodebuffer), i)
                                    for (; 1 < e;) try { return a.stringifyByChunk(t, n, e) } catch (t) { e = Math.floor(e / 2) }
                                return a.stringifyByChar(t)
                            }

                            function d(t, e) { for (var n = 0; n < t.length; n++) e[n] = t[n]; return e }
                            s.applyFromCharCode = o;
                            var f = {};
                            f.string = { string: r, array: function(t) { return h(t, new Array(t.length)) }, arraybuffer: function(t) { return f.string.uint8array(t).buffer }, uint8array: function(t) { return h(t, new Uint8Array(t.length)) }, nodebuffer: function(t) { return h(t, n.allocBuffer(t.length)) } }, f.array = { string: o, array: r, arraybuffer: function(t) { return new Uint8Array(t).buffer }, uint8array: function(t) { return new Uint8Array(t) }, nodebuffer: function(t) { return n.newBufferFrom(t) } }, f.arraybuffer = { string: function(t) { return o(new Uint8Array(t)) }, array: function(t) { return d(new Uint8Array(t), new Array(t.byteLength)) }, arraybuffer: r, uint8array: function(t) { return new Uint8Array(t) }, nodebuffer: function(t) { return n.newBufferFrom(new Uint8Array(t)) } }, f.uint8array = { string: o, array: function(t) { return d(t, new Array(t.length)) }, arraybuffer: function(t) { return t.buffer }, uint8array: r, nodebuffer: function(t) { return n.newBufferFrom(t) } }, f.nodebuffer = { string: o, array: function(t) { return d(t, new Array(t.length)) }, arraybuffer: function(t) { return f.nodebuffer.uint8array(t).buffer }, uint8array: function(t) { return d(t, new Uint8Array(t.length)) }, nodebuffer: r }, s.transformTo = function(t, e) {
                                if (e = e || "", !t) return e;
                                s.checkSupport(t);
                                var n = s.getTypeOf(e);
                                return f[n][t](e)
                            }, s.getTypeOf = function(t) { return "string" == typeof t ? "string" : "[object Array]" === Object.prototype.toString.call(t) ? "array" : l.nodebuffer && n.isBuffer(t) ? "nodebuffer" : l.uint8array && t instanceof Uint8Array ? "uint8array" : l.arraybuffer && t instanceof ArrayBuffer ? "arraybuffer" : void 0 }, s.checkSupport = function(t) { if (!l[t.toLowerCase()]) throw new Error(t + " is not supported by this platform") }, s.MAX_VALUE_16BITS = 65535, s.MAX_VALUE_32BITS = -1, s.pretty = function(t) { var e, n, i = ""; for (n = 0; n < (t || "").length; n++) i += "\\x" + ((e = t.charCodeAt(n)) < 16 ? "0" : "") + e.toString(16).toUpperCase(); return i }, s.delay = function(t, e, n) { i(function() { t.apply(n || null, e || []) }) }, s.inherits = function(t, e) {
                                function n() {}
                                n.prototype = e.prototype, t.prototype = new n
                            }, s.extend = function() {
                                var t, e, n = {};
                                for (t = 0; t < arguments.length; t++)
                                    for (e in arguments[t]) arguments[t].hasOwnProperty(e) && void 0 === n[e] && (n[e] = arguments[t][e]);
                                return n
                            }, s.prepareContent = function(i, t, r, a, o) {
                                return u.Promise.resolve(t).then(function(i) {
                                    return l.blob && (i instanceof Blob || -1 !== ["[object File]", "[object Blob]"].indexOf(Object.prototype.toString.call(i))) && "undefined" != typeof FileReader ? new u.Promise(function(e, n) {
                                        var t = new FileReader;
                                        t.onload = function(t) { e(t.target.result) }, t.onerror = function(t) { n(t.target.error) }, t.readAsArrayBuffer(i)
                                    }) : i
                                }).then(function(t) { var e, n = s.getTypeOf(t); return n ? ("arraybuffer" === n ? t = s.transformTo("uint8array", t) : "string" === n && (o ? t = c.decode(t) : r && !0 !== a && (t = h(e = t, new(l.uint8array ? Uint8Array : Array)(e.length)))), t) : u.Promise.reject(new Error("Can't read the data of '" + i + "'. Is it in a supported JavaScript type (String, Blob, ArrayBuffer, etc) ?")) })
                            }
                        }, { "./base64": 1, "./external": 6, "./nodejsUtils": 14, "./support": 30, "set-immediate-shim": 54 }],
                        33: [function(t, e, n) {
                            var i = t("./reader/readerFor"),
                                r = t("./utils"),
                                a = t("./signature"),
                                o = t("./zipEntry"),
                                s = (t("./utf8"), t("./support"));

                            function l(t) { this.files = [], this.loadOptions = t }
                            l.prototype = {
                                checkSignature: function(t) { if (!this.reader.readAndCheckSignature(t)) { this.reader.index -= 4; var e = this.reader.readString(4); throw new Error("Corrupted zip or bug: unexpected signature (" + r.pretty(e) + ", expected " + r.pretty(t) + ")") } },
                                isSignature: function(t, e) {
                                    var n = this.reader.index;
                                    this.reader.setIndex(t);
                                    var i = this.reader.readString(4) === e;
                                    return this.reader.setIndex(n), i
                                },
                                readBlockEndOfCentral: function() {
                                    this.diskNumber = this.reader.readInt(2), this.diskWithCentralDirStart = this.reader.readInt(2), this.centralDirRecordsOnThisDisk = this.reader.readInt(2), this.centralDirRecords = this.reader.readInt(2), this.centralDirSize = this.reader.readInt(4), this.centralDirOffset = this.reader.readInt(4), this.zipCommentLength = this.reader.readInt(2);
                                    var t = this.reader.readData(this.zipCommentLength),
                                        e = s.uint8array ? "uint8array" : "array",
                                        n = r.transformTo(e, t);
                                    this.zipComment = this.loadOptions.decodeFileName(n)
                                },
                                readBlockZip64EndOfCentral: function() { this.zip64EndOfCentralSize = this.reader.readInt(8), this.reader.skip(4), this.diskNumber = this.reader.readInt(4), this.diskWithCentralDirStart = this.reader.readInt(4), this.centralDirRecordsOnThisDisk = this.reader.readInt(8), this.centralDirRecords = this.reader.readInt(8), this.centralDirSize = this.reader.readInt(8), this.centralDirOffset = this.reader.readInt(8), this.zip64ExtensibleData = {}; for (var t, e, n, i = this.zip64EndOfCentralSize - 44; 0 < i;) t = this.reader.readInt(2), e = this.reader.readInt(4), n = this.reader.readData(e), this.zip64ExtensibleData[t] = { id: t, length: e, value: n } },
                                readBlockZip64EndOfCentralLocator: function() { if (this.diskWithZip64CentralDirStart = this.reader.readInt(4), this.relativeOffsetEndOfZip64CentralDir = this.reader.readInt(8), this.disksCount = this.reader.readInt(4), 1 < this.disksCount) throw new Error("Multi-volumes zip are not supported") },
                                readLocalFiles: function() { var t, e; for (t = 0; t < this.files.length; t++) e = this.files[t], this.reader.setIndex(e.localHeaderOffset), this.checkSignature(a.LOCAL_FILE_HEADER), e.readLocalPart(this.reader), e.handleUTF8(), e.processAttributes() },
                                readCentralDir: function() { var t; for (this.reader.setIndex(this.centralDirOffset); this.reader.readAndCheckSignature(a.CENTRAL_FILE_HEADER);)(t = new o({ zip64: this.zip64 }, this.loadOptions)).readCentralPart(this.reader), this.files.push(t); if (this.centralDirRecords !== this.files.length && 0 !== this.centralDirRecords && 0 === this.files.length) throw new Error("Corrupted zip or bug: expected " + this.centralDirRecords + " records in central dir, got " + this.files.length) },
                                readEndOfCentral: function() {
                                    var t = this.reader.lastIndexOfSignature(a.CENTRAL_DIRECTORY_END);
                                    if (t < 0) throw !this.isSignature(0, a.LOCAL_FILE_HEADER) ? new Error("Can't find end of central directory : is this a zip file ? If it is, see https://stuk.github.io/jszip/documentation/howto/read_zip.html") : new Error("Corrupted zip: can't find end of central directory");
                                    this.reader.setIndex(t);
                                    var e = t;
                                    if (this.checkSignature(a.CENTRAL_DIRECTORY_END), this.readBlockEndOfCentral(), this.diskNumber === r.MAX_VALUE_16BITS || this.diskWithCentralDirStart === r.MAX_VALUE_16BITS || this.centralDirRecordsOnThisDisk === r.MAX_VALUE_16BITS || this.centralDirRecords === r.MAX_VALUE_16BITS || this.centralDirSize === r.MAX_VALUE_32BITS || this.centralDirOffset === r.MAX_VALUE_32BITS) {
                                        if (this.zip64 = !0, (t = this.reader.lastIndexOfSignature(a.ZIP64_CENTRAL_DIRECTORY_LOCATOR)) < 0) throw new Error("Corrupted zip: can't find the ZIP64 end of central directory locator");
                                        if (this.reader.setIndex(t), this.checkSignature(a.ZIP64_CENTRAL_DIRECTORY_LOCATOR), this.readBlockZip64EndOfCentralLocator(), !this.isSignature(this.relativeOffsetEndOfZip64CentralDir, a.ZIP64_CENTRAL_DIRECTORY_END) && (this.relativeOffsetEndOfZip64CentralDir = this.reader.lastIndexOfSignature(a.ZIP64_CENTRAL_DIRECTORY_END), this.relativeOffsetEndOfZip64CentralDir < 0)) throw new Error("Corrupted zip: can't find the ZIP64 end of central directory");
                                        this.reader.setIndex(this.relativeOffsetEndOfZip64CentralDir), this.checkSignature(a.ZIP64_CENTRAL_DIRECTORY_END), this.readBlockZip64EndOfCentral()
                                    }
                                    var n = this.centralDirOffset + this.centralDirSize;
                                    this.zip64 && (n += 20, n += 12 + this.zip64EndOfCentralSize);
                                    var i = e - n;
                                    if (0 < i) this.isSignature(e, a.CENTRAL_FILE_HEADER) || (this.reader.zero = i);
                                    else if (i < 0) throw new Error("Corrupted zip: missing " + Math.abs(i) + " bytes.")
                                },
                                prepareReader: function(t) { this.reader = i(t) },
                                load: function(t) { this.prepareReader(t), this.readEndOfCentral(), this.readCentralDir(), this.readLocalFiles() }
                            }, e.exports = l
                        }, { "./reader/readerFor": 22, "./signature": 23, "./support": 30, "./utf8": 31, "./utils": 32, "./zipEntry": 34 }],
                        34: [function(t, e, n) {
                            var i = t("./reader/readerFor"),
                                a = t("./utils"),
                                r = t("./compressedObject"),
                                o = t("./crc32"),
                                s = t("./utf8"),
                                l = t("./compressions"),
                                c = t("./support");

                            function u(t, e) { this.options = t, this.loadOptions = e }
                            u.prototype = {
                                isEncrypted: function() { return 1 == (1 & this.bitFlag) },
                                useUTF8: function() { return 2048 == (2048 & this.bitFlag) },
                                readLocalPart: function(t) {
                                    var e, n;
                                    if (t.skip(22), this.fileNameLength = t.readInt(2), n = t.readInt(2), this.fileName = t.readData(this.fileNameLength), t.skip(n), -1 === this.compressedSize || -1 === this.uncompressedSize) throw new Error("Bug or corrupted zip : didn't get enough informations from the central directory (compressedSize === -1 || uncompressedSize === -1)");
                                    if (null === (e = function(t) {
                                            for (var e in l)
                                                if (l.hasOwnProperty(e) && l[e].magic === t) return l[e];
                                            return null
                                        }(this.compressionMethod))) throw new Error("Corrupted zip : compression " + a.pretty(this.compressionMethod) + " unknown (inner file : " + a.transformTo("string", this.fileName) + ")");
                                    this.decompressed = new r(this.compressedSize, this.uncompressedSize, this.crc32, e, t.readData(this.compressedSize))
                                },
                                readCentralPart: function(t) {
                                    this.versionMadeBy = t.readInt(2), t.skip(2), this.bitFlag = t.readInt(2), this.compressionMethod = t.readString(2), this.date = t.readDate(), this.crc32 = t.readInt(4), this.compressedSize = t.readInt(4), this.uncompressedSize = t.readInt(4);
                                    var e = t.readInt(2);
                                    if (this.extraFieldsLength = t.readInt(2), this.fileCommentLength = t.readInt(2), this.diskNumberStart = t.readInt(2), this.internalFileAttributes = t.readInt(2), this.externalFileAttributes = t.readInt(4), this.localHeaderOffset = t.readInt(4), this.isEncrypted()) throw new Error("Encrypted zip are not supported");
                                    t.skip(e), this.readExtraFields(t), this.parseZIP64ExtraField(t), this.fileComment = t.readData(this.fileCommentLength)
                                },
                                processAttributes: function() {
                                    this.unixPermissions = null, this.dosPermissions = null;
                                    var t = this.versionMadeBy >> 8;
                                    this.dir = !!(16 & this.externalFileAttributes), 0 == t && (this.dosPermissions = 63 & this.externalFileAttributes), 3 == t && (this.unixPermissions = this.externalFileAttributes >> 16 & 65535), this.dir || "/" !== this.fileNameStr.slice(-1) || (this.dir = !0)
                                },
                                parseZIP64ExtraField: function() {
                                    if (this.extraFields[1]) {
                                        var t = i(this.extraFields[1].value);
                                        this.uncompressedSize === a.MAX_VALUE_32BITS && (this.uncompressedSize = t.readInt(8)), this.compressedSize === a.MAX_VALUE_32BITS && (this.compressedSize = t.readInt(8)), this.localHeaderOffset === a.MAX_VALUE_32BITS && (this.localHeaderOffset = t.readInt(8)), this.diskNumberStart === a.MAX_VALUE_32BITS && (this.diskNumberStart = t.readInt(4))
                                    }
                                },
                                readExtraFields: function(t) { var e, n, i, r = t.index + this.extraFieldsLength; for (this.extraFields || (this.extraFields = {}); t.index < r;) e = t.readInt(2), n = t.readInt(2), i = t.readData(n), this.extraFields[e] = { id: e, length: n, value: i } },
                                handleUTF8: function() {
                                    var t = c.uint8array ? "uint8array" : "array";
                                    if (this.useUTF8()) this.fileNameStr = s.utf8decode(this.fileName), this.fileCommentStr = s.utf8decode(this.fileComment);
                                    else {
                                        var e = this.findExtraFieldUnicodePath();
                                        if (null !== e) this.fileNameStr = e;
                                        else {
                                            var n = a.transformTo(t, this.fileName);
                                            this.fileNameStr = this.loadOptions.decodeFileName(n)
                                        }
                                        var i = this.findExtraFieldUnicodeComment();
                                        if (null !== i) this.fileCommentStr = i;
                                        else {
                                            var r = a.transformTo(t, this.fileComment);
                                            this.fileCommentStr = this.loadOptions.decodeFileName(r)
                                        }
                                    }
                                },
                                findExtraFieldUnicodePath: function() { var t = this.extraFields[28789]; if (t) { var e = i(t.value); return 1 !== e.readInt(1) ? null : o(this.fileName) !== e.readInt(4) ? null : s.utf8decode(e.readData(t.length - 5)) } return null },
                                findExtraFieldUnicodeComment: function() { var t = this.extraFields[25461]; if (t) { var e = i(t.value); return 1 !== e.readInt(1) ? null : o(this.fileComment) !== e.readInt(4) ? null : s.utf8decode(e.readData(t.length - 5)) } return null }
                            }, e.exports = u
                        }, { "./compressedObject": 2, "./compressions": 3, "./crc32": 4, "./reader/readerFor": 22, "./support": 30, "./utf8": 31, "./utils": 32 }],
                        35: [function(t, e, n) {
                            function i(t, e, n) { this.name = t, this.dir = n.dir, this.date = n.date, this.comment = n.comment, this.unixPermissions = n.unixPermissions, this.dosPermissions = n.dosPermissions, this._data = e, this._dataBinary = n.binary, this.options = { compression: n.compression, compressionOptions: n.compressionOptions } }
                            var a = t("./stream/StreamHelper"),
                                r = t("./stream/DataWorker"),
                                o = t("./utf8"),
                                s = t("./compressedObject"),
                                l = t("./stream/GenericWorker");
                            i.prototype = {
                                internalStream: function(t) {
                                    var e = null,
                                        n = "string";
                                    try {
                                        if (!t) throw new Error("No output type specified.");
                                        var i = "string" === (n = t.toLowerCase()) || "text" === n;
                                        "binarystring" !== n && "text" !== n || (n = "string"), e = this._decompressWorker();
                                        var r = !this._dataBinary;
                                        r && !i && (e = e.pipe(new o.Utf8EncodeWorker)), !r && i && (e = e.pipe(new o.Utf8DecodeWorker))
                                    } catch (t) {
                                        (e = new l("error")).error(t)
                                    }
                                    return new a(e, n, "")
                                },
                                async: function(t, e) { return this.internalStream(t).accumulate(e) },
                                nodeStream: function(t, e) { return this.internalStream(t || "nodebuffer").toNodejsStream(e) },
                                _compressWorker: function(t, e) { if (this._data instanceof s && this._data.compression.magic === t.magic) return this._data.getCompressedWorker(); var n = this._decompressWorker(); return this._dataBinary || (n = n.pipe(new o.Utf8EncodeWorker)), s.createWorkerFrom(n, t, e) },
                                _decompressWorker: function() { return this._data instanceof s ? this._data.getContentWorker() : this._data instanceof l ? this._data : new r(this._data) }
                            };
                            for (var c = ["asText", "asBinary", "asNodeBuffer", "asUint8Array", "asArrayBuffer"], u = function() { throw new Error("This method has been removed in JSZip 3.0, please check the upgrade guide.") }, h = 0; h < c.length; h++) i.prototype[c[h]] = u;
                            e.exports = i
                        }, { "./compressedObject": 2, "./stream/DataWorker": 27, "./stream/GenericWorker": 28, "./stream/StreamHelper": 29, "./utf8": 31 }],
                        36: [function(t, u, e) {
                            (function(e) {
                                var n, i, t = e.MutationObserver || e.WebKitMutationObserver;
                                if (t) {
                                    var r = 0,
                                        a = new t(c),
                                        o = e.document.createTextNode("");
                                    a.observe(o, { characterData: !0 }), n = function() { o.data = r = ++r % 2 }
                                } else if (e.setImmediate || void 0 === e.MessageChannel) n = "document" in e && "onreadystatechange" in e.document.createElement("script") ? function() {
                                    var t = e.document.createElement("script");
                                    t.onreadystatechange = function() { c(), t.onreadystatechange = null, t.parentNode.removeChild(t), t = null }, e.document.documentElement.appendChild(t)
                                } : function() { setTimeout(c, 0) };
                                else {
                                    var s = new e.MessageChannel;
                                    s.port1.onmessage = c, n = function() { s.port2.postMessage(0) }
                                }
                                var l = [];

                                function c() {
                                    var t, e;
                                    i = !0;
                                    for (var n = l.length; n;) {
                                        for (e = l, l = [], t = -1; ++t < n;) e[t]();
                                        n = l.length
                                    }
                                    i = !1
                                }
                                u.exports = function(t) { 1 !== l.push(t) || i || n() }
                            }).call(this, void 0 !== n ? n : "undefined" != typeof self ? self : "undefined" != typeof window ? window : {})
                        }, {}],
                        37: [function(t, e, n) {
                            var r = t("immediate");

                            function c() {}
                            var u = {},
                                a = ["REJECTED"],
                                o = ["FULFILLED"],
                                i = ["PENDING"];

                            function s(t) {
                                if ("function" != typeof t) throw new TypeError("resolver must be a function");
                                this.state = i, this.queue = [], this.outcome = void 0, t !== c && f(this, t)
                            }

                            function l(t, e, n) { this.promise = t, "function" == typeof e && (this.onFulfilled = e, this.callFulfilled = this.otherCallFulfilled), "function" == typeof n && (this.onRejected = n, this.callRejected = this.otherCallRejected) }

                            function h(e, n, i) {
                                r(function() {
                                    var t;
                                    try { t = n(i) } catch (t) { return u.reject(e, t) }
                                    t === e ? u.reject(e, new TypeError("Cannot resolve promise with itself")) : u.resolve(e, t)
                                })
                            }

                            function d(t) { var e = t && t.then; if (t && ("object" === p()(t) || "function" == typeof t) && "function" == typeof e) return function() { e.apply(t, arguments) } }

                            function f(e, t) {
                                var n = !1;

                                function i(t) { n || (n = !0, u.reject(e, t)) }

                                function r(t) { n || (n = !0, u.resolve(e, t)) }
                                var a = m(function() { t(r, i) });
                                "error" === a.status && i(a.value)
                            }

                            function m(t, e) { var n = {}; try { n.value = t(e), n.status = "success" } catch (t) { n.status = "error", n.value = t } return n }(e.exports = s).prototype.finally = function(e) { if ("function" != typeof e) return this; var n = this.constructor; return this.then(function(t) { return n.resolve(e()).then(function() { return t }) }, function(t) { return n.resolve(e()).then(function() { throw t }) }) }, s.prototype.catch = function(t) { return this.then(null, t) }, s.prototype.then = function(t, e) {
                                if ("function" != typeof t && this.state === o || "function" != typeof e && this.state === a) return this;
                                var n = new this.constructor(c);
                                this.state !== i ? h(n, this.state === o ? t : e, this.outcome) : this.queue.push(new l(n, t, e));
                                return n
                            }, l.prototype.callFulfilled = function(t) { u.resolve(this.promise, t) }, l.prototype.otherCallFulfilled = function(t) { h(this.promise, this.onFulfilled, t) }, l.prototype.callRejected = function(t) { u.reject(this.promise, t) }, l.prototype.otherCallRejected = function(t) { h(this.promise, this.onRejected, t) }, u.resolve = function(t, e) {
                                var n = m(d, e);
                                if ("error" === n.status) return u.reject(t, n.value);
                                var i = n.value;
                                if (i) f(t, i);
                                else { t.state = o, t.outcome = e; for (var r = -1, a = t.queue.length; ++r < a;) t.queue[r].callFulfilled(e) }
                                return t
                            }, u.reject = function(t, e) { t.state = a, t.outcome = e; for (var n = -1, i = t.queue.length; ++n < i;) t.queue[n].callRejected(e); return t }, s.resolve = function(t) { if (t instanceof this) return t; return u.resolve(new this(c), t) }, s.reject = function(t) { var e = new this(c); return u.reject(e, t) }, s.all = function(t) {
                                var n = this;
                                if ("[object Array]" !== Object.prototype.toString.call(t)) return this.reject(new TypeError("must be an array"));
                                var i = t.length,
                                    r = !1;
                                if (!i) return this.resolve([]);
                                var a = new Array(i),
                                    o = 0,
                                    e = -1,
                                    s = new this(c);
                                for (; ++e < i;) l(t[e], e);
                                return s;

                                function l(t, e) { n.resolve(t).then(function(t) { a[e] = t, ++o !== i || r || (r = !0, u.resolve(s, a)) }, function(t) { r || (r = !0, u.reject(s, t)) }) }
                            }, s.race = function(t) {
                                var e = this;
                                if ("[object Array]" !== Object.prototype.toString.call(t)) return this.reject(new TypeError("must be an array"));
                                var n = t.length,
                                    i = !1;
                                if (!n) return this.resolve([]);
                                var r = -1,
                                    a = new this(c);
                                for (; ++r < n;) o = t[r], e.resolve(o).then(function(t) { i || (i = !0, u.resolve(a, t)) }, function(t) { i || (i = !0, u.reject(a, t)) });
                                var o;
                                return a
                            }
                        }, { immediate: 36 }],
                        38: [function(t, e, n) {
                            var i = {};
                            (0, t("./lib/utils/common").assign)(i, t("./lib/deflate"), t("./lib/inflate"), t("./lib/zlib/constants")), e.exports = i
                        }, { "./lib/deflate": 39, "./lib/inflate": 40, "./lib/utils/common": 41, "./lib/zlib/constants": 44 }],
                        39: [function(t, e, n) {
                            var o = t("./zlib/deflate"),
                                s = t("./utils/common"),
                                l = t("./utils/strings"),
                                r = t("./zlib/messages"),
                                a = t("./zlib/zstream"),
                                c = Object.prototype.toString,
                                u = 0,
                                h = -1,
                                d = 0,
                                f = 8;

                            function m(t) {
                                if (!(this instanceof m)) return new m(t);
                                this.options = s.assign({ level: h, method: f, chunkSize: 16384, windowBits: 15, memLevel: 8, strategy: d, to: "" }, t || {});
                                var e = this.options;
                                e.raw && 0 < e.windowBits ? e.windowBits = -e.windowBits : e.gzip && 0 < e.windowBits && e.windowBits < 16 && (e.windowBits += 16), this.err = 0, this.msg = "", this.ended = !1, this.chunks = [], this.strm = new a, this.strm.avail_out = 0;
                                var n = o.deflateInit2(this.strm, e.level, e.method, e.windowBits, e.memLevel, e.strategy);
                                if (n !== u) throw new Error(r[n]);
                                if (e.header && o.deflateSetHeader(this.strm, e.header), e.dictionary) {
                                    var i;
                                    if (i = "string" == typeof e.dictionary ? l.string2buf(e.dictionary) : "[object ArrayBuffer]" === c.call(e.dictionary) ? new Uint8Array(e.dictionary) : e.dictionary, (n = o.deflateSetDictionary(this.strm, i)) !== u) throw new Error(r[n]);
                                    this._dict_set = !0
                                }
                            }

                            function i(t, e) { var n = new m(e); if (n.push(t, !0), n.err) throw n.msg || r[n.err]; return n.result }
                            m.prototype.push = function(t, e) {
                                var n, i, r = this.strm,
                                    a = this.options.chunkSize;
                                if (this.ended) return !1;
                                i = e === ~~e ? e : !0 === e ? 4 : 0, "string" == typeof t ? r.input = l.string2buf(t) : "[object ArrayBuffer]" === c.call(t) ? r.input = new Uint8Array(t) : r.input = t, r.next_in = 0, r.avail_in = r.input.length;
                                do {
                                    if (0 === r.avail_out && (r.output = new s.Buf8(a), r.next_out = 0, r.avail_out = a), 1 !== (n = o.deflate(r, i)) && n !== u) return this.onEnd(n), !(this.ended = !0);
                                    0 !== r.avail_out && (0 !== r.avail_in || 4 !== i && 2 !== i) || ("string" === this.options.to ? this.onData(l.buf2binstring(s.shrinkBuf(r.output, r.next_out))) : this.onData(s.shrinkBuf(r.output, r.next_out)))
                                } while ((0 < r.avail_in || 0 === r.avail_out) && 1 !== n);
                                return 4 === i ? (n = o.deflateEnd(this.strm), this.onEnd(n), this.ended = !0, n === u) : 2 !== i || (this.onEnd(u), !(r.avail_out = 0))
                            }, m.prototype.onData = function(t) { this.chunks.push(t) }, m.prototype.onEnd = function(t) { t === u && ("string" === this.options.to ? this.result = this.chunks.join("") : this.result = s.flattenChunks(this.chunks)), this.chunks = [], this.err = t, this.msg = this.strm.msg }, n.Deflate = m, n.deflate = i, n.deflateRaw = function(t, e) { return (e = e || {}).raw = !0, i(t, e) }, n.gzip = function(t, e) { return (e = e || {}).gzip = !0, i(t, e) }
                        }, { "./utils/common": 41, "./utils/strings": 42, "./zlib/deflate": 46, "./zlib/messages": 51, "./zlib/zstream": 53 }],
                        40: [function(t, e, n) {
                            var d = t("./zlib/inflate"),
                                f = t("./utils/common"),
                                m = t("./utils/strings"),
                                p = t("./zlib/constants"),
                                i = t("./zlib/messages"),
                                r = t("./zlib/zstream"),
                                a = t("./zlib/gzheader"),
                                v = Object.prototype.toString;

                            function o(t) {
                                if (!(this instanceof o)) return new o(t);
                                this.options = f.assign({ chunkSize: 16384, windowBits: 0, to: "" }, t || {});
                                var e = this.options;
                                e.raw && 0 <= e.windowBits && e.windowBits < 16 && (e.windowBits = -e.windowBits, 0 === e.windowBits && (e.windowBits = -15)), !(0 <= e.windowBits && e.windowBits < 16) || t && t.windowBits || (e.windowBits += 32), 15 < e.windowBits && e.windowBits < 48 && 0 == (15 & e.windowBits) && (e.windowBits |= 15), this.err = 0, this.msg = "", this.ended = !1, this.chunks = [], this.strm = new r, this.strm.avail_out = 0;
                                var n = d.inflateInit2(this.strm, e.windowBits);
                                if (n !== p.Z_OK) throw new Error(i[n]);
                                this.header = new a, d.inflateGetHeader(this.strm, this.header)
                            }

                            function s(t, e) { var n = new o(e); if (n.push(t, !0), n.err) throw n.msg || i[n.err]; return n.result }
                            o.prototype.push = function(t, e) {
                                var n, i, r, a, o, s, l = this.strm,
                                    c = this.options.chunkSize,
                                    u = this.options.dictionary,
                                    h = !1;
                                if (this.ended) return !1;
                                i = e === ~~e ? e : !0 === e ? p.Z_FINISH : p.Z_NO_FLUSH, "string" == typeof t ? l.input = m.binstring2buf(t) : "[object ArrayBuffer]" === v.call(t) ? l.input = new Uint8Array(t) : l.input = t, l.next_in = 0, l.avail_in = l.input.length;
                                do {
                                    if (0 === l.avail_out && (l.output = new f.Buf8(c), l.next_out = 0, l.avail_out = c), (n = d.inflate(l, p.Z_NO_FLUSH)) === p.Z_NEED_DICT && u && (s = "string" == typeof u ? m.string2buf(u) : "[object ArrayBuffer]" === v.call(u) ? new Uint8Array(u) : u, n = d.inflateSetDictionary(this.strm, s)), n === p.Z_BUF_ERROR && !0 === h && (n = p.Z_OK, h = !1), n !== p.Z_STREAM_END && n !== p.Z_OK) return this.onEnd(n), !(this.ended = !0);
                                    l.next_out && (0 !== l.avail_out && n !== p.Z_STREAM_END && (0 !== l.avail_in || i !== p.Z_FINISH && i !== p.Z_SYNC_FLUSH) || ("string" === this.options.to ? (r = m.utf8border(l.output, l.next_out), a = l.next_out - r, o = m.buf2string(l.output, r), l.next_out = a, l.avail_out = c - a, a && f.arraySet(l.output, l.output, r, a, 0), this.onData(o)) : this.onData(f.shrinkBuf(l.output, l.next_out)))), 0 === l.avail_in && 0 === l.avail_out && (h = !0)
                                } while ((0 < l.avail_in || 0 === l.avail_out) && n !== p.Z_STREAM_END);
                                return n === p.Z_STREAM_END && (i = p.Z_FINISH), i === p.Z_FINISH ? (n = d.inflateEnd(this.strm), this.onEnd(n), this.ended = !0, n === p.Z_OK) : i !== p.Z_SYNC_FLUSH || (this.onEnd(p.Z_OK), !(l.avail_out = 0))
                            }, o.prototype.onData = function(t) { this.chunks.push(t) }, o.prototype.onEnd = function(t) { t === p.Z_OK && ("string" === this.options.to ? this.result = this.chunks.join("") : this.result = f.flattenChunks(this.chunks)), this.chunks = [], this.err = t, this.msg = this.strm.msg }, n.Inflate = o, n.inflate = s, n.inflateRaw = function(t, e) { return (e = e || {}).raw = !0, s(t, e) }, n.ungzip = s
                        }, { "./utils/common": 41, "./utils/strings": 42, "./zlib/constants": 44, "./zlib/gzheader": 47, "./zlib/inflate": 49, "./zlib/messages": 51, "./zlib/zstream": 53 }],
                        41: [function(t, e, n) {
                            var i = "undefined" != typeof Uint8Array && "undefined" != typeof Uint16Array && "undefined" != typeof Int32Array;
                            n.assign = function(t) { for (var e = Array.prototype.slice.call(arguments, 1); e.length;) { var n = e.shift(); if (n) { if ("object" !== p()(n)) throw new TypeError(n + "must be non-object"); for (var i in n) n.hasOwnProperty(i) && (t[i] = n[i]) } } return t }, n.shrinkBuf = function(t, e) { return t.length === e ? t : t.subarray ? t.subarray(0, e) : (t.length = e, t) };
                            var r = {
                                    arraySet: function(t, e, n, i, r) {
                                        if (e.subarray && t.subarray) t.set(e.subarray(n, n + i), r);
                                        else
                                            for (var a = 0; a < i; a++) t[r + a] = e[n + a]
                                    },
                                    flattenChunks: function(t) { var e, n, i, r, a, o; for (e = i = 0, n = t.length; e < n; e++) i += t[e].length; for (o = new Uint8Array(i), e = r = 0, n = t.length; e < n; e++) a = t[e], o.set(a, r), r += a.length; return o }
                                },
                                a = { arraySet: function(t, e, n, i, r) { for (var a = 0; a < i; a++) t[r + a] = e[n + a] }, flattenChunks: function(t) { return [].concat.apply([], t) } };
                            n.setTyped = function(t) { t ? (n.Buf8 = Uint8Array, n.Buf16 = Uint16Array, n.Buf32 = Int32Array, n.assign(n, r)) : (n.Buf8 = Array, n.Buf16 = Array, n.Buf32 = Array, n.assign(n, a)) }, n.setTyped(i)
                        }, {}],
                        42: [function(t, e, n) {
                            var l = t("./common"),
                                r = !0,
                                a = !0;
                            try { String.fromCharCode.apply(null, [0]) } catch (t) { r = !1 }
                            try { String.fromCharCode.apply(null, new Uint8Array(1)) } catch (t) { a = !1 }
                            for (var c = new l.Buf8(256), i = 0; i < 256; i++) c[i] = 252 <= i ? 6 : 248 <= i ? 5 : 240 <= i ? 4 : 224 <= i ? 3 : 192 <= i ? 2 : 1;

                            function u(t, e) { if (e < 65537 && (t.subarray && a || !t.subarray && r)) return String.fromCharCode.apply(null, l.shrinkBuf(t, e)); for (var n = "", i = 0; i < e; i++) n += String.fromCharCode(t[i]); return n }
                            c[254] = c[254] = 1, n.string2buf = function(t) {
                                var e, n, i, r, a, o = t.length,
                                    s = 0;
                                for (r = 0; r < o; r++) 55296 == (64512 & (n = t.charCodeAt(r))) && r + 1 < o && 56320 == (64512 & (i = t.charCodeAt(r + 1))) && (n = 65536 + (n - 55296 << 10) + (i - 56320), r++), s += n < 128 ? 1 : n < 2048 ? 2 : n < 65536 ? 3 : 4;
                                for (e = new l.Buf8(s), r = a = 0; a < s; r++) 55296 == (64512 & (n = t.charCodeAt(r))) && r + 1 < o && 56320 == (64512 & (i = t.charCodeAt(r + 1))) && (n = 65536 + (n - 55296 << 10) + (i - 56320), r++), n < 128 ? e[a++] = n : (n < 2048 ? e[a++] = 192 | n >>> 6 : (n < 65536 ? e[a++] = 224 | n >>> 12 : (e[a++] = 240 | n >>> 18, e[a++] = 128 | n >>> 12 & 63), e[a++] = 128 | n >>> 6 & 63), e[a++] = 128 | 63 & n);
                                return e
                            }, n.buf2binstring = function(t) { return u(t, t.length) }, n.binstring2buf = function(t) { for (var e = new l.Buf8(t.length), n = 0, i = e.length; n < i; n++) e[n] = t.charCodeAt(n); return e }, n.buf2string = function(t, e) {
                                var n, i, r, a, o = e || t.length,
                                    s = new Array(2 * o);
                                for (n = i = 0; n < o;)
                                    if ((r = t[n++]) < 128) s[i++] = r;
                                    else if (4 < (a = c[r])) s[i++] = 65533, n += a - 1;
                                else {
                                    for (r &= 2 === a ? 31 : 3 === a ? 15 : 7; 1 < a && n < o;) r = r << 6 | 63 & t[n++], a--;
                                    1 < a ? s[i++] = 65533 : r < 65536 ? s[i++] = r : (r -= 65536, s[i++] = 55296 | r >> 10 & 1023, s[i++] = 56320 | 1023 & r)
                                }
                                return u(s, i)
                            }, n.utf8border = function(t, e) { var n; for ((e = e || t.length) > t.length && (e = t.length), n = e - 1; 0 <= n && 128 == (192 & t[n]);) n--; return !(n < 0) && 0 !== n && n + c[t[n]] > e ? n : e }
                        }, { "./common": 41 }],
                        43: [function(t, e, n) {
                            e.exports = function(t, e, n, i) {
                                for (var r = 65535 & t | 0, a = t >>> 16 & 65535 | 0, o = 0; 0 !== n;) {
                                    for (n -= o = 2e3 < n ? 2e3 : n; a = a + (r = r + e[i++] | 0) | 0, --o;);
                                    r %= 65521, a %= 65521
                                }
                                return r | a << 16 | 0
                            }
                        }, {}],
                        44: [function(t, e, n) { e.exports = { Z_NO_FLUSH: 0, Z_PARTIAL_FLUSH: 1, Z_SYNC_FLUSH: 2, Z_FULL_FLUSH: 3, Z_FINISH: 4, Z_BLOCK: 5, Z_TREES: 6, Z_OK: 0, Z_STREAM_END: 1, Z_NEED_DICT: 2, Z_ERRNO: -1, Z_STREAM_ERROR: -2, Z_DATA_ERROR: -3, Z_BUF_ERROR: -5, Z_NO_COMPRESSION: 0, Z_BEST_SPEED: 1, Z_BEST_COMPRESSION: 9, Z_DEFAULT_COMPRESSION: -1, Z_FILTERED: 1, Z_HUFFMAN_ONLY: 2, Z_RLE: 3, Z_FIXED: 4, Z_DEFAULT_STRATEGY: 0, Z_BINARY: 0, Z_TEXT: 1, Z_UNKNOWN: 2, Z_DEFLATED: 8 } }, {}],
                        45: [function(t, e, n) {
                            var s = function() {
                                for (var t, e = [], n = 0; n < 256; n++) {
                                    t = n;
                                    for (var i = 0; i < 8; i++) t = 1 & t ? 3988292384 ^ t >>> 1 : t >>> 1;
                                    e[n] = t
                                }
                                return e
                            }();
                            e.exports = function(t, e, n, i) {
                                var r = s,
                                    a = i + n;
                                t ^= -1;
                                for (var o = i; o < a; o++) t = t >>> 8 ^ r[255 & (t ^ e[o])];
                                return -1 ^ t
                            }
                        }, {}],
                        46: [function(t, e, n) {
                            var l, d = t("../utils/common"),
                                c = t("./trees"),
                                f = t("./adler32"),
                                m = t("./crc32"),
                                i = t("./messages"),
                                u = 0,
                                h = 4,
                                p = 0,
                                v = -2,
                                _ = -1,
                                g = 4,
                                r = 2,
                                w = 8,
                                b = 9,
                                a = 286,
                                o = 30,
                                s = 19,
                                y = 2 * a + 1,
                                k = 15,
                                I = 3,
                                x = 258,
                                A = x + I + 1,
                                E = 42,
                                S = 113,
                                F = 1,
                                R = 2,
                                M = 3,
                                C = 4;

                            function P(t, e) { return t.msg = i[e], e }

                            function D(t) { return (t << 1) - (4 < t ? 9 : 0) }

                            function T(t) { for (var e = t.length; 0 <= --e;) t[e] = 0 }

                            function z(t) {
                                var e = t.state,
                                    n = e.pending;
                                n > t.avail_out && (n = t.avail_out), 0 !== n && (d.arraySet(t.output, e.pending_buf, e.pending_out, n, t.next_out), t.next_out += n, e.pending_out += n, t.total_out += n, t.avail_out -= n, e.pending -= n, 0 === e.pending && (e.pending_out = 0))
                            }

                            function L(t, e) { c._tr_flush_block(t, 0 <= t.block_start ? t.block_start : -1, t.strstart - t.block_start, e), t.block_start = t.strstart, z(t.strm) }

                            function B(t, e) { t.pending_buf[t.pending++] = e }

                            function O(t, e) { t.pending_buf[t.pending++] = e >>> 8 & 255, t.pending_buf[t.pending++] = 255 & e }

                            function W(t, e) {
                                var n, i, r = t.max_chain_length,
                                    a = t.strstart,
                                    o = t.prev_length,
                                    s = t.nice_match,
                                    l = t.strstart > t.w_size - A ? t.strstart - (t.w_size - A) : 0,
                                    c = t.window,
                                    u = t.w_mask,
                                    h = t.prev,
                                    d = t.strstart + x,
                                    f = c[a + o - 1],
                                    m = c[a + o];
                                t.prev_length >= t.good_match && (r >>= 2), s > t.lookahead && (s = t.lookahead);
                                do {
                                    if (c[(n = e) + o] === m && c[n + o - 1] === f && c[n] === c[a] && c[++n] === c[a + 1]) {
                                        a += 2, n++;
                                        do {} while (c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && c[++a] === c[++n] && a < d);
                                        if (i = x - (d - a), a = d - x, o < i) {
                                            if (t.match_start = e, s <= (o = i)) break;
                                            f = c[a + o - 1], m = c[a + o]
                                        }
                                    }
                                } while ((e = h[e & u]) > l && 0 != --r);
                                return o <= t.lookahead ? o : t.lookahead
                            }

                            function H(t) {
                                var e, n, i, r, a, o, s, l, c, u, h = t.w_size;
                                do {
                                    if (r = t.window_size - t.lookahead - t.strstart, t.strstart >= h + (h - A)) {
                                        for (d.arraySet(t.window, t.window, h, h, 0), t.match_start -= h, t.strstart -= h, t.block_start -= h, e = n = t.hash_size; i = t.head[--e], t.head[e] = h <= i ? i - h : 0, --n;);
                                        for (e = n = h; i = t.prev[--e], t.prev[e] = h <= i ? i - h : 0, --n;);
                                        r += h
                                    }
                                    if (0 === t.strm.avail_in) break;
                                    if (o = t.strm, s = t.window, l = t.strstart + t.lookahead, c = r, u = void 0, u = o.avail_in, c < u && (u = c), n = 0 === u ? 0 : (o.avail_in -= u, d.arraySet(s, o.input, o.next_in, u, l), 1 === o.state.wrap ? o.adler = f(o.adler, s, u, l) : 2 === o.state.wrap && (o.adler = m(o.adler, s, u, l)), o.next_in += u, o.total_in += u, u), t.lookahead += n, t.lookahead + t.insert >= I)
                                        for (a = t.strstart - t.insert, t.ins_h = t.window[a], t.ins_h = (t.ins_h << t.hash_shift ^ t.window[a + 1]) & t.hash_mask; t.insert && (t.ins_h = (t.ins_h << t.hash_shift ^ t.window[a + I - 1]) & t.hash_mask, t.prev[a & t.w_mask] = t.head[t.ins_h], t.head[t.ins_h] = a, a++, t.insert--, !(t.lookahead + t.insert < I)););
                                } while (t.lookahead < A && 0 !== t.strm.avail_in)
                            }

                            function U(t, e) {
                                for (var n, i;;) {
                                    if (t.lookahead < A) { if (H(t), t.lookahead < A && e === u) return F; if (0 === t.lookahead) break }
                                    if (n = 0, t.lookahead >= I && (t.ins_h = (t.ins_h << t.hash_shift ^ t.window[t.strstart + I - 1]) & t.hash_mask, n = t.prev[t.strstart & t.w_mask] = t.head[t.ins_h], t.head[t.ins_h] = t.strstart), 0 !== n && t.strstart - n <= t.w_size - A && (t.match_length = W(t, n)), t.match_length >= I)
                                        if (i = c._tr_tally(t, t.strstart - t.match_start, t.match_length - I), t.lookahead -= t.match_length, t.match_length <= t.max_lazy_match && t.lookahead >= I) {
                                            for (t.match_length--; t.strstart++, t.ins_h = (t.ins_h << t.hash_shift ^ t.window[t.strstart + I - 1]) & t.hash_mask, n = t.prev[t.strstart & t.w_mask] = t.head[t.ins_h], t.head[t.ins_h] = t.strstart, 0 != --t.match_length;);
                                            t.strstart++
                                        } else t.strstart += t.match_length, t.match_length = 0, t.ins_h = t.window[t.strstart], t.ins_h = (t.ins_h << t.hash_shift ^ t.window[t.strstart + 1]) & t.hash_mask;
                                    else i = c._tr_tally(t, 0, t.window[t.strstart]), t.lookahead--, t.strstart++;
                                    if (i && (L(t, !1), 0 === t.strm.avail_out)) return F
                                }
                                return t.insert = t.strstart < I - 1 ? t.strstart : I - 1, e === h ? (L(t, !0), 0 === t.strm.avail_out ? M : C) : t.last_lit && (L(t, !1), 0 === t.strm.avail_out) ? F : R
                            }

                            function j(t, e) { for (var n, i, r;;) { if (t.lookahead < A) { if (H(t), t.lookahead < A && e === u) return F; if (0 === t.lookahead) break } if (n = 0, t.lookahead >= I && (t.ins_h = (t.ins_h << t.hash_shift ^ t.window[t.strstart + I - 1]) & t.hash_mask, n = t.prev[t.strstart & t.w_mask] = t.head[t.ins_h], t.head[t.ins_h] = t.strstart), t.prev_length = t.match_length, t.prev_match = t.match_start, t.match_length = I - 1, 0 !== n && t.prev_length < t.max_lazy_match && t.strstart - n <= t.w_size - A && (t.match_length = W(t, n), t.match_length <= 5 && (1 === t.strategy || t.match_length === I && 4096 < t.strstart - t.match_start) && (t.match_length = I - 1)), t.prev_length >= I && t.match_length <= t.prev_length) { for (r = t.strstart + t.lookahead - I, i = c._tr_tally(t, t.strstart - 1 - t.prev_match, t.prev_length - I), t.lookahead -= t.prev_length - 1, t.prev_length -= 2; ++t.strstart <= r && (t.ins_h = (t.ins_h << t.hash_shift ^ t.window[t.strstart + I - 1]) & t.hash_mask, n = t.prev[t.strstart & t.w_mask] = t.head[t.ins_h], t.head[t.ins_h] = t.strstart), 0 != --t.prev_length;); if (t.match_available = 0, t.match_length = I - 1, t.strstart++, i && (L(t, !1), 0 === t.strm.avail_out)) return F } else if (t.match_available) { if ((i = c._tr_tally(t, 0, t.window[t.strstart - 1])) && L(t, !1), t.strstart++, t.lookahead--, 0 === t.strm.avail_out) return F } else t.match_available = 1, t.strstart++, t.lookahead-- } return t.match_available && (i = c._tr_tally(t, 0, t.window[t.strstart - 1]), t.match_available = 0), t.insert = t.strstart < I - 1 ? t.strstart : I - 1, e === h ? (L(t, !0), 0 === t.strm.avail_out ? M : C) : t.last_lit && (L(t, !1), 0 === t.strm.avail_out) ? F : R }

                            function N(t, e, n, i, r) { this.good_length = t, this.max_lazy = e, this.nice_length = n, this.max_chain = i, this.func = r }

                            function K() { this.strm = null, this.status = 0, this.pending_buf = null, this.pending_buf_size = 0, this.pending_out = 0, this.pending = 0, this.wrap = 0, this.gzhead = null, this.gzindex = 0, this.method = w, this.last_flush = -1, this.w_size = 0, this.w_bits = 0, this.w_mask = 0, this.window = null, this.window_size = 0, this.prev = null, this.head = null, this.ins_h = 0, this.hash_size = 0, this.hash_bits = 0, this.hash_mask = 0, this.hash_shift = 0, this.block_start = 0, this.match_length = 0, this.prev_match = 0, this.match_available = 0, this.strstart = 0, this.match_start = 0, this.lookahead = 0, this.prev_length = 0, this.max_chain_length = 0, this.max_lazy_match = 0, this.level = 0, this.strategy = 0, this.good_match = 0, this.nice_match = 0, this.dyn_ltree = new d.Buf16(2 * y), this.dyn_dtree = new d.Buf16(2 * (2 * o + 1)), this.bl_tree = new d.Buf16(2 * (2 * s + 1)), T(this.dyn_ltree), T(this.dyn_dtree), T(this.bl_tree), this.l_desc = null, this.d_desc = null, this.bl_desc = null, this.bl_count = new d.Buf16(k + 1), this.heap = new d.Buf16(2 * a + 1), T(this.heap), this.heap_len = 0, this.heap_max = 0, this.depth = new d.Buf16(2 * a + 1), T(this.depth), this.l_buf = 0, this.lit_bufsize = 0, this.last_lit = 0, this.d_buf = 0, this.opt_len = 0, this.static_len = 0, this.matches = 0, this.insert = 0, this.bi_buf = 0, this.bi_valid = 0 }

                            function Z(t) { var e; return t && t.state ? (t.total_in = t.total_out = 0, t.data_type = r, (e = t.state).pending = 0, e.pending_out = 0, e.wrap < 0 && (e.wrap = -e.wrap), e.status = e.wrap ? E : S, t.adler = 2 === e.wrap ? 0 : 1, e.last_flush = u, c._tr_init(e), p) : P(t, v) }

                            function G(t) { var e, n = Z(t); return n === p && ((e = t.state).window_size = 2 * e.w_size, T(e.head), e.max_lazy_match = l[e.level].max_lazy, e.good_match = l[e.level].good_length, e.nice_match = l[e.level].nice_length, e.max_chain_length = l[e.level].max_chain, e.strstart = 0, e.block_start = 0, e.lookahead = 0, e.insert = 0, e.match_length = e.prev_length = I - 1, e.match_available = 0, e.ins_h = 0), n }

                            function Y(t, e, n, i, r, a) {
                                if (!t) return v;
                                var o = 1;
                                if (e === _ && (e = 6), i < 0 ? (o = 0, i = -i) : 15 < i && (o = 2, i -= 16), r < 1 || b < r || n !== w || i < 8 || 15 < i || e < 0 || 9 < e || a < 0 || g < a) return P(t, v);
                                8 === i && (i = 9);
                                var s = new K;
                                return (t.state = s).strm = t, s.wrap = o, s.gzhead = null, s.w_bits = i, s.w_size = 1 << s.w_bits, s.w_mask = s.w_size - 1, s.hash_bits = r + 7, s.hash_size = 1 << s.hash_bits, s.hash_mask = s.hash_size - 1, s.hash_shift = ~~((s.hash_bits + I - 1) / I), s.window = new d.Buf8(2 * s.w_size), s.head = new d.Buf16(s.hash_size), s.prev = new d.Buf16(s.w_size), s.lit_bufsize = 1 << r + 6, s.pending_buf_size = 4 * s.lit_bufsize, s.pending_buf = new d.Buf8(s.pending_buf_size), s.d_buf = +s.lit_bufsize, s.l_buf = 3 * s.lit_bufsize, s.level = e, s.strategy = a, s.method = n, G(t)
                            }
                            l = [new N(0, 0, 0, 0, function(t, e) {
                                var n = 65535;
                                for (n > t.pending_buf_size - 5 && (n = t.pending_buf_size - 5);;) {
                                    if (t.lookahead <= 1) { if (H(t), 0 === t.lookahead && e === u) return F; if (0 === t.lookahead) break }
                                    t.strstart += t.lookahead, t.lookahead = 0;
                                    var i = t.block_start + n;
                                    if ((0 === t.strstart || t.strstart >= i) && (t.lookahead = t.strstart - i, t.strstart = i, L(t, !1), 0 === t.strm.avail_out)) return F;
                                    if (t.strstart - t.block_start >= t.w_size - A && (L(t, !1), 0 === t.strm.avail_out)) return F
                                }
                                return t.insert = 0, e === h ? (L(t, !0), 0 === t.strm.avail_out ? M : C) : (t.strstart > t.block_start && (L(t, !1), t.strm.avail_out), F)
                            }), new N(4, 4, 8, 4, U), new N(4, 5, 16, 8, U), new N(4, 6, 32, 32, U), new N(4, 4, 16, 16, j), new N(8, 16, 32, 32, j), new N(8, 16, 128, 128, j), new N(8, 32, 128, 256, j), new N(32, 128, 258, 1024, j), new N(32, 258, 258, 4096, j)], n.deflateInit = function(t, e) { return Y(t, e, w, 15, 8, 0) }, n.deflateInit2 = Y, n.deflateReset = G, n.deflateResetKeep = Z, n.deflateSetHeader = function(t, e) { return !t || !t.state || 2 !== t.state.wrap ? v : (t.state.gzhead = e, p) }, n.deflate = function(t, e) {
                                var n, i, r, a;
                                if (!t || !t.state || 5 < e || e < 0) return t ? P(t, v) : v;
                                if (i = t.state, !t.output || !t.input && 0 !== t.avail_in || 666 === i.status && e !== h) return P(t, 0 === t.avail_out ? -5 : v);
                                if (i.strm = t, n = i.last_flush, i.last_flush = e, i.status === E)
                                    if (2 === i.wrap) t.adler = 0, B(i, 31), B(i, 139), B(i, 8), i.gzhead ? (B(i, (i.gzhead.text ? 1 : 0) + (i.gzhead.hcrc ? 2 : 0) + (i.gzhead.extra ? 4 : 0) + (i.gzhead.name ? 8 : 0) + (i.gzhead.comment ? 16 : 0)), B(i, 255 & i.gzhead.time), B(i, i.gzhead.time >> 8 & 255), B(i, i.gzhead.time >> 16 & 255), B(i, i.gzhead.time >> 24 & 255), B(i, 9 === i.level ? 2 : 2 <= i.strategy || i.level < 2 ? 4 : 0), B(i, 255 & i.gzhead.os), i.gzhead.extra && i.gzhead.extra.length && (B(i, 255 & i.gzhead.extra.length), B(i, i.gzhead.extra.length >> 8 & 255)), i.gzhead.hcrc && (t.adler = m(t.adler, i.pending_buf, i.pending, 0)), i.gzindex = 0, i.status = 69) : (B(i, 0), B(i, 0), B(i, 0), B(i, 0), B(i, 0), B(i, 9 === i.level ? 2 : 2 <= i.strategy || i.level < 2 ? 4 : 0), B(i, 3), i.status = S);
                                    else {
                                        var o = w + (i.w_bits - 8 << 4) << 8;
                                        o |= (2 <= i.strategy || i.level < 2 ? 0 : i.level < 6 ? 1 : 6 === i.level ? 2 : 3) << 6, 0 !== i.strstart && (o |= 32), o += 31 - o % 31, i.status = S, O(i, o), 0 !== i.strstart && (O(i, t.adler >>> 16), O(i, 65535 & t.adler)), t.adler = 1
                                    }
                                if (69 === i.status)
                                    if (i.gzhead.extra) {
                                        for (r = i.pending; i.gzindex < (65535 & i.gzhead.extra.length) && (i.pending !== i.pending_buf_size || (i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), z(t), r = i.pending, i.pending !== i.pending_buf_size));) B(i, 255 & i.gzhead.extra[i.gzindex]), i.gzindex++;
                                        i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), i.gzindex === i.gzhead.extra.length && (i.gzindex = 0, i.status = 73)
                                    } else i.status = 73;
                                if (73 === i.status)
                                    if (i.gzhead.name) {
                                        r = i.pending;
                                        do {
                                            if (i.pending === i.pending_buf_size && (i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), z(t), r = i.pending, i.pending === i.pending_buf_size)) { a = 1; break }
                                            a = i.gzindex < i.gzhead.name.length ? 255 & i.gzhead.name.charCodeAt(i.gzindex++) : 0, B(i, a)
                                        } while (0 !== a);
                                        i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), 0 === a && (i.gzindex = 0, i.status = 91)
                                    } else i.status = 91;
                                if (91 === i.status)
                                    if (i.gzhead.comment) {
                                        r = i.pending;
                                        do {
                                            if (i.pending === i.pending_buf_size && (i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), z(t), r = i.pending, i.pending === i.pending_buf_size)) { a = 1; break }
                                            a = i.gzindex < i.gzhead.comment.length ? 255 & i.gzhead.comment.charCodeAt(i.gzindex++) : 0, B(i, a)
                                        } while (0 !== a);
                                        i.gzhead.hcrc && i.pending > r && (t.adler = m(t.adler, i.pending_buf, i.pending - r, r)), 0 === a && (i.status = 103)
                                    } else i.status = 103;
                                if (103 === i.status && (i.gzhead.hcrc ? (i.pending + 2 > i.pending_buf_size && z(t), i.pending + 2 <= i.pending_buf_size && (B(i, 255 & t.adler), B(i, t.adler >> 8 & 255), t.adler = 0, i.status = S)) : i.status = S), 0 !== i.pending) { if (z(t), 0 === t.avail_out) return i.last_flush = -1, p } else if (0 === t.avail_in && D(e) <= D(n) && e !== h) return P(t, -5);
                                if (666 === i.status && 0 !== t.avail_in) return P(t, -5);
                                if (0 !== t.avail_in || 0 !== i.lookahead || e !== u && 666 !== i.status) {
                                    var s = 2 === i.strategy ? function(t, e) { for (var n;;) { if (0 === t.lookahead && (H(t), 0 === t.lookahead)) { if (e === u) return F; break } if (t.match_length = 0, n = c._tr_tally(t, 0, t.window[t.strstart]), t.lookahead--, t.strstart++, n && (L(t, !1), 0 === t.strm.avail_out)) return F } return t.insert = 0, e === h ? (L(t, !0), 0 === t.strm.avail_out ? M : C) : t.last_lit && (L(t, !1), 0 === t.strm.avail_out) ? F : R }(i, e) : 3 === i.strategy ? function(t, e) {
                                        for (var n, i, r, a, o = t.window;;) {
                                            if (t.lookahead <= x) { if (H(t), t.lookahead <= x && e === u) return F; if (0 === t.lookahead) break }
                                            if (t.match_length = 0, t.lookahead >= I && 0 < t.strstart && (i = o[r = t.strstart - 1]) === o[++r] && i === o[++r] && i === o[++r]) {
                                                a = t.strstart + x;
                                                do {} while (i === o[++r] && i === o[++r] && i === o[++r] && i === o[++r] && i === o[++r] && i === o[++r] && i === o[++r] && i === o[++r] && r < a);
                                                t.match_length = x - (a - r), t.match_length > t.lookahead && (t.match_length = t.lookahead)
                                            }
                                            if (t.match_length >= I ? (n = c._tr_tally(t, 1, t.match_length - I), t.lookahead -= t.match_length, t.strstart += t.match_length, t.match_length = 0) : (n = c._tr_tally(t, 0, t.window[t.strstart]), t.lookahead--, t.strstart++), n && (L(t, !1), 0 === t.strm.avail_out)) return F
                                        }
                                        return t.insert = 0, e === h ? (L(t, !0), 0 === t.strm.avail_out ? M : C) : t.last_lit && (L(t, !1), 0 === t.strm.avail_out) ? F : R
                                    }(i, e) : l[i.level].func(i, e);
                                    if (s !== M && s !== C || (i.status = 666), s === F || s === M) return 0 === t.avail_out && (i.last_flush = -1), p;
                                    if (s === R && (1 === e ? c._tr_align(i) : 5 !== e && (c._tr_stored_block(i, 0, 0, !1), 3 === e && (T(i.head), 0 === i.lookahead && (i.strstart = 0, i.block_start = 0, i.insert = 0))), z(t), 0 === t.avail_out)) return i.last_flush = -1, p
                                }
                                return e !== h ? p : i.wrap <= 0 ? 1 : (2 === i.wrap ? (B(i, 255 & t.adler), B(i, t.adler >> 8 & 255), B(i, t.adler >> 16 & 255), B(i, t.adler >> 24 & 255), B(i, 255 & t.total_in), B(i, t.total_in >> 8 & 255), B(i, t.total_in >> 16 & 255), B(i, t.total_in >> 24 & 255)) : (O(i, t.adler >>> 16), O(i, 65535 & t.adler)), z(t), 0 < i.wrap && (i.wrap = -i.wrap), 0 !== i.pending ? p : 1)
                            }, n.deflateEnd = function(t) { var e; return t && t.state ? (e = t.state.status) !== E && 69 !== e && 73 !== e && 91 !== e && 103 !== e && e !== S && 666 !== e ? P(t, v) : (t.state = null, e === S ? P(t, -3) : p) : v }, n.deflateSetDictionary = function(t, e) {
                                var n, i, r, a, o, s, l, c, u = e.length;
                                if (!t || !t.state) return v;
                                if (2 === (a = (n = t.state).wrap) || 1 === a && n.status !== E || n.lookahead) return v;
                                for (1 === a && (t.adler = f(t.adler, e, u, 0)), n.wrap = 0, u >= n.w_size && (0 === a && (T(n.head), n.strstart = 0, n.block_start = 0, n.insert = 0), c = new d.Buf8(n.w_size), d.arraySet(c, e, u - n.w_size, n.w_size, 0), e = c, u = n.w_size), o = t.avail_in, s = t.next_in, l = t.input, t.avail_in = u, t.next_in = 0, t.input = e, H(n); n.lookahead >= I;) {
                                    for (i = n.strstart, r = n.lookahead - (I - 1); n.ins_h = (n.ins_h << n.hash_shift ^ n.window[i + I - 1]) & n.hash_mask, n.prev[i & n.w_mask] = n.head[n.ins_h], n.head[n.ins_h] = i, i++, --r;);
                                    n.strstart = i, n.lookahead = I - 1, H(n)
                                }
                                return n.strstart += n.lookahead, n.block_start = n.strstart, n.insert = n.lookahead, n.lookahead = 0, n.match_length = n.prev_length = I - 1, n.match_available = 0, t.next_in = s, t.input = l, t.avail_in = o, n.wrap = a, p
                            }, n.deflateInfo = "pako deflate (from Nodeca project)"
                        }, { "../utils/common": 41, "./adler32": 43, "./crc32": 45, "./messages": 51, "./trees": 52 }],
                        47: [function(t, e, n) { e.exports = function() { this.text = 0, this.time = 0, this.xflags = 0, this.os = 0, this.extra = null, this.extra_len = 0, this.name = "", this.comment = "", this.hcrc = 0, this.done = !1 } }, {}],
                        48: [function(t, e, n) {
                            e.exports = function(t, e) {
                                var n, i, r, a, o, s, l, c, u, h, d, f, m, p, v, _, g, w, b, y, k, I, x, A, E;
                                n = t.state, i = t.next_in, A = t.input, r = i + (t.avail_in - 5), a = t.next_out, E = t.output, o = a - (e - t.avail_out), s = a + (t.avail_out - 257), l = n.dmax, c = n.wsize, u = n.whave, h = n.wnext, d = n.window, f = n.hold, m = n.bits, p = n.lencode, v = n.distcode, _ = (1 << n.lenbits) - 1, g = (1 << n.distbits) - 1;
                                t: do {
                                    m < 15 && (f += A[i++] << m, m += 8, f += A[i++] << m, m += 8), w = p[f & _];
                                    e: for (;;) {
                                        if (f >>>= b = w >>> 24, m -= b, 0 === (b = w >>> 16 & 255)) E[a++] = 65535 & w;
                                        else {
                                            if (!(16 & b)) {
                                                if (0 == (64 & b)) { w = p[(65535 & w) + (f & (1 << b) - 1)]; continue e }
                                                if (32 & b) { n.mode = 12; break t }
                                                t.msg = "invalid literal/length code", n.mode = 30;
                                                break t
                                            }
                                            y = 65535 & w, (b &= 15) && (m < b && (f += A[i++] << m, m += 8), y += f & (1 << b) - 1, f >>>= b, m -= b), m < 15 && (f += A[i++] << m, m += 8, f += A[i++] << m, m += 8), w = v[f & g];
                                            n: for (;;) {
                                                if (f >>>= b = w >>> 24, m -= b, !(16 & (b = w >>> 16 & 255))) {
                                                    if (0 == (64 & b)) { w = v[(65535 & w) + (f & (1 << b) - 1)]; continue n }
                                                    t.msg = "invalid distance code", n.mode = 30;
                                                    break t
                                                }
                                                if (k = 65535 & w, m < (b &= 15) && (f += A[i++] << m, (m += 8) < b && (f += A[i++] << m, m += 8)), l < (k += f & (1 << b) - 1)) { t.msg = "invalid distance too far back", n.mode = 30; break t }
                                                if (f >>>= b, m -= b, (b = a - o) < k) {
                                                    if (u < (b = k - b) && n.sane) { t.msg = "invalid distance too far back", n.mode = 30; break t }
                                                    if (x = d, (I = 0) === h) {
                                                        if (I += c - b, b < y) {
                                                            for (y -= b; E[a++] = d[I++], --b;);
                                                            I = a - k, x = E
                                                        }
                                                    } else if (h < b) {
                                                        if (I += c + h - b, (b -= h) < y) {
                                                            for (y -= b; E[a++] = d[I++], --b;);
                                                            if (I = 0, h < y) {
                                                                for (y -= b = h; E[a++] = d[I++], --b;);
                                                                I = a - k, x = E
                                                            }
                                                        }
                                                    } else if (I += h - b, b < y) {
                                                        for (y -= b; E[a++] = d[I++], --b;);
                                                        I = a - k, x = E
                                                    }
                                                    for (; 2 < y;) E[a++] = x[I++], E[a++] = x[I++], E[a++] = x[I++], y -= 3;
                                                    y && (E[a++] = x[I++], 1 < y && (E[a++] = x[I++]))
                                                } else {
                                                    for (I = a - k; E[a++] = E[I++], E[a++] = E[I++], E[a++] = E[I++], 2 < (y -= 3););
                                                    y && (E[a++] = E[I++], 1 < y && (E[a++] = E[I++]))
                                                }
                                                break
                                            }
                                        }
                                        break
                                    }
                                } while (i < r && a < s);
                                i -= y = m >> 3, f &= (1 << (m -= y << 3)) - 1, t.next_in = i, t.next_out = a, t.avail_in = i < r ? r - i + 5 : 5 - (i - r), t.avail_out = a < s ? s - a + 257 : 257 - (a - s), n.hold = f, n.bits = m
                            }
                        }, {}],
                        49: [function(t, e, n) {
                            var R = t("../utils/common"),
                                M = t("./adler32"),
                                C = t("./crc32"),
                                P = t("./inffast"),
                                D = t("./inftrees"),
                                T = 1,
                                z = 2,
                                L = 0,
                                B = -2,
                                O = 1,
                                i = 852,
                                r = 592;

                            function W(t) { return (t >>> 24 & 255) + (t >>> 8 & 65280) + ((65280 & t) << 8) + ((255 & t) << 24) }

                            function a() { this.mode = 0, this.last = !1, this.wrap = 0, this.havedict = !1, this.flags = 0, this.dmax = 0, this.check = 0, this.total = 0, this.head = null, this.wbits = 0, this.wsize = 0, this.whave = 0, this.wnext = 0, this.window = null, this.hold = 0, this.bits = 0, this.length = 0, this.offset = 0, this.extra = 0, this.lencode = null, this.distcode = null, this.lenbits = 0, this.distbits = 0, this.ncode = 0, this.nlen = 0, this.ndist = 0, this.have = 0, this.next = null, this.lens = new R.Buf16(320), this.work = new R.Buf16(288), this.lendyn = null, this.distdyn = null, this.sane = 0, this.back = 0, this.was = 0 }

                            function o(t) { var e; return t && t.state ? (e = t.state, t.total_in = t.total_out = e.total = 0, t.msg = "", e.wrap && (t.adler = 1 & e.wrap), e.mode = O, e.last = 0, e.havedict = 0, e.dmax = 32768, e.head = null, e.hold = 0, e.bits = 0, e.lencode = e.lendyn = new R.Buf32(i), e.distcode = e.distdyn = new R.Buf32(r), e.sane = 1, e.back = -1, L) : B }

                            function s(t) { var e; return t && t.state ? ((e = t.state).wsize = 0, e.whave = 0, e.wnext = 0, o(t)) : B }

                            function l(t, e) { var n, i; return t && t.state ? (i = t.state, e < 0 ? (n = 0, e = -e) : (n = 1 + (e >> 4), e < 48 && (e &= 15)), e && (e < 8 || 15 < e) ? B : (null !== i.window && i.wbits !== e && (i.window = null), i.wrap = n, i.wbits = e, s(t))) : B }

                            function c(t, e) { var n, i; return t ? (i = new a, (t.state = i).window = null, (n = l(t, e)) !== L && (t.state = null), n) : B }
                            var u, h, d = !0;

                            function H(t) {
                                if (d) {
                                    var e;
                                    for (u = new R.Buf32(512), h = new R.Buf32(32), e = 0; e < 144;) t.lens[e++] = 8;
                                    for (; e < 256;) t.lens[e++] = 9;
                                    for (; e < 280;) t.lens[e++] = 7;
                                    for (; e < 288;) t.lens[e++] = 8;
                                    for (D(T, t.lens, 0, 288, u, 0, t.work, { bits: 9 }), e = 0; e < 32;) t.lens[e++] = 5;
                                    D(z, t.lens, 0, 32, h, 0, t.work, { bits: 5 }), d = !1
                                }
                                t.lencode = u, t.lenbits = 9, t.distcode = h, t.distbits = 5
                            }

                            function U(t, e, n, i) { var r, a = t.state; return null === a.window && (a.wsize = 1 << a.wbits, a.wnext = 0, a.whave = 0, a.window = new R.Buf8(a.wsize)), i >= a.wsize ? (R.arraySet(a.window, e, n - a.wsize, a.wsize, 0), a.wnext = 0, a.whave = a.wsize) : (i < (r = a.wsize - a.wnext) && (r = i), R.arraySet(a.window, e, n - i, r, a.wnext), (i -= r) ? (R.arraySet(a.window, e, n - i, i, 0), a.wnext = i, a.whave = a.wsize) : (a.wnext += r, a.wnext === a.wsize && (a.wnext = 0), a.whave < a.wsize && (a.whave += r))), 0 }
                            n.inflateReset = s, n.inflateReset2 = l, n.inflateResetKeep = o, n.inflateInit = function(t) { return c(t, 15) }, n.inflateInit2 = c, n.inflate = function(t, e) {
                                var n, i, r, a, o, s, l, c, u, h, d, f, m, p, v, _, g, w, b, y, k, I, x, A, E = 0,
                                    S = new R.Buf8(4),
                                    F = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];
                                if (!t || !t.state || !t.output || !t.input && 0 !== t.avail_in) return B;
                                12 === (n = t.state).mode && (n.mode = 13), o = t.next_out, r = t.output, l = t.avail_out, a = t.next_in, i = t.input, s = t.avail_in, c = n.hold, u = n.bits, h = s, d = l, I = L;
                                t: for (;;) switch (n.mode) {
                                    case O:
                                        if (0 === n.wrap) { n.mode = 13; break }
                                        for (; u < 16;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if (2 & n.wrap && 35615 === c) { S[n.check = 0] = 255 & c, S[1] = c >>> 8 & 255, n.check = C(n.check, S, 2, 0), u = c = 0, n.mode = 2; break }
                                        if (n.flags = 0, n.head && (n.head.done = !1), !(1 & n.wrap) || (((255 & c) << 8) + (c >> 8)) % 31) { t.msg = "incorrect header check", n.mode = 30; break }
                                        if (8 != (15 & c)) { t.msg = "unknown compression method", n.mode = 30; break }
                                        if (u -= 4, k = 8 + (15 & (c >>>= 4)), 0 === n.wbits) n.wbits = k;
                                        else if (k > n.wbits) { t.msg = "invalid window size", n.mode = 30; break }
                                        n.dmax = 1 << k, t.adler = n.check = 1, n.mode = 512 & c ? 10 : 12, u = c = 0;
                                        break;
                                    case 2:
                                        for (; u < 16;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if (n.flags = c, 8 != (255 & n.flags)) { t.msg = "unknown compression method", n.mode = 30; break }
                                        if (57344 & n.flags) { t.msg = "unknown header flags set", n.mode = 30; break }
                                        n.head && (n.head.text = c >> 8 & 1), 512 & n.flags && (S[0] = 255 & c, S[1] = c >>> 8 & 255, n.check = C(n.check, S, 2, 0)), u = c = 0, n.mode = 3;
                                    case 3:
                                        for (; u < 32;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        n.head && (n.head.time = c), 512 & n.flags && (S[0] = 255 & c, S[1] = c >>> 8 & 255, S[2] = c >>> 16 & 255, S[3] = c >>> 24 & 255, n.check = C(n.check, S, 4, 0)), u = c = 0, n.mode = 4;
                                    case 4:
                                        for (; u < 16;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        n.head && (n.head.xflags = 255 & c, n.head.os = c >> 8), 512 & n.flags && (S[0] = 255 & c, S[1] = c >>> 8 & 255, n.check = C(n.check, S, 2, 0)), u = c = 0, n.mode = 5;
                                    case 5:
                                        if (1024 & n.flags) {
                                            for (; u < 16;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            n.length = c, n.head && (n.head.extra_len = c), 512 & n.flags && (S[0] = 255 & c, S[1] = c >>> 8 & 255, n.check = C(n.check, S, 2, 0)), u = c = 0
                                        } else n.head && (n.head.extra = null);
                                        n.mode = 6;
                                    case 6:
                                        if (1024 & n.flags && (s < (f = n.length) && (f = s), f && (n.head && (k = n.head.extra_len - n.length, n.head.extra || (n.head.extra = new Array(n.head.extra_len)), R.arraySet(n.head.extra, i, a, f, k)), 512 & n.flags && (n.check = C(n.check, i, f, a)), s -= f, a += f, n.length -= f), n.length)) break t;
                                        n.length = 0, n.mode = 7;
                                    case 7:
                                        if (2048 & n.flags) { if (0 === s) break t; for (f = 0; k = i[a + f++], n.head && k && n.length < 65536 && (n.head.name += String.fromCharCode(k)), k && f < s;); if (512 & n.flags && (n.check = C(n.check, i, f, a)), s -= f, a += f, k) break t } else n.head && (n.head.name = null);
                                        n.length = 0, n.mode = 8;
                                    case 8:
                                        if (4096 & n.flags) { if (0 === s) break t; for (f = 0; k = i[a + f++], n.head && k && n.length < 65536 && (n.head.comment += String.fromCharCode(k)), k && f < s;); if (512 & n.flags && (n.check = C(n.check, i, f, a)), s -= f, a += f, k) break t } else n.head && (n.head.comment = null);
                                        n.mode = 9;
                                    case 9:
                                        if (512 & n.flags) {
                                            for (; u < 16;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            if (c !== (65535 & n.check)) { t.msg = "header crc mismatch", n.mode = 30; break }
                                            u = c = 0
                                        }
                                        n.head && (n.head.hcrc = n.flags >> 9 & 1, n.head.done = !0), t.adler = n.check = 0, n.mode = 12;
                                        break;
                                    case 10:
                                        for (; u < 32;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        t.adler = n.check = W(c), u = c = 0, n.mode = 11;
                                    case 11:
                                        if (0 === n.havedict) return t.next_out = o, t.avail_out = l, t.next_in = a, t.avail_in = s, n.hold = c, n.bits = u, 2;
                                        t.adler = n.check = 1, n.mode = 12;
                                    case 12:
                                        if (5 === e || 6 === e) break t;
                                    case 13:
                                        if (n.last) { c >>>= 7 & u, u -= 7 & u, n.mode = 27; break }
                                        for (; u < 3;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        switch (n.last = 1 & c, --u, 3 & (c >>>= 1)) {
                                            case 0:
                                                n.mode = 14;
                                                break;
                                            case 1:
                                                if (H(n), n.mode = 20, 6 !== e) break;
                                                c >>>= 2, u -= 2;
                                                break t;
                                            case 2:
                                                n.mode = 17;
                                                break;
                                            case 3:
                                                t.msg = "invalid block type", n.mode = 30
                                        }
                                        c >>>= 2, u -= 2;
                                        break;
                                    case 14:
                                        for (c >>>= 7 & u, u -= 7 & u; u < 32;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if ((65535 & c) != (c >>> 16 ^ 65535)) { t.msg = "invalid stored block lengths", n.mode = 30; break }
                                        if (n.length = 65535 & c, u = c = 0, n.mode = 15, 6 === e) break t;
                                    case 15:
                                        n.mode = 16;
                                    case 16:
                                        if (f = n.length) {
                                            if (s < f && (f = s), l < f && (f = l), 0 === f) break t;
                                            R.arraySet(r, i, a, f, o), s -= f, a += f, l -= f, o += f, n.length -= f;
                                            break
                                        }
                                        n.mode = 12;
                                        break;
                                    case 17:
                                        for (; u < 14;) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if (n.nlen = 257 + (31 & c), c >>>= 5, u -= 5, n.ndist = 1 + (31 & c), c >>>= 5, u -= 5, n.ncode = 4 + (15 & c), c >>>= 4, u -= 4, 286 < n.nlen || 30 < n.ndist) { t.msg = "too many length or distance symbols", n.mode = 30; break }
                                        n.have = 0, n.mode = 18;
                                    case 18:
                                        for (; n.have < n.ncode;) {
                                            for (; u < 3;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            n.lens[F[n.have++]] = 7 & c, c >>>= 3, u -= 3
                                        }
                                        for (; n.have < 19;) n.lens[F[n.have++]] = 0;
                                        if (n.lencode = n.lendyn, n.lenbits = 7, x = { bits: n.lenbits }, I = D(0, n.lens, 0, 19, n.lencode, 0, n.work, x), n.lenbits = x.bits, I) { t.msg = "invalid code lengths set", n.mode = 30; break }
                                        n.have = 0, n.mode = 19;
                                    case 19:
                                        for (; n.have < n.nlen + n.ndist;) {
                                            for (; _ = (E = n.lencode[c & (1 << n.lenbits) - 1]) >>> 16 & 255, g = 65535 & E, !((v = E >>> 24) <= u);) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            if (g < 16) c >>>= v, u -= v, n.lens[n.have++] = g;
                                            else {
                                                if (16 === g) {
                                                    for (A = v + 2; u < A;) {
                                                        if (0 === s) break t;
                                                        s--, c += i[a++] << u, u += 8
                                                    }
                                                    if (c >>>= v, u -= v, 0 === n.have) { t.msg = "invalid bit length repeat", n.mode = 30; break }
                                                    k = n.lens[n.have - 1], f = 3 + (3 & c), c >>>= 2, u -= 2
                                                } else if (17 === g) {
                                                    for (A = v + 3; u < A;) {
                                                        if (0 === s) break t;
                                                        s--, c += i[a++] << u, u += 8
                                                    }
                                                    u -= v, k = 0, f = 3 + (7 & (c >>>= v)), c >>>= 3, u -= 3
                                                } else {
                                                    for (A = v + 7; u < A;) {
                                                        if (0 === s) break t;
                                                        s--, c += i[a++] << u, u += 8
                                                    }
                                                    u -= v, k = 0, f = 11 + (127 & (c >>>= v)), c >>>= 7, u -= 7
                                                }
                                                if (n.have + f > n.nlen + n.ndist) { t.msg = "invalid bit length repeat", n.mode = 30; break }
                                                for (; f--;) n.lens[n.have++] = k
                                            }
                                        }
                                        if (30 === n.mode) break;
                                        if (0 === n.lens[256]) { t.msg = "invalid code -- missing end-of-block", n.mode = 30; break }
                                        if (n.lenbits = 9, x = { bits: n.lenbits }, I = D(T, n.lens, 0, n.nlen, n.lencode, 0, n.work, x), n.lenbits = x.bits, I) { t.msg = "invalid literal/lengths set", n.mode = 30; break }
                                        if (n.distbits = 6, n.distcode = n.distdyn, x = { bits: n.distbits }, I = D(z, n.lens, n.nlen, n.ndist, n.distcode, 0, n.work, x), n.distbits = x.bits, I) { t.msg = "invalid distances set", n.mode = 30; break }
                                        if (n.mode = 20, 6 === e) break t;
                                    case 20:
                                        n.mode = 21;
                                    case 21:
                                        if (6 <= s && 258 <= l) { t.next_out = o, t.avail_out = l, t.next_in = a, t.avail_in = s, n.hold = c, n.bits = u, P(t, d), o = t.next_out, r = t.output, l = t.avail_out, a = t.next_in, i = t.input, s = t.avail_in, c = n.hold, u = n.bits, 12 === n.mode && (n.back = -1); break }
                                        for (n.back = 0; _ = (E = n.lencode[c & (1 << n.lenbits) - 1]) >>> 16 & 255, g = 65535 & E, !((v = E >>> 24) <= u);) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if (_ && 0 == (240 & _)) {
                                            for (w = v, b = _, y = g; _ = (E = n.lencode[y + ((c & (1 << w + b) - 1) >> w)]) >>> 16 & 255, g = 65535 & E, !(w + (v = E >>> 24) <= u);) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            c >>>= w, u -= w, n.back += w
                                        }
                                        if (c >>>= v, u -= v, n.back += v, n.length = g, 0 === _) { n.mode = 26; break }
                                        if (32 & _) { n.back = -1, n.mode = 12; break }
                                        if (64 & _) { t.msg = "invalid literal/length code", n.mode = 30; break }
                                        n.extra = 15 & _, n.mode = 22;
                                    case 22:
                                        if (n.extra) {
                                            for (A = n.extra; u < A;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            n.length += c & (1 << n.extra) - 1, c >>>= n.extra, u -= n.extra, n.back += n.extra
                                        }
                                        n.was = n.length, n.mode = 23;
                                    case 23:
                                        for (; _ = (E = n.distcode[c & (1 << n.distbits) - 1]) >>> 16 & 255, g = 65535 & E, !((v = E >>> 24) <= u);) {
                                            if (0 === s) break t;
                                            s--, c += i[a++] << u, u += 8
                                        }
                                        if (0 == (240 & _)) {
                                            for (w = v, b = _, y = g; _ = (E = n.distcode[y + ((c & (1 << w + b) - 1) >> w)]) >>> 16 & 255, g = 65535 & E, !(w + (v = E >>> 24) <= u);) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            c >>>= w, u -= w, n.back += w
                                        }
                                        if (c >>>= v, u -= v, n.back += v, 64 & _) { t.msg = "invalid distance code", n.mode = 30; break }
                                        n.offset = g, n.extra = 15 & _, n.mode = 24;
                                    case 24:
                                        if (n.extra) {
                                            for (A = n.extra; u < A;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            n.offset += c & (1 << n.extra) - 1, c >>>= n.extra, u -= n.extra, n.back += n.extra
                                        }
                                        if (n.offset > n.dmax) { t.msg = "invalid distance too far back", n.mode = 30; break }
                                        n.mode = 25;
                                    case 25:
                                        if (0 === l) break t;
                                        if (f = d - l, n.offset > f) {
                                            if ((f = n.offset - f) > n.whave && n.sane) { t.msg = "invalid distance too far back", n.mode = 30; break }
                                            m = f > n.wnext ? (f -= n.wnext, n.wsize - f) : n.wnext - f, f > n.length && (f = n.length), p = n.window
                                        } else p = r, m = o - n.offset, f = n.length;
                                        for (l < f && (f = l), l -= f, n.length -= f; r[o++] = p[m++], --f;);
                                        0 === n.length && (n.mode = 21);
                                        break;
                                    case 26:
                                        if (0 === l) break t;
                                        r[o++] = n.length, l--, n.mode = 21;
                                        break;
                                    case 27:
                                        if (n.wrap) {
                                            for (; u < 32;) {
                                                if (0 === s) break t;
                                                s--, c |= i[a++] << u, u += 8
                                            }
                                            if (d -= l, t.total_out += d, n.total += d, d && (t.adler = n.check = (n.flags ? C : M)(n.check, r, d, o - d)), d = l, (n.flags ? c : W(c)) !== n.check) { t.msg = "incorrect data check", n.mode = 30; break }
                                            u = c = 0
                                        }
                                        n.mode = 28;
                                    case 28:
                                        if (n.wrap && n.flags) {
                                            for (; u < 32;) {
                                                if (0 === s) break t;
                                                s--, c += i[a++] << u, u += 8
                                            }
                                            if (c !== (4294967295 & n.total)) { t.msg = "incorrect length check", n.mode = 30; break }
                                            u = c = 0
                                        }
                                        n.mode = 29;
                                    case 29:
                                        I = 1;
                                        break t;
                                    case 30:
                                        I = -3;
                                        break t;
                                    case 31:
                                        return -4;
                                    case 32:
                                    default:
                                        return B
                                }
                                return t.next_out = o, t.avail_out = l, t.next_in = a, t.avail_in = s, n.hold = c, n.bits = u, (n.wsize || d !== t.avail_out && n.mode < 30 && (n.mode < 27 || 4 !== e)) && U(t, t.output, t.next_out, d - t.avail_out) ? (n.mode = 31, -4) : (h -= t.avail_in, d -= t.avail_out, t.total_in += h, t.total_out += d, n.total += d, n.wrap && d && (t.adler = n.check = (n.flags ? C : M)(n.check, r, d, t.next_out - d)), t.data_type = n.bits + (n.last ? 64 : 0) + (12 === n.mode ? 128 : 0) + (20 === n.mode || 15 === n.mode ? 256 : 0), (0 == h && 0 === d || 4 === e) && I === L && (I = -5), I)
                            }, n.inflateEnd = function(t) { if (!t || !t.state) return B; var e = t.state; return e.window && (e.window = null), t.state = null, L }, n.inflateGetHeader = function(t, e) { var n; return !t || !t.state || 0 == (2 & (n = t.state).wrap) ? B : ((n.head = e).done = !1, L) }, n.inflateSetDictionary = function(t, e) { var n, i = e.length; return !t || !t.state || 0 !== (n = t.state).wrap && 11 !== n.mode ? B : 11 === n.mode && M(1, e, i, 0) !== n.check ? -3 : U(t, e, i, i) ? (n.mode = 31, -4) : (n.havedict = 1, L) }, n.inflateInfo = "pako inflate (from Nodeca project)"
                        }, { "../utils/common": 41, "./adler32": 43, "./crc32": 45, "./inffast": 48, "./inftrees": 50 }],
                        50: [function(t, e, n) {
                            var T = t("../utils/common"),
                                z = [3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 17, 19, 23, 27, 31, 35, 43, 51, 59, 67, 83, 99, 115, 131, 163, 195, 227, 258, 0, 0],
                                L = [16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20, 21, 21, 21, 21, 16, 72, 78],
                                B = [1, 2, 3, 4, 5, 7, 9, 13, 17, 25, 33, 49, 65, 97, 129, 193, 257, 385, 513, 769, 1025, 1537, 2049, 3073, 4097, 6145, 8193, 12289, 16385, 24577, 0, 0],
                                O = [16, 16, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25, 25, 26, 26, 27, 27, 28, 28, 29, 29, 64, 64];
                            e.exports = function(t, e, n, i, r, a, o, s) {
                                var l, c, u, h, d, f, m, p, v, _ = s.bits,
                                    g = 0,
                                    w = 0,
                                    b = 0,
                                    y = 0,
                                    k = 0,
                                    I = 0,
                                    x = 0,
                                    A = 0,
                                    E = 0,
                                    S = 0,
                                    F = null,
                                    R = 0,
                                    M = new T.Buf16(16),
                                    C = new T.Buf16(16),
                                    P = null,
                                    D = 0;
                                for (g = 0; g <= 15; g++) M[g] = 0;
                                for (w = 0; w < i; w++) M[e[n + w]]++;
                                for (k = _, y = 15; 1 <= y && 0 === M[y]; y--);
                                if (y < k && (k = y), 0 === y) return r[a++] = 20971520, r[a++] = 20971520, s.bits = 1, 0;
                                for (b = 1; b < y && 0 === M[b]; b++);
                                for (k < b && (k = b), g = A = 1; g <= 15; g++)
                                    if (A <<= 1, (A -= M[g]) < 0) return -1;
                                if (0 < A && (0 === t || 1 !== y)) return -1;
                                for (C[1] = 0, g = 1; g < 15; g++) C[g + 1] = C[g] + M[g];
                                for (w = 0; w < i; w++) 0 !== e[n + w] && (o[C[e[n + w]]++] = w);
                                if (f = 0 === t ? (F = P = o, 19) : 1 === t ? (F = z, R -= 257, P = L, D -= 257, 256) : (F = B, P = O, -1), g = b, d = a, x = w = S = 0, u = -1, h = (E = 1 << (I = k)) - 1, 1 === t && 852 < E || 2 === t && 592 < E) return 1;
                                for (;;) {
                                    for (m = g - x, v = o[w] < f ? (p = 0, o[w]) : o[w] > f ? (p = P[D + o[w]], F[R + o[w]]) : (p = 96, 0), l = 1 << g - x, b = c = 1 << I; r[d + (S >> x) + (c -= l)] = m << 24 | p << 16 | v | 0, 0 !== c;);
                                    for (l = 1 << g - 1; S & l;) l >>= 1;
                                    if (0 !== l ? (S &= l - 1, S += l) : S = 0, w++, 0 == --M[g]) {
                                        if (g === y) break;
                                        g = e[n + o[w]]
                                    }
                                    if (k < g && (S & h) !== u) {
                                        for (0 === x && (x = k), d += b, A = 1 << (I = g - x); I + x < y && !((A -= M[I + x]) <= 0);) I++, A <<= 1;
                                        if (E += 1 << I, 1 === t && 852 < E || 2 === t && 592 < E) return 1;
                                        r[u = S & h] = k << 24 | I << 16 | d - a | 0
                                    }
                                }
                                return 0 !== S && (r[d + S] = g - x << 24 | 64 << 16 | 0), s.bits = k, 0
                            }
                        }, { "../utils/common": 41 }],
                        51: [function(t, e, n) { e.exports = { 2: "need dictionary", 1: "stream end", 0: "", "-1": "file error", "-2": "stream error", "-3": "data error", "-4": "insufficient memory", "-5": "buffer error", "-6": "incompatible version" } }, {}],
                        52: [function(t, e, n) {
                            var l = t("../utils/common"),
                                s = 0,
                                c = 1;

                            function i(t) { for (var e = t.length; 0 <= --e;) t[e] = 0 }
                            var u = 0,
                                o = 29,
                                h = 256,
                                d = h + 1 + o,
                                f = 30,
                                m = 19,
                                v = 2 * d + 1,
                                _ = 15,
                                r = 16,
                                p = 7,
                                g = 256,
                                w = 16,
                                b = 17,
                                y = 18,
                                k = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0],
                                I = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13],
                                x = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7],
                                A = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15],
                                E = new Array(2 * (d + 2));
                            i(E);
                            var S = new Array(2 * f);
                            i(S);
                            var F = new Array(512);
                            i(F);
                            var R = new Array(256);
                            i(R);
                            var M = new Array(o);
                            i(M);
                            var C, P, D, T = new Array(f);

                            function z(t, e, n, i, r) { this.static_tree = t, this.extra_bits = e, this.extra_base = n, this.elems = i, this.max_length = r, this.has_stree = t && t.length }

                            function a(t, e) { this.dyn_tree = t, this.max_code = 0, this.stat_desc = e }

                            function L(t) { return t < 256 ? F[t] : F[256 + (t >>> 7)] }

                            function B(t, e) { t.pending_buf[t.pending++] = 255 & e, t.pending_buf[t.pending++] = e >>> 8 & 255 }

                            function O(t, e, n) { t.bi_valid > r - n ? (t.bi_buf |= e << t.bi_valid & 65535, B(t, t.bi_buf), t.bi_buf = e >> r - t.bi_valid, t.bi_valid += n - r) : (t.bi_buf |= e << t.bi_valid & 65535, t.bi_valid += n) }

                            function W(t, e, n) { O(t, n[2 * e], n[2 * e + 1]) }

                            function H(t, e) { for (var n = 0; n |= 1 & t, t >>>= 1, n <<= 1, 0 < --e;); return n >>> 1 }

                            function U(t, e, n) {
                                var i, r, a = new Array(_ + 1),
                                    o = 0;
                                for (i = 1; i <= _; i++) a[i] = o = o + n[i - 1] << 1;
                                for (r = 0; r <= e; r++) {
                                    var s = t[2 * r + 1];
                                    0 !== s && (t[2 * r] = H(a[s]++, s))
                                }
                            }

                            function j(t) {
                                var e;
                                for (e = 0; e < d; e++) t.dyn_ltree[2 * e] = 0;
                                for (e = 0; e < f; e++) t.dyn_dtree[2 * e] = 0;
                                for (e = 0; e < m; e++) t.bl_tree[2 * e] = 0;
                                t.dyn_ltree[2 * g] = 1, t.opt_len = t.static_len = 0, t.last_lit = t.matches = 0
                            }

                            function N(t) { 8 < t.bi_valid ? B(t, t.bi_buf) : 0 < t.bi_valid && (t.pending_buf[t.pending++] = t.bi_buf), t.bi_buf = 0, t.bi_valid = 0 }

                            function K(t, e, n, i) {
                                var r = 2 * e,
                                    a = 2 * n;
                                return t[r] < t[a] || t[r] === t[a] && i[e] <= i[n]
                            }

                            function Z(t, e, n) {
                                for (var i = t.heap[n], r = n << 1; r <= t.heap_len && (r < t.heap_len && K(e, t.heap[r + 1], t.heap[r], t.depth) && r++, !K(e, i, t.heap[r], t.depth));) t.heap[n] = t.heap[r], n = r, r <<= 1;
                                t.heap[n] = i
                            }

                            function G(t, e, n) {
                                var i, r, a, o, s = 0;
                                if (0 !== t.last_lit)
                                    for (; i = t.pending_buf[t.d_buf + 2 * s] << 8 | t.pending_buf[t.d_buf + 2 * s + 1], r = t.pending_buf[t.l_buf + s], s++, 0 === i ? W(t, r, e) : (W(t, (a = R[r]) + h + 1, e), 0 !== (o = k[a]) && O(t, r -= M[a], o), W(t, a = L(--i), n), 0 !== (o = I[a]) && O(t, i -= T[a], o)), s < t.last_lit;);
                                W(t, g, e)
                            }

                            function Y(t, e) {
                                var n, i, r, a = e.dyn_tree,
                                    o = e.stat_desc.static_tree,
                                    s = e.stat_desc.has_stree,
                                    l = e.stat_desc.elems,
                                    c = -1;
                                for (t.heap_len = 0, t.heap_max = v, n = 0; n < l; n++) 0 !== a[2 * n] ? (t.heap[++t.heap_len] = c = n, t.depth[n] = 0) : a[2 * n + 1] = 0;
                                for (; t.heap_len < 2;) a[2 * (r = t.heap[++t.heap_len] = c < 2 ? ++c : 0)] = 1, t.depth[r] = 0, t.opt_len--, s && (t.static_len -= o[2 * r + 1]);
                                for (e.max_code = c, n = t.heap_len >> 1; 1 <= n; n--) Z(t, a, n);
                                for (r = l; n = t.heap[1], t.heap[1] = t.heap[t.heap_len--], Z(t, a, 1), i = t.heap[1], t.heap[--t.heap_max] = n, t.heap[--t.heap_max] = i, a[2 * r] = a[2 * n] + a[2 * i], t.depth[r] = (t.depth[n] >= t.depth[i] ? t.depth[n] : t.depth[i]) + 1, a[2 * n + 1] = a[2 * i + 1] = r, t.heap[1] = r++, Z(t, a, 1), 2 <= t.heap_len;);
                                t.heap[--t.heap_max] = t.heap[1],
                                    function(t, e) {
                                        var n, i, r, a, o, s, l = e.dyn_tree,
                                            c = e.max_code,
                                            u = e.stat_desc.static_tree,
                                            h = e.stat_desc.has_stree,
                                            d = e.stat_desc.extra_bits,
                                            f = e.stat_desc.extra_base,
                                            m = e.stat_desc.max_length,
                                            p = 0;
                                        for (a = 0; a <= _; a++) t.bl_count[a] = 0;
                                        for (l[2 * t.heap[t.heap_max] + 1] = 0, n = t.heap_max + 1; n < v; n++) m < (a = l[2 * l[2 * (i = t.heap[n]) + 1] + 1] + 1) && (a = m, p++), l[2 * i + 1] = a, c < i || (t.bl_count[a]++, o = 0, f <= i && (o = d[i - f]), s = l[2 * i], t.opt_len += s * (a + o), h && (t.static_len += s * (u[2 * i + 1] + o)));
                                        if (0 !== p) {
                                            do {
                                                for (a = m - 1; 0 === t.bl_count[a];) a--;
                                                t.bl_count[a]--, t.bl_count[a + 1] += 2, t.bl_count[m]--, p -= 2
                                            } while (0 < p);
                                            for (a = m; 0 !== a; a--)
                                                for (i = t.bl_count[a]; 0 !== i;) c < (r = t.heap[--n]) || (l[2 * r + 1] !== a && (t.opt_len += (a - l[2 * r + 1]) * l[2 * r], l[2 * r + 1] = a), i--)
                                        }
                                    }(t, e), U(a, c, t.bl_count)
                            }

                            function V(t, e, n) {
                                var i, r, a = -1,
                                    o = e[1],
                                    s = 0,
                                    l = 7,
                                    c = 4;
                                for (0 === o && (l = 138, c = 3), e[2 * (n + 1) + 1] = 65535, i = 0; i <= n; i++) r = o, o = e[2 * (i + 1) + 1], ++s < l && r === o || (s < c ? t.bl_tree[2 * r] += s : 0 !== r ? (r !== a && t.bl_tree[2 * r]++, t.bl_tree[2 * w]++) : s <= 10 ? t.bl_tree[2 * b]++ : t.bl_tree[2 * y]++, a = r, c = (s = 0) === o ? (l = 138, 3) : r === o ? (l = 6, 3) : (l = 7, 4))
                            }

                            function Q(t, e, n) {
                                var i, r, a = -1,
                                    o = e[1],
                                    s = 0,
                                    l = 7,
                                    c = 4;
                                for (0 === o && (l = 138, c = 3), i = 0; i <= n; i++)
                                    if (r = o, o = e[2 * (i + 1) + 1], !(++s < l && r === o)) {
                                        if (s < c)
                                            for (; W(t, r, t.bl_tree), 0 != --s;);
                                        else 0 !== r ? (r !== a && (W(t, r, t.bl_tree), s--), W(t, w, t.bl_tree), O(t, s - 3, 2)) : s <= 10 ? (W(t, b, t.bl_tree), O(t, s - 3, 3)) : (W(t, y, t.bl_tree), O(t, s - 11, 7));
                                        a = r, c = (s = 0) === o ? (l = 138, 3) : r === o ? (l = 6, 3) : (l = 7, 4)
                                    }
                            }
                            i(T);
                            var J = !1;

                            function X(t, e, n, i) {
                                var r, a, o, s;
                                O(t, (u << 1) + (i ? 1 : 0), 3), a = e, o = n, s = !0, N(r = t), s && (B(r, o), B(r, ~o)), l.arraySet(r.pending_buf, r.window, a, o, r.pending), r.pending += o
                            }
                            n._tr_init = function(t) {
                                J || (function() {
                                    var t, e, n, i, r, a = new Array(_ + 1);
                                    for (i = n = 0; i < o - 1; i++)
                                        for (M[i] = n, t = 0; t < 1 << k[i]; t++) R[n++] = i;
                                    for (R[n - 1] = i, i = r = 0; i < 16; i++)
                                        for (T[i] = r, t = 0; t < 1 << I[i]; t++) F[r++] = i;
                                    for (r >>= 7; i < f; i++)
                                        for (T[i] = r << 7, t = 0; t < 1 << I[i] - 7; t++) F[256 + r++] = i;
                                    for (e = 0; e <= _; e++) a[e] = 0;
                                    for (t = 0; t <= 143;) E[2 * t + 1] = 8, t++, a[8]++;
                                    for (; t <= 255;) E[2 * t + 1] = 9, t++, a[9]++;
                                    for (; t <= 279;) E[2 * t + 1] = 7, t++, a[7]++;
                                    for (; t <= 287;) E[2 * t + 1] = 8, t++, a[8]++;
                                    for (U(E, d + 1, a), t = 0; t < f; t++) S[2 * t + 1] = 5, S[2 * t] = H(t, 5);
                                    C = new z(E, k, h + 1, d, _), P = new z(S, I, 0, f, _), D = new z(new Array(0), x, 0, m, p)
                                }(), J = !0), t.l_desc = new a(t.dyn_ltree, C), t.d_desc = new a(t.dyn_dtree, P), t.bl_desc = new a(t.bl_tree, D), t.bi_buf = 0, t.bi_valid = 0, j(t)
                            }, n._tr_stored_block = X, n._tr_flush_block = function(t, e, n, i) {
                                var r, a, o = 0;
                                0 < t.level ? (2 === t.strm.data_type && (t.strm.data_type = function(t) {
                                    var e, n = 4093624447;
                                    for (e = 0; e <= 31; e++, n >>>= 1)
                                        if (1 & n && 0 !== t.dyn_ltree[2 * e]) return s;
                                    if (0 !== t.dyn_ltree[18] || 0 !== t.dyn_ltree[20] || 0 !== t.dyn_ltree[26]) return c;
                                    for (e = 32; e < h; e++)
                                        if (0 !== t.dyn_ltree[2 * e]) return c;
                                    return s
                                }(t)), Y(t, t.l_desc), Y(t, t.d_desc), o = function(t) { var e; for (V(t, t.dyn_ltree, t.l_desc.max_code), V(t, t.dyn_dtree, t.d_desc.max_code), Y(t, t.bl_desc), e = m - 1; 3 <= e && 0 === t.bl_tree[2 * A[e] + 1]; e--); return t.opt_len += 3 * (e + 1) + 5 + 5 + 4, e }(t), r = t.opt_len + 3 + 7 >>> 3, (a = t.static_len + 3 + 7 >>> 3) <= r && (r = a)) : r = a = n + 5, n + 4 <= r && -1 !== e ? X(t, e, n, i) : 4 === t.strategy || a === r ? (O(t, 2 + (i ? 1 : 0), 3), G(t, E, S)) : (O(t, 4 + (i ? 1 : 0), 3), function(t, e, n, i) {
                                    var r;
                                    for (O(t, e - 257, 5), O(t, n - 1, 5), O(t, i - 4, 4), r = 0; r < i; r++) O(t, t.bl_tree[2 * A[r] + 1], 3);
                                    Q(t, t.dyn_ltree, e - 1), Q(t, t.dyn_dtree, n - 1)
                                }(t, t.l_desc.max_code + 1, t.d_desc.max_code + 1, o + 1), G(t, t.dyn_ltree, t.dyn_dtree)), j(t), i && N(t)
                            }, n._tr_tally = function(t, e, n) { return t.pending_buf[t.d_buf + 2 * t.last_lit] = e >>> 8 & 255, t.pending_buf[t.d_buf + 2 * t.last_lit + 1] = 255 & e, t.pending_buf[t.l_buf + t.last_lit] = 255 & n, t.last_lit++, 0 === e ? t.dyn_ltree[2 * n]++ : (t.matches++, e--, t.dyn_ltree[2 * (R[n] + h + 1)]++, t.dyn_dtree[2 * L(e)]++), t.last_lit === t.lit_bufsize - 1 }, n._tr_align = function(t) {
                                var e;
                                O(t, 2, 3), W(t, g, E), 16 === (e = t).bi_valid ? (B(e, e.bi_buf), e.bi_buf = 0, e.bi_valid = 0) : 8 <= e.bi_valid && (e.pending_buf[e.pending++] = 255 & e.bi_buf, e.bi_buf >>= 8, e.bi_valid -= 8)
                            }
                        }, { "../utils/common": 41 }],
                        53: [function(t, e, n) { e.exports = function() { this.input = null, this.next_in = 0, this.avail_in = 0, this.total_in = 0, this.output = null, this.next_out = 0, this.avail_out = 0, this.total_out = 0, this.msg = "", this.state = null, this.data_type = 2, this.adler = 0 } }, {}],
                        54: [function(t, e, n) {
                            e.exports = "function" == typeof i.a ? i.a : function() {
                                var t = [].slice.apply(arguments);
                                t.splice(1, 0, 0), setTimeout.apply(null, t)
                            }
                        }, {}]
                    }, {}, [10])(10)
                })
            }).call(this, l("tjlA").Buffer, l("yLpj"), l("3UD+")(t))
        },
        LYNF: function(t, e, n) {
            "use strict";
            var o = n("OH9c");
            t.exports = function(t, e, n, i, r) { var a = new Error(t); return o(a, e, n, i, r) }
        },
        Lmem: function(t, e, n) {
            "use strict";
            t.exports = function(t) { return !(!t || !t.__CANCEL__) }
        },
        LzdP: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S, "Date", { now: function() { return (new Date).getTime() } })
        },
        M1xp: function(t, e, n) {
            var i = n("a0xu");
            t.exports = Object("z").propertyIsEnumerable(0) ? Object : function(t) { return "String" == i(t) ? t.split("") : Object(t) }
        },
        MCSJ: function(t, e) {
            t.exports = function(t, e, n) {
                var i = void 0 === n;
                switch (e.length) {
                    case 0:
                        return i ? t() : t.call(n);
                    case 1:
                        return i ? t(e[0]) : t.call(n, e[0]);
                    case 2:
                        return i ? t(e[0], e[1]) : t.call(n, e[0], e[1]);
                    case 3:
                        return i ? t(e[0], e[1], e[2]) : t.call(n, e[0], e[1], e[2]);
                    case 4:
                        return i ? t(e[0], e[1], e[2], e[3]) : t.call(n, e[0], e[1], e[2], e[3])
                }
                return t.apply(n, e)
            }
        },
        MLWZ: function(t, e, n) {
            "use strict";
            var o = n("xTJ+");

            function s(t) { return encodeURIComponent(t).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]") }
            t.exports = function(t, e, n) {
                if (!e) return t;
                var i;
                if (n) i = n(e);
                else if (o.isURLSearchParams(e)) i = e.toString();
                else {
                    var r = [];
                    o.forEach(e, function(t, e) { null != t && (o.isArray(t) ? e += "[]" : t = [t], o.forEach(t, function(t) { o.isDate(t) ? t = t.toISOString() : o.isObject(t) && (t = JSON.stringify(t)), r.push(s(e) + "=" + s(t)) })) }), i = r.join("&")
                }
                if (i) { var a = t.indexOf("#"); - 1 !== a && (t = t.slice(0, a)), t += (-1 === t.indexOf("?") ? "?" : "&") + i }
                return t
            }
        },
        MPFp: function(t, e, n) {
            "use strict";

            function w() { return this }
            var b = n("uOPS"),
                y = n("Y7ZC"),
                k = n("kTiW"),
                I = n("NegM"),
                x = n("SBuE"),
                A = n("j2DC"),
                E = n("RfKB"),
                S = n("U+KD"),
                F = n("UWiX")("iterator"),
                R = !([].keys && "next" in [].keys()),
                M = "values";
            t.exports = function(t, e, n, i, r, a, o) {
                A(n, e, i);

                function s(t) {
                    if (!R && t in m) return m[t];
                    switch (t) {
                        case "keys":
                        case M:
                            return function() { return new n(this, t) }
                    }
                    return function() { return new n(this, t) }
                }
                var l, c, u, h = e + " Iterator",
                    d = r == M,
                    f = !1,
                    m = t.prototype,
                    p = m[F] || m["@@iterator"] || r && m[r],
                    v = p || s(r),
                    _ = r ? d ? s("entries") : v : void 0,
                    g = "Array" == e && m.entries || p;
                if (g && (u = S(g.call(new t))) !== Object.prototype && u.next && (E(u, h, !0), b || "function" == typeof u[F] || I(u, F, w)), d && p && p.name !== M && (f = !0, v = function() { return p.call(this) }), b && !o || !R && !f && m[F] || I(m, F, v), x[e] = v, x[h] = w, r)
                    if (l = { values: d ? v : s(M), keys: a ? v : s("keys"), entries: _ }, o)
                        for (c in l) c in m || k(m, c, l[c]);
                    else y(y.P + y.F * (R || f), e, l);
                return l
            }
        },
        Mqbl: function(t, e, n) {
            var i = n("JB68"),
                r = n("w6GO");
            n("zn7N")("keys", function() { return function(t) { return r(i(t)) } })
        },
        MvwC: function(t, e, n) {
            var i = n("5T2Y").document;
            t.exports = i && i.documentElement
        },
        N9n2: function(t, e, n) {
            var i = n("SqZg"),
                r = n("vjea");
            t.exports = function(t, e) {
                if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function");
                t.prototype = i(e && e.prototype, { constructor: { value: t, writable: !0, configurable: !0 } }), e && r(t, e)
            }
        },
        NV0k: function(t, e) { e.f = {}.propertyIsEnumerable },
        NegM: function(t, e, n) {
            var i = n("2faE"),
                r = n("rr1i");
            t.exports = n("jmDH") ? function(t, e, n) { return i.f(t, e, r(1, n)) } : function(t, e, n) { return t[e] = n, t }
        },
        "NsO/": function(t, e, n) {
            var i = n("M1xp"),
                r = n("Jes0");
            t.exports = function(t) { return i(r(t)) }
        },
        NwJ3: function(t, e, n) {
            var i = n("SBuE"),
                r = n("UWiX")("iterator"),
                a = Array.prototype;
            t.exports = function(t) { return void 0 !== t && (i.Array === t || a[r] === t) }
        },
        OH9c: function(t, e, n) {
            "use strict";
            t.exports = function(t, e, n, i, r) { return t.config = e, n && (t.code = n), t.request = i, t.response = r, t.isAxiosError = !0, t.toJSON = function() { return { message: this.message, name: this.name, description: this.description, number: this.number, fileName: this.fileName, lineNumber: this.lineNumber, columnNumber: this.columnNumber, stack: this.stack, config: this.config, code: this.code } }, t }
        },
        OTTw: function(t, e, n) {
            "use strict";
            var i, r, a, o = n("xTJ+");

            function s(t) { var e = t; return r && (a.setAttribute("href", e), e = a.href), a.setAttribute("href", e), { href: a.href, protocol: a.protocol ? a.protocol.replace(/:$/, "") : "", host: a.host, search: a.search ? a.search.replace(/^\?/, "") : "", hash: a.hash ? a.hash.replace(/^#/, "") : "", hostname: a.hostname, port: a.port, pathname: "/" === a.pathname.charAt(0) ? a.pathname : "/" + a.pathname } }
            t.exports = o.isStandardBrowserEnv() ? (r = /(msie|trident)/i.test(navigator.userAgent), a = document.createElement("a"), i = s(window.location.href), function(t) { var e = o.isString(t) ? s(t) : t; return e.protocol === i.protocol && e.host === i.host }) : function() { return !0 }
        },
        Ojgd: function(t, e) {
            var n = Math.ceil,
                i = Math.floor;
            t.exports = function(t) { return isNaN(t = +t) ? 0 : (0 < t ? i : n)(t) }
        },
        PBE1: function(t, e, n) {
            "use strict";
            var i = n("Y7ZC"),
                r = n("WEpk"),
                a = n("5T2Y"),
                o = n("8gHz"),
                s = n("zXhZ");
            i(i.P + i.R, "Promise", {
                finally: function(e) {
                    var n = o(this, r.Promise || a.Promise),
                        t = "function" == typeof e;
                    return this.then(t ? function(t) { return s(n, e()).then(function() { return t }) } : e, t ? function(t) { return s(n, e()).then(function() { throw t }) } : e)
                }
            })
        },
        PDX0: function(e, t) {
            (function(t) { e.exports = t }).call(this, {})
        },
        "Q/yX": function(t, e, n) {
            "use strict";
            var i = n("Y7ZC"),
                r = n("ZW5q"),
                a = n("RDmV");
            i(i.S, "Promise", {
                try: function(t) {
                    var e = r.f(this),
                        n = a(t);
                    return (n.e ? e.reject : e.resolve)(n.v), e.promise
                }
            })
        },
        QMMT: function(t, e, n) {
            var r = n("a0xu"),
                a = n("UWiX")("toStringTag"),
                o = "Arguments" == r(function() { return arguments }());
            t.exports = function(t) { var e, n, i; return void 0 === t ? "Undefined" : null === t ? "Null" : "string" == typeof(n = function(t, e) { try { return t[e] } catch (t) {} }(e = Object(t), a)) ? n : o ? r(e) : "Object" == (i = r(e)) && "function" == typeof e.callee ? "Arguments" : i }
        },
        QN8s: function(t, e, n) {
            var i = n("d04V"),
                r = n("HoUH");
            t.exports = function(t, e) { if (t) { if ("string" == typeof t) return r(t, e); var n = Object.prototype.toString.call(t).slice(8, -1); return "Object" === n && t.constructor && (n = t.constructor.name), "Map" === n || "Set" === n ? i(n) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? r(t, e) : void 0 } }
        },
        QXhf: function(t, e, n) {
            function i() {
                var t = +this;
                if (w.hasOwnProperty(t)) {
                    var e = w[t];
                    delete w[t], e()
                }
            }

            function r(t) { i.call(t.data) }
            var a, o, s, l = n("2GTP"),
                c = n("MCSJ"),
                u = n("MvwC"),
                h = n("Hsns"),
                d = n("5T2Y"),
                f = d.process,
                m = d.setImmediate,
                p = d.clearImmediate,
                v = d.MessageChannel,
                _ = d.Dispatch,
                g = 0,
                w = {},
                b = "onreadystatechange";
            m && p || (m = function(t) { for (var e = [], n = 1; n < arguments.length;) e.push(arguments[n++]); return w[++g] = function() { c("function" == typeof t ? t : Function(t), e) }, a(g), g }, p = function(t) { delete w[t] }, "process" == n("a0xu")(f) ? a = function(t) { f.nextTick(l(i, t, 1)) } : _ && _.now ? a = function(t) { _.now(l(i, t, 1)) } : v ? (s = (o = new v).port2, o.port1.onmessage = r, a = l(s.postMessage, s, 1)) : d.addEventListener && "function" == typeof postMessage && !d.importScripts ? (a = function(t) { d.postMessage(t + "", "*") }, d.addEventListener("message", r, !1)) : a = b in h("script") ? function(t) { u.appendChild(h("script"))[b] = function() { u.removeChild(this), i.call(t) } } : function(t) { setTimeout(l(i, t, 1), 0) }), t.exports = { set: m, clear: p }
        },
        "R+7+": function(t, e, n) {
            var s = n("w6GO"),
                l = n("mqlF"),
                c = n("NV0k");
            t.exports = function(t) {
                var e = s(t),
                    n = l.f;
                if (n)
                    for (var i, r = n(t), a = c.f, o = 0; r.length > o;) a.call(t, i = r[o++]) && e.push(i);
                return e
            }
        },
        RDmV: function(t, e) { t.exports = function(t) { try { return { e: !1, v: t() } } catch (t) { return { e: !0, v: t } } } },
        "RU/L": function(t, e, n) {
            n("Rqdy");
            var i = n("WEpk").Object;
            t.exports = function(t, e, n) { return i.defineProperty(t, e, n) }
        },
        RfKB: function(t, e, n) {
            var i = n("2faE").f,
                r = n("B+OT"),
                a = n("UWiX")("toStringTag");
            t.exports = function(t, e, n) { t && !r(t = n ? t : t.prototype, a) && i(t, a, { configurable: !0, value: e }) }
        },
        "Rn+g": function(t, e, n) {
            "use strict";
            var r = n("LYNF");
            t.exports = function(t, e, n) { var i = n.config.validateStatus;!i || i(n.status) ? t(n) : e(r("Request failed with status code " + n.status, n.config, null, n.request, n)) }
        },
        Rp86: function(t, e, n) { n("bBy9"), n("FlQf"), t.exports = n("fXsU") },
        Rqdy: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S + i.F * !n("jmDH"), "Object", { defineProperty: n("2faE").f })
        },
        S5KH: function(t, e, n) {
            (t.exports = n("JPst")(!1)).push([t.i, "", ""])
        },
        SBuE: function(t, e) { t.exports = {} },
        SRBb: function(t, e, n) { n("HLdI"), t.exports = n("WEpk").Reflect.get },
        SntB: function(t, e, n) {
            "use strict";
            var l = n("xTJ+");
            t.exports = function(e, n) {
                n = n || {};
                var i = {},
                    t = ["url", "method", "params", "data"],
                    r = ["headers", "auth", "proxy"],
                    a = ["baseURL", "url", "transformRequest", "transformResponse", "paramsSerializer", "timeout", "withCredentials", "adapter", "responseType", "xsrfCookieName", "xsrfHeaderName", "onUploadProgress", "onDownloadProgress", "maxContentLength", "validateStatus", "maxRedirects", "httpAgent", "httpsAgent", "cancelToken", "socketPath"];
                l.forEach(t, function(t) { void 0 !== n[t] && (i[t] = n[t]) }), l.forEach(r, function(t) { l.isObject(n[t]) ? i[t] = l.deepMerge(e[t], n[t]) : void 0 !== n[t] ? i[t] = n[t] : l.isObject(e[t]) ? i[t] = l.deepMerge(e[t]) : void 0 !== e[t] && (i[t] = e[t]) }), l.forEach(a, function(t) { void 0 !== n[t] ? i[t] = n[t] : void 0 !== e[t] && (i[t] = e[t]) });
                var o = t.concat(r).concat(a),
                    s = Object.keys(n).filter(function(t) { return -1 === o.indexOf(t) });
                return l.forEach(s, function(t) { void 0 !== n[t] ? i[t] = n[t] : void 0 !== e[t] && (i[t] = e[t]) }), i
            }
        },
        SqZg: function(t, e, n) { t.exports = n("3GJH") },
        TJWN: function(t, e, n) {
            "use strict";
            var i = n("5T2Y"),
                r = n("WEpk"),
                a = n("2faE"),
                o = n("jmDH"),
                s = n("UWiX")("species");
            t.exports = function(t) {
                var e = "function" == typeof r[t] ? r[t] : i[t];
                o && e && !e[s] && a.f(e, s, { configurable: !0, get: function() { return this } })
            }
        },
        TRZx: function(t, e, n) { t.exports = n("JbBM") },
        TuGD: function(t, e, n) {
            var a = n("UWiX")("iterator"),
                o = !1;
            try {
                var i = [7][a]();
                i.return = function() { o = !0 }, Array.from(i, function() { throw 2 })
            } catch (t) {}
            t.exports = function(t, e) {
                if (!e && !o) return !1;
                var n = !1;
                try {
                    var i = [7],
                        r = i[a]();
                    r.next = function() { return { done: n = !0 } }, i[a] = function() { return r }, t(i)
                } catch (t) {}
                return n
            }
        },
        "U+KD": function(t, e, n) {
            var i = n("B+OT"),
                r = n("JB68"),
                a = n("VVlx")("IE_PROTO"),
                o = Object.prototype;
            t.exports = Object.getPrototypeOf || function(t) { return t = r(t), i(t, a) ? t[a] : "function" == typeof t.constructor && t instanceof t.constructor ? t.constructor.prototype : t instanceof Object ? o : null }
        },
        UO39: function(t, e) { t.exports = function(t, e) { return { value: e, done: !!t } } },
        UWiX: function(t, e, n) {
            var i = n("29s/")("wks"),
                r = n("YqAc"),
                a = n("5T2Y").Symbol,
                o = "function" == typeof a;
            (t.exports = function(t) { return i[t] || (i[t] = o && a[t] || (o ? a : r)("Symbol." + t)) }).store = i
        },
        UnBK: function(t, e, n) {
            "use strict";
            var i = n("xTJ+"),
                r = n("xAGQ"),
                a = n("Lmem"),
                o = n("JEQr");

            function s(t) { t.cancelToken && t.cancelToken.throwIfRequested() }
            t.exports = function(e) { return s(e), e.headers = e.headers || {}, e.data = r(e.data, e.headers, e.transformRequest), e.headers = i.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers), i.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function(t) { delete e.headers[t] }), (e.adapter || o.adapter)(e).then(function(t) { return s(e), t.data = r(t.data, t.headers, e.transformResponse), t }, function(t) { return a(t) || (s(e), t && t.response && (t.response.data = r(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t) }) }
        },
        VJsP: function(t, e, n) {
            "use strict";
            var m = n("2GTP"),
                i = n("Y7ZC"),
                p = n("JB68"),
                v = n("sNwI"),
                _ = n("NwJ3"),
                g = n("tEej"),
                w = n("IP1Z"),
                b = n("fNZA");
            i(i.S + i.F * !n("TuGD")(function(t) { Array.from(t) }), "Array", {
                from: function(t, e, n) {
                    var i, r, a, o, s = p(t),
                        l = "function" == typeof this ? this : Array,
                        c = arguments.length,
                        u = 1 < c ? e : void 0,
                        h = void 0 !== u,
                        d = 0,
                        f = b(s);
                    if (h && (u = m(u, 2 < c ? n : void 0, 2)), null == f || l == Array && _(f))
                        for (r = new l(i = g(s.length)); d < i; d++) w(r, d, h ? u(s[d], d) : s[d]);
                    else
                        for (o = f.call(s), r = new l; !(a = o.next()).done; d++) w(r, d, h ? v(o, u, [a.value, d], !0) : a.value);
                    return r.length = d, r
                }
            })
        },
        VKFn: function(t, e, n) { n("bBy9"), n("FlQf"), t.exports = n("ldVq") },
        VVlx: function(t, e, n) {
            var i = n("29s/")("keys"),
                r = n("YqAc");
            t.exports = function(t) { return i[t] || (i[t] = r(t)) }
        },
        W070: function(t, e, n) {
            var l = n("NsO/"),
                c = n("tEej"),
                u = n("D8kY");
            t.exports = function(s) {
                return function(t, e, n) {
                    var i, r = l(t),
                        a = c(r.length),
                        o = u(n, a);
                    if (s && e != e) {
                        for (; o < a;)
                            if ((i = r[o++]) != i) return !0
                    } else
                        for (; o < a; o++)
                            if ((s || o in r) && r[o] === e) return s || o || 0; return !s && -1
                }
            }
        },
        W7oM: function(t, e, n) {
            n("nZgG");
            var i = n("WEpk").Object;
            t.exports = function(t, e) { return i.defineProperties(t, e) }
        },
        WEpk: function(t, e) { var n = t.exports = { version: "2.6.11" }; "number" == typeof __e && (__e = n) },
        WaGi: function(t, e, n) {
            var r = n("hfKm");

            function i(t, e) {
                for (var n = 0; n < e.length; n++) {
                    var i = e[n];
                    i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), r(t, i.key, i)
                }
            }
            t.exports = function(t, e, n) { return e && i(t.prototype, e), n && i(t, n), t }
        },
        XE39: function(t, e) { t.exports = '<div class="mtar__header">\n    <a href="javascript:;" class="mtar__flex mtar__header-car">\n        <svg class="icon" width="24" height="22" viewBox="0 0 24 22" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill-rule="nonzero" stroke="#000" stroke-width="1.5" fill="none"><path d="M.4 0h2.73l3.468 13.333H19.28l3.025-9.945h-13.9"/><ellipse cx="7.105" cy="18.467" rx="1.709" ry="1.667"/><ellipse cx="18.337" cy="18.467" rx="1.709" ry="1.667"/></g></svg>\n        <div class="mtar__elli car-num js__mtar-car-num"></div>\n    </a>\n</div>\n\x3c!-- 横屏提示 --\x3e\n<div class="mtar__pos-all mtar__landscape"></div>\n<div class="mtar__wrap">\n    \x3c!-- 预览窗口 --\x3e\n    <div class="mtar__preview-wrap">\n        <div class="mtar__video-wrap">\n            <canvas class="mainCanvas" id="mainCanvas"></canvas>\n            <video class="video" muted playsinline autoplay id="video"></video>\n        </div>\n    \n        \x3c!-- 上传图片预览 --\x3e\n        <div class="mtar__pos-a mtar__result-content">\n            <img class="mtar__result-img" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNg5+D8DwABSgEYsZo8DAAAAABJRU5ErkJggg==" alt="">\n        </div>\n    \n        \x3c!-- 实时图片截图 --\x3e\n        <div class="mtar__pos-a mtar__detect-photo-result">\n            <div class="mtar__img">\n                <img class="mtar__result-img" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNg5+D8DwABSgEYsZo8DAAAAABJRU5ErkJggg==" alt="">\n            </div>\n            <div class="mtar__cir_btn js_mtar__detect-close">\n                <svg class="icon" width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg"><g stroke="#FFF" stroke-width="1.5" fill="none" fill-rule="evenodd"><path d="M.929.929L15.07 15.07M15.071.929L.93 15.07"/></g></svg>\n            </div>\n        </div>\n    \n        \x3c!-- 使用浏览器打开提示 --\x3e\n        <div class="mtar__pos-all mtar__browser-tip">\n            \x3c!-- <img src="https://h5.meitu.com/images/ios.png" alt=""> --\x3e\n            <div class="mtar__browser-tip-text"></div>\n        </div>\n    \n        \x3c!-- 更新浏览器弹窗 --\x3e\n        <div class="mtar__pos-all mtar__update">\n            <div class="mtar__update-tit"></div>\n            <div class="mtar__update-sub-tit"></div>\n            <div class="mtar__update-text"></div>\n            <div class="mtar__update-list">\n                \x3c!-- <div class="mtar__update-tag">IPhone</div>\n                <div class="mtar__update-text">iOS11safari 或更高版本</div> --\x3e\n            </div>\n        </div>\n    \n        \x3c!-- 右上角功能键 --\x3e\n        <div class="mtar__rt_btns">\n            <a href="javascript:;" class="mtar__cir_btn car mtar__show">\n                <svg class="icon" width="17" height="16" viewBox="0 0 17 16" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill-rule="nonzero" stroke="#FFF" fill="none"><path d="M.28 0h1.911l2.427 9.333h8.878l2.117-6.962H5.884"/><ellipse cx="4.973" cy="12.927" rx="1.197" ry="1.167"/><ellipse cx="12.836" cy="12.927" rx="1.197" ry="1.167"/></g></svg>\n                <div class="mtar__elli car-num js__mtar-car-num-mobile"></div>\n            </a>\n        </div>\n    \n        \x3c!-- 右下角功能键 --\x3e\n        <div class="mtar__rb_btns">\n            <a href="javascript:;" class="mtar__cir_btn zoom-in">\n                <svg class="icon" width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><g stroke="#FFF" stroke-width="1.5" fill="none" fill-rule="evenodd"><path d="M0 10h20M10 0v20"/></g></svg>\n            </a>\n            <a href="javascript:;" class="mtar__cir_btn zoom-out">\n                <svg class="icon" width="20" height="2" viewBox="0 0 20 2" xmlns="http://www.w3.org/2000/svg"><path d="M0 1h20" stroke="#FFF" stroke-width="1.5" fill="none" fill-rule="evenodd"/></svg>\n            </a>\n            \x3c!-- 截图 --\x3e\n            <a href="javascript:;" class="mtar__cir_btn mtar__screenshot mtar__show">\n                <svg class="icon i3" width="20" height="18" viewBox="0 0 20 18" xmlns="http://www.w3.org/2000/svg"><g transform="translate(1 1.333)" fill="none" fill-rule="evenodd"><path stroke="#FFF" stroke-width="1.5" d="M.667 3H18v12H.667z"/><circle stroke="#FFF" stroke-width="1.5" cx="9.333" cy="9" r="2.667"/><circle fill="#FFF" cx="15.333" cy="5.667" r="1"/><path d="M0 .333h4.667" stroke="#FFF" stroke-width="1.5"/></g></svg>\n            </a>\n            \x3c!-- 切换 --\x3e\n            <div href="javascript:;" class="mtar__cir_btn material-switch mtar__show">\n                <svg class="icon i4" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg"><path d="M7.2.2V1H0v16h7.2v.8h1.6V17H16V1H8.8V.2H7.2zm1.6 6.243L12.643 2.6H14.4v.506l-5.6 5.6V6.443zm0-3.843h1.58L8.8 4.18V2.6zm-7.2 0h5.6v12.8H1.6V2.6zm7.2 8.369l5.6-5.6v2.263l-5.6 5.6v-2.263zm5.6-1.075v2.263L11.157 15.4H8.894L14.4 9.894zm0 4.526v.98h-.98l.98-.98z" fill="#FFF" fill-rule="evenodd"/></svg>\n            </div>\n        </div>\n    \n        <a href="javascript:;" class="mtar__cir_btn button-download mtar__download">\n            <svg class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2490" width="32" height="32"><path d="M896 896H128c-38.4 0-64 25.6-64 64s25.6 64 64 64h768c38.4 0 64-25.6 64-64s-25.6-64-64-64" p-id="2491" fill="#ffffff"></path><path d="M467.2 748.8c6.4 6.4 12.8 12.8 19.2 12.8 6.4 6.4 19.2 6.4 25.6 6.4 6.4 0 19.2 0 25.6-6.4 6.4-6.4 12.8-6.4 19.2-12.8l256-256c25.6-25.6 25.6-64 0-89.6-25.6-25.6-64-25.6-89.6 0L576 550.4V64c0-38.4-25.6-64-64-64S448 25.6 448 64v486.4L300.8 403.2c-25.6-25.6-64-25.6-89.6 0-25.6 25.6-25.6 64 0 89.6l256 256z" p-id="2492" fill="#ffffff"></path></svg>\n        </a>\n    \n        \x3c!-- loading --\x3e\n        <div class="mtar__pos-all mtar__loading mtar__show">\n            <div class="lds-eclipse"><div></div></div>\n            <div class="text"></div>\n        </div>\n    \n        <a href="" id="download-image-link" style="display: none;"></a>\n    \n        \x3c!-- 选择试妆方式 --\x3e\n        <div class="mtar__pos-a mtar__sel">\n            <div class="bg-rt"></div>\n            <div class="bg-lb"></div>\n            <div class="mtar__center">\n                <div class="mtar__sel-tit"></div>\n                <div class="mtar__sel-btn import mtar__flex">\n                    <svg class="i" width="22" height="19" viewBox="0 0 22 19" xmlns="http://www.w3.org/2000/svg"><path d="M22 0v19H0V0h22zm-1.5 1.5h-19v16h19v-16zm-2.366 4.169l1.058 1.062-7.419 7.392-2.66-2.66-4.292 4.24-1.054-1.067 5.352-5.288 2.656 2.655 6.359-6.334zM6 4.5a1.5 1.5 0 1 1 0 3 1.5 1.5 0 0 1 0-3z" fill="#FFF" fill-rule="nonzero"/></svg>\n                    <div class="mtar__sel-import-text"></div>\n                    <input type="file" class="mtar__pos-all mtar__file js_mtar__sel-import" accept="image/*">\n                </div>\n                <div class="mtar__sel-detect-wrap">\n                    <div class="mtar__sel-btn mtar__flex gray js_mtar__sel-detect">\n                        <svg class="i" width="23" height="21" viewBox="0 0 23 21" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill="none" fill-rule="evenodd"><path stroke="#FFF" stroke-width="1.5" d="M.815 3.667H22v14.667H.815z"/><circle stroke="#FFF" stroke-width="1.5" cx="11.407" cy="11" r="3.259"/><circle fill="#FFF" cx="18.741" cy="6.926" r="1"/><path d="M0 .407h5.704" stroke="#FFF" stroke-width="1.5"/></g></svg>\n                        <div class="mtar__sel-detect-text"></div>\n                    </div>\n                    <div class="mtar__sel-tip mtar__flex">\n                        <svg class="i" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M6.5.5a6 6 0 1 1 0 12 6 6 0 0 1 0-12zm0 1a5 5 0 1 0 0 10 5 5 0 0 0 0-10zm.4 4.2v4h-1v-4h1zm-.45-2.4a.75.75 0 1 1 0 1.5.75.75 0 0 1 0-1.5z" fill="#FC3D3D" fill-rule="nonzero"/></svg>\n                        <div class="mtar__sel-tip-text"></div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        \x3c!-- 切换试妆方式 pc --\x3e\n        <div class="mtar__switch-sel mtar__flex">\n            <svg class="i" width="15" height="17" viewBox="0 0 15 17" xmlns="http://www.w3.org/2000/svg"><path d="M5.667 6.5v1H3.499v4h2.334v5H.167v-5H2.5v-4H.332v-1h5.334zM10.333 0l3.334 2v3.833h.5v2h.666v8.6H9.167v-8.6h.666v-2h.5V0zm-5.5 12.5H1.167v3h3.666v-3zm9-3.667h-3.666v6.6h3.666v-6.6zm-.666-2h-2.334v1h2.334v-1zm-1.834-5.067V5.8h1.334V2.566l-1.334-.8zM5.667 4.5v1H.333v-1h5.334zM5 2.5v1H1v-1h4zm-.667-2v1H1.667v-1h2.666z" fill-rule="nonzero" fill="#FFF"/></svg>\n            <div class="mtar__switch-sel-text"></div>\n        </div>\n        <div class="mtar__lb_btns">\n            \x3c!-- 切换试妆方式 mobile --\x3e\n            <div class="mtar__switch-sel-mobile mtar__flex">\n                <svg class="i" width="15" height="17" viewBox="0 0 15 17" xmlns="http://www.w3.org/2000/svg"><path d="M5.667 6.5v1H3.499v4h2.334v5H.167v-5H2.5v-4H.332v-1h5.334zM10.333 0l3.334 2v3.833h.5v2h.666v8.6H9.167v-8.6h.666v-2h.5V0zm-5.5 12.5H1.167v3h3.666v-3zm9-3.667h-3.666v6.6h3.666v-6.6zm-.666-2h-2.334v1h2.334v-1zm-1.834-5.067V5.8h1.334V2.566l-1.334-.8zM5.667 4.5v1H.333v-1h5.334zM5 2.5v1H1v-1h4zm-.667-2v1H1.667v-1h2.666z" fill-rule="nonzero" fill="#FFF"/></svg>\n                <div class="mtar__switch-sel-text"></div>\n            </div>\n        </div>\n        \x3c!-- 实时试色不可用提示 --\x3e\n        <div class="mtar__pos-a mtar__modal js__mtar-modal-nocam">\n            <div class="mtar__pos-all mask"></div>\n            <div class="modalBox">\n                <svg class="icon-cam" width="50" height="50" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg"><g fill="#000" fill-rule="evenodd"><path d="M43.973 23.374c0-10.186-8.452-18.369-18.973-18.369-10.521 0-18.973 8.183-18.973 18.37 0 8.6 6.123 15.78 14.402 17.784l-2.242 2.171h-5.002c-.431 0-.776.334-.776.835 0 .501.345.835.776.835h24.234c.43 0 .776-.334.776-.835 0-.5-.345-.835-.776-.835h-5.606l-2.242-2.17c8.279-2.005 14.402-9.185 14.402-17.786zM20.602 43.247l1.897-1.754c.862.084 1.639.167 2.501.167.862 0 1.639-.083 2.501-.25l1.897 1.753h-8.796v.084zM25 40.074c-9.487 0-17.248-7.515-17.248-16.7C7.752 14.19 15.513 6.675 25 6.675c9.487 0 17.248 7.515 17.248 16.7 0 9.184-7.761 16.699-17.248 16.699z" fill-rule="nonzero"/><path d="M25 16.695c-3.795 0-6.9 3.006-6.9 6.68 0 3.673 3.105 6.68 6.9 6.68 3.795 0 6.9-3.007 6.9-6.68 0-3.674-3.105-6.68-6.9-6.68zm0 11.69c-2.846 0-5.174-2.255-5.174-5.01 0-2.756 2.328-5.01 5.174-5.01 2.846 0 5.174 2.254 5.174 5.01 0 2.755-2.328 5.01-5.174 5.01z" fill-rule="nonzero"/><path d="M23.813 11.191c0 .65.531 1.177 1.187 1.177s1.188-.527 1.188-1.177-.532-1.176-1.188-1.176c-.656 0-1.188.527-1.188 1.176z"/></g></svg>\n                <div class="text"></div>\n                <div class="icon-close-wrap">\n                    <svg class="icon-close" width="40" height="40" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path d="M26.54 12.399l1.061 1.06L21.06 20l6.541 6.542-1.06 1.06L20 21.06 13.46 27.6l-1.061-1.06L18.939 20l-6.54-6.54 1.06-1.061 6.54 6.54 6.542-6.54z" fill="#000" fill-rule="nonzero"/></g></svg>\n                </div>\n            </div>\n        </div>\n        \x3c!-- 弹窗提示 --\x3e\n        <div class="mtar__pos-a mtar__modal js__mtar-modal-tip">\n            <div class="mtar__pos-all mask"></div>\n            <div class="modalBox">\n                <div class="text js__mtar-modal-tip-text"></div>\n            </div>\n        </div>\n    </div>\n    \x3c!-- 产品信息 --\x3e\n    <div class="mtar__product-wrap mtar__flex-y mtar__hide">\n        \x3c!-- 产品列表 --\x3e\n        \x3c!-- pc --\x3e\n        <div class="mtar__skus-wrap">\n            <div class="mtar__product-name title-line"></div>\n            <div class="mtar__sku-name"></div>\n            <div class="mtar__sku-list">\n                \x3c!-- <div class="mtar__sku-list-li">\n                    <img class="img" src="" alt="">\n                </div> --\x3e\n            </div>\n            <div class="mtar__btn-wrap"></div>\n        </div>\n        \x3c!-- 移动 --\x3e\n        <div class="mtar__skus-wrap-mobile">\n            <div class="mtar__skus-product-wrap-mobile">\n                <div class="mtar__sku-list-li-mobile product">\n                    \x3c!-- <div class="img-wrap">\n                        <img class="img" src="" alt="">\n                    </div>\n                    <div class="text mtar__elli-2"></div> --\x3e\n                </div>\n                <div class="split-line"></div>\n                <div class="mtar__scroll-wrap mtar__sku-list-mobile">\n                    \x3c!-- <div class="mtar__sku-list-li-mobile">\n                        <div class="img-wrap">\n                            <img class="img" src="" alt="">\n                            <div class="mtar__pos-a mtar__flex mask">\n                                <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                            </div>\n                        </div>\n                        <div class="text mtar__elli-2"></div>\n                    </div> --\x3e\n                </div>\n            </div>\n            <div class="mtar__btn-wrap"></div>\n        </div>\n        \x3c!-- 妆容列表 --\x3e\n        \x3c!-- pc --\x3e\n        <div class="mtar__look-wrap">\n            <div class="mtar__look-name title-line"></div>\n            <div class="mtar__tab mtar__tab-look">\n                <div class="mtar__tab-click-l">\n                    <svg class="icon" width="9" height="16" viewBox="0 0 9 16" xmlns="http://www.w3.org/2000/svg"><path d="M7.929 15.093L.858 8.022 7.928.95" stroke="#000" fill="none" fill-rule="evenodd"/></svg>\n                </div>\n                <div class="mtar__tab-list mtar__tab-look-list">\n                    \x3c!-- <div class="mtar__tab-item active">\n                        <div class="mtar__img-wrap mtar__tab-img-wrap">\n                            <img class="tab-img" src="https://makeup-magic.zone1.meitudata.com/4786488a-6770-11ea-8890-4eb0f1f344d1.png" alt="">\n                        </div>\n                        <div class="mtar__tab-text mtar__elli-2">收到回复好的稻盛和夫可视角度非收到回复三等奖</div>\n                    </div> --\x3e\n                </div>\n                <div class="mtar__tab-click-r">\n                    <svg class="icon" width="9" height="16" viewBox="0 0 9 16" xmlns="http://www.w3.org/2000/svg"><path d="M1.071 15.093l7.071-7.071L1.072.95" stroke="#000" fill="none" fill-rule="evenodd"/></svg>\n                </div>\n            </div>\n            <div class="mtar__look-product-detail title-line"></div>\n            <div class="mtar__look-sku-tab mtar__tab-look-sku">\n                <div class="mtar__tab-click-l">\n                    <svg class="icon" width="9" height="16" viewBox="0 0 9 16" xmlns="http://www.w3.org/2000/svg"><path d="M7.929 15.093L.858 8.022 7.928.95" stroke="#000" fill="none" fill-rule="evenodd"/></svg>\n                </div>\n                <div class="mtar__look-sku-tab-list mtar__look-sku-list">\n                    \x3c!-- <div class="mtar__look-sku-tab-item">\n                        <div class="mtar__img-wrap mtar__look-sku-tab-img-wrap">\n                            <img class="mtar__look-sku-tab-img" src="https://makeup-magic.zone1.meitudata.com/4786488a-6770-11ea-8890-4eb0f1f344d1.png" alt="">\n                        </div>\n                        <div class="mtar__look-sku-tab-text-wrap">\n                            <div class="mtar__look-sku-tab-id">sdfhjkdhf</div>\n                            <div class="mtar__look-sku-tab-tit mtar__elli-2">好的数据恢计划</div>\n                            <div class="mtar__look-sku-tab-text mtar__elli-2">复计划dhfjshdfdshkfj</div>\n                        </div>\n                    </div> --\x3e\n                </div>\n                <div class="mtar__tab-click-r">\n                    <svg class="icon" width="9" height="16" viewBox="0 0 9 16" xmlns="http://www.w3.org/2000/svg"><path d="M1.071 15.093l7.071-7.071L1.072.95" stroke="#000" fill="none" fill-rule="evenodd"/></svg>\n                </div>\n            </div>\n            <div class="mtar__btn-wrap"></div>\n        </div>\n        \x3c!-- 移动 --\x3e\n        <div class="mtar__look-wrap-mobile">\n            <div class="js__mtar-slide-look">\n                <div class="mtar__flex mtar__look-slide">\n                    <div class="icon mtar__flex">\n                        <svg class="i" width="32" height="3" viewBox="0 0 32 3" xmlns="http://www.w3.org/2000/svg"><rect x="172" y="402" width="32" height="3" rx="1.5" transform="translate(-172 -402)" fill="#FFF" fill-rule="evenodd" fill-opacity=".8"/></svg>\n                    </div>\n                </div>\n                <div class="mtar__scroll-wrap mtar__look-list-mobile">\n                    \x3c!-- <div class="mtar__look-list-li-mobile">\n                        <div class="img-wrap">\n                            <img class="img" src="https://makeup-magic.zone1.meitudata.com/4786488a-6770-11ea-8890-4eb0f1f344d1.png" alt="">\n                            <div class="mtar__pos-a mtar__flex mask">\n                                <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                            </div>\n                        </div>\n                        <div class="text mtar__elli"></div>\n                    </div> --\x3e\n                </div>\n                <div class="mtar__look-product-detail"></div>\n                <div class="mtar__scroll-wrap mtar__look-sku-list-mobile">\n                    \x3c!-- <div class="mtar__look-sku-list-li-mobile active">\n                        <div class="img-wrap">\n                            <img class="img" src="https://makeup-magic.zone1.meitudata.com/4786488a-6770-11ea-8890-4eb0f1f344d1.png" alt="">\n                            <div class="mtar__pos-a mtar__flex mask">\n                                <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                            </div>\n                        </div>\n                        <div class="text-wrap">\n                            <div class="text-1 mtar__elli">大煞风景收到宏和科技</div>\n                            <div class="text-2 mtar__elli-2">大煞风景收到宏dd和科技</div>\n                            <div class="text-3 mtar__elli-2">大煞风景收到宏和ddd科技</div>\n                        </div>\n                    </div> --\x3e\n                </div>\n            </div>\n            <div class="mtar__btn-wrap"></div>\n        </div>\n        \x3c!-- 无产品信息 --\x3e\n        <div class="mtar__no-pro"></div>\n    </div>\n</div>' },
        "XJU/": function(t, e, n) {
            var r = n("NegM");
            t.exports = function(t, e, n) { for (var i in e) n && t[i] ? t[i] = e[i] : r(t, i, e[i]); return t }
        },
        XVgq: function(t, e, n) { t.exports = n("2Nb0") },
        XWtR: function(t, e, n) {
            var i = n("5T2Y").parseInt,
                r = n("oc46").trim,
                a = n("5pKv"),
                o = /^[-+]?0[xX]/;
            t.exports = 8 !== i(a + "08") || 22 !== i(a + "0x16") ? function(t, e) { var n = r(String(t), 3); return i(n, e >>> 0 || (o.test(n) ? 16 : 10)) } : i
        },
        XXOK: function(t, e, n) { t.exports = n("Rp86") },
        XoMD: function(t, e, n) { t.exports = n("hYAz") },
        Y7ZC: function(t, e, n) {
            var p = n("5T2Y"),
                v = n("WEpk"),
                _ = n("2GTP"),
                g = n("NegM"),
                w = n("B+OT"),
                b = "prototype",
                y = function(t, e, n) {
                    var i, r, a, o = t & y.F,
                        s = t & y.G,
                        l = t & y.S,
                        c = t & y.P,
                        u = t & y.B,
                        h = t & y.W,
                        d = s ? v : v[e] || (v[e] = {}),
                        f = d[b],
                        m = s ? p : l ? p[e] : (p[e] || {})[b];
                    for (i in s && (n = e), n)(r = !o && m && void 0 !== m[i]) && w(d, i) || (a = r ? m[i] : n[i], d[i] = s && "function" != typeof m[i] ? n[i] : u && r ? _(a, p) : h && m[i] == a ? function(i) {
                        function t(t, e, n) {
                            if (this instanceof i) {
                                switch (arguments.length) {
                                    case 0:
                                        return new i;
                                    case 1:
                                        return new i(t);
                                    case 2:
                                        return new i(t, e)
                                }
                                return new i(t, e, n)
                            }
                            return i.apply(this, arguments)
                        }
                        return t[b] = i[b], t
                    }(a) : c && "function" == typeof a ? _(Function.call, a) : a, c && ((d.virtual || (d.virtual = {}))[i] = a, t & y.R && f && !f[i] && g(f, i, a)))
                };
            y.F = 1, y.G = 2, y.S = 4, y.P = 8, y.B = 16, y.W = 32, y.U = 64, y.R = 128, t.exports = y
        },
        YkN1: function(t, e, n) {
            var i = n("eeAt");
            "string" == typeof i && (i = [
                [t.i, i, ""]
            ]);
            var r = { hmr: !0, transform: void 0, insertInto: void 0 };
            n("aET+")(i, r);
            i.locals && (t.exports = i.locals)
        },
        YqAc: function(t, e) {
            var n = 0,
                i = Math.random();
            t.exports = function(t) { return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++n + i).toString(36)) }
        },
        Z7t5: function(t, e, n) { t.exports = n("+SFK") },
        ZBjz: function(t, e, n) {
            var i = n("C6ey");
            "string" == typeof i && (i = [
                [t.i, i, ""]
            ]);
            var r = { hmr: !0, transform: void 0, insertInto: void 0 };
            n("aET+")(i, r);
            i.locals && (t.exports = i.locals)
        },
        ZDA2: function(t, e, n) {
            var i = n("iZP3"),
                r = n("K47E");
            t.exports = function(t, e) { return !e || "object" !== i(e) && "function" != typeof e ? r(t) : e }
        },
        ZW5q: function(t, e, n) {
            "use strict";
            var r = n("eaoh");

            function i(t) {
                var n, i;
                this.promise = new t(function(t, e) {
                    if (void 0 !== n || void 0 !== i) throw TypeError("Bad Promise constructor");
                    n = t, i = e
                }), this.resolve = r(n), this.reject = r(i)
            }
            t.exports.f = function(t) { return new i(t) }
        },
        Zxgi: function(t, e, n) {
            var i = n("5T2Y"),
                r = n("WEpk"),
                a = n("uOPS"),
                o = n("zLkG"),
                s = n("2faE").f;
            t.exports = function(t) { var e = r.Symbol || (r.Symbol = !a && i.Symbol || {}); "_" == t.charAt(0) || t in e || s(e, t, { value: o.f(t) }) }
        },
        a0xu: function(t, e) {
            var n = {}.toString;
            t.exports = function(t) { return n.call(t).slice(8, -1) }
        },
        "aET+": function(t, e, n) {
            var i, r, a, l = {},
                c = (i = function() { return window && document && document.all && !window.atob }, function() { return void 0 === r && (r = i.apply(this, arguments)), r }),
                o = (a = {}, function(t) {
                    if ("function" == typeof t) return t();
                    if (void 0 === a[t]) {
                        var e = function(t) { return document.querySelector(t) }.call(this, t);
                        if (window.HTMLIFrameElement && e instanceof window.HTMLIFrameElement) try { e = e.contentDocument.head } catch (t) { e = null }
                        a[t] = e
                    }
                    return a[t]
                }),
                u = null,
                h = 0,
                s = [],
                d = n("9tPo");

            function f(t, e) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n],
                        r = l[i.id];
                    if (r) { r.refs++; for (var a = 0; a < r.parts.length; a++) r.parts[a](i.parts[a]); for (; a < i.parts.length; a++) r.parts.push(w(i.parts[a], e)) } else {
                        var o = [];
                        for (a = 0; a < i.parts.length; a++) o.push(w(i.parts[a], e));
                        l[i.id] = { id: i.id, refs: 1, parts: o }
                    }
                }
            }

            function m(t, e) {
                for (var n = [], i = {}, r = 0; r < t.length; r++) {
                    var a = t[r],
                        o = e.base ? a[0] + e.base : a[0],
                        s = { css: a[1], media: a[2], sourceMap: a[3] };
                    i[o] ? i[o].parts.push(s) : n.push(i[o] = { id: o, parts: [s] })
                }
                return n
            }

            function p(t, e) {
                var n = o(t.insertInto);
                if (!n) throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
                var i = s[s.length - 1];
                if ("top" === t.insertAt) i ? i.nextSibling ? n.insertBefore(e, i.nextSibling) : n.appendChild(e) : n.insertBefore(e, n.firstChild), s.push(e);
                else if ("bottom" === t.insertAt) n.appendChild(e);
                else {
                    if ("object" != typeof t.insertAt || !t.insertAt.before) throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
                    var r = o(t.insertInto + " " + t.insertAt.before);
                    n.insertBefore(e, r)
                }
            }

            function v(t) {
                if (null !== t.parentNode) {
                    t.parentNode.removeChild(t);
                    var e = s.indexOf(t);
                    0 <= e && s.splice(e, 1)
                }
            }

            function _(t) { var e = document.createElement("style"); return void 0 === t.attrs.type && (t.attrs.type = "text/css"), g(e, t.attrs), p(t, e), e }

            function g(e, n) { Object.keys(n).forEach(function(t) { e.setAttribute(t, n[t]) }) }

            function w(e, t) {
                var n, i, r, a, o, s;
                if (t.transform && e.css) {
                    if (!(a = t.transform(e.css))) return function() {};
                    e.css = a
                }
                if (t.singleton) {
                    var l = h++;
                    n = u = u || _(t), i = k.bind(null, n, l, !1), r = k.bind(null, n, l, !0)
                } else r = e.sourceMap && "function" == typeof URL && "function" == typeof URL.createObjectURL && "function" == typeof URL.revokeObjectURL && "function" == typeof Blob && "function" == typeof btoa ? (o = t, s = document.createElement("link"), void 0 === o.attrs.type && (o.attrs.type = "text/css"), o.attrs.rel = "stylesheet", g(s, o.attrs), p(o, s), i = function(t, e, n) {
                    var i = n.css,
                        r = n.sourceMap,
                        a = void 0 === e.convertToAbsoluteUrls && r;
                    (e.convertToAbsoluteUrls || a) && (i = d(i));
                    r && (i += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(r)))) + " */");
                    var o = new Blob([i], { type: "text/css" }),
                        s = t.href;
                    t.href = URL.createObjectURL(o), s && URL.revokeObjectURL(s)
                }.bind(null, n = s, t), function() { v(n), n.href && URL.revokeObjectURL(n.href) }) : (n = _(t), i = function(t, e) {
                    var n = e.css,
                        i = e.media;
                    i && t.setAttribute("media", i);
                    if (t.styleSheet) t.styleSheet.cssText = n;
                    else {
                        for (; t.firstChild;) t.removeChild(t.firstChild);
                        t.appendChild(document.createTextNode(n))
                    }
                }.bind(null, n), function() { v(n) });
                return i(e),
                    function(t) {
                        if (t) {
                            if (t.css === e.css && t.media === e.media && t.sourceMap === e.sourceMap) return;
                            i(e = t)
                        } else r()
                    }
            }
            t.exports = function(t, o) {
                if ("undefined" != typeof DEBUG && DEBUG && "object" != typeof document) throw new Error("The style-loader cannot be used in a non-browser environment");
                (o = o || {}).attrs = "object" == typeof o.attrs ? o.attrs : {}, o.singleton || "boolean" == typeof o.singleton || (o.singleton = c()), o.insertInto || (o.insertInto = "head"), o.insertAt || (o.insertAt = "bottom");
                var s = m(t, o);
                return f(s, o),
                    function(t) {
                        for (var e = [], n = 0; n < s.length; n++) {
                            var i = s[n];
                            (r = l[i.id]).refs--, e.push(r)
                        }
                        t && f(m(t, o), o);
                        for (n = 0; n < e.length; n++) {
                            var r;
                            if (0 === (r = e[n]).refs) {
                                for (var a = 0; a < r.parts.length; a++) r.parts[a]();
                                delete l[r.id]
                            }
                        }
                    }
            };
            var b, y = (b = [], function(t, e) { return b[t] = e, b.filter(Boolean).join("\n") });

            function k(t, e, n, i) {
                var r = n ? "" : i.css;
                if (t.styleSheet) t.styleSheet.cssText = y(e, r);
                else {
                    var a = document.createTextNode(r),
                        o = t.childNodes;
                    o[e] && t.removeChild(o[e]), o.length ? t.insertBefore(a, o[e]) : t.appendChild(a)
                }
            }
        },
        aW7e: function(t, e, n) { n("wgeU"), n("FlQf"), n("bBy9"), n("JMW+"), n("PBE1"), n("Q/yX"), t.exports = n("WEpk").Promise },
        adOz: function(t, e, n) { n("Zxgi")("asyncIterator") },
        "ar/p": function(t, e, n) {
            var i = n("5vMV"),
                r = n("FpHa").concat("length", "prototype");
            e.f = Object.getOwnPropertyNames || function(t) { return i(t, r) }
        },
        bBy9: function(t, e, n) {
            n("w2d+");
            for (var i = n("5T2Y"), r = n("NegM"), a = n("SBuE"), o = n("UWiX")("toStringTag"), s = "CSSRuleList,CSSStyleDeclaration,CSSValueList,ClientRectList,DOMRectList,DOMStringList,DOMTokenList,DataTransferItemList,FileList,HTMLAllCollection,HTMLCollection,HTMLFormElement,HTMLSelectElement,MediaList,MimeTypeArray,NamedNodeMap,NodeList,PaintRequestList,Plugin,PluginArray,SVGLengthList,SVGNumberList,SVGPathSegList,SVGPointList,SVGStringList,SVGTransformList,SourceBufferList,StyleSheetList,TextTrackCueList,TextTrackList,TouchList".split(","), l = 0; l < s.length; l++) {
                var c = s[l],
                    u = i[c],
                    h = u && u.prototype;
                h && !h[o] && r(h, o, c), a[c] = a.Array
            }
        },
        br1p: function(t, e, n) { t.exports = n("gHCg") },
        ccE7: function(t, e, n) {
            var l = n("Ojgd"),
                c = n("Jes0");
            t.exports = function(s) {
                return function(t, e) {
                    var n, i, r = String(c(t)),
                        a = l(e),
                        o = r.length;
                    return a < 0 || o <= a ? s ? "" : void 0 : (n = r.charCodeAt(a)) < 55296 || 56319 < n || a + 1 === o || (i = r.charCodeAt(a + 1)) < 56320 || 57343 < i ? s ? r.charAt(a) : n : s ? r.slice(a, a + 2) : i - 56320 + (n - 55296 << 10) + 65536
                }
            }
        },
        czwh: function(t, e, n) {
            var i = n("Y7ZC"),
                l = n("oVml"),
                c = n("eaoh"),
                u = n("5K7Z"),
                h = n("93I4"),
                r = n("KUxP"),
                d = n("wYmx"),
                f = (n("5T2Y").Reflect || {}).construct,
                m = r(function() {
                    function t() {}
                    return !(f(function() {}, [], t) instanceof t)
                }),
                p = !r(function() { f(function() {}) });
            i(i.S + i.F * (m || p), "Reflect", {
                construct: function(t, e, n) {
                    c(t), u(e);
                    var i = arguments.length < 3 ? t : c(n);
                    if (p && !m) return f(t, e, i);
                    if (t == i) {
                        switch (e.length) {
                            case 0:
                                return new t;
                            case 1:
                                return new t(e[0]);
                            case 2:
                                return new t(e[0], e[1]);
                            case 3:
                                return new t(e[0], e[1], e[2]);
                            case 4:
                                return new t(e[0], e[1], e[2], e[3])
                        }
                        var r = [null];
                        return r.push.apply(r, e), new(d.apply(t, r))
                    }
                    var a = i.prototype,
                        o = l(h(a) ? a : Object.prototype),
                        s = Function.apply.call(t, o, e);
                    return h(s) ? s : o
                }
            })
        },
        d04V: function(t, e, n) { t.exports = n("0tVQ") },
        dEVD: function(t, e, n) {
            var i = n("Y7ZC"),
                r = n("XWtR");
            i(i.G + i.F * (parseInt != r), { parseInt: r })
        },
        dl0q: function(t, e, n) { n("Zxgi")("observable") },
        eUtF: function(t, e, n) { t.exports = !n("jmDH") && !n("KUxP")(function() { return 7 != Object.defineProperty(n("Hsns")("div"), "a", { get: function() { return 7 } }).a }) },
        eVuF: function(t, e, n) { t.exports = n("aW7e") },
        eaoh: function(t, e) { t.exports = function(t) { if ("function" != typeof t) throw TypeError(t + " is not a function!"); return t } },
        eeAt: function(t, e, n) {
            (t.exports = n("JPst")(!1)).push([t.i, ".mtar__fixed{position:fixed!important;top:0;left:0;right:0;bottom:0}.mtar_zindex{z-index:1}.mtar{width:100%;height:100%;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mtar__result-content.mtar__comparing .mtar__result-img{-webkit-touch-callout:none}.mtar a{-webkit-tap-highlight-color:transparent}.mtar__wrap{width:100%;height:100%;overflow:hidden;position:relative}.mtar__preview-wrap{font-size:2.13333vw}.mtar__landscape{font-size:4vw;position:fixed}.mtar__modal .modalBox{width:64.53333vw;border-radius:1.06667vw;-webkit-box-shadow:0 0 4.26667vw 0 rgba(0,0,0,.25);box-shadow:0 0 4.26667vw 0 rgba(0,0,0,.25);padding:13.33333vw 5.33333vw}.mtar__modal .modalBox .icon-cam{width:8.53333vw;height:8.53333vw;margin-bottom:5.33333vw}.mtar__modal .modalBox .text{font-size:4vw;line-height:5.86667vw}.mtar__modal .modalBox .icon-close-wrap{width:6.4vw;height:6.4vw;top:2.66667vw;right:2.66667vw}.mtar__sel .bg-lb,.mtar__sel .bg-rt{width:90%}.mtar__sel-tit{font-size:8.53333vw;font-weight:300;line-height:12vw;height:17.86667vw;margin-bottom:19.2vw}.mtar__sel-tit:after{width:8vw}.mtar__sel-btn{width:74.4vw;height:12.8vw;font-size:3.73333vw;margin-bottom:8vw}.mtar__sel-btn .i{margin-right:2.66667vw}.mtar__sel-btn.gray{margin-bottom:5.33333vw}.mtar__sel-tip{font-size:3.2vw}.mtar__sel-tip .i{margin-right:1.33333vw}.mtar__switch-sel-mobile{min-width:10.66667vw;height:10.66667vw;-webkit-box-sizing:border-box;box-sizing:border-box;padding:2.13333vw 2.66667vw;border-radius:5.33333vw;background-color:rgba(0,0,0,.5);cursor:pointer;color:#fff;font-size:3.73333vw}.mtar__switch-sel-mobile .i{width:4.26667vw;height:5.06667vw}.mtar__switch-sel-mobile .mtar__switch-sel-text{-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:1.6vw;white-space:nowrap;-webkit-transition:all .3s;transition:all .3s;overflow:hidden}.mtar__switch-sel-mobile.retract .mtar__switch-sel-text{width:0;padding-left:0}.mtar__rt_btns{right:4vw;top:4vw}.mtar__rt_btns .car{position:relative}.mtar__rt_btns .car .car-num{font-size:2.66667vw;width:4.8vw;height:4.8vw;line-height:4.8vw;text-align:center;background-color:#fff;color:#000;border-radius:50%;position:absolute;top:-0.66667vw;right:-0.66667vw}.mtar__rb_btns{right:4vw;bottom:4vw}.mtar__lb_btns{left:4vw;bottom:4vw}.mtar__in-look .is-up .mtar__lb_btns,.mtar__in-look .is-up .mtar__rb_btns{display:none}.mtar__show-product:not(.mtar__no-sku-look) .mtar__lb_btns,.mtar__show-product:not(.mtar__no-sku-look) .mtar__rb_btns{bottom:40vw}.mtar__show-product:not(.mtar__no-sku-look).mtar__in-look .mtar__lb_btns,.mtar__show-product:not(.mtar__no-sku-look).mtar__in-look .mtar__rb_btns{bottom:49.33333vw}.mtar__cir_btn{width:10.66667vw;height:10.66667vw}.mtar__cir_btn .icon{width:5.33333vw;height:5.33333vw}.mtar__cir_btn .i3{width:5.33333vw;height:4.8vw}.mtar__cir_btn .i4{width:4.26667vw;height:4.8vw}.mtar .button-download{right:4vw;bottom:4vw}.mtar__loading .text{font-size:3.2vw;margin-top:1.33333vw;line-height:1.8}.mtar__loading .lds-eclipse{width:10.66667vw;height:10.66667vw}.mtar__loading .lds-eclipse div{width:10.66667vw;height:10.66667vw;-webkit-box-shadow:0 0.26667vw 0 0 #fff;box-shadow:0 0.26667vw 0 0 #fff}.mtar__skus-wrap-mobile{width:100%;height:35.73333vw;background-color:rgba(0,0,0,.4);position:absolute;left:0;bottom:0;z-index:20}.mtar__skus-product-wrap-mobile{-webkit-box-sizing:border-box;box-sizing:border-box;width:100%;height:25.06667vw;display:-webkit-box;display:-ms-flexbox;display:flex;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding-left:3.2vw}.mtar__skus-product-wrap-mobile .mtar__scroll-wrap{-webkit-box-flex:1;-ms-flex:1;flex:1;overflow-y:hidden;overflow-x:auto;-webkit-box-sizing:border-box;box-sizing:border-box;white-space:nowrap;position:relative}.mtar .split-line{display:inline-block;width:0.26667vw;height:6.4vw;border-radius:0.21333vw;background-color:hsla(0,0%,100%,.2);margin-right:2.66667vw}.mtar__sku-list-li-mobile{vertical-align:bottom;display:inline-block;width:16vw;height:18.13333vw;margin-right:2.66667vw}.mtar__sku-list-li-mobile.active .img-wrap{-webkit-animation:imgIn .3s;animation:imgIn .3s}@-webkit-keyframes imgIn{0%{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(.9);transform:scale(.9)}to{-webkit-transform:scale(1);transform:scale(1)}}@keyframes imgIn{0%{-webkit-transform:scale(1);transform:scale(1)}50%{-webkit-transform:scale(.9);transform:scale(.9)}to{-webkit-transform:scale(1);transform:scale(1)}}.mtar__sku-list-li-mobile .img-wrap{width:10.66667vw;height:10.66667vw;margin:0 auto 1.06667vw}.mtar__sku-list-li-mobile .text{font-size:2.66667vw;line-height:3.2vw;color:#fff;text-align:center}.mtar__look-wrap-mobile .btn-wrap,.mtar__skus-wrap-mobile .btn-wrap{width:100%;position:absolute;left:0;bottom:0}.mtar__look-wrap-mobile .btn,.mtar__skus-wrap-mobile .btn{width:50%;height:10.66667vw;font-size:3.73333vw;-webkit-box-shadow:none;box-shadow:none;border:none}.mtar__look-wrap-mobile .btn.mtar__product-car,.mtar__skus-wrap-mobile .btn.mtar__product-car{background-color:#fff}.mtar__look-wrap-mobile .btn .icon,.mtar__skus-wrap-mobile .btn .icon{margin-right:1.06667vw}.mtar__look-list-li-mobile,.mtar__look-sku-list-li-mobile,.mtar__sku-list-li-mobile{cursor:pointer}.mtar__look-list-li-mobile .img-wrap,.mtar__look-sku-list-li-mobile .img-wrap,.mtar__sku-list-li-mobile .img-wrap{position:relative}.mtar__look-list-li-mobile .img-wrap .img,.mtar__look-sku-list-li-mobile .img-wrap .img,.mtar__sku-list-li-mobile .img-wrap .img{width:100%;height:100%;-webkit-touch-callout:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.mtar__look-list-li-mobile .mask,.mtar__look-sku-list-li-mobile .mask,.mtar__sku-list-li-mobile .mask{visibility:hidden;opacity:0;background-color:rgba(0,0,0,.5);-webkit-transition:all .3s;transition:all .3s}.mtar__look-list-li-mobile .mask .i,.mtar__look-sku-list-li-mobile .mask .i,.mtar__sku-list-li-mobile .mask .i{width:5.33333vw;height:5.33333vw}.mtar__look-list-li-mobile.active .mask,.mtar__look-sku-list-li-mobile.active .mask,.mtar__sku-list-li-mobile.active .mask{visibility:visible;opacity:1}.mtar__look-wrap-mobile{width:100%;height:90.93333vw;background-color:rgba(0,0,0,.4);position:absolute;left:0;bottom:0;z-index:20;-webkit-transform:translateY(50%);transform:translateY(50%)}.mtar__look-wrap-mobile.down{-webkit-animation:lookDown .5s both;animation:lookDown .5s both}.mtar__look-wrap-mobile.up{-webkit-animation:lookUp .5s both;animation:lookUp .5s both}@-webkit-keyframes lookDown{0%{-webkit-transform:translate(0);transform:translate(0)}70%{-webkit-transform:translateY(52%);transform:translateY(52%)}85%{-webkit-transform:translateY(49%);transform:translateY(49%)}to{-webkit-transform:translateY(50%);transform:translateY(50%)}}@keyframes lookDown{0%{-webkit-transform:translate(0);transform:translate(0)}70%{-webkit-transform:translateY(52%);transform:translateY(52%)}85%{-webkit-transform:translateY(49%);transform:translateY(49%)}to{-webkit-transform:translateY(50%);transform:translateY(50%)}}@-webkit-keyframes lookUp{0%{-webkit-transform:translateY(50%);transform:translateY(50%)}70%{-webkit-transform:translateY(-2%);transform:translateY(-2%)}85%{-webkit-transform:translateY(1%);transform:translateY(1%)}to{-webkit-transform:translate(0);transform:translate(0)}}@keyframes lookUp{0%{-webkit-transform:translateY(50%);transform:translateY(50%)}70%{-webkit-transform:translateY(-2%);transform:translateY(-2%)}85%{-webkit-transform:translateY(1%);transform:translateY(1%)}to{-webkit-transform:translate(0);transform:translate(0)}}.mtar__look-wrap-mobile .mtar__scroll-wrap{-webkit-box-sizing:border-box;width:100%;padding:0 4.8vw;overflow-y:hidden;overflow-x:auto;box-sizing:border-box;white-space:nowrap}.mtar__look-wrap-mobile .mtar__look-list-mobile{height:23.46667vw}.mtar__look-wrap-mobile .no-text{color:#fff;font-size:3.2vw;padding-left:0.53333vw}.mtar__look-list-li-mobile{display:inline-block;width:17.06667vw;height:23.46667vw;background-color:#fff}.mtar__look-list-li-mobile:not(:last-child){margin-right:4vw}.mtar__look-list-li-mobile .img-wrap{width:17.06667vw;height:17.06667vw}.mtar__look-list-li-mobile .text{-webkit-box-sizing:border-box;box-sizing:border-box;height:6.4vw;line-height:6.4vw;font-size:2.4vw;text-align:center;padding:0 1.33333vw}.mtar__look-product-detail{font-size:3.73333vw;color:#fff;padding:3.73333vw 5.33333vw 1.06667vw;line-height:5.33333vw}.mtar__look-sku-list-li-mobile{display:inline-block;width:17.06667vw;height:23.46667vw;text-align:center}.mtar__look-sku-list-li-mobile:not(:last-child){margin-right:4vw}.mtar__look-sku-list-li-mobile .img-wrap{width:17.06667vw;height:17.06667vw;margin-bottom:1.6vw}.mtar__look-sku-list-li-mobile .text-1{font-size:2.4vw;line-height:3.46667vw;margin-bottom:1.06667vw;color:hsla(0,0%,100%,.65)}.mtar__look-sku-list-li-mobile .text-2{height:6.4vw;font-size:2.66667vw;line-height:3.2vw;margin-bottom:1.06667vw;color:#fff}.mtar__look-sku-list-li-mobile .text-3{height:6.4vw;font-size:2.4vw;line-height:3.2vw;margin-bottom:0;color:hsla(0,0%,100%,.8)}.mtar__look-slide{width:100%;height:8.8vw}.mtar__look-slide .icon{width:13.33333vw;height:8.53333vw;cursor:pointer}.mtar__look-slide .icon svg{width:8.53333vw;height:0.8vw}#mt-toast{max-width:80%;padding:2.66667vw 5.33333vw;font-size:3.73333vw;border-radius:2.66667vw;position:fixed}@media screen and (max-width:600px){.a{width:100px}}", ""])
        },
        endd: function(t, e, n) {
            "use strict";

            function i(t) { this.message = t }
            i.prototype.toString = function() { return "Cancel" + (this.message ? ": " + this.message : "") }, i.prototype.__CANCEL__ = !0, t.exports = i
        },
        eqyj: function(t, e, n) {
            "use strict";
            var s = n("xTJ+");
            t.exports = s.isStandardBrowserEnv() ? {
                write: function(t, e, n, i, r, a) {
                    var o = [];
                    o.push(t + "=" + encodeURIComponent(e)), s.isNumber(n) && o.push("expires=" + new Date(n).toGMTString()), s.isString(i) && o.push("path=" + i), s.isString(r) && o.push("domain=" + r), !0 === a && o.push("secure"), document.cookie = o.join("; ")
                },
                read: function(t) { var e = document.cookie.match(new RegExp("(^|;\\s*)(" + t + ")=([^;]*)")); return e ? decodeURIComponent(e[3]) : null },
                remove: function(t) { this.write(t, "", Date.now() - 864e5) }
            } : { write: function() {}, read: function() { return null }, remove: function() {} }
        },
        fNZA: function(t, e, n) {
            var i = n("QMMT"),
                r = n("UWiX")("iterator"),
                a = n("SBuE");
            t.exports = n("WEpk").getIteratorMethod = function(t) { if (null != t) return t[r] || t["@@iterator"] || a[i(t)] }
        },
        fXsU: function(t, e, n) {
            var i = n("5K7Z"),
                r = n("fNZA");
            t.exports = n("WEpk").getIterator = function(t) { var e = r(t); if ("function" != typeof e) throw TypeError(t + " is not iterable!"); return i(e.call(t)) }
        },
        fpC5: function(t, e, n) {
            var o = n("2faE"),
                s = n("5K7Z"),
                l = n("w6GO");
            t.exports = n("jmDH") ? Object.defineProperties : function(t, e) { s(t); for (var n, i = l(e), r = i.length, a = 0; a < r;) o.f(t, n = i[a++], e[n]); return t }
        },
        fprZ: function(t, e, n) {
            var l = n("XXOK"),
                c = n("yLu3"),
                u = n("Z7t5");
            t.exports = function(t, e) {
                if (void 0 !== u && c(Object(t))) {
                    var n = [],
                        i = !0,
                        r = !1,
                        a = void 0;
                    try { for (var o, s = l(t); !(i = (o = s.next()).done) && (n.push(o.value), !e || n.length !== e); i = !0); } catch (t) { r = !0, a = t } finally { try { i || null == s.return || s.return() } finally { if (r) throw a } }
                    return n
                }
            }
        },
        g7np: function(t, e, n) {
            "use strict";
            var i = n("2SVd"),
                r = n("5oMp");
            t.exports = function(t, e) { return t && !i(e) ? r(t, e) : e }
        },
        gHCg: function(t, e, n) { n("ieHM"), t.exports = n("WEpk").setImmediate },
        hDam: function(t, e) { t.exports = function() {} },
        hYAz: function(t, e, n) { n("7m0m"), t.exports = n("WEpk").Object.getOwnPropertyDescriptors },
        hfKm: function(t, e, n) { t.exports = n("RU/L") },
        iZP3: function(e, t, n) {
            var i = n("XVgq"),
                r = n("Z7t5");

            function a(t) { return e.exports = a = "function" == typeof r && "symbol" == typeof i ? function(t) { return typeof t } : function(t) { return t && "function" == typeof r && t.constructor === r && t !== r.prototype ? "symbol" : typeof t }, a(t) }
            e.exports = a
        },
        ieHM: function(t, e, n) {
            var i = n("Y7ZC"),
                r = n("QXhf");
            i(i.G + i.B, { setImmediate: r.set, clearImmediate: r.clear })
        },
        iq4v: function(t, e, n) { n("Mqbl"), t.exports = n("WEpk").Object.keys },
        "j+vE": function(t, e, n) { t.exports = n("SRBb") },
        j2DC: function(t, e, n) {
            "use strict";
            var i = n("oVml"),
                r = n("rr1i"),
                a = n("RfKB"),
                o = {};
            n("NegM")(o, n("UWiX")("iterator"), function() { return this }), t.exports = function(t, e, n) { t.prototype = i(o, { next: r(1, n) }), a(t, e + " Iterator") }
        },
        "jfS+": function(t, e, n) {
            "use strict";
            var i = n("endd");

            function r(t) {
                if ("function" != typeof t) throw new TypeError("executor must be a function.");
                var e;
                this.promise = new Promise(function(t) { e = t });
                var n = this;
                t(function(t) { n.reason || (n.reason = new i(t), e(n.reason)) })
            }
            r.prototype.throwIfRequested = function() { if (this.reason) throw this.reason }, r.source = function() { var e; return { token: new r(function(t) { e = t }), cancel: e } }, t.exports = r
        },
        jmDH: function(t, e, n) { t.exports = !n("KUxP")(function() { return 7 != Object.defineProperty({}, "a", { get: function() { return 7 } }).a }) },
        kAMH: function(t, e, n) {
            var i = n("a0xu");
            t.exports = Array.isArray || function(t) { return "Array" == i(t) }
        },
        kTiW: function(t, e, n) { t.exports = n("NegM") },
        "kVK+": function(t, e) {
            e.read = function(t, e, n, i, r) {
                var a, o, s = 8 * r - i - 1,
                    l = (1 << s) - 1,
                    c = l >> 1,
                    u = -7,
                    h = n ? r - 1 : 0,
                    d = n ? -1 : 1,
                    f = t[e + h];
                for (h += d, a = f & (1 << -u) - 1, f >>= -u, u += s; 0 < u; a = 256 * a + t[e + h], h += d, u -= 8);
                for (o = a & (1 << -u) - 1, a >>= -u, u += i; 0 < u; o = 256 * o + t[e + h], h += d, u -= 8);
                if (0 === a) a = 1 - c;
                else {
                    if (a === l) return o ? NaN : 1 / 0 * (f ? -1 : 1);
                    o += Math.pow(2, i), a -= c
                }
                return (f ? -1 : 1) * o * Math.pow(2, a - i)
            }, e.write = function(t, e, n, i, r, a) {
                var o, s, l, c = 8 * a - r - 1,
                    u = (1 << c) - 1,
                    h = u >> 1,
                    d = 23 === r ? Math.pow(2, -24) - Math.pow(2, -77) : 0,
                    f = i ? 0 : a - 1,
                    m = i ? 1 : -1,
                    p = e < 0 || 0 === e && 1 / e < 0 ? 1 : 0;
                for (e = Math.abs(e), isNaN(e) || e === 1 / 0 ? (s = isNaN(e) ? 1 : 0, o = u) : (o = Math.floor(Math.log(e) / Math.LN2), e * (l = Math.pow(2, -o)) < 1 && (o--, l *= 2), 2 <= (e += 1 <= o + h ? d / l : d * Math.pow(2, 1 - h)) * l && (o++, l /= 2), u <= o + h ? (s = 0, o = u) : 1 <= o + h ? (s = (e * l - 1) * Math.pow(2, r), o += h) : (s = e * Math.pow(2, h - 1) * Math.pow(2, r), o = 0)); 8 <= r; t[n + f] = 255 & s, f += m, s /= 256, r -= 8);
                for (o = o << r | s, c += r; 0 < c; t[n + f] = 255 & o, f += m, o /= 256, c -= 8);
                t[n + f - m] |= 128 * p
            }
        },
        lCc8: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S, "Object", { create: n("oVml") })
        },
        ldVq: function(t, e, n) {
            var i = n("QMMT"),
                r = n("UWiX")("iterator"),
                a = n("SBuE");
            t.exports = n("WEpk").isIterable = function(t) { var e = Object(t); return void 0 !== e[r] || "@@iterator" in e || a.hasOwnProperty(i(e)) }
        },
        ln6h: function(t, e, n) { t.exports = n("ls82") },
        ls82: function(t, e, n) {
            var i = function(o) {
                "use strict";
                var l, t = Object.prototype,
                    u = t.hasOwnProperty,
                    e = "function" == typeof Symbol ? Symbol : {},
                    r = e.iterator || "@@iterator",
                    n = e.asyncIterator || "@@asyncIterator",
                    i = e.toStringTag || "@@toStringTag";

                function s(t, e, n, i) {
                    var a, o, s, l, r = e && e.prototype instanceof _ ? e : _,
                        c = Object.create(r.prototype),
                        u = new S(i || []);
                    return c._invoke = (a = t, o = n, s = u, l = d, function(t, e) {
                        if (l === m) throw new Error("Generator is already running");
                        if (l === p) { if ("throw" === t) throw e; return R() }
                        for (s.method = t, s.arg = e;;) {
                            var n = s.delegate;
                            if (n) { var i = x(n, s); if (i) { if (i === v) continue; return i } }
                            if ("next" === s.method) s.sent = s._sent = s.arg;
                            else if ("throw" === s.method) {
                                if (l === d) throw l = p, s.arg;
                                s.dispatchException(s.arg)
                            } else "return" === s.method && s.abrupt("return", s.arg);
                            l = m;
                            var r = h(a, o, s);
                            if ("normal" === r.type) { if (l = s.done ? p : f, r.arg === v) continue; return { value: r.arg, done: s.done } }
                            "throw" === r.type && (l = p, s.method = "throw", s.arg = r.arg)
                        }
                    }), c
                }

                function h(t, e, n) { try { return { type: "normal", arg: t.call(e, n) } } catch (t) { return { type: "throw", arg: t } } }
                o.wrap = s;
                var d = "suspendedStart",
                    f = "suspendedYield",
                    m = "executing",
                    p = "completed",
                    v = {};

                function _() {}

                function a() {}

                function c() {}
                var g = {};
                g[r] = function() { return this };
                var w = Object.getPrototypeOf,
                    b = w && w(w(F([])));
                b && b !== t && u.call(b, r) && (g = b);
                var y = c.prototype = _.prototype = Object.create(g);

                function k(t) {
                    ["next", "throw", "return"].forEach(function(e) { t[e] = function(t) { return this._invoke(e, t) } })
                }

                function I(l, c) {
                    var e;
                    this._invoke = function(n, i) {
                        function t() {
                            return new c(function(t, e) {
                                ! function e(t, n, i, r) {
                                    var a = h(l[t], l, n);
                                    if ("throw" !== a.type) {
                                        var o = a.arg,
                                            s = o.value;
                                        return s && "object" == typeof s && u.call(s, "__await") ? c.resolve(s.__await).then(function(t) { e("next", t, i, r) }, function(t) { e("throw", t, i, r) }) : c.resolve(s).then(function(t) { o.value = t, i(o) }, function(t) { return e("throw", t, i, r) })
                                    }
                                    r(a.arg)
                                }(n, i, t, e)
                            })
                        }
                        return e = e ? e.then(t, t) : t()
                    }
                }

                function x(t, e) {
                    var n = t.iterator[e.method];
                    if (n === l) {
                        if (e.delegate = null, "throw" === e.method) {
                            if (t.iterator.return && (e.method = "return", e.arg = l, x(t, e), "throw" === e.method)) return v;
                            e.method = "throw", e.arg = new TypeError("The iterator does not provide a 'throw' method")
                        }
                        return v
                    }
                    var i = h(n, t.iterator, e.arg);
                    if ("throw" === i.type) return e.method = "throw", e.arg = i.arg, e.delegate = null, v;
                    var r = i.arg;
                    return r ? r.done ? (e[t.resultName] = r.value, e.next = t.nextLoc, "return" !== e.method && (e.method = "next", e.arg = l), e.delegate = null, v) : r : (e.method = "throw", e.arg = new TypeError("iterator result is not an object"), e.delegate = null, v)
                }

                function A(t) {
                    var e = { tryLoc: t[0] };
                    1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), this.tryEntries.push(e)
                }

                function E(t) {
                    var e = t.completion || {};
                    e.type = "normal", delete e.arg, t.completion = e
                }

                function S(t) { this.tryEntries = [{ tryLoc: "root" }], t.forEach(A, this), this.reset(!0) }

                function F(e) {
                    if (e) {
                        var t = e[r];
                        if (t) return t.call(e);
                        if ("function" == typeof e.next) return e;
                        if (!isNaN(e.length)) {
                            var n = -1,
                                i = function t() {
                                    for (; ++n < e.length;)
                                        if (u.call(e, n)) return t.value = e[n], t.done = !1, t;
                                    return t.value = l, t.done = !0, t
                                };
                            return i.next = i
                        }
                    }
                    return { next: R }
                }

                function R() { return { value: l, done: !0 } }
                return a.prototype = y.constructor = c, c.constructor = a, c[i] = a.displayName = "GeneratorFunction", o.isGeneratorFunction = function(t) { var e = "function" == typeof t && t.constructor; return !!e && (e === a || "GeneratorFunction" === (e.displayName || e.name)) }, o.mark = function(t) { return Object.setPrototypeOf ? Object.setPrototypeOf(t, c) : (t.__proto__ = c, i in t || (t[i] = "GeneratorFunction")), t.prototype = Object.create(y), t }, o.awrap = function(t) { return { __await: t } }, k(I.prototype), I.prototype[n] = function() { return this }, o.AsyncIterator = I, o.async = function(t, e, n, i, r) { void 0 === r && (r = Promise); var a = new I(s(t, e, n, i), r); return o.isGeneratorFunction(e) ? a : a.next().then(function(t) { return t.done ? t.value : a.next() }) }, k(y), y[i] = "Generator", y[r] = function() { return this }, y.toString = function() { return "[object Generator]" }, o.keys = function(n) {
                    var i = [];
                    for (var t in n) i.push(t);
                    return i.reverse(),
                        function t() { for (; i.length;) { var e = i.pop(); if (e in n) return t.value = e, t.done = !1, t } return t.done = !0, t }
                }, o.values = F, S.prototype = {
                    constructor: S,
                    reset: function(t) {
                        if (this.prev = 0, this.next = 0, this.sent = this._sent = l, this.done = !1, this.delegate = null, this.method = "next", this.arg = l, this.tryEntries.forEach(E), !t)
                            for (var e in this) "t" === e.charAt(0) && u.call(this, e) && !isNaN(+e.slice(1)) && (this[e] = l)
                    },
                    stop: function() { this.done = !0; var t = this.tryEntries[0].completion; if ("throw" === t.type) throw t.arg; return this.rval },
                    dispatchException: function(n) {
                        if (this.done) throw n;
                        var i = this;

                        function t(t, e) { return a.type = "throw", a.arg = n, i.next = t, e && (i.method = "next", i.arg = l), !!e }
                        for (var e = this.tryEntries.length - 1; 0 <= e; --e) {
                            var r = this.tryEntries[e],
                                a = r.completion;
                            if ("root" === r.tryLoc) return t("end");
                            if (r.tryLoc <= this.prev) {
                                var o = u.call(r, "catchLoc"),
                                    s = u.call(r, "finallyLoc");
                                if (o && s) { if (this.prev < r.catchLoc) return t(r.catchLoc, !0); if (this.prev < r.finallyLoc) return t(r.finallyLoc) } else if (o) { if (this.prev < r.catchLoc) return t(r.catchLoc, !0) } else { if (!s) throw new Error("try statement without catch or finally"); if (this.prev < r.finallyLoc) return t(r.finallyLoc) }
                            }
                        }
                    },
                    abrupt: function(t, e) {
                        for (var n = this.tryEntries.length - 1; 0 <= n; --n) { var i = this.tryEntries[n]; if (i.tryLoc <= this.prev && u.call(i, "finallyLoc") && this.prev < i.finallyLoc) { var r = i; break } }
                        r && ("break" === t || "continue" === t) && r.tryLoc <= e && e <= r.finallyLoc && (r = null);
                        var a = r ? r.completion : {};
                        return a.type = t, a.arg = e, r ? (this.method = "next", this.next = r.finallyLoc, v) : this.complete(a)
                    },
                    complete: function(t, e) { if ("throw" === t.type) throw t.arg; return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && e && (this.next = e), v },
                    finish: function(t) { for (var e = this.tryEntries.length - 1; 0 <= e; --e) { var n = this.tryEntries[e]; if (n.finallyLoc === t) return this.complete(n.completion, n.afterLoc), E(n), v } },
                    catch: function(t) {
                        for (var e = this.tryEntries.length - 1; 0 <= e; --e) {
                            var n = this.tryEntries[e];
                            if (n.tryLoc === t) {
                                var i = n.completion;
                                if ("throw" === i.type) {
                                    var r = i.arg;
                                    E(n)
                                }
                                return r
                            }
                        }
                        throw new Error("illegal catch attempt")
                    },
                    delegateYield: function(t, e, n) { return this.delegate = { iterator: F(t), resultName: e, nextLoc: n }, "next" === this.method && (this.arg = l), v }
                }, o
            }(t.exports);
            try { regeneratorRuntime = i } catch (t) { Function("r", "regeneratorRuntime = r")(i) }
        },
        mqlF: function(t, e) { e.f = Object.getOwnPropertySymbols },
        nZgG: function(t, e, n) {
            var i = n("Y7ZC");
            i(i.S + i.F * !n("jmDH"), "Object", { defineProperties: n("fpC5") })
        },
        oVml: function(t, e, i) {
            function r() {}
            var a = i("5K7Z"),
                o = i("fpC5"),
                s = i("FpHa"),
                l = i("VVlx")("IE_PROTO"),
                c = "prototype",
                u = function() {
                    var t, e = i("Hsns")("iframe"),
                        n = s.length;
                    for (e.style.display = "none", i("MvwC").appendChild(e), e.src = "javascript:", (t = e.contentWindow.document).open(), t.write("<script>document.F=Object<\/script>"), t.close(), u = t.F; n--;) delete u[c][s[n]];
                    return u()
                };
            t.exports = Object.create || function(t, e) { var n; return null !== t ? (r[c] = a(t), n = new r, r[c] = null, n[l] = t) : n = u(), void 0 === e ? n : o(n, e) }
        },
        oc46: function(t, e, n) {
            function i(t, e, n) {
                var i = {},
                    r = s(function() { return !!l[t]() || "​" != "​" [t]() }),
                    a = i[t] = r ? e(h) : l[t];
                n && (i[n] = a), o(o.P + o.F * r, "String", i)
            }
            var o = n("Y7ZC"),
                r = n("Jes0"),
                s = n("KUxP"),
                l = n("5pKv"),
                a = "[" + l + "]",
                c = RegExp("^" + a + a + "*"),
                u = RegExp(a + a + "*$"),
                h = i.trim = function(t, e) { return t = String(r(t)), 1 & e && (t = t.replace(c, "")), 2 & e && (t = t.replace(u, "")), t };
            t.exports = i
        },
        "oh+g": function(t, e, n) {
            var i = n("WEpk"),
                r = i.JSON || (i.JSON = { stringify: JSON.stringify });
            t.exports = function(t) { return r.stringify.apply(r, arguments) }
        },
        oioR: function(t, e, n) {
            var d = n("2GTP"),
                f = n("sNwI"),
                m = n("NwJ3"),
                p = n("5K7Z"),
                v = n("tEej"),
                _ = n("fNZA"),
                g = {},
                w = {};
            (e = t.exports = function(t, e, n, i, r) {
                var a, o, s, l, c = r ? function() { return t } : _(t),
                    u = d(n, i, e ? 2 : 1),
                    h = 0;
                if ("function" != typeof c) throw TypeError(t + " is not iterable!");
                if (m(c)) {
                    for (a = v(t.length); h < a; h++)
                        if ((l = e ? u(p(o = t[h])[0], o[1]) : u(t[h])) === g || l === w) return l
                } else
                    for (s = c.call(t); !(o = s.next()).done;)
                        if ((l = f(s, u, o.value, e)) === g || l === w) return l
            }).BREAK = g, e.RETURN = w
        },
        p0XB: function(t, e, n) { t.exports = n("9BDd") },
        pD7o: function(t, e, n) {
            var i = n("S5KH");
            "string" == typeof i && (i = [
                [t.i, i, ""]
            ]);
            var r = { hmr: !0, transform: void 0, insertInto: void 0 };
            n("aET+")(i, r);
            i.locals && (t.exports = i.locals)
        },
        pLtp: function(t, e, n) { t.exports = n("iq4v") },
        pbKT: function(t, e, n) { t.exports = n("qijr") },
        q6LJ: function(t, e, n) {
            var s = n("5T2Y"),
                l = n("QXhf").set,
                c = s.MutationObserver || s.WebKitMutationObserver,
                u = s.process,
                h = s.Promise,
                d = "process" == n("a0xu")(u);
            t.exports = function() {
                function t() {
                    var t, e;
                    for (d && (t = u.domain) && t.exit(); n;) { e = n.fn, n = n.next; try { e() } catch (t) { throw n ? r() : i = void 0, t } }
                    i = void 0, t && t.enter()
                }
                var n, i, r;
                if (d) r = function() { u.nextTick(t) };
                else if (!c || s.navigator && s.navigator.standalone)
                    if (h && h.resolve) {
                        var e = h.resolve(void 0);
                        r = function() { e.then(t) }
                    } else r = function() { l.call(s, t) };
                else {
                    var a = !0,
                        o = document.createTextNode("");
                    new c(t).observe(o, { characterData: !0 }), r = function() { o.data = a = !a }
                }
                return function(t) {
                    var e = { fn: t, next: void 0 };
                    i && (i.next = e), n || (n = e, r()), i = e
                }
            }
        },
        qijr: function(t, e, n) { n("czwh"), t.exports = n("WEpk").Reflect.construct },
        rr1i: function(t, e) { t.exports = function(t, e) { return { enumerable: !(1 & t), configurable: !(2 & t), writable: !(4 & t), value: e } } },
        sNwI: function(t, e, n) {
            var a = n("5K7Z");
            t.exports = function(e, t, n, i) { try { return i ? t(a(n)[0], n[1]) : t(n) } catch (t) { var r = e.return; throw void 0 !== r && a(r.call(e)), t } }
        },
        tEej: function(t, e, n) {
            var i = n("Ojgd"),
                r = Math.min;
            t.exports = function(t) { return 0 < t ? r(i(t), 9007199254740991) : 0 }
        },
        tQ2B: function(t, e, h) {
            "use strict";
            var d = h("xTJ+"),
                f = h("Rn+g"),
                m = h("MLWZ"),
                p = h("g7np"),
                v = h("w0Vi"),
                _ = h("OTTw"),
                g = h("LYNF");
            t.exports = function(u) {
                return new Promise(function(n, i) {
                    var r = u.data,
                        a = u.headers;
                    d.isFormData(r) && delete a["Content-Type"];
                    var o = new XMLHttpRequest;
                    if (u.auth) {
                        var t = u.auth.username || "",
                            e = u.auth.password || "";
                        a.Authorization = "Basic " + btoa(t + ":" + e)
                    }
                    var s = p(u.baseURL, u.url);
                    if (o.open(u.method.toUpperCase(), m(s, u.params, u.paramsSerializer), !0), o.timeout = u.timeout, o.onreadystatechange = function() {
                            if (o && 4 === o.readyState && (0 !== o.status || o.responseURL && 0 === o.responseURL.indexOf("file:"))) {
                                var t = "getAllResponseHeaders" in o ? v(o.getAllResponseHeaders()) : null,
                                    e = { data: u.responseType && "text" !== u.responseType ? o.response : o.responseText, status: o.status, statusText: o.statusText, headers: t, config: u, request: o };
                                f(n, i, e), o = null
                            }
                        }, o.onabort = function() { o && (i(g("Request aborted", u, "ECONNABORTED", o)), o = null) }, o.onerror = function() { i(g("Network Error", u, null, o)), o = null }, o.ontimeout = function() {
                            var t = "timeout of " + u.timeout + "ms exceeded";
                            u.timeoutErrorMessage && (t = u.timeoutErrorMessage), i(g(t, u, "ECONNABORTED", o)), o = null
                        }, d.isStandardBrowserEnv()) {
                        var l = h("eqyj"),
                            c = (u.withCredentials || _(s)) && u.xsrfCookieName ? l.read(u.xsrfCookieName) : void 0;
                        c && (a[u.xsrfHeaderName] = c)
                    }
                    if ("setRequestHeader" in o && d.forEach(a, function(t, e) { void 0 === r && "content-type" === e.toLowerCase() ? delete a[e] : o.setRequestHeader(e, t) }), d.isUndefined(u.withCredentials) || (o.withCredentials = !!u.withCredentials), u.responseType) try { o.responseType = u.responseType } catch (t) { if ("json" !== u.responseType) throw t }
                    "function" == typeof u.onDownloadProgress && o.addEventListener("progress", u.onDownloadProgress), "function" == typeof u.onUploadProgress && o.upload && o.upload.addEventListener("progress", u.onUploadProgress), u.cancelToken && u.cancelToken.promise.then(function(t) { o && (o.abort(), i(t), o = null) }), void 0 === r && (r = null), o.send(r)
                })
            }
        },
        tXnD: function(t) { t.exports = JSON.parse('{"vertical_tip":"为了更好体验，请使用竖屏浏览","detect":"实时试色","import_img":"导入照片","long_press_img":"长按保存图片","loading_hard":"拼命加载中…","faceErrorText":{"1000":"未检测到人脸，请上传清晰的正面人脸图","1003":"人脸数不可大于2"},"file_limit":"文件只允许{0}格式","save_image_tip":"长按或截图保存图片","buy":"立即购买","add_to_car":"加入购物车","no_sku_tip":"商品/妆容不存在","custome_btn_tip":"由商家自定义操作函数","product_detail":"买同款","update_page":{"sorry":"抱歉","tip":"当前功能不可用，请更新系统后重试。","advice":"建议系统规格：","suggest_list":{"1":{"tag":"iPhone","text":"iOS V11 以上"},"2":{"tag":"Android","text":"Android OS 6.0、Chrome V60.0以上"},"3":{"tag":"Windows","text":"Chrome V60.0、Firefox V70、QQ浏览器 10.5.1(3824)、 Opera V64、Edge V44以上"},"4":{"tag":"macOS","text":"Chrome V60.0、Safari V11、Firefox V70、Opera V64以上"}}},"no_action_tip":"无法执行当前操作","select_modal":{"select":"选择试妆方式","import":"导入照片","detect":"实时试色","tip":"实时试色不可用，如需试妆请导入照片"},"switch_select":"切换试妆方式","no_camera_tip":"摄像头无法工作，请检查浏览器设置，确保已打开摄像头","no_sup_camera_tip":"设备浏览器不支持相机，请更换浏览器或设备","select_skus_tip":"请先选择商品","server_error":"服务端错误","horizontal_refresh":"请勿在横屏下操作相机权限，如需继续体验，请刷新重试","error_tip":"发生意外错误，请检查网络后刷新重试","err_page_tip":"显示异常，请杀掉Safari进程后重新扫码","no_product":"暂无商品","browser_tip":{"1":"请点击右上角“...”选择“在Safari中打开”","2":"请点击右上角“...”选择“在Chrome中打开”"},"error_msg":{"1":"当前处于选择模式，无法上妆","2":"sku id不存在","3":"妆容id不存在","4":"请先取消对比模式","5":"商品/妆容不存在","6":"无法执行当前操作","7":"授权未完成，请稍后再调用","100":"未知错误","101":"其他错误","300":"License已过期，请联系美图"}}') },
        tjUo: function(t, e, n) {
            "use strict";
            n.r(e), n.d(e, "default", function() { return en });
            var i = n("hfKm"),
                o = n.n(i),
                r = n("2Eek"),
                s = n.n(r),
                a = n("XoMD"),
                l = n.n(a),
                c = n("Jo+v"),
                u = n.n(c),
                h = n("4mXO"),
                d = n.n(h),
                f = n("pbKT"),
                m = n.n(f),
                p = n("pLtp"),
                b = n.n(p),
                v = n("xHqa"),
                _ = n.n(v),
                g = n("/HRN"),
                w = n.n(g),
                y = n("WaGi"),
                k = n.n(y),
                I = n("N9n2"),
                x = n.n(I),
                A = n("ZDA2"),
                E = n.n(A),
                S = n("/+P4"),
                F = n.n(S),
                R = n("9Jkg"),
                M = n.n(R),
                C = n("eVuF"),
                P = n.n(C),
                D = n("8+Nu"),
                T = n.n(D),
                z = n("ln6h"),
                L = n.n(z),
                B = n("+oT+"),
                O = n.n(B),
                W = (n("ZBjz"), n("JKvY"), n("LG64"), n("vDqi")),
                H = n.n(W),
                U = n("iZP3"),
                j = n.n(U),
                N = {},
                K = N.TiffTags = { 274: "Orientation" };

            function Z(t) { return t.exifdata }

            function G(n, i) {
                function e(t) {
                    var e = function(t) {
                        var e = new DataView(t);
                        if (255 != e.getUint8(0) || 216 != e.getUint8(1)) return !1;
                        var n = 2,
                            i = t.byteLength;
                        for (; n < i;) {
                            if (255 != e.getUint8(n)) return !1;
                            if (225 == e.getUint8(n + 1)) return Q(e, n + 4, e.getUint16(n + 2));
                            n += 2 + e.getUint16(n + 2)
                        }
                    }(t);
                    n.exifdata = e || {}, i && i.call(n)
                }
                if (window.FileReader && (n instanceof window.Blob || n instanceof window.File)) {
                    var t = new FileReader;
                    t.onload = function(t) { e(t.target.result) }, t.readAsArrayBuffer(n)
                }
            }

            function Y(t, e, n, i, r) {
                var a, o, s, l, c, u, h = t.getUint16(e + 2, !r),
                    d = t.getUint32(e + 4, !r),
                    f = t.getUint32(e + 8, !r) + n;
                switch (h) {
                    case 1:
                    case 7:
                        if (1 == d) return t.getUint8(e + 8, !r);
                        for (a = 4 < d ? f : e + 8, o = [], l = 0; l < d; l++) o[l] = t.getUint8(a + l);
                        return o;
                    case 2:
                        return V(t, a = 4 < d ? f : e + 8, d - 1);
                    case 3:
                        if (1 == d) return t.getUint16(e + 8, !r);
                        for (a = 2 < d ? f : e + 8, o = [], l = 0; l < d; l++) o[l] = t.getUint16(a + 2 * l, !r);
                        return o;
                    case 4:
                        if (1 == d) return t.getUint32(e + 8, !r);
                        for (o = [], l = 0; l < d; l++) o[l] = t.getUint32(f + 4 * l, !r);
                        return o;
                    case 5:
                        if (1 == d) return c = t.getUint32(f, !r), u = t.getUint32(f + 4, !r), (s = new Number(c / u)).numerator = c, s.denominator = u, s;
                        for (o = [], l = 0; l < d; l++) c = t.getUint32(f + 8 * l, !r), u = t.getUint32(f + 4 + 8 * l, !r), o[l] = new Number(c / u), o[l].numerator = c, o[l].denominator = u;
                        return o;
                    case 9:
                        if (1 == d) return t.getInt32(e + 8, !r);
                        for (o = [], l = 0; l < d; l++) o[l] = t.getInt32(f + 4 * l, !r);
                        return o;
                    case 10:
                        if (1 == d) return t.getInt32(f, !r) / t.getInt32(f + 4, !r);
                        for (o = [], l = 0; l < d; l++) o[l] = t.getInt32(f + 8 * l, !r) / t.getInt32(f + 4 + 8 * l, !r);
                        return o
                }
            }

            function V(t, e, n) { for (var i = "", r = e; r < e + n; r++) i += String.fromCharCode(t.getUint8(r)); return i }

            function Q(t, e) {
                if ("Exif" != V(t, e, 4)) return !1;
                var n, i = e + 6;
                if (18761 == t.getUint16(i)) n = !1;
                else {
                    if (19789 != t.getUint16(i)) return !1;
                    n = !0
                }
                if (42 != t.getUint16(i + 2, !n)) return !1;
                var r = t.getUint32(i + 4, !n);
                return !(r < 8) && function(t, e, n, i, r) {
                    var a, o, s = t.getUint16(n, !r),
                        l = {};
                    for (o = 0; o < s; o++) a = n + 12 * o + 2, l[i[t.getUint16(a, !r)]] = Y(t, a, e, 0, r);
                    return l
                }(t, i, i + r, K, n)
            }
            N.getData = function(t, e) { return !((t instanceof Image || t instanceof HTMLImageElement) && !t.complete) && (Z(t) ? e && e.call(t) : G(t, e), !0) }, N.getTag = function(t, e) { if (Z(t)) return t.exifdata[e] };
            var J, X = N;

            function q(t, e, n) {
                var i = 0 < arguments.length && void 0 !== t ? t : "",
                    r = 1 < arguments.length && void 0 !== e ? e : 3e3,
                    a = 2 < arguments.length ? n : void 0,
                    o = document.getElementById("mt-toast");
                o ? (at([o], "mtar__show"), (a ? rt : at)([o], "pc-toast")) : ((o = document.createElement("div")).id = "mt-toast", a && (o.className = "pc-toast"), document.querySelector(".mtar__preview-wrap").appendChild(o)), o.innerHTML = i, clearTimeout(J), setTimeout(function() { rt([o], "mtar__show") }, 50), J = setTimeout(function() { at([o], "mtar__show") }, r)
            }
            var $ = n("d04V"),
                tt = n.n($);
            n("A8zu");

            function et(t) { return "[object Array]" == Object.prototype.toString.call(t) }
            var nt = {
                    one: function(t, e) { try { return (e || document).querySelector(t) || void 0 } catch (t) { return } },
                    all: function(t, e) { try { var n = (e || document).querySelectorAll(t); return tt()(n) } catch (t) { return [] } },
                    addClass: function(t, e) { if (t) { et(t) || (t = [t]); for (var n = 0; n < t.length; n++) { var i = (t[n].className || "").split(" "); - 1 < i.indexOf(e) || (i.push(e), t[n].className = i.join(" ")) } } },
                    removeClass: function(t, e) {
                        if (t) {
                            et(t) || (t = [t]);
                            for (var n = 0; n < t.length; n++) {
                                for (var i = t[n].className.split(" "), r = 0; r < i.length; r++) i[r] == e && (i[r] = "");
                                t[n].className = i.join(" ").trim()
                            }
                        }
                    },
                    hasClass: function(t, e) { return !(!t || !t.classList) && t.classList.contains(e) },
                    bind: function(t, e, n, i) { t && (et(t) || (t = [t]), t.forEach(function(t) { t.addEventListener(e, n, !!i) })) },
                    delegate: function(r, t, a, o) {
                        r && r.addEventListener(t, function(t) {
                            var e = nt.all(a, r);
                            if (e) t: for (var n = 0; n < e.length; n++)
                                for (var i = t.target; i;) { if (i == e[n]) { o.call(i, t); break t } if ((i = i.parentNode) == r) break }
                        }, !1)
                    }
                },
                it = nt;

            function rt(t, e) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.classList ? i.classList.add(e) : i.className += " ".concat(e)
                }
            }

            function at(t, e) {
                for (var n = 0; n < t.length; n++) {
                    var i = t[n];
                    i.classList ? i.classList.remove(e) : i.className = i.className.replace(new RegExp("(^|\\b)" + e.split(" ").join("|") + "(\\b|$)", "gi"), " ")
                }
            }

            function ot() { try { return "undefined" != typeof WebAssembly && "object" === ("undefined" == typeof WebAssembly ? "undefined" : j()(WebAssembly)) } catch (t) { console.log("isWebAssemblyEnabled", t) } return !1 }
            var st = function() { return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream },
                lt = function() { return !document.documentMode && !!window.StyleMedia };

            function ct() {
                var t = !1;
                try {
                    if (st()) t = -1 === navigator.userAgent.indexOf("11_2_2") && -1 === navigator.userAgent.indexOf("11_2_5") && -1 === navigator.userAgent.indexOf("11_2_6") && ot();
                    else if (lt()) { 1717134 < Math.trunc(1e5 * window.navigator.userAgent.toLowerCase().match(/edge\/((\d+\.?)*)/)[1]) && (t = !0) } else t = ot();
                    t = t && function() {
                        var t = !1,
                            e = null;
                        try { if ("object" === ("undefined" == typeof WebAssembly ? "undefined" : j()(WebAssembly)) && "function" == typeof WebAssembly.instantiate) e = new WebAssembly.Module(Uint8Array.of(0, 97, 115, 109, 1, 0, 0, 0, 1, 6, 1, 96, 1, 127, 1, 127, 3, 2, 1, 0, 5, 3, 1, 0, 1, 7, 8, 1, 4, 116, 101, 115, 116, 0, 0, 10, 16, 1, 14, 0, 32, 0, 65, 1, 54, 2, 0, 32, 0, 40, 2, 0, 11)), new WebAssembly.Instance(e, {}).exports.test(4) && (t = !0), e = null } catch (t) { console.log("error : ", t), e = null }
                        return t
                    }()
                } catch (t) { console.log("isSupportWasm error:", t) }
                return t
            }

            function ut() { var t, e, n = {}; return n.webgl = Number((t = document.getElementById("mtar__mainCanvas") || document.createElement("canvas"), e = t.getContext("webgl") || t.getContext("experimental-webgl") || t.getContext("moz-webgl") || t.getContext("webkit-3d"), console.log("webgl=".concat(!!e)), !!e)), n.asm = Number(function() { try { return !0 } catch (t) { console.log("[ERROR] NOT SUPPORT ASM.JS") } return !1 }()), n.wasm = Number(ct()), n }

            function ht(t) {
                var e = !1;
                try {
                    var n = document.getElementById(t),
                        i = n.getContext("webgl") || n.getContext("experimental-webgl") || n.getContext("moz-webgl") || n.getContext("webkit-3d");
                    if (void 0 !== i) {
                        var r = i.getExtension("OES_texture_float"),
                            a = i.getExtension("OES_texture_half_float");
                        r && a && (e = !0)
                    }
                } catch (t) { console.log("isSupportWebGLExtensions : ", t) }
                return e
            }

            function dt(s) {
                return new P.a(function(t, n) {
                    var e = ct(),
                        i = "cpu" === s ? "_CPU" : "",
                        r = "https://makeup-magic.zone1.meitudata.com/webar/release/H5/1.2.5" + "/H5/NativeModule".concat(i, "/wasm/RenderModule.js?_=").concat(4),
                        a = "https://makeup-magic.zone1.meitudata.com/webar/release/H5/1.2.5" + "/H5/NativeModule".concat(i, "/asmjs/RenderModule.js?_=").concat(4),
                        o = document.createElement("script");
                    o.setAttribute("src", e ? r : a), o.onload = function() { t() }, o.onerror = function(t) {
                        var e = window.mtar__i18n.t("error_tip");
                        xt(e), n(t || e)
                    }, document.getElementsByTagName("head")[0].appendChild(o)
                })
            }

            function ft(t, e) { e = e || window.location.href, t = t.replace(/[\[\]]/g, "\\$&"); var n = new RegExp("[?&]" + t + "(=([^&#]*)|&|#|$)").exec(e); return n ? n[2] ? decodeURIComponent(n[2].replace(/\+/g, " ")) : "" : null }

            function mt(t, e) {
                for (var n = [], i = 0; i < t.length; i++) {
                    var r = t[i].name,
                        a = r.split(".");
                    if (!("/" === r || a.length <= 1)) {
                        var o = a[a.length - 1].toLowerCase(),
                            s = ["jpg", "jpeg", "png"];
                        if (0 <= s.indexOf(o)) n.push(t[i]);
                        else q(e.t("file_limit").replace("{0}", s.join("、")))
                    }
                }
                return n
            }

            function pt(e, u) {
                return new P.a(function(l) {
                    var c = null;
                    X.getData(e, function() {
                        c = X.getTag(this, "Orientation"), console.log("orientation=", c);
                        var t = new FileReader;
                        t.onload = function(t) {
                            var a = document.createElement("canvas"),
                                o = a.getContext("2d");
                            o.imageSmoothingEnabled = !0, o.imageSmoothingQuality = "low";
                            var s = new Image;
                            s.onload = function() {
                                var t = s.width,
                                    e = s.height;
                                console.log("原图尺寸", t, "-", e);
                                var n, i = t,
                                    r = e;
                                e < t && 4e3 < t ? r = (i = 4e3) * e / t : t < e && 4e3 < e && (i = (r = 4e3) * t / e), new P.a(function(t) {
                                    if (void 0 === n) {
                                        var e = new Image;
                                        e.onload = function() { n = 1 === e.width && 2 === e.height, t(n) }, e.src = "data:image/jpeg;base64,/9j/4QAiRXhpZgAATU0AKgAAAAgAAQESAAMAAAABAAYAAAAAAAD/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIAAEAAgMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAALEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwA/8H//2Q=="
                                    } else t(n)
                                }).then(function(t) {
                                    if (t) a.width = i, a.height = r, o.drawImage(s, 0, 0, i, r);
                                    else switch (c) {
                                        case 3:
                                            a.width = i, a.height = r, o.rotate(Math.PI), o.drawImage(s, -i, -r, i, r);
                                            break;
                                        case 6:
                                            a.width = r, a.height = i, o.rotate(Math.PI / 2), o.drawImage(s, 0, -r, i, r);
                                            break;
                                        case 8:
                                            a.width = r, a.height = i, o.rotate(3 * Math.PI / 2), o.drawImage(s, -i, 0, i, r);
                                            break;
                                        default:
                                            a.width = i, a.height = r, o.drawImage(s, 0, 0, i, r)
                                    }
                                    if (console.log("fileProcess:", a.width, "----", a.height, "----", u), a.height < 1.5 * u) {
                                        var e = a.toDataURL("image/jpeg", 1);
                                        l(e)
                                    } else {
                                        var n = function t(e, n) {
                                            var i = e.height / n;
                                            console.log("ratio=", i);
                                            if (i <= 1 / .7) return e;
                                            var r = e.width;
                                            var a = e.height;
                                            var o = Math.floor(.7 * r);
                                            var s = Math.floor(.7 * a);
                                            o = 2 * Math.floor((o - 1) / 2);
                                            s = 2 * Math.floor((s - 1) / 2);
                                            console.log(o, "---", s);
                                            var l = document.createElement("canvas");
                                            var c = l.getContext("2d");
                                            l.width = o;
                                            l.height = s;
                                            c.imageSmoothingEnabled = !0;
                                            c.imageSmoothingQuality = "high";
                                            c.drawImage(e, 0, 0, r, a, 0, 0, o, s);
                                            return 1.2 * n < s ? t(l, n) : l
                                        }(a, u).toDataURL("image/jpeg", 1);
                                        l(n)
                                    }
                                })
                            }, s.src = t.target.result
                        }, t.readAsDataURL(e)
                    })
                })
            }

            function vt(t, e) { var n = Math.pow(10, e); return Math.round(t * n, 10) / n }

            function _t(t, e) { t.scrollTo ? t.scrollTo({ left: e, behavior: "smooth" }) : t.scrollLeft = e }

            function gt(t) {
                var e = 0 < arguments.length && void 0 !== t ? t : options,
                    n = e.tabList,
                    i = e.l,
                    r = e.scrW,
                    a = e.tabW,
                    o = e.tabIconL,
                    s = e.tabIconR;
                _t(n, i), it.removeClass(o, "disabled"), it.removeClass(s, "disabled"), i <= 0 && it.addClass(o, "disabled"), r <= i + a && it.addClass(s, "disabled")
            }

            function wt(t) {
                var e, n = 0 < arguments.length && void 0 !== t ? t : options,
                    i = n.tabList,
                    r = n.scrW,
                    a = n.tabW,
                    o = n.tabIconL,
                    s = n.tabIconR,
                    l = n.dir,
                    c = n.w;
                if ("right" === l) {
                    if (it.hasClass(s, "disabled")) return;
                    e = i.scrollLeft + c
                } else {
                    if (it.hasClass(o, "disabled")) return;
                    e = i.scrollLeft - c
                }
                gt({ tabList: i, l: e, scrW: r, tabW: a, tabIconL: o, tabIconR: s })
            }

            function bt(t, e) { e || t.setAttribute("style", "display: none;") }

            function yt() { return document.documentElement.clientHeight < document.documentElement.clientWidth }

            function kt(t, e, n) {
                var o;
                n && ("base64" === t ? n(null, e) : (o = e, new P.a(function(t) {
                    for (var e = o.split(","), n = e[0].match(/:(.*?)/)[1], i = atob(e[1]), r = i.length, a = new Uint8Array(r); r--;) a[r] = i.charCodeAt(r);
                    t(new Blob([a], { type: n }))
                }).then(function(t) { n(null, t) }).catch(function() { console.error("base64ToBlob error"), n("base64ToBlob error") })))
            }

            function It(t, e, n) {
                var i = "string" == typeof t ? it.one(t) : t;
                n ? it.addClass(i, e) : it.removeClass(i, e)
            }

            function xt(t) { it.one(".js__mtar-modal-tip-text").innerText = t, it.addClass(it.one(".js__mtar-modal-tip"), "mtar__show"), it.addClass(it.one(".mtar__video-wrap"), "mtar__hide"), it.removeClass(it.one(".mtar__loading"), "mtar__show") }

            function At(e, t) {
                var n = b()(e);
                if (d.a) {
                    var i = d()(e);
                    t && (i = i.filter(function(t) { return u()(e, t).enumerable })), n.push.apply(n, i)
                }
                return n
            }
            H.a.interceptors.response.use(function(t) { return t }, function(t) { console.log("-----error", t); var e = "string" != typeof t && t && t.message ? t.message : t; return "string" == typeof e && -1 !== e.indexOf("Network Error") && xt(window.mtar__i18n && window.mtar__i18n.t("error_tip") || "Error"), t });
            var Et = H.a.create({ baseURL: "https://api.mplus.meitu.com", timeout: 6e4, headers: {} }),
                St = function(t) {
                    var i = t.url || "",
                        r = t.type || "GET",
                        a = t.data || {};
                    return t.noAuth || (Et.defaults.headers.common.token = localStorage.getItem("_token")), new P.a(function(e, n) {
                        var t = "get" === (r = r.toLowerCase()) ? {
                            params: function(e) {
                                for (var t = 1; t < arguments.length; t++) {
                                    var n = null != arguments[t] ? arguments[t] : {};
                                    t % 2 ? At(Object(n), !0).forEach(function(t) { _()(e, t, n[t]) }) : l.a ? s()(e, l()(n)) : At(Object(n)).forEach(function(t) { o()(e, t, u()(n, t)) })
                                }
                                return e
                            }({}, a)
                        } : a;
                        Et[r](i, t).then(function(t) { return e(t && t.data) }).catch(function(t) { return n(t) })
                    })
                },
                Ft = H.a;

            function Rt(r, a, o, s) {
                var l, c = !1,
                    u = 0;

                function h() { l && clearTimeout(l) }

                function t() {
                    var t = this,
                        e = Date.now() - u,
                        n = arguments;

                    function i() { u = Date.now(), o.apply(t, n) }
                    c || (s && !l && i(), h(), void 0 === s && r < e ? i() : !0 !== a && (l = setTimeout(s ? function() { l = void 0 } : i, void 0 === s ? r - e : r)))
                }
                return "boolean" != typeof a && (s = o, o = a, a = void 0), t.cancel = function() { h(), c = !0 }, t
            }
            var Mt = n("yDJ3"),
                Ct = n.n(Mt);

            function Pt() {
                var i, r, t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
                this.lang = t.lang || "en", this.fallbackLocale = t.fallbackLocale || "en", this.messages = (i = n("JQAc"), r = {}, i.keys().forEach(function(t) {
                    var e = t.match(/([A-Za-z0-9-_]+)\./i);
                    if (e && 1 < e.length) {
                        var n = e[1];
                        r[n] = i(t)
                    }
                }), r)
            }
            Pt.prototype.t = function(t, e) {
                var n = Ct()(this.messages[this.lang], t),
                    i = Ct()(this.messages[this.fallbackLocale], t),
                    r = n;
                if (void 0 === n && (r = i || ""), e)
                    for (var a in e) e.hasOwnProperty(a) && (r = r.replace(new RegExp("{".concat(a, "}"), "g"), e[a]));
                return r
            };
            var Dt = Pt,
                Tt = n("XE39"),
                zt = n.n(Tt),
                Lt = window.navigator.userAgent.toLowerCase(),
                Bt = window.navigator.platform,
                Ot = { iOS: /(iPhone|iPad|iPod|iOS)/gi.test(Lt), Android: /android|adr/gi.test(Lt), Mobile: /(iPhone|iPad|iPod|iOS|Android|adr|Windows Phone|SymbianOS)/gi.test(Lt), Weibo: /(weibo)/gi.test(Lt), WeChat: "micromessenger" == Lt.match(/MicroMessenger/gi), QQ: /qq\//gi.test(Lt), Qzone: -1 !== Lt.indexOf("qzone/"), Meitu: /(com.meitu|bec.meitu|bec.wallet)/gi.test(Lt), Meipai: /meipaimv|meipai|com.meitu.mtmv/gi.test(Lt), Meipu: /com.meitu.meipu/gi.test(Lt), Xiuxiu: /(com.meitu.mtxx)/gi.test(Lt), Meiyan: /(com.meitu.myxj|com.meitu.meiyancamera)/gi.test(Lt), Makeup: /com.meitu.makeup/gi.test(Lt), Selfilecity: /com.meitu.wheecam/gi.test(Lt), Beautyme: /com.meitu.zhi.beauty/gi.test(Lt), Shanliao: /(com.meitu.shanliao|com.meitu.testwheetalk)/gi.test(Lt), Twitter: /Twitter/gi.test(Lt), Facebook: /fbav/gi.test(Lt), Line: /line\//gi.test(Lt), Youyan: /com.meitu.youyanvideo/gi.test(Lt), Yumyum: /com.meitu.yumyum/gi.test(Lt), Mac: /Mac/gi.test(Bt), Windows: /Win/gi.test(Bt), Safari: !(!/safari/i.test(Lt) || /chrome/i.test(Lt) || /crios/i.test(Lt) || /fxios/i.test(Lt)), IE: !!document.documentMode, Edge: !document.documentMode && !!window.StyleMedia },
                Wt = n("XXOK"),
                Ht = n.n(Wt),
                Ut = n("p0XB"),
                jt = n.n(Ut),
                Nt = n("XVgq"),
                Kt = n.n(Nt),
                Zt = n("Z7t5"),
                Gt = n.n(Zt),
                Yt = n("6BQ9"),
                Vt = n.n(Yt),
                Qt = n("Cg2A"),
                Jt = n.n(Qt),
                Xt = n("KhLd"),
                qt = n.n(Xt);

            function $t(t) {
                if (void 0 === Gt.a || null == t[Kt.a]) {
                    if (jt()(t) || (t = function(t, e) { if (!t) return; if ("string" == typeof t) return te(t, e); var n = Object.prototype.toString.call(t).slice(8, -1); "Object" === n && t.constructor && (n = t.constructor.name); if ("Map" === n || "Set" === n) return tt()(n); if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return te(t, e) }(t))) {
                        var e = 0,
                            n = function() {};
                        return { s: n, n: function() { return e >= t.length ? { done: !0 } : { done: !1, value: t[e++] } }, e: function(t) { throw t }, f: n }
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }
                var i, r, a = !0,
                    o = !1;
                return { s: function() { i = Ht()(t) }, n: function() { var t = i.next(); return a = t.done, t }, e: function(t) { o = !0, r = t }, f: function() { try { a || null == i.return || i.return() } finally { if (o) throw r } } }
            }

            function te(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i
            }

            function ee() {}
            var ne = [{ key: "Foundation", name: "Foundation", layer: 1 }, { key: "MakeupPacket", name: "MakeupPacket", layer: 2 }, { key: "Bronzer", name: "Bronzer", layer: 3 }, { key: "Blusher", name: "Blusher", layer: 4 }, { key: "Lipstick", name: "Lipstick", layer: 5 }, { key: "EyePupil", name: "EyePupil", layer: 6 }, { key: "EyeShadow", name: "EyeShadow", layer: 7 }, { key: "EyeLiner", name: "EyeLiner", layer: 8 }, { key: "EyeLash", name: "EyeLash", layer: 9 }, { key: "EyeBrow", name: "EyeBrow", layer: 10 }, { key: "Makeup3D", name: "Makeup3D", layer: 11 }, { key: "HairDye", name: "HairDye", layer: 12 }],
                ie = function() {
                    function a(t, e, n) {
                        var i = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
                            r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
                        w()(this, a), this.supportWasm = !1, this.modulePack = t, this.moduleInstance = null, this.pageInstance = e, this.canvasDomId = null, this.runPluginInstance = !1, this.fdWorker = null, this.fdWorkerInitialized = !1, this.optionEnableDenoise = 1, this.optionEnableFaceDetect = !0, this.optionEnableFaceDetectRun = !0, this.optionEnableLog = !1, this.optionEnableAR = !1, this.optionEnableARRun = !0, this.optionEnableDrawFaces = i, this.centerMode = r, this.optionEnablePrintTime = !1, this.faTime = new oe(50), this.fdTime = new oe(1), this.dl3dTime = new oe(100), this.hairsegmentTime = new oe(40), this.optionEnableDl3DPos = !0, this.optionEnableDl3DPosRun = !1, this.optionEnableHairSegment = !0, this.optionEnableHairSegmentRun = !1, this.faceDetectIntervalTimeNoFace = 1e3, this.faceDetectIntervalTimeHasFace = 3e3, this.moduleInitialized = !1, this.renderContextInitialized = !1, this.nativeImagePtr = null, this.nativeImageSize = 0, this.nativeImageWidth = 0, this.nativeImageHeight = 0, this.nativeImageNeedWarp = !1, this.nativeImageWarpTempPtr = null, this.nativeImageWarpTempSize = 0, this.nativeImageWarpTempWidth = 0, this.nativeImageWarpTempHeight = 0, this.nativeRenderModuleInstance = 0, this.nativeARModuleInstance = 0, this.nativeARModuleInitialized = !1, this.nativeFaceBoundPtr = null, this.nativeFacePosePtr = null, this.nativeFaceAlignmentInstance = 0, this.nativeFaceAlignmentInitialized = !1, this.nativeDl3DPosInstance = 0, this.nativeDl3DPosInitialized = !1, this.nativeHairSegmentInstance = 0, this.nativeHairSegmentInitialized = !1, this.nativeFaceDetectInstance = 0, this.nativeFaceDetectInitialized = !1, this.lastFaceDetectTimestamp = null, this.lastFaceDetectHasFace = !1, this.faceCount = 0, this.nativeARKernelPacketInstance = 0, this.nativeStaticPtr = null, this.configContextDic = {}, this.beautyKey = "default_beauty", this.filterKey = "default_filter", this.faceliftKey = "default_facelift", this.stereoscopicKey = "default_stereoscopic", this.skinRetouch = 1, this.whitening = 1, this.sharpen = 1, this.layout_x = 0, this.layout_y = 0, this.layout_w = 0, this.layout_h = 0
                    }
                    return k()(a, [{ key: "tryCatchFn", value: function(t) { try { return t() } catch (t) { return { code: -1, data: null, msg: t } } } }, { key: "setRunPluginInstance", value: function(t) { this.runPluginInstance = t } }, { key: "getRunPluginInstance", value: function() { return this.runPluginInstance } }, { key: "setOptionEnableAR", value: function(t) { this.optionEnableAR = t } }, { key: "setOptionEnableARRun", value: function(t) { this.nativeRenderModuleInstance && this.nativeARModuleInstance || console.warn("Please initialize RenderModule and ARModule first!"), t ? this.moduleInstance._RenderModule_BindARInstance(this.nativeRenderModuleInstance, this.nativeARModuleInstance) : this.moduleInstance._RenderModule_UnBindARInstance(this.nativeRenderModuleInstance, this.nativeARModuleInstance) } }, { key: "getModuleState", value: function() { return this.moduleInitialized } }, {
                        key: "addARGroupKey",
                        value: function(t) {
                            var c = this,
                                u = 0 < arguments.length && void 0 !== t ? t : [];
                            return this.moduleInitialized && this.renderContextInitialized ? this.tryCatchFn(function() {
                                var t, e = [],
                                    n = $t(u = u || ne);
                                try {
                                    for (n.s(); !(t = n.n()).done;) {
                                        var i = t.value,
                                            r = i.key,
                                            a = i.name,
                                            o = i.layer;
                                        if (r !== c.beautyKey && r !== c.filterKey && r !== c.faceliftKey && r !== c.stereoscopicKey) {
                                            var s = c.stringToNativePtr(r),
                                                l = c.stringToNativePtr(a);
                                            if (0 === c.moduleInstance._ARModule_HasGroupKey(c.nativeARModuleInstance, s)) e.push({ key: r, msg: "ar key already exist" });
                                            else 0 !== c.moduleInstance._ARModule_AddGroupKey(c.nativeARModuleInstance, s, l, o) && e.push({ key: r, msg: "set ar key failed" });
                                            c.moduleInstance._free(s), c.moduleInstance._free(l)
                                        }
                                    }
                                } catch (t) { n.e(t) } finally { n.f() }
                                return { code: 0, data: { failedList: e || [] }, msg: "add ar key group complete" }
                            }) : { code: -1, data: null, msg: "ar class instance renderContext not initialed yet" }
                        }
                    }, {
                        key: "removeARGroupKey",
                        value: function(a) {
                            var o = this;
                            return this.moduleInitialized && this.renderContextInitialized ? this.tryCatchFn(function() {
                                var t, e = [],
                                    n = $t(a = a || []);
                                try {
                                    for (n.s(); !(t = n.n()).done;) {
                                        var i = t.value;
                                        if (i !== o.beautyKey && i !== o.filterKey && i !== o.faceliftKey && i !== o.stereoscopicKey) {
                                            var r = o.stringToNativePtr(i);
                                            if (0 === o.moduleInstance._ARModule_HasGroupKey(o.nativeARModuleInstance, r)) 0 !== o.moduleInstance._ARModule_RemoveGroupKey(o.nativeARModuleInstance, r) && e.push({ key: i, msg: "remove ar key failed" });
                                            else e.push({ key: i, msg: "ar key not exist" });
                                            o.moduleInstance._free(r)
                                        }
                                    }
                                } catch (t) { n.e(t) } finally { n.f() }
                                return { code: 0, data: { failedList: e || [] }, msg: "remove ar key group complete" }
                            }) : { code: -1, msg: "ar class instance renderContext not initialed yet" }
                        }
                    }, { key: "hasARGroupKey", value: function(e) { var n = this; return this.tryCatchFn(function() { if (n.nativeARModuleInitialized) { var t = n.stringToNativePtr(e); return 0 === n.moduleInstance._ARModule_HasGroupKey(n.nativeARModuleInstance, t) ? { code: 0, msg: "ar key exist" } : { code: 1, msg: "ar key not exist" } } return { code: -1, msg: "ar class instance AR not initialed yet" } }) } }, { key: "resetFaces", value: function() { this.nativeFaceAlignmentInstance && (this.moduleInstance._FAModule_Reset(this.nativeFaceAlignmentInstance), this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null)) } }, { key: "setPluginRunInstance", value: function(n) { var i = this; return new P.a(function(t, e) { n ? i._internalSetPluginRunInstance(n, t, e) : (i.setRunPluginInstance(n), t({ code: 0, data: null, msg: "stop plugin instance success" })) }) } }, {
                        key: "_internalSetPluginRunInstance",
                        value: function(t, e, n) {
                            if (this.renderContextInitialized && this.moduleInitialized && this.nativeFaceAlignmentInitialized && this.nativeFaceDetectInitialized) !this.optionEnableAR || this.nativeARModuleInitialized ? (this.setRunPluginInstance(t), e({ code: 0, data: null, msg: "run plugin instance success" })) : n({ code: -1, data: null, msg: "ARKernelResourceBundle not initialized" });
                            else {
                                var i = "setPluginRunInstance failed";
                                this.renderContextInitialized ? this.moduleInitialized ? this.nativeFaceAlignmentInitialized ? this.nativeFaceDetectInitialized ? this.nativeARModuleInitialized || (i = "ARKernelResourceBundle not initialized") : i = "FD model not initialized" : i = "FA model not initialized" : i = "Memory data not initialized" : i = "GL environment not initialized", n({ code: -1, data: null, msg: i })
                            }
                        }
                    }, {
                        key: "getVersion",
                        value: function(t) {
                            var e = 0 < arguments.length && void 0 !== t ? t : "_NativeModule_GetVersion",
                                n = this.moduleInstance._malloc(64),
                                i = this.moduleInstance[e](n, 64),
                                r = this.nativePtrToString(n, i);
                            return this.moduleInstance._free(n), r
                        }
                    }, {
                        key: "initNativeOnline",
                        value: function(t, e) {
                            if (null !== t) {
                                var n = this.stringToNativePtr(t);
                                this.moduleInstance._NativeModule_InitializeOnLine(n, e), this.moduleInstance._free(n)
                            }
                        }
                    }, {
                        key: "_initContextInternal",
                        value: function(t, e) {
                            try {
                                var n = this.getVersion();
                                console.log("[native version]: ", n);
                                var i = this.getVersion("_FDModule_GetVersion");
                                console.log("[mtface version]: ", i);
                                var r = this.getVersion("_ARModule_GetVersion");
                                console.log("[ar version]    : ", r), this.moduleInstance._NativeModule_EnableLog(this.optionEnableLog), this.nativeRenderModuleInstance = this.moduleInstance._RenderModule_CreateInstance(), this.moduleInstance._RenderModule_SetOptions(this.nativeRenderModuleInstance, this.optionEnableDrawFaces, this.centerMode), this.moduleInstance._RenderModule_Initialize(this.nativeRenderModuleInstance), this.optionEnableAR && (this.nativeARModuleInstance = this.moduleInstance._ARModule_CreateInstance(), this.moduleInstance._RenderModule_BindARInstance(this.nativeRenderModuleInstance, this.nativeARModuleInstance)), this.nativeFaceBoundPtr = this.moduleInstance._malloc(128), this.nativeFacePosePtr = this.moduleInstance._malloc(96), this.nativeFaceAlignmentInstance = this.moduleInstance._FAModule_CreateInstance(), this.nativeFaceDetectInstance = this.moduleInstance._FDModule_CreateInstance(), this.optionEnableDl3DPos && (this.nativeDl3DPosInstance = this.moduleInstance._Dl3DPosModule_CreateInstance()), this.optionEnableHairSegment && (this.nativeHairSegmentInstance = this.moduleInstance._HairSegmentModule_CreateInstance()), this.renderContextInitialized = !0, t({ code: 0, data: null, msg: "init render context success" })
                            } catch (t) { e({ code: -1, data: null, msg: "init render context failed " + t }) }
                        }
                    }, {
                        key: "setNativeImage",
                        value: function(t, e, n) {
                            var i = t;
                            if (i.byteLength && (i = new Uint8Array(i)), this.nativeImageNeedWarp) {
                                (null === t || e < 0 || n < 0 || e !== this.nativeImageWarpTempWidth || n !== this.nativeImageWarpTempHeight) && (null !== this.nativeImagePtr && (this.moduleInstance._free(this.nativeImagePtr), this.nativeImagePtr = null), this.nativeImageWidth = 0, this.nativeImageHeight = 0, null !== this.nativeImageWarpTempPtr && (this.moduleInstance._free(this.nativeImageWarpTempPtr), this.nativeImageWarpTempPtr = null), this.nativeImageWarpTempWidth = 0, this.nativeImageWarpTempHeight = 0), null === this.nativeImageWarpTempPtr && (this.nativeImageWarpTempSize = e * n * 4, this.nativeImageWarpTempWidth = e, this.nativeImageWarpTempHeight = n, this.nativeImageWarpTempPtr = this.moduleInstance._malloc(this.nativeImageWarpTempSize)), null === this.nativeImagePtr && (this.nativeImageSize = e * n * 4, this.nativeImageWidth = e, this.nativeImageHeight = n, this.nativeImagePtr = this.moduleInstance._malloc(this.nativeImageSize)), this.moduleInstance.HEAPU8.set(i, this.nativeImageWarpTempPtr);
                                var r = new Date,
                                    a = this.moduleInstance._NativeModule_WarpFrame(this.nativeImageWarpTempPtr, this.nativeImageWarpTempWidth, this.nativeImageWarpTempHeight, 4 * this.nativeImageWarpTempWidth, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth),
                                    o = new Date;
                                this.optionEnablePrintTime && console.log("_NativeModule_WarpFrame use time: " + (o - r) + " ms"), 0 !== a && console.log("_NativeModule_WarpFrame = " + a)
                            } else(null === t || e < 0 || n < 0) && (null !== this.nativeImagePtr && (this.moduleInstance._free(this.nativeImagePtr), this.nativeImagePtr = null), this.nativeImageWidth = 0, this.nativeImageHeight = 0), e === this.nativeImageWidth && n === this.nativeImageHeight || (null !== this.nativeImagePtr && (this.moduleInstance._free(this.nativeImagePtr), this.nativeImagePtr = null), this.nativeImageWidth = 0, this.nativeImageHeight = 0), null === this.nativeImagePtr && (this.nativeImageSize = e * n * 4, this.nativeImageWidth = e, this.nativeImageHeight = n, this.nativeImagePtr = this.moduleInstance._malloc(this.nativeImageSize)), this.moduleInstance.HEAPU8.set(i, this.nativeImagePtr);
                            var s = this.moduleInstance._RenderModule_GetFrameLayout(this.nativeRenderModuleInstance, e, n);
                            this.layout_x = Math.round(this.moduleInstance.HEAPF32[s >> 2]), this.layout_y = Math.round(this.moduleInstance.HEAPF32[1 + (s >> 2)]), this.layout_w = Math.round(this.moduleInstance.HEAPF32[2 + (s >> 2)]), this.layout_h = Math.round(this.moduleInstance.HEAPF32[3 + (s >> 2)])
                        }
                    }, {
                        key: "setRawImage",
                        value: function(t, e, n, i, r, a) {
                            var o = 5 < arguments.length && void 0 !== a ? a : ee,
                                s = {};
                            if (o = o || ee, this.runPluginInstance) {
                                try {
                                    if (this.setNativeImage(t, e, n, i, r), this.optionEnableFaceDetect && this.optionEnableFaceDetectRun) {
                                        if (0 !== this.nativeFaceDetectInstance && this.nativeFaceDetectInitialized && !this.lastFaceDetectHasFace) {
                                            var l = this.lastFaceDetectHasFace ? this.faceDetectIntervalTimeHasFace : this.faceDetectIntervalTimeNoFace,
                                                c = Jt()();
                                            if (null === this.lastFaceDetectTimestamp || c - this.lastFaceDetectTimestamp >= l) {
                                                var u = 4 * this.layout_x,
                                                    h = this.layout_w;
                                                this.lastFaceDetectTimestamp = c, this.fdTime.setenable(this.optionEnablePrintTime), this.fdTime.average_record(0);
                                                var d = this.moduleInstance._FDModule_DetectWithRawImage(this.nativeFaceDetectInstance, this.nativeImagePtr + u, h, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                                if (this.fdTime.average_record(1), this.fdTime.average_print("[FDModule_DetectWithRawImage]"), 0 !== d) throw console.log("FDModule_DetectWithRawImage ret = " + d), "FDModule_DetectWithRawImage ret = " + d;
                                                0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (this.moduleInstance._FAModule_SetFaceBoundWithPoseEuler(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceBoundPtr(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFacePoseEulerPtr(this.nativeFaceDetectInstance)), this.moduleInstance._FAModule_SetFaceLandMark(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceLandMarkPtr(this.nativeFaceDetectInstance))), this.faceCount = this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.lastFaceDetectHasFace = 0 < this.faceCount, console.log("fd ========= ", this.faceCount), this.resetFaces()
                                            }
                                        }
                                        if (this.lastFaceDetectHasFace)
                                            if (0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized) {
                                                var f = 4 * this.layout_x,
                                                    m = this.layout_w;
                                                this.faTime.setenable(this.optionEnablePrintTime), this.faTime.average_record(0);
                                                var p = this.moduleInstance._FAModule_DetectWithRawImage(this.nativeFaceAlignmentInstance, this.nativeImagePtr + f, m, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                                if (this.faTime.average_record(1), this.faTime.average_print("[FAModule_DetectWithRawImage]"), 0 !== p) throw console.log("FAModule_DetectWithRawImage ret = " + p), "FAModule_DetectWithRawImage ret = " + p;
                                                if (this.optionEnableFaceDetect) {
                                                    for (var v = this.moduleInstance._FAModule_GetFacePointsCount(this.nativeFaceAlignmentInstance), _ = this.moduleInstance._malloc(4 * v * 2), g = this.moduleInstance._FAModule_GetFacePointsPtr(this.nativeFaceAlignmentInstance), w = [], b = 0; b < 2 * v; b++) w[b] = b % 2 == 0 ? (this.moduleInstance.HEAPF32[(g >> 2) + b] * m + f / 4) / this.nativeImageWidth : this.moduleInstance.HEAPF32[(g >> 2) + b];
                                                    this.moduleInstance.HEAPF32.set(w, _ >> 2);
                                                    var y = this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance),
                                                        k = this.moduleInstance._malloc(16),
                                                        I = [];
                                                    if (I[0] = (this.moduleInstance.HEAPF32[y >> 2] * m + f / 4) / this.nativeImageWidth, I[1] = this.moduleInstance.HEAPF32[1 + (y >> 2)], I[2] = this.moduleInstance.HEAPF32[2 + (y >> 2)] * m / this.nativeImageWidth, I[3] = this.moduleInstance.HEAPF32[3 + (y >> 2)], this.moduleInstance.HEAPF32.set(I, k >> 2), this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance), k, _), this.nativeDl3DPosInitialized && this.optionEnableDl3DPosRun) {
                                                        this.moduleInstance._Dl3DPosModule_SetFaceLandMark(this.nativeDl3DPosInstance, _, this.nativeImageWidth, this.nativeImageHeight);
                                                        var x = this.moduleInstance._Dl3DPosModule_GetFacePoseEulerPtr(this.nativeDl3DPosInstance),
                                                            A = [];
                                                        A[0] = this.moduleInstance.HEAPF32[x >> 2], A[1] = this.moduleInstance.HEAPF32[1 + (x >> 2)], A[2] = this.moduleInstance.HEAPF32[2 + (x >> 2)];
                                                        var E = this.moduleInstance._Dl3DPosModule_GetFacePoseTranslationPtr(this.nativeDl3DPosInstance),
                                                            S = [];
                                                        S[0] = this.moduleInstance.HEAPF32[E >> 2], S[1] = this.moduleInstance.HEAPF32[1 + (E >> 2)], S[2] = this.moduleInstance.HEAPF32[2 + (E >> 2)];
                                                        for (var F = this.moduleInstance._Dl3DPosModule_GetFacePoseGLMVPPtr(this.nativeDl3DPosInstance), R = [], M = 0; M < 16; M++) R[M] = this.moduleInstance.HEAPF32[(F >> 2) + M];
                                                        this.nativeARModuleInitialized && this.moduleInstance._ARModule_SetDL3DData(this.nativeARModuleInstance, 1, x, E, F)
                                                    }
                                                    this.moduleInstance._free(k), this.moduleInstance._free(_)
                                                }
                                                var C = 0 < this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance);
                                                if (C || (this.lastFaceDetectHasFace = !1), C) {
                                                    var P = this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance),
                                                        D = this.moduleInstance.HEAPF32[P >> 2],
                                                        T = this.moduleInstance.HEAPF32[1 + (P >> 2)],
                                                        z = this.moduleInstance.HEAPF32[2 + (P >> 2)],
                                                        L = this.moduleInstance.HEAPF32[3 + (P >> 2)],
                                                        B = this.moduleInstance._FAModule_GetFacePoseEulerPtr(this.nativeFaceAlignmentInstance),
                                                        O = this.moduleInstance.HEAPF32[B >> 2],
                                                        W = this.moduleInstance.HEAPF32[1 + (B >> 2)],
                                                        H = this.moduleInstance.HEAPF32[2 + (B >> 2)];
                                                    s = { code: 0, data: { faceCount: this.faceCount, pitch: O.toFixed(3), yaw: W.toFixed(3), roll: H.toFixed(3), faceX: D, faceY: T, faceW: z, faceH: L, frameWidth: this.nativeImageWidth, frameHeight: this.nativeImageHeight, imageData: null }, msg: "detect success" }
                                                } else s = { code: 3, data: null, msg: "FA no face error" }
                                            } else this.optionEnableFaceDetect && this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null), s = { code: 2, data: null, msg: "MTFace face alignment not initialized" };
                                        else s = { code: 3, data: null, msg: "FD no face error" }
                                    } else s = { code: 1, data: null, msg: "MTFace face detect not enable" };
                                    if (this.nativeHairSegmentInitialized && this.optionEnableHairSegmentRun) {
                                        this.hairsegmentTime.setenable(this.optionEnablePrintTime), this.hairsegmentTime.average_record(0), this.moduleInstance._HairSegmentModule_DetectWithRawImage(this.nativeHairSegmentInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r), this.hairsegmentTime.average_record(1), this.hairsegmentTime.average_print("[HairSegmentModule_DetectWithRawImage]");
                                        var U = this.moduleInstance._HairSegmentModule_GetMaskRectPtr(this.nativeHairSegmentInstance),
                                            j = this.moduleInstance.HEAP32[2 + (U >> 2)],
                                            N = this.moduleInstance.HEAP32[3 + (U >> 2)],
                                            K = this.moduleInstance._HairSegmentModule_GetMaskTextureID(this.nativeHairSegmentInstance);
                                        this.moduleInstance._ARModule_SetHairMaskTextureID(this.nativeARModuleInstance, K, j, N)
                                    }
                                    this.moduleInstance._RenderModule_SetRawImage(this.nativeRenderModuleInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r)
                                } catch (t) { s = { code: -1, data: null, msg: t } }
                                o(s)
                            } else o({ code: -1, data: null, msg: "instance not allowed run in current state" })
                        }
                    }, {
                        key: "makeupRawImage",
                        value: function(t, e, n, i, r, a, o) {
                            var s = 5 < arguments.length && void 0 !== a ? a : "canvas",
                                l = 6 < arguments.length && void 0 !== o ? o : ee,
                                c = {};
                            if (l = l || ee, this.runPluginInstance) {
                                var u = this.centerMode;
                                try {
                                    if (this.setCenterMode(0), this.setNativeImage(t, e, n, i, r), this.optionEnableFaceDetect) {
                                        if (this.lastFaceDetectHasFace = !1, 0 !== this.nativeFaceDetectInstance && this.nativeFaceDetectInitialized) {
                                            var h = new Date,
                                                d = this.moduleInstance._FDModule_DetectWithRawImage(this.nativeFaceDetectInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r),
                                                f = new Date;
                                            if (0 !== d) throw "FDModule_DetectWithRawImage ret = " + d;
                                            this.optionEnablePrintTime && console.log("FDModule_DetectWithRawImage use time: " + (f - h) + " ms"), 0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (this.setFAModule_PAStrategy(1), this.moduleInstance._FAModule_SetFaceBoundWithPoseEuler(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceBoundPtr(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFacePoseEulerPtr(this.nativeFaceDetectInstance)), this.moduleInstance._FAModule_SetFaceLandMark(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceLandMarkPtr(this.nativeFaceDetectInstance))), this.faceCount = this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.lastFaceDetectHasFace = 0 < this.faceCount, console.log("fd face === ", this.faceCount), this.resetFaces()
                                        }
                                        c = this._internalMakeupRawImageFA(s, r, l)
                                    } else c = { code: 1, data: null, msg: "mtface face detect not enable" };
                                    if (this.nativeHairSegmentInitialized && this.optionEnableHairSegmentRun) {
                                        this.moduleInstance._HairSegmentModule_DetectWithRawImage(this.nativeHairSegmentInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                        var m = this.moduleInstance._HairSegmentModule_GetMaskRectPtr(this.nativeHairSegmentInstance),
                                            p = this.moduleInstance.HEAP32[2 + (m >> 2)],
                                            v = this.moduleInstance.HEAP32[3 + (m >> 2)],
                                            _ = this.moduleInstance._HairSegmentModule_GetMaskTextureID(this.nativeHairSegmentInstance);
                                        this.moduleInstance._ARModule_SetHairMaskTextureID(this.nativeARModuleInstance, _, p, v)
                                    }
                                    if (this.moduleInstance._RenderModule_SetRawImage(this.nativeRenderModuleInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r), 0 === c.code && "imageData" === s) {
                                        var g, w = this.nativeImageWidth * this.nativeImageHeight * 4;
                                        g = this.moduleInstance._malloc(w), this.moduleInstance._RenderModule_GetRenderFrame(this.nativeRenderModuleInstance, g, 0, 0, this.nativeImageWidth, this.nativeImageHeight), c.data.imageData = new Uint8Array(this.moduleInstance.HEAPU8.buffer, g, w), this.moduleInstance._free(g), this.resetFaces()
                                    }
                                } catch (t) { c = { code: 1, data: null, msg: "MTFace face error " + t } }
                                this.setCenterMode(u), l(c)
                            } else l({ code: -1, data: null, msg: "instance not allowed run in current state" })
                        }
                    }, {
                        key: "_internalMakeupRawImageFA",
                        value: function(t, e) {
                            var n = {};
                            try {
                                if (this.lastFaceDetectHasFace)
                                    if (this.resetFaces(), 0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized) {
                                        var i = new oe;
                                        this.moduleInstance._FAModule_SetMemFlag(this.nativeFaceAlignmentInstance, 1);
                                        var r = this.moduleInstance._FAModule_DetectWithRawImage(this.nativeFaceAlignmentInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, e);
                                        if (i.print("[Makeup FAModule_DetectWithRawImage]"), 0 !== r) throw "FAModule_DetectWithRawImage ret = " + r;
                                        if (this.optionEnableFaceDetect && (this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance), this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance), this.moduleInstance._FAModule_GetFacePointsPtr(this.nativeFaceAlignmentInstance)), this.nativeDl3DPosInitialized && this.optionEnableDl3DPosRun)) {
                                            var a = new oe;
                                            this.moduleInstance._Dl3DPosModule_SetFaceLandMark(this.nativeDl3DPosInstance, this.moduleInstance._FAModule_GetFacePointsPtr(this.nativeFaceAlignmentInstance), this.nativeImageWidth, this.nativeImageHeight);
                                            var o = this.moduleInstance._Dl3DPosModule_GetFacePoseEulerPtr(this.nativeDl3DPosInstance),
                                                s = [];
                                            s[0] = this.moduleInstance.HEAPF32[o >> 2], s[1] = this.moduleInstance.HEAPF32[1 + (o >> 2)], s[2] = this.moduleInstance.HEAPF32[2 + (o >> 2)];
                                            var l = this.moduleInstance._Dl3DPosModule_GetFacePoseTranslationPtr(this.nativeDl3DPosInstance),
                                                c = [];
                                            c[0] = this.moduleInstance.HEAPF32[l >> 2], c[1] = this.moduleInstance.HEAPF32[1 + (l >> 2)], c[2] = this.moduleInstance.HEAPF32[2 + (l >> 2)];
                                            for (var u = this.moduleInstance._Dl3DPosModule_GetFacePoseGLMVPPtr(this.nativeDl3DPosInstance), h = [], d = 0; d < 16; d++) h[d] = this.moduleInstance.HEAPF32[(u >> 2) + d];
                                            this.nativeARModuleInitialized && this.moduleInstance._ARModule_SetDL3DData(this.nativeARModuleInstance, 1, o, l, u), a.print("[Makeup Dl3DPosModule_SetFaceLandMark]")
                                        }
                                        var f = this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance);
                                        if (this.lastFaceDetectHasFace = 0 < f, console.log("fa face === ", f), this.lastFaceDetectHasFace) {
                                            var m = this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance),
                                                p = this.moduleInstance.HEAPF32[m >> 2],
                                                v = this.moduleInstance.HEAPF32[1 + (m >> 2)],
                                                _ = this.moduleInstance.HEAPF32[2 + (m >> 2)],
                                                g = this.moduleInstance.HEAPF32[3 + (m >> 2)],
                                                w = this.moduleInstance._FAModule_GetFacePoseEulerPtr(this.nativeFaceAlignmentInstance),
                                                b = this.moduleInstance.HEAPF32[w >> 2],
                                                y = this.moduleInstance.HEAPF32[1 + (w >> 2)],
                                                k = this.moduleInstance.HEAPF32[2 + (w >> 2)];
                                            n = { code: 0, data: { faceCount: this.faceCount, pitch: b.toFixed(3), yaw: y.toFixed(3), roll: k.toFixed(3), faceX: p, faceY: v, faceW: _, faceH: g, frameWidth: this.nativeImageWidth, frameHeight: this.nativeImageHeight, imagData: null }, msg: "detect success" }
                                        } else n = { code: 3, data: null, msg: "FA no face error" };
                                        this.moduleInstance._FAModule_SetMemFlag(this.nativeFaceAlignmentInstance, 0)
                                    } else this.optionEnableFaceDetect && this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null), n = { code: 2, data: null, msg: "MTFace face alignment not initialized" };
                                else this.optionEnableFaceDetect && this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null), n = { code: 3, data: null, msg: "FD no face error" }
                            } catch (t) { n = { code: 1, data: null, msg: "MTFace face error " + t } }
                            return n
                        }
                    }, {
                        key: "setOptions",
                        value: function(t, e) {
                            var n = this,
                                i = 1 < arguments.length && void 0 !== e ? e : "centerMode";
                            return this.tryCatchFn(function() { if (n.moduleInitialized && n.renderContextInitialized) return n[i] = t, n.moduleInstance._RenderModule_SetOptions(n.nativeRenderModuleInstance, n.optionEnableDrawFaces, n.centerMode), { code: 0, msg: "success" }; throw "ar class instance renderContext not initialized yet" })
                        }
                    }, { key: "setCenterMode", value: function(t) { var e = 0 < arguments.length && void 0 !== t ? t : 0; return this.setOptions(e) } }, { key: "setEnableDrawFaces", value: function(t) { var e = 0 < arguments.length && void 0 !== t && t; return this.setOptions(e, "optionEnableDrawFaces") } }, {
                        key: "setFAModule_PAStrategy",
                        value: function(t) {
                            var e = 0 < arguments.length && void 0 !== t ? t : 1,
                                n = -1;
                            return 0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (n = this.moduleInstance._FAModule_SetPAStrategy(this.nativeFaceAlignmentInstance, e)), n
                        }
                    }, {
                        key: "renderFrame",
                        value: function(t, e, n, i, r) {
                            var a = 0 < arguments.length && void 0 !== t ? t : 0,
                                o = 1 < arguments.length && void 0 !== e ? e : 0,
                                s = 2 < arguments.length && void 0 !== n ? n : 0,
                                l = 3 < arguments.length && void 0 !== i ? i : 0,
                                c = 4 < arguments.length && void 0 !== r ? r : ee;
                            if (c = c || ee, this.runPluginInstance) try { if (this.getModuleState()) { this.moduleInstance._RenderModule_RenderFrame(this.nativeRenderModuleInstance, a, o, s, l), c({ code: 0, data: {}, msg: "success" }) } } catch (t) { console.error(t), c({ code: -1, data: null, msg: t }) } else c({ code: -1, data: null, msg: "instance not allowed run in current state" })
                        }
                    }, {
                        key: "getRenderImageData",
                        value: function(t, e, n, i, r) {
                            var a = 0 < arguments.length && void 0 !== t ? t : 0,
                                o = 1 < arguments.length && void 0 !== e ? e : 0,
                                s = 2 < arguments.length && void 0 !== n ? n : 0,
                                l = 3 < arguments.length && void 0 !== i ? i : 0,
                                c = 4 < arguments.length && void 0 !== r ? r : ee;
                            c = c || ee;
                            try {
                                var u = { code: 0, data: {}, msg: "success" };
                                if (!this.getModuleState()) throw "ARClass not allowed get imageData in current state";
                                if (!s || !l) throw "width or height not allow 0";
                                var h, d = s * l * 4;
                                h = this.moduleInstance._malloc(d), this.moduleInstance._RenderModule_GetRenderFrame(this.nativeRenderModuleInstance, h, a, o, s, l), u.data.imageData = new Uint8Array(this.moduleInstance.HEAPU8.buffer, h, d), this.moduleInstance._free(h), u.data.frameWidth = s, u.data.frameHeight = l, c(u)
                            } catch (t) { console.error(t), c({ code: -1, data: null, msg: t }) }
                        }
                    }, { key: "clearCanvas", value: function(t, e, n, i) { var r = {}; if (this.moduleInitialized && this.renderContextInitialized) try { this.moduleInstance._RenderModule_RenderClear(this.nativeRenderModuleInstance, t, e, n, i), r = { code: 0, msg: "success" } } catch (t) { r = { code: 1, msg: "failed " + t } } else r = { code: -1, msg: "ar class instance not initialed yet" }; return r } }, { key: "releaseRenderContext", value: function() { return this._internalReleaseRenderContext() } }, {
                        key: "_internalReleaseRenderContext",
                        value: function(t, e) {
                            var n = this,
                                i = 0 < arguments.length && void 0 !== t && t,
                                r = 1 < arguments.length && void 0 !== e ? e : ee;
                            return this.tryCatchFn(function() { if (!n.getModuleState()) return { code: 0, msg: "release render context success" }; for (var t in n.optionEnableAR && (n.nativeARKernelPacketInstance && (n.moduleInstance._FileManagerModule_PopPacket(n.nativeARKernelPacketInstance), n.moduleInstance._FilePacket_DestroyInstance(n.nativeARKernelPacketInstance), n.nativeARKernelPacketInstance = null), n.moduleInstance._RenderModule_UnBindARInstance(n.nativeRenderModuleInstance, n.nativeARModuleInstance), n.moduleInstance._ARModule_Release(n.nativeARModuleInstance), n.moduleInstance._ARModule_DestroyInstance(n.nativeARModuleInstance), n.nativeARModuleInstance = null, n.nativeARModuleInitialized = !1), i ? "function" == typeof r && r() : (n.moduleInstance._FDModule_Release(n.nativeFaceDetectInstance), n.moduleInstance._FDModule_DestroyInstance(n.nativeFaceDetectInstance), n.nativeFaceDetectInstance = null, n.nativeFaceDetectInitialized = !1), n.optionEnableDl3DPos && n.moduleInstance._Dl3DPosModule_Release(n.nativeDl3DPosInstance), n.optionEnableHairSegment && n.moduleInstance._HairSegmentModule_Release(n.nativeHairSegmentInstance), n.moduleInstance._FAModule_Release(n.nativeFaceAlignmentInstance), n.moduleInstance._FAModule_DestroyInstance(n.nativeFaceAlignmentInstance), n.nativeFaceAlignmentInstance = null, n.nativeFaceAlignmentInitialized = !1, n.moduleInstance._RenderModule_Release(n.nativeRenderModuleInstance), n.moduleInstance._RenderModule_DestroyInstance(n.nativeRenderModuleInstance), n.nativeRenderModuleInstance = null, n.moduleInstance._free(n.nativeStaticPtr), n.configContextDic) n.configContextDic[t].release(n), delete n.configContextDic[t]; return null !== n.nativeImagePtr && (n.moduleInstance._free(n.nativeImagePtr), n.nativeImagePtr = null), n.nativeImageWidth = 0, n.nativeImageHeight = 0, n.moduleInstance._free(n.nativeFaceBoundPtr), n.nativeFaceBoundPtr = null, n.moduleInstance._free(n.nativeFacePosePtr), n.nativeFacePosePtr = null, n.moduleInstance.canvas = null, n.moduleInstance = null, { code: 0, msg: "release render context success" } })
                        }
                    }, {
                        key: "stringToNativePtr",
                        value: function(t) {
                            for (var e = [], n = 0, i = t.length; n < i; ++n) e.push(t.charCodeAt(n));
                            e.push(0);
                            var r = new Uint8Array(e),
                                a = this.moduleInstance._malloc(r.length);
                            return this.moduleInstance.HEAPU8.set(r, a), a
                        }
                    }, { key: "nativePtrToString", value: function(t, e) { for (var n = new Uint8Array(e), i = 0; i < e; i++) n[i] = this.moduleInstance.HEAP8[t + i]; return String.fromCharCode.apply(null, new Uint8Array(n)) } }, {
                        key: "applyMaterialZip",
                        value: function(m, p, v, _) {
                            var g = this;
                            return this.tryCatchFn(function() {
                                if (g.moduleInitialized) {
                                    if (g.nativeARModuleInitialized) {
                                        g.configContextDic[m] && (g.configContextDic[m].release(g), delete g.configContextDic[m]), p = p.endsWith("/") ? p : p + "/";
                                        var t = ae(),
                                            e = g.stringToNativePtr(t);
                                        v.byteLength && (v = new Uint8Array(v));
                                        var n = g.moduleInstance._malloc(v.length);
                                        g.moduleInstance.HEAPU8.set(v, n);
                                        var i, r = g.moduleInstance._FilePacket_CreateInstance(e, _.length),
                                            a = $t(_);
                                        try {
                                            for (a.s(); !(i = a.n()).done;) {
                                                var o = i.value.split(","),
                                                    s = p + o[0],
                                                    l = Vt()(o[1]),
                                                    c = Vt()(o[2]),
                                                    u = g.stringToNativePtr(s);
                                                g.moduleInstance._FilePacket_PushFileData(r, u, n + l, c), g.moduleInstance._free(u)
                                            }
                                        } catch (t) { a.e(t) } finally { a.f() }
                                        g.moduleInstance._free(e), g.moduleInstance._FileManagerModule_PushPacket(r);
                                        var h = g.stringToNativePtr(p + "configuration.plist");
                                        if (0 !== g.hasARGroupKey(m).code) return g.moduleInstance._free(h), { code: -1, msg: "ARModule failed to apply config with type := " + m + " not register" };
                                        var d = g.stringToNativePtr(m),
                                            f = g.moduleInstance._ARModule_ApplyConfigWithKey(g.nativeARModuleInstance, d, h);
                                        return g.moduleInstance._free(h), g.moduleInstance._free(d), g.configContextDic[m] = new re(n, r), 0 !== f ? { code: -1, msg: "ARModule failed to apply config with type := " + m + " url := " + t + ", ret :=" + f } : { code: 0, msg: "ARModule success to apply config with type := " + m + " url := " + t }
                                    }
                                    return { code: -1, msg: "ar model not initialized yet" }
                                }
                                return { code: -1, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyMaterialNull",
                        value: function(n) {
                            var i = this;
                            return this.tryCatchFn(function() {
                                if (i.moduleInitialized) {
                                    if (i.nativeARModuleInitialized) {
                                        if (i.configContextDic[n] && (i.configContextDic[n].release(i), delete i.configContextDic[n]), 0 !== i.hasARGroupKey(n).code) return { code: -1, msg: "ARModule failed to apply config with type := " + n + " not register" };
                                        var t = i.stringToNativePtr(n),
                                            e = i.moduleInstance._ARModule_ApplyConfigWithKey(i.nativeARModuleInstance, t, null);
                                        return i.moduleInstance._free(t), 0 !== e ? { code: -1, msg: "ARModule failed to apply config with type := " + n + " url := null, ret :=" + e } : { code: 0, msg: "ARModule success to apply config with type := " + n + " url := null" }
                                    }
                                    return { code: -1, msg: "ar model not initialized yet" }
                                }
                                return { code: -1, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyConfigDenoise",
                        value: function(t, e) {
                            var n = this,
                                i = 0 < arguments.length && void 0 !== t ? t : 1,
                                r = !(1 < arguments.length && void 0 !== e) || e;
                            return this.tryCatchFn(function() { var t = r ? 1 : 0; return n.nativeARModuleInitialized ? (n.optionEnableDenoise = t, n.moduleInstance._ARModule_SetDenoise(n.nativeARModuleInstance, n.optionEnableDenoise, i), { code: 0, msg: "ARModule success to set alpha with type := beauty" }) : { code: -1, msg: "native ARModule has not initialized yet" } })
                        }
                    }, { key: "setDistributeControlMode", value: function(t) { var e = this; return this.tryCatchFn(function() { if (e.nativeARModuleInstance) return 0 !== (t = Vt()(t)) && 1 !== t ? { code: -1, msg: "mode value not allowed" } : (e.moduleInstance._ARModule_SetDistributeControl(e.nativeARModuleInstance, t), { code: 0, msg: "set distribute control mode success" }); throw "native ARModule has not initialized yet" }) } }, { key: "setBeautyMode", value: function(t) { var e = this; return this.tryCatchFn(function() { if (e.nativeARModuleInstance) return 0 !== (t = Vt()(t)) && 1 !== t ? { code: -1, msg: "mode value not allowed" } : (e.moduleInstance._ARModule_SetBeautyMode(e.nativeARModuleInstance, t), { code: 0, msg: "set beauty mode success" }); throw "native ARModule has not initialized yet" }) } }, {
                        key: "applyConfigBeauty",
                        value: function(t, e, n) {
                            var i = this,
                                r = 0 < arguments.length && void 0 !== t ? t : 1,
                                a = 1 < arguments.length && void 0 !== e ? e : 1,
                                o = 2 < arguments.length && void 0 !== n ? n : 1;
                            return this.tryCatchFn(function() { return i.nativeARModuleInitialized ? (i.moduleInstance._ARModule_SetBeautyConfig(i.nativeARModuleInstance, r, a, o), i.skinRetouch = r, i.whitening = a, i.sharpen = o, { code: 0, msg: "ARModule success to set alpha with type := beauty" }) : { code: -1, msg: "native ARModule has not initialized yet" } })
                        }
                    }, { key: "applyConfigAlpha", value: function(e, n) { var i = this; return this.tryCatchFn(function() { if (i.nativeARModuleInitialized) { var t = i.stringToNativePtr(e); return i.moduleInstance._ARModule_SetAlphaWithKey(i.nativeARModuleInstance, t, n), i.moduleInstance._free(t), { code: 0, msg: "ARModule success to set alpha with type := " + e + " alpha := " + n } } return { code: -1, msg: "native ARModule has not initialized yet" } }) } }, {
                        key: "applyConfigColor",
                        value: function(a, o, s, l, c, t) {
                            var u = this,
                                h = 5 < arguments.length && void 0 !== t ? t : 1;
                            return this.tryCatchFn(function() {
                                if (console.log("applyConfigColor key :", a), u.nativeARModuleInitialized) {
                                    var t = u.stringToNativePtr(a),
                                        e = u.moduleInstance._malloc(4 * h),
                                        n = u.moduleInstance._malloc(4 * h),
                                        i = u.moduleInstance._malloc(4 * h),
                                        r = u.moduleInstance._malloc(4 * h);
                                    return u.moduleInstance.HEAP32.set(o, e >> 2), u.moduleInstance.HEAP32.set(s, n >> 2), u.moduleInstance.HEAP32.set(l, i >> 2), u.moduleInstance.HEAP32.set(c, r >> 2), u.moduleInstance._ARModule_SetORGBWithKey(u.nativeARModuleInstance, t, e, n, i, r, h), u.moduleInstance._free(t), u.moduleInstance._free(e), u.moduleInstance._free(n), u.moduleInstance._free(i), u.moduleInstance._free(r), { code: 0, msg: "ARModule success to set color with type := " + a + "| red := " + s + ", green := " + l + ", blue := " + c }
                                }
                                return { code: -1, msg: "native ARModule has not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyAlphaWithKey",
                        value: function(e, n, t) {
                            var i = this,
                                r = 2 < arguments.length && void 0 !== t ? t : "SetLightAlphaWithKey";
                            return this.tryCatchFn(function() { if (console.log(r + " key :", e), i.nativeARModuleInitialized) { var t = i.stringToNativePtr(e); return i.moduleInstance["_ARModule_" + r](i.nativeARModuleInstance, t, n), i.moduleInstance._free(t), { code: 0, msg: "ARModule success to " + r + ":= " + e + "| alpha := " + n } } return { code: -1, msg: "native ARModule has not initialized yet" } })
                        }
                    }, { key: "applyConfigSetLightAlphaWithKey", value: function(t, e) { return this.applyAlphaWithKey(t, e) } }, { key: "applyConfigSetShimmerAlphaWithKey", value: function(t, e) { return this.applyAlphaWithKey(t, e, "SetShimmerAlphaWithKey") } }, { key: "applyConfigSetDiamondAlphaWithKey", value: function(t, e) { return this.applyAlphaWithKey(t, e, "SetDiamondAlphaWithKey") } }, { key: "applyConfigSetHighLightAlphaWithKey", value: function(t, e) { return this.applyAlphaWithKey(t, e, "SetHighLightAlphaWithKey") } }, {
                        key: "applyLipStickShimmerRGB",
                        value: function(t, e, n, i) {
                            var r = this,
                                a = 3 < arguments.length && void 0 !== i ? i : "LipStickMetalShimmerRGB";
                            return this.tryCatchFn(function() { return r.nativeARModuleInitialized ? (r.moduleInstance["_ARModule_Set" + a](r.nativeARModuleInstance, t, e, n), { code: 0, msg: "ARModule success to set " + a + " red := " + t + ", green := " + e + ", blue := " + n }) : { code: -1, msg: "native ARModule has not initialized yet" } })
                        }
                    }, { key: "applyConfigLipStickMetalShimmerRGB", value: function(t, e, n) { return this.applyLipStickShimmerRGB(t, e, n) } }, { key: "applyConfigLipStickDiamondShimmerRGB", value: function(t, e, n) { return this.applyLipStickShimmerRGB(t, e, n, "LipStickDiamondShimmerRGB") } }, {
                        key: "applyHairSegmentModel",
                        value: function(i) {
                            var r = this;
                            return this.tryCatchFn(function() {
                                var t = { code: -1, msg: "memory data not initialized yet" };
                                if (r.moduleInitialized) {
                                    var e = i;
                                    e.byteLength && (e = new Uint8Array(e));
                                    var n = r.moduleInstance._malloc(e.length);
                                    r.moduleInstance.HEAPU8.set(e, n);
                                    t = 0 == r.moduleInstance._HairSegmentModule_Initialize(r.nativeHairSegmentInstance, n, e.length) ? (r.nativeHairSegmentInitialized = !0, r.moduleInstance._free(n), { code: 0, msg: "apply HairSegment model success" }) : (r.nativeHairSegmentInitialized = !1, r.moduleInstance._free(n), { code: -1, msg: "apply HairSegment model failed" })
                                }
                                return t
                            })
                        }
                    }, {
                        key: "applyDl3DPosModel",
                        value: function(n) {
                            var i = this;
                            return this.tryCatchFn(function() {
                                if (i.moduleInitialized) {
                                    var t = n;
                                    t.byteLength && (t = new Uint8Array(t));
                                    var e = i.moduleInstance._malloc(t.length);
                                    i.moduleInstance.HEAPU8.set(t, e);
                                    return 0 == i.moduleInstance._Dl3DPosModule_Initialize(i.nativeDl3DPosInstance, e, t.length) ? (i.nativeDl3DPosInitialized = !0, i.moduleInstance._free(e), { code: 0, msg: "apply dl3dpos model success" }) : (i.nativeDl3DPosInitialized = !1, i.moduleInstance._free(e), { code: -1, msg: "apply dl3dpos model failed" })
                                }
                                return { code: -1, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyMtFaceModel",
                        value: function(p, t) {
                            var v = this,
                                _ = 1 < arguments.length && void 0 !== t ? t : 1;
                            return this.tryCatchFn(function() {
                                if (v.moduleInitialized) {
                                    var t = p;
                                    t.byteLength && (t = new Uint8Array(t));
                                    var e = v.moduleInstance._malloc(t.length);
                                    v.moduleInstance.HEAPU8.set(t, e);
                                    var n, i = (4294967295 & t[8]) + ((4294967295 & t[9]) << 8) + ((4294967295 & t[10]) << 16) + ((4294967295 & t[11]) << 24),
                                        r = t.subarray(16, 16 + i),
                                        a = (r = String.fromCharCode.apply(null, new Uint16Array(r))).split(";"),
                                        o = $t(a);
                                    try {
                                        for (o.s(); !(n = o.n()).done;) {
                                            var s = n.value.split(",");
                                            if (-1 !== s[0].indexOf("mtface_fa")) {
                                                var l = Vt()(s[1]) + 16 + i,
                                                    c = Vt()(s[2]);
                                                if (0 !== v.moduleInstance._FAModule_Initialize(v.nativeFaceAlignmentInstance, e + l, c, _)) throw "face alignment model initialize failed";
                                                v.nativeFaceAlignmentInitialized = !0
                                            }
                                        }
                                    } catch (t) { o.e(t) } finally { o.f() }
                                    var u, h = $t(a);
                                    try {
                                        for (h.s(); !(u = h.n()).done;) {
                                            var d = u.value.split(",");
                                            if (-1 !== d[0].indexOf("mtface_fd")) {
                                                var f = Vt()(d[1]) + 16 + i,
                                                    m = Vt()(d[2]);
                                                if (0 !== v.moduleInstance._FDModule_Initialize(v.nativeFaceDetectInstance, e + f, m, 3, 2)) throw "face detect model initialize failed";
                                                if (0 !== v.moduleInstance._FDModule_SetMemFlag(v.nativeFaceDetectInstance, 1)) throw "face set memory flag failed";
                                                v.nativeFaceDetectInitialized = !0
                                            }
                                        }
                                    } catch (t) { h.e(t) } finally { h.f() }
                                    return v.moduleInstance._free(e), { code: 0, msg: "apply mtface model success" }
                                }
                                return { code: -1, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyFAModelSep",
                        value: function(r, a, t) {
                            var o = this,
                                s = 2 < arguments.length && void 0 !== t ? t : 1;
                            return this.tryCatchFn(function() {
                                if (o.moduleInitialized) {
                                    var t = r;
                                    t.byteLength && (t = new Uint8Array(t));
                                    var e = a;
                                    e.byteLength && (e = new Uint8Array(e));
                                    var n = o.moduleInstance._malloc(t.length + e.length),
                                        i = t.length;
                                    if (o.moduleInstance.HEAPU8.set(t, n), o.moduleInstance.HEAPU8.set(e, n + i), 0 !== o.moduleInstance._FAModule_Initialize(o.nativeFaceAlignmentInstance, n, t.length + e.length, s)) throw "face alignment model initialize failed";
                                    return o.nativeFaceAlignmentInitialized = !0, o.moduleInstance._free(n), { code: 0, data: null, msg: "apply fa model success" }
                                }
                                return { code: -1, data: null, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyFAModel",
                        value: function(n, t) {
                            var i = this,
                                r = 1 < arguments.length && void 0 !== t ? t : 1;
                            return this.tryCatchFn(function() {
                                if (i.moduleInitialized) {
                                    var t = n;
                                    t.byteLength && (t = new Uint8Array(t));
                                    var e = i.moduleInstance._malloc(t.length);
                                    if (i.moduleInstance.HEAPU8.set(t, e), 0 !== i.moduleInstance._FAModule_Initialize(i.nativeFaceAlignmentInstance, e, t.length, r)) throw "face alignment model initialize failed";
                                    return i.nativeFaceAlignmentInitialized = !0, i.moduleInstance._free(e), { code: 0, data: null, msg: "apply fa model success" }
                                }
                                return { code: -1, data: null, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyFDModel",
                        value: function(i) {
                            var r = this;
                            return this.tryCatchFn(function() {
                                if (r.moduleInitialized) {
                                    var t = i;
                                    t.byteLength && (t = new Uint8Array(t));
                                    var e = r.moduleInstance._malloc(t.length);
                                    r.moduleInstance.HEAPU8.set(t, e);
                                    var n = r.moduleInstance._FDModule_Initialize(r.nativeFaceDetectInstance, e, t.length, 3, 2);
                                    if (0 !== n) throw "face detect model initialize failed";
                                    if (0 !== (n = r.moduleInstance._FDModule_SetMemFlag(r.nativeFaceDetectInstance, 1))) throw "face set memory flag failed";
                                    return r.nativeFaceDetectInitialized = !0, r.moduleInstance._free(e), { code: 0, data: null, msg: "apply fd model success" }
                                }
                                return { code: -1, data: null, msg: "memory data not initialized yet" }
                            })
                        }
                    }, {
                        key: "applyDefaultAREffect",
                        value: function(t, e, n) {
                            var i = 2 < arguments.length && void 0 !== n ? n : -1,
                                r = -1,
                                a = this.stringToNativePtr(e),
                                o = this.stringToNativePtr(t);
                            (this.moduleInstance._ARModule_AddGroupKey(this.nativeARModuleInstance, o, o, i), 0 !== this.hasARGroupKey(t).code) ? console.error("add default effect: " + t + " key error"): 0 !== this.moduleInstance._ARModule_ApplyConfigWithKey(this.nativeARModuleInstance, o, a) ? console.error("apply default effect: " + t + " error") : r = 0;
                            return this.moduleInstance._free(o), this.moduleInstance._free(a), r
                        }
                    }, {
                        key: "applyARModel",
                        value: function(d, f, t, e) {
                            var m = this,
                                p = 2 < arguments.length && void 0 !== t ? t : "",
                                v = 3 < arguments.length && void 0 !== e && e;
                            return this.optionEnableAR ? this.tryCatchFn(function() {
                                p = p.endsWith("/") ? p : p + "/";
                                var t = ae(),
                                    e = m.stringToNativePtr(t);
                                d.byteLength && (d = new Uint8Array(d)), m.nativeStaticPtr = m.moduleInstance._malloc(d.length), m.moduleInstance.HEAPU8.set(d, m.nativeStaticPtr), m.nativeARKernelPacketInstance = m.moduleInstance._FilePacket_CreateInstance(e, f.length - 1);
                                var n, i = $t(f);
                                try {
                                    for (i.s(); !(n = i.n()).done;) {
                                        var r = n.value.split(","),
                                            a = p + r[0],
                                            o = Vt()(r[1]),
                                            s = Vt()(r[2]);
                                        if (-1 === a.indexOf("static_data.bin")) {
                                            var l = m.stringToNativePtr(a);
                                            m.moduleInstance._FilePacket_PushFileData(m.nativeARKernelPacketInstance, l, m.nativeStaticPtr + o, s), m.moduleInstance._free(l)
                                        } else m.moduleInstance._ARModule_SetStaticData(m.nativeStaticPtr + o, s)
                                    }
                                } catch (t) { i.e(t) } finally { i.f() }
                                m.moduleInstance._free(e), m.moduleInstance._FileManagerModule_PushPacket(m.nativeARKernelPacketInstance);
                                var c = m.stringToNativePtr(p + "ARKernelBuiltin");
                                m.moduleInstance._ARModule_Initialize(m.nativeARModuleInstance, c), m.moduleInstance._free(c), m.nativeARModuleInitialized = !0;
                                var u = -1,
                                    h = "";
                                if (0 !== (u = m.applyDefaultAREffect(m.filterKey, p + "ARKernelBuiltin/filter/configuration.plist", -1))) throw "init ARKernelResourceBundle failed, filter apply failed";
                                if (h += " filter ", 0 !== m.applyConfigDenoise(1).code && console.error("apply default denoise error"), v) {
                                    if (u = m.applyDefaultAREffect(m.beautyKey, p + "ARKernelBuiltin/beauty/configuration.plist", -1), 0 !== m.applyConfigBeauty(1, 1, 1).code && console.error("apply default beauty error"), 0 !== u) throw "init ARKernelResourceBundle failed, beauty apply failed";
                                    h += " beauty "
                                }
                                if (0 !== (u = m.applyDefaultAREffect(m.stereoscopicKey, p + "ARKernelBuiltin/stereoscopic/configuration.plist", -1))) throw "init ARKernelResourceBundle failed, stereoscopic apply failed";
                                return h += " stereoscopic ", 0 !== u ? { code: -1, data: null, msg: "apply ARKernelResourceBundle failed" } : { code: 0, data: null, msg: "apply global effect: " + h + " success" }
                            }) : { code: 0, msg: "Without AR success" }
                        }
                    }]), a
                }();

            function re(t, e) {
                var n = t,
                    i = e;
                this.release = function(t) { try { t.moduleInstance._FileManagerModule_PopPacket(i), t.moduleInstance._FilePacket_DestroyInstance(i), t.moduleInstance._free(n), n = 0 } catch (t) { console.error(t) } }
            }

            function ae() { return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(t) { var e = 16 * Math.random() | 0; return ("x" === t ? e : 3 & e | 8).toString(16) }) }

            function oe() {
                var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : 20;
                this._enable = !0, this._average_counts = t, this._last_time = Jt()(), this._total_time = 0, this._counts = 0, this.setenable = function(t) { this._enable = t }, this.record = function() { this._enable && (this._last_time = Jt()()) }, this.print = function() {
                    var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "";
                    this._enable && console.log(t + " => use time : " + (Jt()() - this._last_time) + " ms")
                }, this.average_record = function() {
                    var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : 0;
                    this._enable && (t ? (this._total_time += Jt()() - this._last_time, this._counts += 1) : this._last_time = Jt()())
                }, this.average_print = function() {
                    var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "";
                    this._enable && this._counts >= this._average_counts && (console.log(t + " [ loop " + this._counts + " times ]  => average use time : " + this._total_time / this._counts + " ms "), this._counts = 0, this._total_time = 0)
                }
            }

            function se() { try { return "undefined" != typeof WebAssembly && "object" === ("undefined" == typeof WebAssembly ? "undefined" : j()(WebAssembly)) } catch (t) { console.log("isWebAssemblyEnabled", t) } return !1 }
            console.log("isWebAssemblyEnabled", se());
            var le = function() { var t = window.navigator.userAgent.toLowerCase(); return /qq\//gi.test(t) },
                ce = function() { return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream },
                ue = function() { return !document.documentMode && !!window.StyleMedia };

            function he() {
                var t = !1;
                try {
                    if (le()) return t;
                    if (ce()) t = -1 === navigator.userAgent.indexOf("11_2_2") && -1 === navigator.userAgent.indexOf("11_2_5") && -1 === navigator.userAgent.indexOf("11_2_6") && se();
                    else if (ue()) { 1717134 < Math.trunc(1e5 * window.navigator.userAgent.toLowerCase().match(/edge\/((\d+\.?)*)/)[1]) && (t = !0) } else t = se();
                    t = t && function() {
                        var t = !1,
                            e = null;
                        try { if ("object" === ("undefined" == typeof WebAssembly ? "undefined" : j()(WebAssembly)) && "function" == typeof WebAssembly.instantiate) e = new WebAssembly.Module(Uint8Array.of(0, 97, 115, 109, 1, 0, 0, 0, 1, 6, 1, 96, 1, 127, 1, 127, 3, 2, 1, 0, 5, 3, 1, 0, 1, 7, 8, 1, 4, 116, 101, 115, 116, 0, 0, 10, 16, 1, 14, 0, 32, 0, 65, 1, 54, 2, 0, 32, 0, 40, 2, 0, 11)), new WebAssembly.Instance(e, {}).exports.test(4) && (t = !0), e = null } catch (t) { console.log("error : ", t), e = null }
                        return t
                    }()
                } catch (t) { console.log("isSupportWasm error:", t) }
                return t
            }
            var de = window.navigator.userAgent.toLowerCase(),
                fe = window.navigator.platform;
            console.log("ua info:", de);
            document.documentMode;
            var me = !document.documentMode && !!window.StyleMedia,
                pe = !(!/safari/i.test(de) || /chrome/i.test(de) || /crios/i.test(de) || /fxios/i.test(de) || /huawei/i.test(de)),
                ve = /(iPhone|iPad|iPod|iOS)/gi.test(de),
                _e = /android|adr/gi.test(de),
                ge = /(iPhone|iPad|iPod|iOS|Android|adr|Windows Phone|SymbianOS)/gi.test(de),
                we = (/(weibo)/gi.test(de), de.match(/MicroMessenger/gi), /qq\//gi.test(de), /mqqbrowser\//gi.test(de), de.indexOf("qzone/"), /(;\s*mp)/gi.test(de)),
                be = (/(com.meitu|bec.meitu|bec.wallet)/gi.test(de), /meipaimv|meipai|com.meitu.mtmv/gi.test(de), /com.meitu.meipu/gi.test(de), /(com.meitu.mtxx)/gi.test(de), /(com.meitu.myxj|com.meitu.meiyancamera)/gi.test(de), /com.meitu.makeup/gi.test(de), /com.meitu.wheecam/gi.test(de), /com.meitu.zhi.beauty/gi.test(de), /(com.meitu.shanliao|com.meitu.testwheetalk)/gi.test(de), /Twitter/gi.test(de), /fbav/gi.test(de), /line\//gi.test(de), /com.meitu.youyanvideo/gi.test(de), /com.meitu.yumyum/gi.test(de), /Mac/gi.test(fe), /Win/gi.test(fe), "undefined" != typeof InstallTrigger);

            function ye(t) {
                if (void 0 === Gt.a || null == t[Kt.a]) {
                    if (jt()(t) || (t = function(t, e) { if (!t) return; if ("string" == typeof t) return ke(t, e); var n = Object.prototype.toString.call(t).slice(8, -1); "Object" === n && t.constructor && (n = t.constructor.name); if ("Map" === n || "Set" === n) return tt()(n); if ("Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return ke(t, e) }(t))) {
                        var e = 0,
                            n = function() {};
                        return { s: n, n: function() { return e >= t.length ? { done: !0 } : { done: !1, value: t[e++] } }, e: function(t) { throw t }, f: n }
                    }
                    throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.")
                }
                var i, r, a = !0,
                    o = !1;
                return { s: function() { i = Ht()(t) }, n: function() { var t = i.next(); return a = t.done, t }, e: function(t) { o = !0, r = t }, f: function() { try { a || null == i.return || i.return() } finally { if (o) throw r } } }
            }

            function ke(t, e) {
                (null == e || e > t.length) && (e = t.length);
                for (var n = 0, i = new Array(e); n < e; n++) i[n] = t[n];
                return i
            }

            function Ie(i) {
                return function() {
                    var t, e = F()(i);
                    if (function() { if ("undefined" == typeof Reflect || !m.a) return; if (m.a.sham) return; if ("function" == typeof Proxy) return 1; try { return Date.prototype.toString.call(m()(Date, [], function() {})), 1 } catch (t) { return } }()) {
                        var n = F()(this).constructor;
                        t = m()(e, arguments, n)
                    } else t = e.apply(this, arguments);
                    return E()(this, t)
                }
            }
            var xe, Ae = function(t) {
                    x()(b, t);
                    var s = Ie(b);

                    function b(t, e, n) {
                        var i, r = 3 < arguments.length && void 0 !== arguments[3] && arguments[3],
                            a = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
                            o = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
                        return w()(this, b), (i = s.call(this, t, e, n, r, a, o)).WORKER_SUPPORT = !!window.Worker && r, i.supportWasm = he(), i.canvasDomId = n, i.fdWorker = null, i
                    }
                    return k()(b, [{ key: "setPluginRunInstance", value: function(n) { var i = this; return new P.a(function(t, e) { n ? i.WORKER_SUPPORT && !i.fdWorkerInitialized ? i._waitForFDWorkerInitialized(n, t, e) : i._internalSetPluginRunInstance(n, t, e) : (i.setRunPluginInstance(n), t({ code: 0, msg: "stop plugin instance success" })) }) } }, {
                        key: "_waitForFDWorkerInitialized",
                        value: function(t, e, n) {
                            var i = this;
                            this.fdWorkerInitialized ? this._internalSetPluginRunInstance(t, e, n) : setTimeout(function() { i._waitForFDWorkerInitialized(t, e, n) }, 200)
                        }
                    }, {
                        key: "initRenderContext",
                        value: function(t, e, n, i) {
                            var r = this,
                                a = 0 < arguments.length && void 0 !== t ? t : null,
                                o = 1 < arguments.length && void 0 !== e ? e : null,
                                s = 2 < arguments.length && void 0 !== n ? n : null,
                                l = !(3 < arguments.length && void 0 !== i) || i;
                            return new P.a(function(t, e) {
                                try {
                                    var n = {};
                                    r.supportWasm ? n.wasmBinary = a : n.js_mem_buf = a, r.setOptionEnableAR(l), r.canvasDomId ? (o && (n.locateFile = function(t, e) { return o }), n.onRuntimeInitialized = function() { null !== s && r.initNativeOnline(s, s.length), r.moduleInstance.canvas = document.getElementById(r.canvasDomId), r._initContextInternal(t, e) }) : e({ code: -1, data: null, msg: "canvas dom id not set" }), r.moduleInitialized = !0, r.moduleInstance = r.modulePack(n), n = r.modulePack = null
                                } catch (t) { e({ code: -1, data: null, msg: "webgl environment initialed failed " + t }) }
                            })
                        }
                    }, {
                        key: "releaseRenderContext",
                        value: function() {
                            var t = this;
                            this._internalReleaseRenderContext(this.fdWorker, function() { t.fdWorker.postMessage({ method: "release" }), t.fdWorker.terminate() })
                        }
                    }, { key: "setNativeImage", value: function(t, e, n, i, r) { qt()(F()(b.prototype), "setNativeImage", this).call(this, t, e, n, i, r) } }, {
                        key: "setRawImage",
                        value: function(t, e, n, i, r, a) {
                            var o = 5 < arguments.length && void 0 !== a ? a : function() {},
                                s = {};
                            if (o = o || function() {}, this.runPluginInstance) {
                                try {
                                    if (this.setNativeImage(t, e, n, i, r), this.optionEnableFaceDetect && this.optionEnableFaceDetectRun) {
                                        if (this.WORKER_SUPPORT) this.fdWorker.postMessage({ method: "face_detect", image_ptr: t, image_width: e, image_height: n, image_stride: i, image_orientation: r, layout_x: this.layout_x, layout_w: this.layout_w, lastFaceDetectHasFace: this.lastFaceDetectHasFace, timestamp: new Date });
                                        else if (0 !== this.nativeFaceDetectInstance && this.nativeFaceDetectInitialized && !this.lastFaceDetectHasFace) {
                                            var l = this.lastFaceDetectHasFace ? this.faceDetectIntervalTimeHasFace : this.faceDetectIntervalTimeNoFace,
                                                c = Jt()();
                                            if (null === this.lastFaceDetectTimestamp || c - this.lastFaceDetectTimestamp >= l) {
                                                var u = 4 * this.layout_x,
                                                    h = this.layout_w;
                                                this.lastFaceDetectTimestamp = c, this.fdTime.setenable(this.optionEnablePrintTime), this.fdTime.average_record(0);
                                                var d = this.moduleInstance._FDModule_DetectWithRawImage(this.nativeFaceDetectInstance, this.nativeImagePtr + u, h, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                                if (0 !== d) throw console.log("FDModule_DetectWithRawImage ret = " + d), "FDModule_DetectWithRawImage ret = " + d;
                                                this.fdTime.average_record(1), this.fdTime.average_print("[FDModule_DetectWithRawImage]"), 0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (this.moduleInstance._FAModule_SetFaceBoundWithPoseEuler(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceBoundPtr(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFacePoseEulerPtr(this.nativeFaceDetectInstance)), this.moduleInstance._FAModule_SetFaceLandMark(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceLandMarkPtr(this.nativeFaceDetectInstance))), this.faceCount = this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.lastFaceDetectHasFace = 0 < this.faceCount, console.log("fd ========= ", this.faceCount), this.resetFaces()
                                            }
                                        }
                                        if (this.lastFaceDetectHasFace)
                                            if (0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized) {
                                                var f = 4 * this.layout_x,
                                                    m = this.layout_w;
                                                this.faTime.setenable(this.optionEnablePrintTime), this.faTime.average_record(0);
                                                var p = this.moduleInstance._FAModule_DetectWithRawImage(this.nativeFaceAlignmentInstance, this.nativeImagePtr + f, m, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                                if (0 !== p) throw console.log("FAModule_DetectWithRawImage ret = " + p), "FAModule_DetectWithRawImage ret = " + p;
                                                if (this.faTime.average_record(1), this.faTime.average_print("[FAModule_DetectWithRawImage]"), this.optionEnableFaceDetect) {
                                                    for (var v = this.moduleInstance._FAModule_GetFacePointsCount(this.nativeFaceAlignmentInstance), _ = this.moduleInstance._malloc(4 * v * 2), g = this.moduleInstance._FAModule_GetFacePointsPtr(this.nativeFaceAlignmentInstance), w = [], b = 0; b < 2 * v; b++) w[b] = b % 2 == 0 ? (this.moduleInstance.HEAPF32[(g >> 2) + b] * m + f / 4) / this.nativeImageWidth : this.moduleInstance.HEAPF32[(g >> 2) + b];
                                                    this.moduleInstance.HEAPF32.set(w, _ >> 2);
                                                    var y = this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance),
                                                        k = this.moduleInstance._malloc(16),
                                                        I = [];
                                                    if (I[0] = (this.moduleInstance.HEAPF32[y >> 2] * m + f / 4) / this.nativeImageWidth, I[1] = this.moduleInstance.HEAPF32[1 + (y >> 2)], I[2] = this.moduleInstance.HEAPF32[2 + (y >> 2)] * m / this.nativeImageWidth, I[3] = this.moduleInstance.HEAPF32[3 + (y >> 2)], this.moduleInstance.HEAPF32.set(I, k >> 2), this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance), k, _), this.nativeDl3DPosInitialized && this.optionEnableDl3DPosRun) {
                                                        this.dl3dTime.setenable(this.optionEnablePrintTime), this.dl3dTime.average_record(0), this.moduleInstance._Dl3DPosModule_SetFaceLandMark(this.nativeDl3DPosInstance, _, this.nativeImageWidth, this.nativeImageHeight);
                                                        var x = this.moduleInstance._Dl3DPosModule_GetFacePoseEulerPtr(this.nativeDl3DPosInstance),
                                                            A = [];
                                                        A[0] = this.moduleInstance.HEAPF32[x >> 2], A[1] = this.moduleInstance.HEAPF32[1 + (x >> 2)], A[2] = this.moduleInstance.HEAPF32[2 + (x >> 2)];
                                                        var E = this.moduleInstance._Dl3DPosModule_GetFacePoseTranslationPtr(this.nativeDl3DPosInstance),
                                                            S = [];
                                                        S[0] = this.moduleInstance.HEAPF32[E >> 2], S[1] = this.moduleInstance.HEAPF32[1 + (E >> 2)], S[2] = this.moduleInstance.HEAPF32[2 + (E >> 2)];
                                                        for (var F = this.moduleInstance._Dl3DPosModule_GetFacePoseGLMVPPtr(this.nativeDl3DPosInstance), R = [], M = 0; M < 16; M++) R[M] = this.moduleInstance.HEAPF32[(F >> 2) + M];
                                                        this.dl3dTime.average_record(1), this.dl3dTime.average_print("[Dl3DPosModule_SetFaceLandMark]"), this.nativeARModuleInitialized && this.moduleInstance._ARModule_SetDL3DData(this.nativeARModuleInstance, 1, x, E, F)
                                                    }
                                                    this.moduleInstance._free(k), this.moduleInstance._free(_)
                                                }
                                                var C = 0 < this.moduleInstance._FAModule_GetFaceCount(this.nativeFaceAlignmentInstance);
                                                if (C || (this.lastFaceDetectHasFace = !1), C) {
                                                    var P = this.moduleInstance._FAModule_GetFaceBoundPtr(this.nativeFaceAlignmentInstance),
                                                        D = this.moduleInstance.HEAPF32[P >> 2],
                                                        T = this.moduleInstance.HEAPF32[1 + (P >> 2)],
                                                        z = this.moduleInstance.HEAPF32[2 + (P >> 2)],
                                                        L = this.moduleInstance.HEAPF32[3 + (P >> 2)],
                                                        B = this.moduleInstance._FAModule_GetFacePoseEulerPtr(this.nativeFaceAlignmentInstance),
                                                        O = this.moduleInstance.HEAPF32[B >> 2],
                                                        W = this.moduleInstance.HEAPF32[1 + (B >> 2)],
                                                        H = this.moduleInstance.HEAPF32[2 + (B >> 2)];
                                                    s = { code: 0, data: { faceCount: this.faceCount, pitch: O.toFixed(3), yaw: W.toFixed(3), roll: H.toFixed(3), faceX: D, faceY: T, faceW: z, faceH: L, frameWidth: this.nativeImageWidth, frameHeight: this.nativeImageHeight, imageData: null }, msg: "detect success" }
                                                } else s = { code: 3, data: null, msg: "FA no face error" }
                                            } else this.optionEnableFaceDetect && this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null), s = { code: 2, data: null, msg: "MTFace face alignment not initialized" };
                                        else this.moduleInstance._RenderModule_SetFaces(this.nativeRenderModuleInstance, 0, null, null), s = { code: 3, data: null, msg: "FD no face error" }
                                    } else s = { code: 1, data: null, msg: "MTFace face detect not enable" };
                                    if (this.nativeHairSegmentInitialized && this.optionEnableHairSegmentRun) {
                                        this.hairsegmentTime.setenable(this.optionEnablePrintTime), this.hairsegmentTime.average_record(0), this.moduleInstance._HairSegmentModule_DetectWithRawImage(this.nativeHairSegmentInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r), this.hairsegmentTime.average_record(1), this.hairsegmentTime.average_print("[HairSegmentModule_DetectWithRawImage]");
                                        var U = this.moduleInstance._HairSegmentModule_GetMaskRectPtr(this.nativeHairSegmentInstance),
                                            j = this.moduleInstance.HEAP32[2 + (U >> 2)],
                                            N = this.moduleInstance.HEAP32[3 + (U >> 2)],
                                            K = this.moduleInstance._HairSegmentModule_GetMaskTextureID(this.nativeHairSegmentInstance);
                                        this.moduleInstance._ARModule_SetHairMaskTextureID(this.nativeARModuleInstance, K, j, N)
                                    }
                                    this.moduleInstance._RenderModule_SetRawImage(this.nativeRenderModuleInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r)
                                } catch (t) { s = { code: -1, data: null, msg: t } }
                                o(s)
                            } else o({ code: -1, data: null, msg: "instance not allowed run in current state" })
                        }
                    }, {
                        key: "makeupRawImage",
                        value: function(t, e, n, i, r, a, o) {
                            var s = 5 < arguments.length && void 0 !== a ? a : "canvas",
                                l = 6 < arguments.length && void 0 !== o ? o : function() {},
                                c = {};
                            if (l = l || function() {}, this.runPluginInstance) {
                                var u = this.centerMode;
                                try {
                                    if (this.setCenterMode(0), this.setNativeImage(t, e, n, i, r), this.optionEnableFaceDetect) {
                                        if (this.lastFaceDetectHasFace = !1, this.WORKER_SUPPORT) this.fdWorker.postMessage({ method: "face_detect", image_ptr: t, image_width: e, image_height: n, image_stride: i, image_orientation: r, layout_x: this.layout_x, layout_w: this.layout_w, lastFaceDetectHasFace: this.lastFaceDetectHasFace, timestamp: new Date }), this.makeupRawImageUseWorker = 1, this.makeupRawImageWaiter = 0;
                                        else if (0 !== this.nativeFaceDetectInstance && this.nativeFaceDetectInitialized) {
                                            var h = new oe,
                                                d = this.moduleInstance._FDModule_DetectWithRawImage(this.nativeFaceDetectInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r);
                                            if (0 !== d) throw "FDModule_DetectWithRawImage ret = " + d;
                                            h.print("[Makeup FDModule_DetectWithRawImage]"), 0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (this.moduleInstance._FAModule_SetFaceBoundWithPoseEuler(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceBoundPtr(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFacePoseEulerPtr(this.nativeFaceDetectInstance)), this.moduleInstance._FAModule_SetFaceLandMark(this.nativeFaceAlignmentInstance, this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.moduleInstance._FDModule_GetFaceLandMarkPtr(this.nativeFaceDetectInstance))), this.faceCount = this.moduleInstance._FDModule_GetFaceCount(this.nativeFaceDetectInstance), this.lastFaceDetectHasFace = 0 < this.faceCount, console.log("fd face === ", this.faceCount), this.resetFaces()
                                        }
                                        this.WORKER_SUPPORT && !this.ENVIRONMENT_IS_MICROPROGRAM ? this._waitForFDWorkerMakeupRawImage(s, r, l) : c = qt()(F()(b.prototype), "_internalMakeupRawImageFA", this).call(this, s, r)
                                    } else c = { code: 1, data: null, msg: "mtface face detect not enable" };
                                    if (this.nativeHairSegmentInitialized && this.optionEnableHairSegmentRun) {
                                        var f = new oe;
                                        this.moduleInstance._HairSegmentModule_DetectWithRawImage(this.nativeHairSegmentInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r), f.print("[Makeup HairSegmentModule_DetectWithRawImage]");
                                        var m = this.moduleInstance._HairSegmentModule_GetMaskRectPtr(this.nativeHairSegmentInstance),
                                            p = this.moduleInstance.HEAP32[2 + (m >> 2)],
                                            v = this.moduleInstance.HEAP32[3 + (m >> 2)],
                                            _ = this.moduleInstance._HairSegmentModule_GetMaskTextureID(this.nativeHairSegmentInstance);
                                        this.moduleInstance._ARModule_SetHairMaskTextureID(this.nativeARModuleInstance, _, p, v)
                                    }
                                    if (this.moduleInstance._RenderModule_SetRawImage(this.nativeRenderModuleInstance, this.nativeImagePtr, this.nativeImageWidth, this.nativeImageHeight, 4 * this.nativeImageWidth, r), 0 === c.code && "imageData" === s) {
                                        var g, w = this.nativeImageWidth * this.nativeImageHeight * 4;
                                        g = this.moduleInstance._malloc(w), this.moduleInstance._RenderModule_GetRenderFrame(this.nativeRenderModuleInstance, g, 0, 0, this.nativeImageWidth, this.nativeImageHeight), c.data.imageData = new Uint8Array(this.moduleInstance.HEAPU8.buffer, g, w), this.moduleInstance._free(g), this.resetFaces()
                                    }
                                } catch (t) { c = { code: 1, data: null, msg: "MTFace face error " + t } }
                                this.makeupRawImageUseWorker || (this.setCenterMode(u), l(c))
                            } else l({ code: -1, data: null, msg: "instance not allowed run in current state" })
                        }
                    }, {
                        key: "_waitForFDWorkerMakeupRawImage",
                        value: function(t, e, n) {
                            var i = this;
                            setTimeout(function() { 1 === i.makeupRawImageWaiter ? i._internalMakeupRawImageFA(t, e, n) : setTimeout(function() { console.log("ggg"), i._waitForFDWorkerMakeupRawImage(t, e, n) }, 100) }, 100)
                        }
                    }, { key: "_internalMakeupRawImageFA", value: function(t, e, n) { n(qt()(F()(b.prototype), "_internalMakeupRawImageFA", this).call(this, t, e)) } }, {
                        key: "applyMtFaceModel",
                        value: function(t, e) {
                            var n = 1 < arguments.length && void 0 !== e ? e : 1,
                                i = {};
                            try {
                                if (this.moduleInitialized) {
                                    var r = t;
                                    r.byteLength && (r = new Uint8Array(r));
                                    var a = this.moduleInstance._malloc(r.length);
                                    this.moduleInstance.HEAPU8.set(r, a);
                                    var o, s = (4294967295 & r[8]) + ((4294967295 & r[9]) << 8) + ((4294967295 & r[10]) << 16) + ((4294967295 & r[11]) << 24),
                                        l = r.subarray(16, 16 + s),
                                        c = (l = String.fromCharCode.apply(null, new Uint16Array(l))).split(";"),
                                        u = ye(c);
                                    try {
                                        for (u.s(); !(o = u.n()).done;) {
                                            var h = o.value.split(",");
                                            if (-1 !== h[0].indexOf("mtface_fa")) {
                                                var d = Vt()(h[1]) + 16 + s,
                                                    f = Vt()(h[2]);
                                                if (0 !== this.moduleInstance._FAModule_Initialize(this.nativeFaceAlignmentInstance, a + d, f, n)) throw "face alignment model initialize failed";
                                                this.nativeFaceAlignmentInitialized = !0
                                            }
                                        }
                                    } catch (t) { u.e(t) } finally { u.f() }
                                    if (this.WORKER_SUPPORT) {
                                        this.fdWorker = new Worker("src/api/worker/faceDetect.worker.js");
                                        var m = this;
                                        this.fdWorker.onmessage = function(t) { m.fdWorkerOnMessage(t) }, this.fdWorker.postMessage({ method: "options", faceDetectIntervalTimeNoFace: this.faceDetectIntervalTimeNoFace / 2, faceDetectIntervalTimeHasFace: this.faceDetectIntervalTimeHasFace / 2 }), this.fdWorker.postMessage({ method: "initialize", nativeImageNeedWarp: this.nativeImageNeedWarp, model: t, memoryData: this.supportWasm ? this.moduleInstance.wasmBinary : this.moduleInstance.js_mem_buf }), this.fdWorker.onerror = function(t) { console.log(["ERROR: Line ", t.lineno, " in ", t.filename, ": ", t.message].join("")) }
                                    } else {
                                        var p, v = ye(c);
                                        try {
                                            for (v.s(); !(p = v.n()).done;) {
                                                var _ = p.value.split(",");
                                                if (-1 !== _[0].indexOf("mtface_fd")) {
                                                    var g = Vt()(_[1]) + 16 + s,
                                                        w = Vt()(_[2]);
                                                    if (0 !== this.moduleInstance._FDModule_Initialize(this.nativeFaceDetectInstance, a + g, w, 3, 2)) throw "face detect model initialize failed";
                                                    if (0 !== this.moduleInstance._FDModule_SetMemFlag(this.nativeFaceDetectInstance, 1)) throw "face set memory flag failed";
                                                    this.nativeFaceDetectInitialized = !0
                                                }
                                            }
                                        } catch (t) { v.e(t) } finally { v.f() }
                                    }
                                    this.moduleInstance._free(a), i = { code: 0, data: null, msg: "apply mtface model success" }
                                } else i = { code: -1, data: null, msg: "memory data not initialized yet" }
                            } catch (t) { i = { code: -1, data: null, msg: t + "" } }
                            return i
                        }
                    }, {
                        key: "applyFDModel",
                        value: function(t) {
                            var e = {};
                            try {
                                if (this.moduleInitialized) {
                                    if (this.WORKER_SUPPORT && !this.ENVIRONMENT_IS_MICROPROGRAM) {
                                        this.fdWorker = new Worker("api/worker/faceDetect.worker.js");
                                        var n = this;
                                        this.fdWorker.onmessage = function(t) { n.fdWorkerOnMessage(t) }, this.fdWorker.postMessage({ method: "options", faceDetectIntervalTimeNoFace: this.faceDetectIntervalTimeNoFace / 2, faceDetectIntervalTimeHasFace: this.faceDetectIntervalTimeHasFace / 2 }), this.fdWorker.postMessage({ method: "initialize", nativeImageNeedWarp: this.nativeImageNeedWarp, model: t, memoryData: this.supportWasm ? this.moduleInstance.wasmBinary : this.moduleInstance.js_mem_buf })
                                    } else {
                                        var i = t;
                                        i.byteLength && (i = new Uint8Array(i));
                                        var r = this.moduleInstance._malloc(i.length);
                                        this.moduleInstance.HEAPU8.set(i, r);
                                        var a = this.moduleInstance._FDModule_Initialize(this.nativeFaceDetectInstance, r, i.length, 3, 2);
                                        if (0 !== a) throw "face detect model initialize failed";
                                        if (0 !== (a = this.moduleInstance._FDModule_SetMemFlag(this.nativeFaceDetectInstance, 1))) throw "face set memory flag failed";
                                        this.nativeFaceDetectInitialized = !0, this.moduleInstance._free(r)
                                    }
                                    e = { code: 0, msg: "apply fd model success" }
                                } else e = { code: -1, msg: "memory data not initialized yet" }
                            } catch (t) { e = { code: -1, msg: t } }
                            return e
                        }
                    }, {
                        key: "fdWorkerOnMessage",
                        value: function(t) {
                            if ("initialize" === (t = t.data).method) 0 === t.code ? this.nativeFaceDetectInitialized = !0 : this.nativeFaceDetectInitialized = !1, this.fdWorkerInitialized = !0;
                            else if ("face_detect" === t.method && 0 === t.code) {
                                var e = t.data;
                                0 !== this.nativeFaceAlignmentInstance && this.nativeFaceAlignmentInitialized && (0 < e.face_count ? (this.ENVIRONMENT_IS_MICROPROGRAM ? (this.moduleInstance.HEAPF32[this.nativeFaceBoundPtr >> 2] = e.face_bound[0], this.moduleInstance.HEAPF32[1 + (this.nativeFaceBoundPtr >> 2)] = e.face_bound[1], this.moduleInstance.HEAPF32[2 + (this.nativeFaceBoundPtr >> 2)] = e.face_bound[2], this.moduleInstance.HEAPF32[3 + (this.nativeFaceBoundPtr >> 2)] = e.face_bound[3], this.moduleInstance.HEAPF32[this.nativeFacePosePtr >> 2] = e.face_pose[0], this.moduleInstance.HEAPF32[1 + (this.nativeFacePosePtr >> 2)] = e.face_pose[1], this.moduleInstance.HEAPF32[2 + (this.nativeFacePosePtr >> 2)] = e.face_pose[2]) : (this.moduleInstance.HEAPF32.set(e.face_bound, this.nativeFaceBoundPtr >> 2), this.moduleInstance.HEAPF32.set(e.face_pose, this.nativeFacePosePtr >> 2)), this.faceCount = e.face_count, this.lastFaceDetectHasFace = !0) : (this.faceCount = 0, this.lastFaceDetectHasFace = !1), this.moduleInstance._FAModule_SetFaceBoundWithPoseEuler(this.nativeFaceAlignmentInstance, e.face_count, this.nativeFaceBoundPtr, this.nativeFacePosePtr)), this.optionEnablePrintTime && console.log("faceDetectWorker.onmessage face detect use time: " + e.use_time + " ms"), this.makeupRawImageWaiter = 1
                            }
                        }
                    }, {
                        key: "applyARModel",
                        value: function(d, f, t, e) {
                            var m = this,
                                p = 2 < arguments.length && void 0 !== t ? t : "",
                                v = 3 < arguments.length && void 0 !== e && e;
                            return this.optionEnableAR ? this.tryCatchFn(function() {
                                p = p.endsWith("/") ? p : p + "/";
                                var t = ae(),
                                    e = m.stringToNativePtr(t);
                                d.byteLength && (d = new Uint8Array(d)), m.nativeStaticPtr = m.moduleInstance._malloc(d.length), m.moduleInstance.HEAPU8.set(d, m.nativeStaticPtr), m.nativeARKernelPacketInstance = m.moduleInstance._FilePacket_CreateInstance(e, f.length - 1);
                                var n, i = ye(f);
                                try {
                                    for (i.s(); !(n = i.n()).done;) {
                                        var r = n.value.split(","),
                                            a = p + r[0],
                                            o = Vt()(r[1]),
                                            s = Vt()(r[2]);
                                        if (-1 === a.indexOf("static_data.bin")) {
                                            var l = m.stringToNativePtr(a);
                                            m.moduleInstance._FilePacket_PushFileData(m.nativeARKernelPacketInstance, l, m.nativeStaticPtr + o, s), m.moduleInstance._free(l)
                                        } else m.moduleInstance._ARModule_SetStaticData(m.nativeStaticPtr + o, s)
                                    }
                                } catch (t) { i.e(t) } finally { i.f() }
                                m.moduleInstance._free(e), m.moduleInstance._FileManagerModule_PushPacket(m.nativeARKernelPacketInstance);
                                var c = m.stringToNativePtr(p + "ARKernelBuiltin");
                                m.moduleInstance._ARModule_Initialize(m.nativeARModuleInstance, c), m.moduleInstance._free(c), m.nativeARModuleInitialized = !0;
                                var u = -1,
                                    h = "";
                                if (0 !== (u = m.applyDefaultAREffect(m.filterKey, p + "ARKernelBuiltin/filter/configuration.plist", -1))) throw "init ARKernelResourceBundle failed, filter apply failed";
                                if (h += " filter ", 0 !== m.applyConfigDenoise(1).code && console.error("apply default denoise error"), v) {
                                    if (u = m.applyDefaultAREffect(m.beautyKey, p + "ARKernelBuiltin/beauty/configuration.plist", -1), 0 !== m.applyConfigBeauty(1, 1, 1).code && console.error("apply default beauty error"), 0 !== u) throw "init ARKernelResourceBundle failed, beauty apply failed";
                                    h += " beauty "
                                }
                                if (0 !== (u = m.applyDefaultAREffect(m.stereoscopicKey, p + "ARKernelBuiltin/stereoscopic/configuration.plist", -1))) throw "init ARKernelResourceBundle failed, stereoscopic apply failed";
                                if (h += " stereoscopic ", 0 !== (u = m.applyDefaultAREffect(m.faceliftKey, p + "ARKernelBuiltin/facelift/configuration.plist", 99))) throw "init ARKernelResourceBundle failed, facelift apply failed";
                                return h += " facelift ", 0 !== u ? { code: -1, data: null, msg: "apply ARKernelResourceBundle failed" } : { code: 0, data: null, msg: "apply global effect: " + h + " success" }
                            }) : { code: 0, msg: "Without AR success" }
                        }
                    }]), b
                }(ie),
                Ee = function() {
                    function i(t, e) { w()(this, i), navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia, this.streams = [], this.video = t, this.constraint = e, this.isPaused = !1, this.video.ready = !1, this.isRetryStartStream = !1, this.isHiddened = !1, this._handleVisibilityChange = this.handleVisibilityChange.bind(this), this._handleBlur = this.handleBlur.bind(this), this.hidden = "hidden", this.visibilityChange = "visibilitychange", this.needReload = !1, void 0 !== document.hidden ? (this.hidden = "hidden", this.visibilityChange = "visibilitychange") : void 0 !== document.mozHidden ? (this.hidden = "mozHidden", this.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (this.hidden = "webkitHidden", this.visibilityChange = "webkitvisibilitychange") }
                    return k()(i, [{ key: "handleBlur", value: function() { console.log("onblur"), this.needReload = !0 } }, {
                        key: "handleVisibilityChange",
                        value: function() {
                            if (console.log("onhidden"), this.needReload = !0, ge && pe || we) document[this.hidden] ? (console.log("Visibility hidden -> pause"), this.isHiddened = !0, this.video.pause()) : (console.log("Visibility show -> play"), this.video.play(), this.isHiddened = !1);
                            else if (document[this.hidden]) {
                                if (!this.video.ready) return;
                                this.isHiddened = !0, console.log("Visibility hidden -> stop"), this.stop()
                            } else console.log("Visibility show -> start"), this.initMedia(), this.start(), this.isHiddened = !1
                        }
                    }, {
                        key: "initMedia",
                        value: function() {
                            var e = this;
                            this.streams && this.streams.length || (me && navigator.mediaDevices && navigator.mediaDevices.getUserMedia ? navigator.mediaDevices.getUserMedia(this.constraint).then(function(t) { e.streams.push(t), e.updateSources(function(t) { t ? e.startStream(t) : (console.error("No camera available."), document.dispatchEvent(new CustomEvent("CameraWebRtc", { detail: { RTC: 0 }, bubbles: !1, cancelable: !1 }))) }) }).catch(function(t) { return e.noStream(t) }) : this.updateSources(function(t) { t ? e.startStream(t) : (console.error("No camera available."), document.dispatchEvent(new CustomEvent("CameraWebRtc", { detail: { RTC: 0 }, bubbles: !1, cancelable: !1 }))) }))
                        }
                    }, {
                        key: "updateSources",
                        value: function(n) {
                            try {
                                navigator && navigator.mediaDevices && navigator.mediaDevices.enumerateDevices ? navigator.mediaDevices.enumerateDevices().then(function(t) {
                                    var e = null;
                                    t && t.map(function(t) { "videoinput" === t.kind && i.isValid(t.label) && (e = t) }), n(e)
                                }) : void 0 !== MediaStreamTrack.getSources ? MediaStreamTrack.getSources(function(t) {
                                    var e = null;
                                    t && t.map(function(t) { "video" !== t.kind || i.isValid(t.label) || (e = t) }), n(e)
                                }) : n(0)
                            } catch (t) { n(0) }
                        }
                    }, {
                        key: "startStream",
                        value: function() {
                            var e = this;
                            navigator.mediaDevices && navigator.mediaDevices.getUserMedia ? navigator.mediaDevices.getUserMedia(this.constraint).then(function(t) { e.gotStream(t) }).catch(function(t) {
                                if (!e.isRetryStartStream) return e.isRetryStartStream = !0, e.constraint = { video: { facingMode: "user", width: { ideal: 1280 }, height: { ideal: 720 } } }, void e.startStream();
                                e.noStream(t)
                            }) : navigator.getUserMedia({ video: !0, audio: !1 }, function(t) { return e.gotStream(t) }, function(t) { return e.noStream(t) })
                        }
                    }, {
                        key: "start",
                        value: function() {
                            var t = this;
                            console.log("start"), void 0 === document.addEventListener || void 0 === document[this.hidden] ? console.error("This demo requires a modern browser that supports the Page Visibility API.") : document.addEventListener(this.visibilityChange, this._handleVisibilityChange, !1), window.addEventListener("blur", this._handleBlur, !1), this.video.oncanplaythrough = function() { return console.log("on canplaythrough"), t.video.play(), t.video.ready = !0 }, this.video.onerror = function() { return console.error("on error"), t.stop() }, this.video.onpause = function() { return console.log("on pause"), t.isPaused = !0 }, this.video.onplay = function() { console.log("on play"), t.isPaused && (t.isPaused = !1, t.needReload && (t.needReload = !1, t.video.play())) }, this.video.onsuspend = function() { console.log("on onsuspend"), t.video.play() }, this.video.onerror = function() { console.log("on onerror") }
                        }
                    }, {
                        key: "resume",
                        value: function() {
                            var t = this;
                            console.log("resume"), setTimeout(function() { t.initMedia() }, 1500)
                        }
                    }, {
                        key: "stop",
                        value: function() {
                            console.log("stop");
                            try { this.video.pause(), this.streams && this.streams.map(function(t) { return t.getTracks()[0].stop() }), this.streams = [] } catch (t) { console.error("Failed to stop media track") }
                            if (this.video) {
                                if (this.video.ready = !1, _e || be) try { this.video.srcObject = null } catch (t) { this.video.src = null }
                                this.video.onplay = function() {}
                            }
                        }
                    }, { key: "removelistener", value: function() { void 0 === document.addEventListener || void 0 === document[this.hidden] ? console.error("This demo requires a modern browser that supports the Page Visibility API.") : document.removeEventListener(this.visibilityChange, this._handleVisibilityChange, !1), window.removeEventListener("blur", this._handleBlur, !1) } }, {
                        key: "gotStream",
                        value: function(e) {
                            if (this.streams.push(e), ve || pe) this.video.setAttribute("playsinline", ""), this.video.style.display = "block", this.video.style.zIndex = -1, this.video.srcObject = e;
                            else if (_e) try { this.video.srcObject = e } catch (t) { this.video.src = window.URL.createObjectURL(e) } else if (window.URL) try { this.video.srcObject = e } catch (t) { this.video.src = window.URL.createObjectURL(e) } else window.webkitURL ? this.video.src = window.webkitURL.createObjectURL(e) : this.video.src = e;
                            document.dispatchEvent(new CustomEvent("CameraWebRtc", { detail: { RTC: 1 }, bubbles: !1, cancelable: !1 }))
                        }
                    }, { key: "noStream", value: function(t) {!t || "NotAllowedError" !== t.name && "PermissionDeniedError" !== t.name && 1 !== t.code ? (console.error("No camera available."), document.dispatchEvent(new CustomEvent("CameraWebRtc", { detail: { RTC: 0 }, bubbles: !1, cancelable: !1 }))) : (console.error("User denied access to use camera."), document.dispatchEvent(new CustomEvent("CameraWebRtc", { detail: { RTC: 2 }, bubbles: !1, cancelable: !1 }))), this.video && (this.video.onplay = function() {}) } }], [{ key: "isValid", value: function(t) { return !/cyberlink|videomeeting|youcam|virutal|fake|manycam/i.test(t) } }]), i
                }();

            function Se(t) {
                return new P.a(function(i, r) {
                    JSZipUtils.getBinaryContent(t, function(t, e) {
                        t ? r({ msg: t }) : JSZip.loadAsync(e).then(function(n) {
                            n.file("config.info").async("string").then(function(t) {
                                var e = t.split(";");
                                n.file("data.bin").async("arraybuffer").then(function(t) { i({ fileInfos: e, dataBin: t }) }).catch(function(t) { r({ msg: t }) })
                            }).catch(function(t) { r({ msg: t }) })
                        }).catch(function(t) { r({ msg: t }) })
                    })
                })
            }
            var Fe = [{ key: "Filter", name: "Filter", layer: 1 }, { key: "Foundation", name: "Foundation", layer: 2 }, { key: "MakeupPacket", name: "MakeupPacket", layer: 3 }, { key: "Bronzer", name: "Bronzer", layer: 4 }, { key: "Blusher", name: "Blusher", layer: 5 }, { key: "Lipstick", name: "Lipstick", layer: 6 }, { key: "EyePupil", name: "EyePupil", layer: 7 }, { key: "EyeShadow", name: "EyeShadow", layer: 8 }, { key: "EyeLiner", name: "EyeLiner", layer: 9 }, { key: "EyeLash", name: "EyeLash", layer: 10 }, { key: "EyeBrow", name: "EyeBrow", layer: 11 }, { key: "Makeup3D", name: "Makeup3D", layer: 12 }, { key: "HairDye", name: "HairDye", layer: 13 }],
                Re = { 0: "Foundation", 1: "Lipstick", 2: "Blusher", 3: "Bronzer", 4: "EyeBrow", 5: "EyeShadow", 6: "EyeLiner", 7: "EyeLash", 9: "EyePupil", 18: "HairDye", 24: "Makeup3D" },
                Me = { 11: 2, 12: 4, 13: 6, 14: 7, 29: 5 },
                Ce = (xe = {}, _()(xe, Re[0], "foundation"), _()(xe, Re[1], "lips"), _()(xe, Re[2], "blush"), _()(xe, Re[3], "contour"), _()(xe, Re[4], "brows"), _()(xe, Re[5], "eyeshadow"), _()(xe, Re[6], "eyeliner"), _()(xe, Re[7], "mascara"), _()(xe, Re[9], "contact"), _()(xe, Re[24], "Makeup3D"), xe);

            function Pe(e, t) {
                var n = b()(e);
                if (d.a) {
                    var i = d()(e);
                    t && (i = i.filter(function(t) { return u()(e, t).enumerable })), n.push.apply(n, i)
                }
                return n
            }

            function De(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? Pe(Object(n), !0).forEach(function(t) { _()(e, t, n[t]) }) : l.a ? s()(e, l()(n)) : Pe(Object(n)).forEach(function(t) { o()(e, t, u()(n, t)) })
                }
                return e
            }
            var Te = Ot.Mobile,
                ze = Te ? "-mobile" : "";
            n(Te ? "YkN1" : "pD7o");

            function Le() {}

            function Be(t) { return t.then(function(t) { return [null, t] }).catch(function(t) { return [t] }) }

            function Oe() { return it.hasClass(it.one(".mtar__sel"), "mtar__show") }

            function We(t) { It(it.one(".mtar"), "mtar__zindex-max", t) }
            var He = !1,
                Ue = !1,
                je = 0,
                Ne = 0,
                Ke = function() {
                    function i() {
                        var r = this,
                            t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
                        if (w()(this, i), He) throw console.log("MTAR is already exists."), "MTAR is already exists.";
                        He = !0;
                        if (this.options = De({}, { el: "", lang: "zh", showScreenshotBtn: !0, showZoomBtn: !0, showCompareBtn: !0, mode: "select", isMobileFullScreen: !0, customWidth: 400, customHeight: 450, showProduct: !0, isRedirectHttps: !1, showCarIcon: !0, isCustomSwitchPage: !1, showSwitchBtn: !0, cusI18n: { no_camera_tip: "", no_sup_camera_tip: "" } }, {}, t), this.el = "string" == typeof this.options.el ? it.one("#".concat(this.options.el)) : el, !this.el) throw new Error("Did not find the el node, Please check if the `options.el` is right");
                        if (this.options.isCustomSwitchPage && (!it.one("#mtar-custom-wrap") || !it.one("#mtar-custom-switch-page"))) throw new Error("请确保#mtar-custom-wrap与#mtar-custom-switch-page元素的存在");
                        if (Te) {
                            var e = this.options.isCustomSwitchPage ? it.one("#mtar-custom-wrap") : it.one(".mtar");
                            this.options.isMobileFullScreen && it.addClass(e, "mtar__fixed"), it.addClass(e, "mtar_zindex")
                        }
                        it.addClass(this.el, "mtar ".concat(Te ? "is-mobile" : "is-pc")), this.el.innerHTML = zt.a, this._setLoading(!0), this._initBeforeAuth();
                        var n = new FormData;
                        n.append("client_id", this.options.apiKey), n.append("client_secret", this.options.apiSecret), St({ url: "/web_sdk/auth/token", type: "POST", data: n, noAuth: !0 }).then(function(t) {
                            if (0 === t.code) {
                                r.authInfo = t.data, r.defaultLang = r.authInfo && r.authInfo.defaultLang;
                                var e = t.data.access_token;
                                localStorage.setItem("_token", e), r.isAuthReady = !0, r._emit("authReady", { code: 0, value: !0, token: e });
                                var n = (r.authInfo.switch_list || []).find(function(t) { return "meitu_data_collect" === t.key });
                                Ue = 1 == (n || {}).status, r._init()
                            } else {
                                r._setLoading(!1);
                                var i = (t || {}).msg;
                                1100409 === t.code ? (i = r.i18n.t("error_msg.300"), r._emit("authReady", { code: 1100409, value: !1, msg: i })) : r._emit("authReady", { code: 101, value: !1, msg: i }), console.error(i), r.authErr = { code: t.code, msg: i }, xt(i)
                            }
                        }).catch(function(t) { r._emit("authReady", { code: 101, value: !1 }), console.error("获取授权信息失败", t), r.authErr = { code: 101, msg: t } })
                    }
                    var e, r, n, t, a, o, s;
                    return k()(i, [{ key: "_initBeforeAuth", value: function() { this._initParams(), this._initCanvasVideoSize(), this._initLang() } }, { key: "_init", value: function() { this._initMode(this.options.mode), this._initDOM(), this._initMTAR(), this._bindEvent(), this._initTrack() } }, { key: "_initMode", value: function(t) { this.notSupportWebar || ("detect" === t ? (this._track("webar_realtime_enter", { source: "other" }), this.options.isCustomSwitchPage && this._emit("openSelect", { value: !1 }), this.openDetectMode()) : (it.addClass(this.$els.selModal, "mtar__show"), this.detectMode = !1, this._setLoading(!1))) } }, { key: "_initParams", value: function() { this.arDomain = "https://makeup-magic.zone1.meitudata.com/webar/release/H5/1.2.5", this.arVersion = 4, this.compatibility = ut(), this.supportWasm = !!this.compatibility.wasm, this.notSupDetect = "https:" !== location.protocol && "localhost" !== location.hostname, this.canvasId = "mainCanvas", this.videoId = "video", this.limitFPS = !0, this.sourceLeft = 0, this.sourceTop = 0, this.mainCanvasWidth = 600, this.mainCanvasHeight = 800, this.sourceWidth = 0, this.sourceHeight = 0, this.renderTime = new oe(100), this.flag = !0, this.timeoutId = null, this.timeNow = null, this.timeLast = null, this.elapsedMs = null, this.frameBuffer = null, this.fpsInterval = 1e3 / 30, this.animateID = null, this.faceErrorCode = 0, this.faceErrorText = {}, this.isInvalid = !1, this.detectMode = null, this.isCompare = !1, this.currentImgBase64 = null, this.currentImg = null, this.detectPhotoResult = null, this.handleImageResult = null, this.beautyMode = Te ? Ot.iOS ? "mobileIos" : "mobileAndroid" : "pc", this.nowSelectKeys = {}, this.BeautyConfig = { pc: { Denoise: .4, SkinRetouch: .4, Whitening: 0, Sharpen: 0 }, mobileAndroid: { Denoise: .3, SkinRetouch: .2, Whitening: .3, Sharpen: .3 }, mobileIos: { Denoise: .5, SkinRetouch: .4, Whitening: .3, Sharpen: 1 } }[this.beautyMode], this.ImageBeautyConfig = { pc: { Denoise: .2, SkinRetouch: .2, Whitening: 0, Sharpen: 0 }, mobileAndroid: { Denoise: .15, SkinRetouch: .1, Whitening: .15, Sharpen: .15 }, mobileIos: { Denoise: .25, SkinRetouch: .2, Whitening: .15, Sharpen: .5 } }[this.beautyMode], this.mode = "webgl", this.manual_mode = "", this.isInited = !1, this.allZips = [], this.nowAllZips = [], this.effectID = {}, this.flag = !0, this.zoom = 1, this.maxZoom = 1.8, this.minZoom = 1, this.zoomStep = .2, this.notSupportWebar = !this.compatibility.webgl || !this.compatibility.asm && !this.supportWasm, this.isCusSizeMob = !this.options.isMobileFullScreen && this.options.customWidth && this.options.customHeight, this.$els = { wrap: it.one(".mtar"), mtarWrap: it.one(".mtar__preview-wrap"), videoWrap: it.one(".mtar__video-wrap"), canvas: it.one("#".concat(this.canvasId)), video: it.one("#".concat(this.videoId)), updateModal: it.one(".mtar__update"), uploadImgWrap: it.one(".mtar__result-content"), screenshotWrap: it.one(".mtar__detect-photo-result"), screenshotIcon: it.one(".mtar__screenshot"), zoomIn: it.one(".mtar__rb_btns .zoom-in"), zoomOut: it.one(".mtar__rb_btns .zoom-out"), compareIcon: it.one(".mtar__rb_btns .material-switch"), loading: it.one(".mtar__loading"), selModal: it.one(".mtar__sel"), switchSel: it.one(".mtar__switch-sel".concat(ze)), noCamModal: it.one(".js__mtar-modal-nocam") }, console.log("[webar sdk version]", this.arVersion), console.log("[support]", this.compatibility, this.compatibility.webgl, this.compatibility.asm, this.supportWasm), console.log("[notSupportWebar]", this.notSupportWebar) } }, {
                        key: "_initLang",
                        value: function() {
                            this.lang = this.options.lang;
                            var t = this.lang || "en",
                                e = this.i18n = new Dt({ lang: t, fallbackLocale: "en" }),
                                r = (window.mtar__i18n = e).t("update_page.suggest_list"),
                                a = "";
                            b()(r).forEach(function(t) {
                                var e = r[t],
                                    n = e.tag,
                                    i = e.text;
                                a += '\n                <div class="mtar__update-tag">'.concat(n, '</div>\n                <div class="mtar__update-text">').concat(i, "</div>\n            ")
                            }), this.faceErrorText = e.t("faceErrorText"), it.one(".mtar__landscape").innerText = e.t("vertical_tip"), it.one(".mtar__loading .text").innerText = e.t("loading_hard"), it.one(".mtar__update-tit").innerText = e.t("update_page.sorry"), it.one(".mtar__update-sub-tit").innerText = e.t("update_page.tip"), it.one(".mtar__update-text").innerText = e.t("update_page.advice"), it.one(".mtar__update-list").innerHTML = a, it.one(".mtar__sel-tit").innerText = e.t("select_modal.select"), it.one(".mtar__sel-import-text").innerText = e.t("select_modal.import"), it.one(".mtar__sel-detect-text").innerText = e.t("select_modal.detect"), it.one(".mtar__sel-tip-text").innerText = e.t("select_modal.tip"), it.one(".mtar__switch-sel .mtar__switch-sel-text").innerText = e.t("switch_select"), it.one(".mtar__switch-sel-mobile .mtar__switch-sel-text").innerText = e.t("switch_select"), it.one(".js__mtar-modal-nocam .text").innerText = e.t("no_camera_tip"), it.one(".mtar__browser-tip-text").innerText = e.t("browser_tip.".concat(Ot.iOS ? 1 : 2))
                        }
                    }, { key: "_initDOM", value: function() { this.options.isCustomSwitchPage && it.addClass(this.$els.selModal, "mtar__hide"), this._dealBtns && this._dealBtns(), this._dealElementDisplay() } }, {
                        key: "returnData",
                        value: function(t, e, n) {
                            switch (n = n || this.i18n.t("error_msg")[t], t) {
                                case 0:
                                    n = "success"
                            }
                            return { code: t, msg: n, data: e }
                        }
                    }, {
                        key: "_dealElementDisplay",
                        value: function() {
                            var t = this.$els,
                                e = this.options,
                                n = n;
                            bt(t.screenshotIcon, e.showScreenshotBtn), bt(t.compareIcon, e.showCompareBtn), bt(t.switchSel, e.showSwitchBtn), bt(t.zoomIn, !n && e.showZoomBtn), bt(t.zoomOut, !n && e.showZoomBtn)
                        }
                    }, {
                        key: "_initCanvasVideoSize",
                        value: function() {
                            var t, e, n, i, r = Number(this.options.customWidth),
                                a = Number(this.options.customHeight);
                            Te ? (this.winW = window.innerWidth, i = this.isCusSizeMob ? (n = r, a) : (n = this.winW, window.innerHeight), t = 2 * n, e = 2 * i) : (n = r || 360, (i = a || 480) < 480 ? n < 360 ? e = i / n * (t = 540) : t = n / i * (e = 720) : (t = 1.5 * n, e = 1.5 * i));
                            var o = "width:".concat(n, "px;height:").concat(i, "px;");
                            this.$els.wrap.setAttribute("style", o), this.$els.mtarWrap.setAttribute("style", o), this.$els.videoWrap.setAttribute("style", o), this.$els.canvas.setAttribute("style", o), this.$els.canvas.width = this.canvasWidth = t, this.$els.canvas.height = this.canvasHeight = e;
                            var s = this.options,
                                l = s.showProduct,
                                c = s.isCustomSwitchPage;
                            if (!Te && l && it.one(".mtar").setAttribute("style", "width:".concat(n + 520, "px;")), c) {
                                !Te && l ? it.one("#mtar-custom-wrap").setAttribute("style", "width:".concat(n + 520, "px;")) : it.one("#mtar-custom-wrap").setAttribute("style", o);
                                var u = this.options.showProduct && this.options.showCarIcon ? it.one(".mtar__header").clientHeight : 0;
                                it.one("#mtar-custom-switch-page").setAttribute("style", o + "top: ".concat(u, "px;"))
                            }
                        }
                    }, {
                        key: "_setLandscape",
                        value: function(t) {
                            (this.isLandscape = t) && this._setLoading(!1), It(".mtar__landscape", "mtar__show", t), We(t)
                        }
                    }, {
                        key: "onorientationchange",
                        value: (s = O()(L.a.mark(function t() {
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        yt() ? this._setLandscape(!0) : (this._setLandscape(!1), this._initCanvasVideoSize(), this.isShowBrowserTip || Oe() || this.isPageInit || (this._setLoading(!0), console.log("onorientationchange", this.canvasWidth, this.canvasHeight), this.pageInit()));
                                    case 1:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function() { return s.apply(this, arguments) })
                    }, { key: "handleResume", value: function() { console.log("---resume", this.arClassInstance), this.arClassInstance || window.history.go() } }, { key: "_unload", value: function() { He = !1, this.destroyAR(), window.removeEventListener("pagehide", this._unloadHandler), window.removeEventListener("beforeunload", this._unloadHandler) } }, {
                        key: "_initMTAR",
                        value: function() {
                            var t, e = this;
                            if (this.notSupDetect && (it.addClass(it.one(".mtar__sel-detect-wrap"), "disabled"), console.warn("当前为非https环境，无法正常使用webar实时预览，请使用https访问当前页面"), this.options.isRedirectHttps)) return t = location.href, void("http:" === location.protocol && location.replace(t.replace("http:", "https:")));
                            if (Te && (Ot.iOS && (Ot.WeChat || Ot.QQ) || Ot.Android && Ot.QQ)) return this.isCusSizeMob || We(!0), It(".mtar__browser-tip", "mtar__show", !0), void(this.isShowBrowserTip = !0);
                            if (this.notSupportWebar) it.addClass(this.$els.updateModal, "mtar__show");
                            else {
                                if (this._unloadHandler = this._unload.bind(this), window.addEventListener("pagehide", this._unloadHandler), window.addEventListener("beforeunload", this._unloadHandler), Te) {
                                    if (!this.isCusSizeMob && (this._orientHandler = this.onorientationchange.bind(this), window.addEventListener("onorientationchange", this._orientHandler), window.addEventListener("resize", Rt(400, function() { console.log("-----resize-----"), e.onorientationchange() })), yt())) return void this._setLandscape(!0);
                                    this._resumeHandler = this.handleResume.bind(this), document.addEventListener("resume", this._resumeHandler), document.addEventListener("touchstart", function(t) { 1 < t.touches.length && t.preventDefault() }), document.addEventListener("touchmove", function(t) {
                                        (1 < t.touches.length || 1 !== t.scale) && t.preventDefault()
                                    });
                                    var n = 0;
                                    document.addEventListener("touchend", function(t) {
                                        var e = (new Date).getTime();
                                        e - n <= 300 && t.preventDefault(), n = e
                                    }, !1), document.addEventListener("gesturestart", function(t) { t.preventDefault() })
                                }
                                if (this.pageShowResetFaces(), window.dat && 1 == ft("debug")) {
                                    var i = new dat.GUI,
                                        r = this.BeautyConfig,
                                        a = i.add(r, "Denoise", 0, 1).step(.05),
                                        o = i.add(r, "SkinRetouch", 0, 1).step(.05),
                                        s = i.add(r, "Whitening", 0, 1).step(.05),
                                        l = i.add(r, "Sharpen", 0, 1).step(.05);
                                    a.onFinishChange(function(t) { console.log("Denoise", t, r), e.configBeautyAndDenoise(r) }), o.onFinishChange(function(t) { console.log("SkinRetouch", t, r), e.configBeautyAndDenoise(r) }), s.onFinishChange(function(t) { console.log("Whitening", t, r), e.configBeautyAndDenoise(r) }), l.onFinishChange(function(t) { console.log("Sharpen", t, r), e.configBeautyAndDenoise(r) })
                                }
                                console.log(window.innerWidth + "-" + window.innerHeight), setTimeout(function() { console.log(window.innerWidth + "-" + window.innerHeight) }, 1200)
                            }
                        }
                    }, { key: "_setLoading", value: function(t) { t !== this.loading && ((this.loading = t) ? it.addClass(it.one(".mtar__loading"), "mtar__show") : it.removeClass(it.one(".mtar__loading"), "mtar__show"), this._emit("loading", { value: t })) } }, {
                        key: "_setDetectMode",
                        value: function(t) {
                            this.detectMode = !!t;
                            var e = t ? this.BeautyConfig : this.ImageBeautyConfig;
                            this.configBeautyAndDenoise(e), console.log("设置".concat(t ? "实时" : "导图", "美颜参数"), e), Te || (It(this.$els.zoomIn, "mtar__show", !t), It(this.$els.zoomOut, "mtar__show", !t))
                        }
                    }, {
                        key: "pageShowResetFaces",
                        value: function() {
                            var t, e, n = this;
                            void 0 !== document.hidden ? (t = "hidden", e = "visibilitychange") : void 0 !== document.mozHidden ? (t = "mozHidden", e = "mozvisibilitychange") : void 0 !== document.webkitHidden && (t = "webkitHidden", e = "webkitvisibilitychange"), void 0 === document.addEventListener || void 0 === document[t] || document.addEventListener(e, function() { "visible" === document.visibilityState && n.arClassInstance && n.arClassInstance.getModuleState() && n.arClassInstance.getRunPluginInstance() && n.arClassInstance.resetFaces() })
                        }
                    }, {
                        key: "pageInit",
                        value: (o = O()(L.a.mark(function t() {
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.next = 2, this._initBrowser();
                                    case 2:
                                        this._initCamera(), this._initPlugin(), this.startAnimate();
                                    case 5:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function() { return o.apply(this, arguments) })
                    }, {
                        key: "_initBrowser",
                        value: (a = O()(L.a.mark(function t() {
                            var e, n, i;
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return this.pageInitStartTime = +new Date, this.isPageInit = !0, this.manual_mode = ft("mode"), this.manual_mode ? (this.mode = this.manual_mode, console.log("==> manual mode , current manis mode is " + this.mode)) : "webgl" === this.mode && (ht("mainCanvas") || (this.mode = "cpu", console.warn("==> cannot support the specified gl extensions , change mode [webgl] to [cpu] !")), Ot.iOS && (console.log("==> ios system ,the cpu mode is better "), this.mode = "cpu")), console.error("当前mode方式：", this.mode), t.next = 7, Be(dt(this.mode));
                                    case 7:
                                        if (e = t.sent, n = T()(e, 1), i = n[0]) throw this._setLoading(!1), i;
                                        t.next = 13;
                                        break;
                                    case 13:
                                        console.log("[time loadModule]", new Date - this.pageInitStartTime), this.arClassInstance = new Ae(this.supportWasm ? window.wasmModule : window.asmModule, this, this.canvasId, !1);
                                    case 15:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function() { return a.apply(this, arguments) })
                    }, {
                        key: "_initCamera",
                        value: function() {
                            var i = this;
                            document.addEventListener("CameraWebRtc", function(t) { 1 === t.detail.RTC && Te && yt() ? xt(i.i18n.t("horizontal_refresh")) : (i._CameraWebRtc(t.detail.RTC), i._emit("cameraCanUse", { value: 1 === t.detail.RTC })) }), this.video = document.getElementById(this.videoId);
                            this.cameraManager = new Ee(this.video, { audio: !1, video: { facingMode: "user", width: { min: 640, ideal: 1280, max: 1280 }, height: { min: 480, ideal: 720, max: 960 } } }), this.cameraManager.initMedia(), this.cameraManager.start(), this.video.addEventListener("play", function() {
                                clearTimeout(i.timerVideo), i.timerVideo = setTimeout(function() {
                                    var t = i.video.played,
                                        e = i.video,
                                        n = i.compatibility.RTC;
                                    console.log("-----【video.readyState】", t.length, e.ready, n, e.readyState), 1 !== n || !t || t.length || e.ready || e.readyState || xt(i.i18n.t("err_page_tip"))
                                }, 1500)
                            })
                        }
                    }, {
                        key: "_CameraWebRtc",
                        value: function(t) {
                            if (console.log("webrtc事件监听结果:", t, this.compatibility.RTC, this._fromClick), 1 !== (this.compatibility.RTC = t)) {
                                if (it.addClass(this.$els.selModal, "mtar__show"), this._emit("openSelect", { value: !0 }), 0 <= this.timeoutId && clearTimeout(this.timeoutId), cancelAnimationFrame(this.animateID), this.animateID = null, this.detectMode = !1, this._fromClick) {
                                    this._fromClick = !1;
                                    var e = 2 == t ? "no_camera_tip" : "no_sup_camera_tip";
                                    it.one(".js__mtar-modal-nocam .text").innerText = (this.options.cusI18n || {})[e] || this.i18n.t(e), it.addClass(this.$els.noCamModal, "mtar__show")
                                }
                                this._setLoading(!1), console.error("[ERROR] 设备浏览器不支持相机")
                            }
                        }
                    }, {
                        key: "_initPlugin",
                        value: function() {
                            console.log("initPlugin");
                            var t = this.getResourceList(),
                                e = t.modelDict,
                                n = t.ARKernelBuiltin;
                            this.modelDict = e, this.ARKernelBuiltin = n, this.imageCanvas = document.createElement("canvas"), this.imageContext = this.imageCanvas.getContext("2d"), this._initModel()
                        }
                    }, {
                        key: "_initModel",
                        value: (t = O()(L.a.mark(function t() {
                            var e, n, i, r, a, o, s, l, c, u, h, d, f, m, p, v = this;
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (console.log("initModel"), this.isAuthReady) { t.next = 3; break }
                                        return t.abrupt("return");
                                    case 3:
                                        return e = this.supportWasm ? this.modelDict["H5-WasmBin"].url : this.modelDict["H5-AsmjsBin"].url, t.next = 6, Be(Ft.get(e, { responseType: "arraybuffer" }));
                                    case 6:
                                        if (n = t.sent, i = T()(n, 2), r = i[0], a = i[1], r || 200 !== a.status) return console.error("[_initModel error-1]", r || a.status), t.abrupt("return", !1);
                                        t.next = 13;
                                        break;
                                    case 13:
                                        return o = /http|https/i.test(e) ? e : "".concat(location.protocol, "//").concat(location.host).concat(e), t.next = 16, Be(this.arClassInstance.initRenderContext(a.data, o, this.authInfo.License));
                                    case 16:
                                        if (s = t.sent, l = T()(s, 1), c = l[0]) return console.error("[_initModel error-2]", c), t.abrupt("return", !1);
                                        t.next = 22;
                                        break;
                                    case 22:
                                        return this.arClassInstance.optionEnableHairSegment && (u = Ot.Mobile ? this.modelDict.hair_segment_mobile.url : this.modelDict.hair_segment_pc.url, Ft.get(u, { responseType: "arraybuffer" }).then(function(t) {
                                            if (200 === t.status) {
                                                var e = v.arClassInstance.applyHairSegmentModel(t.data);
                                                0 === e.code ? console.log(e.msg) : console.error(e.msg)
                                            }
                                        }).catch(function(t) { throw t })), this.arClassInstance.optionEnableDl3DPos && Ft.get(this.modelDict.mtface_3dpos.url, { responseType: "arraybuffer" }).then(function(t) {
                                            if (200 === t.status) {
                                                var e = v.arClassInstance.applyDl3DPosModel(t.data);
                                                0 === e.code ? console.log(e.msg) : console.error("[_initModel error-3]", e.msg)
                                            }
                                        }).catch(function(t) { throw t }), t.next = 26, Be(Ft.get(this.modelDict["H5-MtFace"].url, { responseType: "arraybuffer" }));
                                    case 26:
                                        if (h = t.sent, d = T()(h, 2), f = d[0], m = d[1], f || 200 !== m.status) return console.error("[_initModel error-3]", f || "down load mtface error"), t.abrupt("return", !1);
                                        t.next = 33;
                                        break;
                                    case 33:
                                        if (0 !== (p = this.arClassInstance.applyMtFaceModel(m.data, 2)).code) return console.error("[_initModel error-4]", p.msg), t.abrupt("return", !1);
                                        t.next = 37;
                                        break;
                                    case 37:
                                        Se(this.ARKernelBuiltin.url).then(function(t) {
                                            var e = t.dataBin,
                                                n = t.fileInfos,
                                                i = v.arClassInstance.applyARModel(e, n, "/" + v.ARKernelBuiltin.id, !0);
                                            if (0 !== i.code) return console.error("[_initModel error-5]", i.msg), !1;
                                            0 === (i = v.arClassInstance.addARGroupKey(Fe)).code ? (v._setDetectMode(v.detectMode), v.arClassInstance.setPluginRunInstance(!0).then(O()(L.a.mark(function t() {
                                                return L.a.wrap(function(t) {
                                                    for (;;) switch (t.prev = t.next) {
                                                        case 0:
                                                            console.log("跑起来了"), v.isInited = !0, v.resetEffect(function(t) { v.firstMakeupInfo && v.productCb && v.productCb(De({}, t || {}, { data: v.firstMakeupInfo })), v._setLoading(!1), Te && setTimeout(function() { it.addClass(it.one(".mtar__switch-sel-mobile"), "retract") }, 3e3) }), v._emit("init", { value: !0 }), console.log("Time页面初始化时间=", new Date - v.pageInitStartTime);
                                                        case 5:
                                                        case "end":
                                                            return t.stop()
                                                    }
                                                }, t)
                                            })))) : (v._setLoading(!1), console.error("[_initModel error-6]", i.msg))
                                        });
                                    case 38:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function() { return t.apply(this, arguments) })
                    }, {
                        key: "configBeautyAndDenoise",
                        value: function(t) {
                            var e = 0 < arguments.length && void 0 !== t ? t : {};
                            console.log("[configBeautyAndDenoise]:", e), this.arClassInstance && (this.arClassInstance.applyConfigBeauty(e.SkinRetouch, e.Whitening, e.Sharpen), this.arClassInstance.applyConfigDenoise(e.Denoise))
                        }
                    }, { key: "oncameraframe", value: function() { var t = null; return this.video.ready && !this.cameraManager.isHiddened && 0 !== this.sourceWidth && 0 !== this.sourceHeight && (this.imageContext.setTransform(-1, 0, 0, 1, this.video.videoWidth, 0), this.imageContext.drawImage(this.video, 0, 0, this.video.videoWidth, this.video.videoHeight), this.imageContext.setTransform(1, 0, 0, 1, 0, 0), t = this.imageContext.getImageData(this.sourceLeft, this.sourceTop, this.sourceWidth, this.sourceHeight)), t } }, {
                        key: "doRenderFrame",
                        value: function(t) {
                            if (this.video.ready && !this.cameraManager.isHiddened && 0 !== this.sourceWidth && 0 !== this.sourceHeight && this.arClassInstance.getModuleState() && this.arClassInstance.getRunPluginInstance() && null != t)
                                if (this.flag) {
                                    this.arClassInstance.setCenterMode(2);
                                    var e = new oe(1);
                                    this.arClassInstance.setRawImage(t.data, this.sourceWidth, this.sourceHeight, 4 * this.sourceWidth, 1), this.arClassInstance.renderFrame(0, 0, 0, 0), e.print("【首帧渲染耗时】"), this.flag = !1
                                } else this.renderTime.setenable(this.enableTimePrint), this.renderTime.average_record(0), this.arClassInstance.setRawImage(t.data, this.sourceWidth, this.sourceHeight, 4 * this.sourceWidth, 1), this.arClassInstance.renderFrame(0, 0, 0, 0), this.renderTime.average_record(1), this.renderTime.average_print("[doRenderFrame]")
                        }
                    }, {
                        key: "calcFramelayout",
                        value: function(t, e, n, i) {
                            var r = 0,
                                a = 0,
                                o = 0,
                                s = 0;
                            if (0 === t || 0 === e || 0 === n || 0 === i) return { x: r, y: a, w: o, h: s };
                            var l = 1,
                                c = 0;
                            return a = (r = (t / e < n / i ? (c = i / e, o = Math.round(t * c * l), s = Math.round(i * l), (n < o || i < s) && (l = 1, o = Math.round(t * c * l), s = Math.round(i * l))) : (c = n / t, o = Math.round(n * l), s = Math.round(e * c * l), (n < o || i < s) && (l = 1, o = Math.round(n * l), s = Math.round(e * c * l))), Math.round((n - o) / 2)), Math.round((i - s) / 2)), { x: r, y: a, w: o, h: s }
                        }
                    }, {
                        key: "startAnimate",
                        value: function() {
                            var t = this;
                            if (0 === this.sourceWidth || 0 === this.sourceHeight) {
                                this.mainCanvasWidth = this.video.videoWidth, this.mainCanvasHeight = this.video.videoHeight;
                                var e = this.calcFramelayout(this.mainCanvasWidth, this.mainCanvasHeight, this.video.videoWidth, this.video.videoHeight);
                                this.sourceLeft = e.x, this.sourceTop = e.y, this.sourceWidth = e.w, this.sourceHeight = e.h, 0 !== this.sourceWidth && 0 !== this.sourceHeight && (this.imageContext.width = this.video.videoWidth, this.imageContext.height = this.video.videoHeight, this.imageCanvas.width = this.video.videoWidth, this.imageCanvas.height = this.video.videoHeight, console.log("sourceLeft : ", this.sourceLeft), console.log("sourceTop := ", this.sourceTop), console.log("sourceWidth := ", this.sourceWidth), console.log("sourceHeight := ", this.sourceHeight), this.realFrameWidth = this.video.videoWidth, this.realFrameHeight = this.video.videoHeight)
                            }
                            this._emit("startAnimate"), this.limitFPS ? (this.timeNow = window.performance.now(), this.elapsedMs = this.timeNow - this.timeLast, this.elapsedMs >= this.fpsInterval && null !== this.frameBuffer ? (this.elapsedMs >= 2 * this.fpsInterval ? this.timeLast = this.timeNow - this.fpsInterval : this.timeLast = this.timeNow - this.elapsedMs % this.fpsInterval, this.doRenderFrame(this.frameBuffer), this.frameBuffer = null) : null === this.frameBuffer && (this.frameBuffer = this.oncameraframe()), this.animateID = requestAnimationFrame(function() { t.startAnimate() })) : (this.oncameraframe(), this.doRenderFrame(), 0 <= this.timeoutId && clearTimeout(this.timeoutId), this.timeoutId = Ot.Safari || Ot.Edge ? setTimeout(function() { t.animateID = requestAnimationFrame(function() { t.startAnimate() }) }, 20) : setTimeout(function() { t.animateID = requestAnimationFrame(function() { t.startAnimate() }) }, 1e3 / 60))
                        }
                    }, {
                        key: "getResourceList",
                        value: function() {
                            var t = this.arDomain,
                                e = this.arVersion,
                                n = "cpu" === this.mode ? "_CPU" : "";
                            return { modelDict: { "H5-MtFace": { key: "H5-MtFace", url: "".concat(t, "/data/H5").concat(n, "/mtface_medium.bin?_=").concat(e) }, "H5-WasmBin": { key: "H5-WasmBin", url: "".concat(t, "/H5/NativeModule").concat(n, "/wasm/RenderModule.wasm?_=").concat(e) }, "H5-AsmjsBin": { key: "H5-AsmjsBin", url: "".concat(t, "/H5/NativeModule").concat(n, "/asmjs/RenderModule.js.mem?_=").concat(e) }, mtface_3dpos: { key: "mtface_3dpos", url: "".concat(t, "/data/mtface_3dpos.manis?_=").concat(e) }, hair_segment_mobile: { key: "hair_segment_mobile", url: "".concat(t, "/data/hairsegment/hair_segment_mobile.manis?_=").concat(e) }, hair_segment_pc: { key: "hair_segment_pc", url: "".concat(t, "/data/hairsegment/hair_segment_pc.manis?_=").concat(e) } }, ARKernelBuiltin: { category: "ARKernelBuiltin", key: "ARKernelBuiltin", id: "003d4d624f3730f559edbcfd6439eec9", url: "".concat(t, "/resource/ARKernelBuiltin.zip?_=").concat(e) } }
                        }
                    }, {
                        key: "resetEffect",
                        value: function(i) {
                            var r = this;
                            if (this.isInited) {
                                this.detectPhotoResult && it.one(".js_mtar__detect-close").click();
                                var a = this.returnData.bind(this);
                                if (i = i || Le, this.isCompare) { var t = a(4, {}); return q(t.msg), void i(t) }
                                if (this.isInited && this.arClassInstance) Oe() ? i(a(1, {})) : it.hasClass(this.$els.noCamModal, "mtar__show") || (console.log("-------resetEffect", this.allZips, this.nowAllZips), this._setLoading(!0), setTimeout(function() {
                                    r.allZips = r.allZips.filter(function(t) { return 0 !== t.filter });
                                    var t = [],
                                        n = [];
                                    r.nowAllZips.forEach(function(e) {-1 === r.allZips.findIndex(function(t) { return t.key === e.key }) && t.push(r._removeMaterial(e.key).catch(function() {})) }), P.a.all(t).finally(function() {
                                        if (console.log("[素材移除完毕]"), je = Ne = 0, !it.hasClass(r.$els.noCamModal, "mtar__show")) {
                                            r.nowAllZips = JSON.parse(M()(r.allZips));
                                            var e = r.nowAllZips && r.nowAllZips[0] && void 0 !== r.nowAllZips[0].filter;
                                            r.arClassInstance.setDistributeControlMode(e ? 1 : 0), r.nowAllZips.forEach(function(t) { e && r.arClassInstance.applyConfigAlpha(t.key, t.filter), n.push(r._selectMaterial(t).catch(function() {})) }), P.a.all(n).finally(function() { r.detectMode ? r._setLoading(!1) : r._handleImage(r.currentImg).finally(function() { r._setLoading(!1) }), i(a(0, {})), console.log("[素材总添加时间]", je), console.log("[素材总应用时间]", Ne) })
                                        }
                                    })
                                }, 50));
                                else i(a(6, {}))
                            }
                        }
                    }, {
                        key: "_selectMaterial",
                        value: (n = O()(L.a.mark(function t(i) {
                            var r = this;
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        return t.abrupt("return", new P.a(function() {
                                            var n = O()(L.a.mark(function t(e, n) {
                                                return L.a.wrap(function(t) {
                                                    for (;;) switch (t.prev = t.next) {
                                                        case 0:
                                                            if (it.hasClass(r.$els.noCamModal, "mtar__show")) return t.abrupt("return");
                                                            t.next = 2;
                                                            break;
                                                        case 2:
                                                            if (i.key && i.url) { t.next = 6; break }
                                                            return console.error("[selectMaterial] key或者url不存在"), n("[selectMaterial] key或者url不存在"), t.abrupt("return", !1);
                                                        case 6:
                                                            if (!r.detectMode && r.faceErrorCode) return q(r.faceErrorText[r.faceErrorCode] || ""), n(r.faceErrorText[r.faceErrorCode] || ""), t.abrupt("return");
                                                            t.next = 10;
                                                            break;
                                                        case 10:
                                                            if (r.isCompare) return n(), t.abrupt("return");
                                                            t.next = 13;
                                                            break;
                                                        case 13:
                                                            r.detectPhotoResult && (r._setImg(2, null), r.animateID || r.startAnimate()), r._addMaterial(i).then(function() { e() }).catch(function() { n() });
                                                        case 15:
                                                        case "end":
                                                            return t.stop()
                                                    }
                                                }, t)
                                            }));
                                            return function(t, e) { return n.apply(this, arguments) }
                                        }()));
                                    case 1:
                                    case "end":
                                        return t.stop()
                                }
                            }, t)
                        })), function(t) { return n.apply(this, arguments) })
                    }, { key: "_removeMaterial", value: function(n) { var i = this; return new P.a(function(t, e) { it.hasClass(i.$els.noCamModal, "mtar__show") || (console.log("[LOG] 移除素材", n), i.changeModuleRunState(n, !1), 0 === i.arClassInstance.applyMaterialNull(n).code ? t() : (console.error("[ERROR] 取消素材失败", n), e())) }) } }, {
                        key: "_addMaterial",
                        value: function(u) {
                            var h = this;
                            return new P.a(function(a, o) {
                                console.log("[LOG] 添加素材", u);
                                var s = u.id,
                                    l = u.key,
                                    t = u.url,
                                    c = +new Date;
                                h.changeModuleRunState(l, !0), Se(t + "?_=".concat(h.arVersion)).then(function(t) {
                                    var e = t.dataBin,
                                        n = t.fileInfos,
                                        i = +new Date;
                                    if (0 === h.arClassInstance.applyMaterialZip(l, "/" + s, e, n).code) {
                                        var r = +new Date;
                                        console.log("Time添加素材时间=", r - c), console.log("Time应用素材时间=", r - i), je += r - c, Ne += r - i, a()
                                    } else console.error("[ERROR] 应用素材失败", u), o()
                                }).catch(function(t) { console.error("[ERROR] 应用素材失败1", t), o() })
                            })
                        }
                    }, {
                        key: "changeModuleRunState",
                        value: function(t, e) {
                            var n = this;
                            switch (t) {
                                case "HairDye":
                                    this.arClassInstance.optionEnableHairSegmentRun = e, console.log("当前 Key : ", t, ", 头发分割状态: ", e);
                                case "Makeup3D":
                                    this.arClassInstance.optionEnableDl3DPosRun = e, console.log("当前 Key : ", t, ", 3D姿态预估状态:", e);
                                default:
                                    this.nowSelectKeys[t] = e;
                                    var i = b()(this.nowSelectKeys).filter(function(t) { return n.nowSelectKeys[t] });
                                    console.log("引用情况： ", i), 0 < i.length ? this.arClassInstance.optionEnableFaceDetectRun = !0 : this.arClassInstance.optionEnableFaceDetectRun = !1, console.log("当前 Key : ", t, ", 人脸检测状态:", this.arClassInstance.optionEnableFaceDetectRun)
                            }
                        }
                    }, {
                        key: "_handleImage",
                        value: function(i) {
                            var u = this;
                            return new P.a(function(l, t) {
                                if (i) {
                                    var e = i.width,
                                        n = i.height;
                                    if (u.arClassInstance.getModuleState() && u.arClassInstance.getRunPluginInstance()) {
                                        it.addClass(u.$els.uploadImgWrap, "mtar__show"), u.arClassInstance.setCenterMode(0), u.flag = !0;
                                        var c = +new Date;
                                        u.arClassInstance.makeupRawImage(i.data, e, n, 4 * e, 1, "imageData", function(t) {
                                            console.log("[LOG] makeupRawImage callback", t);
                                            var e = +new Date,
                                                n = t.code,
                                                i = t.data;
                                            if (u._setFaceErr(0), 3 === n || 0 === n && i && 0 === i.faceCount) return u.currentImg = null, u.currentImgBase64 = null, u._setFaceErr(1e3), void l({ code: 1e3 });
                                            if (i) {
                                                var r = new ImageData(new Uint8ClampedArray(i.imageData), i.frameWidth),
                                                    a = document.createElement("canvas"),
                                                    o = a.getContext("2d");
                                                a.width = i.frameWidth, a.height = i.frameHeight, o.putImageData(r, 0, 0), u._setImg(1, a.toDataURL("image/jpeg", 1)), l();
                                                var s = +new Date;
                                                console.log("图片处理时间1", s - c), console.log("图片处理时间2", e - c)
                                            } else it.removeClass(u.$els.uploadImgWrap, "mtar__show")
                                        }), u.arClassInstance.renderFrame()
                                    } else t(), u._setLoading(!1), console.error("[ERROR] 上传图片 handleImage: 插件未初始化成功")
                                }
                            })
                        }
                    }, { key: "destroyAR", value: function() { console.log("destroyAR"), 0 <= this.timeoutId && clearTimeout(this.timeoutId), cancelAnimationFrame(this.animateID), this.animateID = null, this.arClassInstance && this.arClassInstance.getModuleState() && this.arClassInstance.getRunPluginInstance() && (this.arClassInstance.releaseRenderContext(), this.arClassInstance = null, this.cameraManager && (this.cameraManager.stop(), this.cameraManager.removelistener())) } }, { key: "_uploadImg", value: function(t) { It(this.$els.selModal, "mtar__show", !1), this.faceErrorCode = 0, this.fileChange(t), this._trackUse() } }, {
                        key: "_bindEvent",
                        value: function() {
                            var e = this;
                            it.one(".js_mtar__sel-import").addEventListener("change", function(t) { e._track("webar_photo_enter", { source: "button_clk" }), e._uploadImg(t) }), it.one(".js_mtar__sel-detect").addEventListener("click", function() { 0 !== e.compatibility.RTC && 2 !== e.compatibility.RTC ? (e._track("webar_realtime_enter", { source: "button_clk" }), e.openDetectMode()) : it.addClass(e.$els.noCamModal, "mtar__show") }), this.$els.switchSel.addEventListener("click", function() { e._track("webar_switch_method", {}), e.openSelectModal() }), this.$els.screenshotIcon.addEventListener("click", this.photoClick.bind(this)), it.one(".js_mtar__detect-close").addEventListener("click", function() { e._setImg(2, null), e.resumeCamera() }), Te ? (this.$els.compareIcon.addEventListener("touchstart", this.switchMaterialTouchStart.bind(this)), this.$els.compareIcon.addEventListener("touchend", this.switchMaterialTouchEnd.bind(this))) : (this.$els.zoomIn.addEventListener("click", this.zoomInClick.bind(this)), this.$els.zoomOut.addEventListener("click", this.zoomOutClick.bind(this)), this.$els.compareIcon.addEventListener("click", function() { e.toggleCompare(null, !0) })), it.one(".js__mtar-modal-nocam .icon-close-wrap").addEventListener("click", function() { it.removeClass(e.$els.noCamModal, "mtar__show") }), (Te ? it.one(".mtar__cir_btn.car") : it.one(".mtar__header-car")).addEventListener("click", function() { e._emit("clickCar", { value: e.carInfo.map(function(t) { return t.releatedSkuId }) }) })
                        }
                    }, {
                        key: "_initTrack",
                        value: function() {
                            var t = this;
                            if (Ue) {
                                var e = document.createElement("script");
                                e.setAttribute("src", "https://public.static.meitudata.com/meitu/mtstat-sdk/mtstat-sdk.min.js"), e.onload = function() { window.mtstat.init({ app_version: "1.0.0", app_language: "zh", debug: !1, app_key: "52537BC149BB528A", env: "release", log_level: 0 }), window.mtstat.pageview({ title: "webar SDK-试妆页面", event_id: "webar_enter", channel: t.authInfo.channel_code, webar_type: Te ? "mobile" : "pc", userAgent: window.navigator.userAgent, system: Te ? Ot.iOS ? "ios" : "android" : Ot.Mac ? "mac" : "windows", platform: window.navigator.platform, webgl: t.compatibility.webgl, asm: t.compatibility.asm, wasm: t.compatibility.wasm }), t._emit("trackData", { eventName: "webar_enter", params: { channel: t.authInfo.channel_code } }) }, document.getElementsByTagName("head")[0].appendChild(e)
                            }
                        }
                    }, {
                        key: "_trackUse",
                        value: function() {
                            var t = { function: this.detectMode ? "ar_makeup" : "photo_makeup", way: "native_button" },
                                e = this.productType,
                                n = this.nowSkuId,
                                i = this.nowLookId;
                            "look" === e && i ? this._track("webar_makeup_use", De({}, t, { look_id: i })) : "sku" === e && n && this._track("webar_products_use", De({}, t, { sku_id: n }))
                        }
                    }, {
                        key: "switchMaterialTouchStart",
                        value: function() {
                            if (this.isCompare) return !1;
                            this.toggleCompare(null, !0)
                        }
                    }, { key: "switchMaterialTouchEnd", value: function() { this.toggleCompare("end") } }, {
                        key: "toggleCompare",
                        value: (r = O()(L.a.mark(function t(e, n, i) {
                            var r, a = this;
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (i = i || Le, !this.detectMode && this.faceErrorCode) return q(this.faceErrorText[this.faceErrorCode]), t.abrupt("return");
                                        t.next = 4;
                                        break;
                                    case 4:
                                        if (r = !this.isCompare, this._setCompare(r), this.detectMode) { t.next = 14; break }
                                        if ("end" === e && this.handleImageResultCache == this.handleImageResult) return this.resetEffect(), t.abrupt("return", !1);
                                        t.next = 10;
                                        break;
                                    case 10:
                                        r ? (this.handleImageResultCache = this.handleImageResult, this._setImg(1, this.currentImgBase64)) : (this._setImg(1, this.handleImageResultCache), this.handleImageResultCache = null), i(), t.next = 16;
                                        break;
                                    case 14:
                                        this._setLoading(!0), r ? setTimeout(O()(L.a.mark(function t() {
                                            var e;
                                            return L.a.wrap(function(t) {
                                                for (;;) switch (t.prev = t.next) {
                                                    case 0:
                                                        e = [], a.nowAllZips.forEach(function(t) { e.push(a._removeMaterial(t.key).catch(function() {})) }), P.a.all(e).finally(function() { a.nowAllZips = [], console.log("[素材移除完毕]"), a._setLoading(!1), i() });
                                                    case 3:
                                                    case "end":
                                                        return t.stop()
                                                }
                                            }, t)
                                        })), 50) : this.resetEffect(i);
                                    case 16:
                                        r && n && this._track("webar_effect_comparison", { function: this.detectMode ? "ar_makeup" : "photo_makeup" });
                                    case 17:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function(t, e, n) { return r.apply(this, arguments) })
                    }, { key: "_setCompare", value: function(t) { this.isCompare = t, It(this.$els.compareIcon, "active", t), It(this.$els.uploadImgWrap, "mtar__comparing", t), It(this.$els.screenshotIcon, "disabled", t) } }, {
                        key: "photoClick",
                        value: function(t, s, l) {
                            var c = this;
                            if (t && t.preventDefault(), this.isCompare) {
                                var e = this.i18n.t("error_msg.4");
                                (l || q)(e)
                            } else if (this.detectPhotoResult) kt(s, this.detectPhotoResult, l);
                            else {
                                if (this.detectMode) {
                                    this.pauseCamera();
                                    var n, i, r, a, o = this.imageContext.getImageData(0, 0, this.sourceWidth, this.sourceHeight);
                                    if (this.arClassInstance.setRawImage(o.data, this.sourceWidth, this.sourceHeight, 4 * this.sourceWidth, 1), this.sourceWidth / this.sourceHeight > this.canvasWidth / this.canvasHeight) {
                                        var u = this.sourceHeight / this.canvasHeight;
                                        i = 0, n = (this.sourceWidth - this.canvasWidth * u) / 2, r = this.canvasWidth * u, a = this.canvasHeight * u
                                    } else {
                                        var h = this.sourceWidth / this.canvasWidth;
                                        n = 0, i = (this.sourceHeight - this.canvasHeight * h) / 2, r = this.canvasWidth * h, a = this.canvasHeight * h
                                    }
                                    this.arClassInstance.getRenderImageData(n, i, Math.round(r), Math.round(a), function(t) {
                                        var e = t.code,
                                            n = t.data;
                                        if (0 === e && n) {
                                            var i = new ImageData(new Uint8ClampedArray(n.imageData), n.frameWidth),
                                                r = document.createElement("canvas"),
                                                a = r.getContext("2d");
                                            r.width = n.frameWidth, r.height = n.frameHeight, a.putImageData(i, 0, 0), c._setImg(2, r.toDataURL("image/jpeg", 1));
                                            var o = c.detectPhotoResult;
                                            l ? kt(s, o, l) : Te ? q(c.i18n.t("save_image_tip")) : c.downloadImage(o)
                                        }
                                    })
                                } else {
                                    if (this.faceErrorCode) return void(l || q)(this.faceErrorText[this.faceErrorCode] || "");
                                    var d = this.handleImageResult;
                                    if (l) return void kt(s, d, l);
                                    Te ? q(this.i18n.t("save_image_tip")) : this.downloadImage(d)
                                }
                                if (!Oe())
                                    if ("sku" === this.productType) {
                                        var f = {};
                                        this.allZips.forEach(function(t) { f[t.trackKey] = t.skuId }), this._track("webar_phototaken", De({}, f, { function: this.detectMode ? "ar_makeup" : "photo_makeup" }))
                                    } else this._track("webar_makeup_takephoto", { look_id: this.nowLookId, function: this.detectMode ? "ar_makeup" : "photo_makeup" })
                            }
                        }
                    }, {
                        key: "downloadImage",
                        value: function(t) {
                            var e = document.createElement("canvas"),
                                n = +new Date + ".jpeg";
                            if (e.msToBlob && window.navigator.msSaveBlob) {
                                var i = new Image;
                                i.onload = function() {
                                    e.width = i.width, e.height = i.height, e.getContext("2d").drawImage(i, 0, 0);
                                    var t = e.msToBlob();
                                    window.navigator.msSaveBlob(t, n)
                                }, i.src = t
                            } else {
                                var r = document.getElementById("download-image-link") || document.createElement("a");
                                r.download = n, r.href = t, r.click()
                            }
                        }
                    }, {
                        key: "fileChange",
                        value: (e = O()(L.a.mark(function t(e) {
                            var i, n, r, a, o = this;
                            return L.a.wrap(function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (i = function() {
                                                var t = it.one(".js_mtar__sel-import");
                                                t && (t.value = null)
                                            }, (n = mt(n = e.target.files, this.i18n)).length) { t.next = 7; break }
                                        return this.openSelectModal(), i(), t.abrupt("return");
                                    case 7:
                                        return this._setLoading(!0), this._clearImgs(), It(this.$els.screenshotIcon, "mtar__show", !0), It(this.$els.compareIcon, "mtar__show", !0), t.next = 13, pt(n[0], this.canvasHeight);
                                    case 13:
                                        r = t.sent, (a = new Image).onload = O()(L.a.mark(function t() {
                                            var e, n;
                                            return L.a.wrap(function(t) {
                                                for (;;) switch (t.prev = t.next) {
                                                    case 0:
                                                        if (o.zoom = 1, o.setZoom(), e = document.createElement("canvas"), n = e.getContext("2d"), e.width = a.width, e.height = a.height, console.log("缩放后尺寸", a.width, "-", a.height), n.drawImage(a, 0, 0), o.currentImgBase64 = r, o.currentImg = n.getImageData(0, 0, a.width, a.height), !o.isInited) { t.next = 15; break }
                                                        o.switchDetectMode(!1), o.resetEffect(function() { o._setLoading(!1) }), t.next = 18;
                                                        break;
                                                    case 15:
                                                        return t.next = 17, o._initBrowser();
                                                    case 17:
                                                        o._initPlugin();
                                                    case 18:
                                                        i();
                                                    case 19:
                                                    case "end":
                                                        return t.stop()
                                                }
                                            }, t)
                                        })), a.onerror = function() { o._setLoading(!1), i() }, a.src = r;
                                    case 18:
                                    case "end":
                                        return t.stop()
                                }
                            }, t, this)
                        })), function(t) { return e.apply(this, arguments) })
                    }, { key: "_clearImgs", value: function() { this.arClassInstance && this.arClassInstance.clearCanvas(0, 0, 0, 255), this.handleImageResultCache = null, this._setImg(1, null), this._setImg(2, null) } }, {
                        key: "setZoom",
                        value: function() {
                            var t = it.one(".mtar__result-img");
                            t && (t.style.transform = "translate(-50%, -50%) scale(".concat(this.zoom, ")"), t.style.webkitTransform = "translate(-50%, -50%) scale(".concat(this.zoom, ")")), It(this.$els.zoomIn, "disabled", this.zoom >= this.maxZoom), It(this.$els.zoomOut, "disabled", this.zoom <= this.minZoom)
                        }
                    }, { key: "zoomInClick", value: function(t) { t.preventDefault(), this.zoom >= this.maxZoom || (this.detectMode || !this.faceErrorCode ? (this.zoom = vt(this.zoom + this.zoomStep, 2), this.setZoom(), this._track("webar_zoomin", {})) : q(this.faceErrorText[this.faceErrorCode])) } }, { key: "zoomOutClick", value: function(t) { t.preventDefault(), this.detectMode || !this.faceErrorCode ? this.zoom <= this.minZoom || (this.zoom = vt(this.zoom - this.zoomStep, 2), this.setZoom(), this._track("webar_zoomout", {})) : q(this.faceErrorText[this.faceErrorCode]) } }, { key: "switchDetectMode", value: function(t) { this._setDetectMode(t), this.isInited && this._setCameraStatus(t) } }, {
                        key: "_setCameraStatus",
                        value: function(t) {
                            (this.cameraStatus = t) ? (this.arClassInstance.clearCanvas(0, 0, 0, 255), this.cameraManager && (this.video.paused && this.video.play(), this.animateID || (this.cameraManager.initMedia(), this.cameraManager.start(), this.startAnimate())), this.arClassInstance && this.arClassInstance.getModuleState() && this.arClassInstance.getRunPluginInstance() && this.arClassInstance.resetFaces()) : (0 <= this.timeoutId && clearTimeout(this.timeoutId), cancelAnimationFrame(this.animateID), this.animateID = null, this.cameraManager && (this.cameraManager.stop(), this.cameraManager.removelistener()))
                        }
                    }, {
                        key: "_setImg",
                        value: function(t, e) {
                            var n, i;
                            switch (t) {
                                case 1:
                                    n = i = this.$els.uploadImgWrap, this.handleImageResult = e;
                                    break;
                                case 2:
                                    n = this.$els.screenshotWrap, i = it.one(".mtar__detect-photo-result .mtar__img"), this.detectPhotoResult = e
                            }
                            e ? it.addClass(n, "mtar__show") : it.removeClass(n, "mtar__show");
                            var r = it.one(".mtar__result-img", i);
                            r && r.setAttribute("src", e || "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNg5+D8DwABSgEYsZo8DAAAAABJRU5ErkJggg==")
                        }
                    }, { key: "_setFaceErr", value: function(t) { 0 !== (this.faceErrorCode = t) && (q(this.faceErrorText[this.faceErrorCode] || ""), this.openSelectModal()) } }, {
                        key: "_emit",
                        value: function(t, e) {
                            var n = this;
                            setTimeout(function() { n.el.dispatchEvent(new CustomEvent(t, { detail: e, bubbles: !1, cancelable: !1 })) }, 0)
                        }
                    }, {
                        key: "_track",
                        value: function(t, e) {
                            console.log("[track event]", t, e), this._emit("trackData", { eventName: t, params: e });
                            Ue && window.mtstat && window.mtstat.track(t, e)
                        }
                    }, { key: "openSelectModal", value: function() { this._clearImgs(), it.addClass(this.$els.selModal, "mtar__show"), this.options.isCustomSwitchPage && this._emit("openSelect", { value: !0 }), this.isCompare && (this.toggleCompare(), this._setLoading(!1)), this.detectMode && (this.detectMode = !1, this._setCameraStatus(!1)) } }, {
                        key: "openDetectMode",
                        value: function() {
                            var t = this;
                            this.notSupDetect || this.notSupportWebar || Te && yt() ? this._emit("openSelect", { value: !1 }) : (this._clearImgs(), this._fromClick = !0, It(this.$els.selModal, "mtar__show", !1), It(this.$els.screenshotIcon, "mtar__show", !0), It(this.$els.compareIcon, "mtar__show", !0), this.isInited ? (this.cameraManager ? (this._setLoading(!0), this.switchDetectMode(!0)) : (this._setLoading(!0), this.switchDetectMode(!0), this._initCamera(), this.startAnimate()), this.resetEffect(function() { t._setLoading(!1) })) : (this.detectMode = !0, this._setLoading(!0), this.pageInit()), this._trackUse())
                        }
                    }, { key: "addEventListener", value: function(t, e) { this.el.addEventListener(t, e) } }, { key: "removeEventListener", value: function(t, e) { this.el.removeEventListener(t, e) } }, { key: "reset", value: function() {} }, {
                        key: "pauseCamera",
                        value: function() {
                            if (this.detectMode) {
                                if (this.detectPhotoResult) return;
                                It(this.$els.screenshotIcon, "mtar__show", !1), It(this.$els.compareIcon, "mtar__show", !1), this._setCameraStatus(!1)
                            }
                        }
                    }, {
                        key: "resumeCamera",
                        value: function() {
                            if (this.detectMode) {
                                if (this.detectPhotoResult) return;
                                It(this.$els.screenshotIcon, "mtar__show", !0), It(this.$els.compareIcon, "mtar__show", !0), this._setCameraStatus(!0)
                            }
                        }
                    }, {
                        key: "screenshot",
                        value: function(t, e) {
                            var n = 0 < arguments.length && void 0 !== t ? t : "base64",
                                i = 1 < arguments.length && void 0 !== e ? e : Le;
                            Oe() ? i(this.i18n.t("no_action_tip")) : this.photoClick(null, n, i)
                        }
                    }, { key: "isApplyLoaded", value: function() {} }, { key: "isInCompare", value: function() { return this.isCompare } }, { key: "enableCompare", value: function() { Oe() || this.detectPhotoResult || this.isCompare || this.switchMaterialTouchStart() } }, { key: "disableCompare", value: function() { Oe() || this.detectPhotoResult || this.isCompare && this.switchMaterialTouchEnd() } }, { key: "uploadimageFile", value: function(t) { this._track("webar_photo_enter", { source: "other" }), this._uploadImg(t) } }, {
                        key: "enterDetectMode",
                        value: function() {
                            if (!this.detectMode) {
                                if (0 === this.compatibility.RTC || 2 === this.compatibility.RTC) return this._emit("openSelect", { value: !0 }), void it.addClass(this.$els.noCamModal, "mtar__show");
                                this._track("webar_realtime_enter", { source: "other" }), this.openDetectMode()
                            }
                        }
                    }, { key: "enterCustomSwitchPage", value: function() { this.openSelectModal() } }, { key: "changePreviewSize", value: function(t, e) { this.options.customWidth = t, this.options.customHeight = e, this._initCanvasVideoSize(t, e) } }]), i
                }(),
                Ze = n("uxLE"),
                Ge = n.n(Ze);

            function Ye(e, t) {
                var n = b()(e);
                if (d.a) {
                    var i = d()(e);
                    t && (i = i.filter(function(t) { return u()(e, t).enumerable })), n.push.apply(n, i)
                }
                return n
            }

            function Ve(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? Ye(Object(n), !0).forEach(function(t) { _()(e, t, n[t]) }) : l.a ? s()(e, l()(n)) : Ye(Object(n)).forEach(function(t) { o()(e, t, u()(n, t)) })
                }
                return e
            }

            function Qe(i) {
                return function() {
                    var t, e = F()(i);
                    if (function() { if ("undefined" == typeof Reflect || !m.a) return; if (m.a.sham) return; if ("function" == typeof Proxy) return 1; try { return Date.prototype.toString.call(m()(Date, [], function() {})), 1 } catch (t) { return } }()) {
                        var n = F()(this).constructor;
                        t = m()(e, arguments, n)
                    } else t = e.apply(this, arguments);
                    return E()(this, t)
                }
            }

            function Je() {}

            function Xe(t, e, n) { n ? (it.addClass(t, "mtar__hide"), it.addClass(e, "mtar__hide")) : (it.removeClass(t, "mtar__hide"), it.removeClass(e, "mtar__hide"), it.removeClass(e, "disabled"), it.addClass(t, "disabled")) }

            function qe() { return it.hasClass(it.one(".mtar__sel"), "mtar__show") }
            var $e = Ot.Mobile,
                tn = $e ? "-mobile" : "";
            var en = function(t) {
                x()(a, t);
                var r = Qe(a);

                function a(t) {
                    var e, n;
                    w()(this, a), (n = r.call(this, t))._dealProductType("none"), n.showProduct = n.options.showProduct;
                    var i = !(n.showProduct && n.options.showCarIcon);
                    return It(it.one(".mtar__product-wrap"), "mtar__hide", !n.showProduct), It(it.one(".mtar__header"), "mtar__hide", i), It(it.one(".mtar__cir_btn.car"), "mtar__hide", i), n.showProduct && it.addClass(it.one(".mtar"), "mtar__show-product"), n._setCarInfo([]), n.proEls = (e = { proWrap: it.one(".mtar__product-wrap"), proName: it.one(".mtar__product-name"), skuName: it.one(".mtar__sku-name"), skuList: it.one(".mtar__sku-list".concat(tn)), skuBtnWrap: it.one(".mtar__skus-wrap".concat(tn, " .mtar__btn-wrap")) }, _()(e, "proWrap", it.one(".mtar__product-wrap")), _()(e, "lookName", it.one(".mtar__look-name")), _()(e, "lookList", it.one(".mtar__look-list")), _()(e, "lookSkuList", it.one(".mtar__look-sku-list".concat(tn))), _()(e, "tabList", it.one($e ? ".mtar__look-list-mobile" : ".mtar__tab-look-list")), _()(e, "tabIconL", it.one(".mtar__tab-look .mtar__tab-click-l")), _()(e, "tabIconR", it.one(".mtar__tab-look .mtar__tab-click-r")), _()(e, "skuTabIconL", it.one(".mtar__tab-look-sku .mtar__tab-click-l")), _()(e, "skuTabIconR", it.one(".mtar__tab-look-sku .mtar__tab-click-r")), _()(e, "lookBtnWrap", it.one(".mtar__look-wrap".concat(tn, " .mtar__btn-wrap"))), _()(e, "lookDetail", it.one(".mtar__look-wrap".concat(tn, " .mtar__look-product-detail"))), e), n.bindProductEvent(), n
                }
                return k()(a, [{
                    key: "bindProductEvent",
                    value: function() {
                        var r = this;
                        if (this.showProduct)
                            if ($e) {
                                var a, o;
                                it.one(".mtar__look-slide").addEventListener("click", this._lookListSlide.bind(this)), it.one(".mtar__look-sku-list-mobile").addEventListener("click", function() { r.isLookUp = !1, r._lookListSlide() });
                                var s = 0;
                                it.one(".js__mtar-slide-look").addEventListener("touchstart", function(t) { a = t.touches[0].pageX, o = t.touches[0].pageY }), it.one(".js__mtar-slide-look").addEventListener("touchend", function(t) {
                                    if (new Date - s < 500) return !1;
                                    s = +new Date;
                                    var e = t.changedTouches[0].pageX,
                                        n = t.changedTouches[0].pageY,
                                        i = function(t, e, n, i) {
                                            var r = n - t,
                                                a = i - e;
                                            if (Math.abs(r) < 2 && Math.abs(a) < 2) return "";
                                            var o = 180 * Math.atan2(a, r) / Math.PI;
                                            return -135 <= o && o <= -45 ? "top" : 45 < o && o < 135 ? "bottom" : ""
                                        }(a, o, e, n);
                                    console.log("------dir", i), i && ("top" === i ? r.isLookUp = !1 : "bottom" === i && (r.isLookUp = !0), r._lookListSlide())
                                })
                            } else this.proEls.tabIconL.addEventListener("click", function() { r._clickLookLR("left") }), this.proEls.tabIconR.addEventListener("click", function() { r._clickLookLR("right") }), this.proEls.skuTabIconL.addEventListener("click", function() { r._clickSkuLookLR("left") }), this.proEls.skuTabIconR.addEventListener("click", function() { r._clickSkuLookLR("right") })
                    }
                }, {
                    key: "_setCarInfo",
                    value: function(t) {
                        this.carInfo = t;
                        var e = this.carInfo.length,
                            n = it.one(".js__mtar-car-num".concat(tn));
                        n.innerText = e, It(n, "mtar__hide", !e)
                    }
                }, {
                    key: "_dealBtns",
                    value: function() {
                        var i = this,
                            t = document.createElement("div");
                        t.className = "btn-wrap", t.innerHTML = Ge.a, t.querySelector(".mtar__product-car .text").innerText = this.i18n.t("add_to_car"), t.querySelector(".mtar__product-buy .text").innerText = this.i18n.t("buy"), t.querySelector(".mtar__product-car").addEventListener("click", function() {
                            var t = i._getNowSelectSkus();
                            if (t.length) {
                                i._setCarInfo(i.carInfo.concat(t));
                                var e = t.map(function(t) { return t.releatedSkuId }) || [];
                                i._emit("addCar", { value: e });
                                var n = qe();
                                i._track("webar_shopping_cart", { sku_id: e.join(","), function: n ? "other" : i.detectMode ? "ar_makeup" : "photo_makeup" })
                            } else q(i.i18n.t("select_skus_tip"), 3e3, "pc-toast")
                        }), t.querySelector(".mtar__product-buy").addEventListener("click", function() {
                            var t = i._getNowSelectSkus().map(function(t) { return t.releatedSkuId }) || [];
                            i._emit("buyNow", { value: t });
                            var e = qe();
                            i._track("webar_buy_now", { sku_id: t.join(","), function: e ? "other" : i.detectMode ? "ar_makeup" : "photo_makeup" })
                        }), this.btnEle = t, it.one(".mtar__no-pro").innerText = this.i18n.t("no_sku_tip")
                    }
                }, {
                    key: "_getNowSelectSkus",
                    value: function() {
                        var n = this,
                            i = [];
                        if ("sku" === this.productType) {
                            var t = this.skuInfo.skuMap,
                                e = it.one(".mtar__sku-list-li".concat(tn, ".active"));
                            if (e && e.dataset) {
                                var r = e.dataset.id;
                                t[r] && i.push(t[r])
                            }
                        } else if ("look" === this.productType) {
                            (($e ? it.all(".mtar__look-sku-list-li-mobile.active") : it.all(".mtar__look-sku-tab-item.active")) || []).forEach(function(t) {
                                var e = t.dataset.id;
                                n.lookSkuMap[e] && i.push(n.lookSkuMap[e])
                            })
                        }
                        return i
                    }
                }, {
                    key: "_name",
                    value: function(t, e) {
                        var n = 0 < arguments.length && void 0 !== t ? t : {},
                            i = 1 < arguments.length && void 0 !== e ? e : "string";
                        return n[this.lang] || n[this.defaultLang] || ("string" === i ? "-" : {})
                    }
                }, {
                    key: "initProductBySkuId",
                    value: function(l, c, u) {
                        var h = this;
                        c = c || Je;
                        var d = this.returnData.bind(this);
                        if (!this.isAuthReady) {
                            var t = this.authErr || {},
                                e = d(1100409 === t.code ? 300 : 7, {}, t.msg);
                            return c(e), void console.error(e.msg)
                        }
                        St({ url: "/web_sdk/product/sku_info", data: { sku_id: l } }).then(function(t) {
                            if (0 === t.code) {
                                var i = t.data;
                                if (!b()(i).length || !i.sku_list || !i.sku_list.length) { var e = d(5); return u && q(e.msg), c(e), void h._dealProductType("none") }
                                var n = {},
                                    r = {};
                                (i.lang_data || []).forEach(function(t) { return n[t.lang] = t });
                                var a = h._name(n).product_name || "-",
                                    o = i.sku_list.map(function(t) {
                                        var e = h._name(t.lang_data),
                                            n = { id: t.id, sku_id: t.sku_id, sku_pic: t.sku_pic, zip_url: t.zip_url, category: i.category, categoryKey: Re[i.category], skuName: e };
                                        return r[t.sku_id] = Ve({}, n, { releatedSkuId: t.sku_id }), n
                                    });
                                h.skuInfo = { id: i.id, product_id: i.product_id, product_pic: i.product_pic, productName: a, skuList: o, skuMap: r }, h.initProductSkuDom(), h._selectSkuById(l, c, u)
                            } else {
                                var s = t && t.msg || h.i18n.t("server_error");
                                u && q(s), c(d(101, {}, s)), h._dealProductType("none")
                            }
                        })
                    }
                }, {
                    key: "initProductByLookId",
                    value: function(a, o, s) {
                        var l = this;
                        o = o || Je;
                        var c = this.returnData.bind(this);
                        if (!this.isAuthReady) {
                            var t = this.authErr || {},
                                e = c(1100409 === t.code ? 300 : 7, {}, t.msg);
                            return o(e), void console.error(e.msg)
                        }
                        $e && (this.isLookUp = !1, it.removeClass(this.$els.mtarWrap, "is-up"), it.removeClass(this.lookWrapEle, "up"), it.removeClass(this.lookWrapEle, "down")), this.lookWrapEle ? this._clickLook(a, o, s) : St({ url: "/web_sdk/makeup/get_list" }).then(function(t) {
                            if (0 === t.code) {
                                var e = t.data;
                                if (!e || !e.length) { var n = c(5); return o(n), void l._dealProductType("none") }
                                var i = e.map(function(t) { var e = t.lang_data || {}; return Ve({}, t, { lookName: l._name(e) }) });
                                l.lookInfo = i, l.initProductLookDom(), l._clickLook(a, o, s)
                            } else {
                                var r = t && t.msg || l.i18n.t("server_error");
                                s && q(r), o(c(101, {}, r)), l._dealProductType("none")
                            }
                        })
                    }
                }, {
                    key: "initProductSkuDom",
                    value: function() {
                        var e = this;
                        if (this.showProduct) {
                            it.all(".mtar__sku-list-li".concat(tn, ":not(.product)")).forEach(function(t) { t.removeEventListener("click", e.skuHandler) });
                            var t = this.skuInfo,
                                n = t.productName,
                                i = t.skuList,
                                r = t.product_pic,
                                a = "";
                            $e ? (it.one(".mtar__sku-list-li-mobile.product").innerHTML = '\n                <div class="img-wrap">\n                    <img class="img" src="'.concat(r, '" alt="">\n                </div>\n                <div class="text mtar__elli-2">').concat(n, "</div>\n            "), i.forEach(function(t) { a += '\n                    <div class="mtar__sku-list-li-mobile" data-id="'.concat(t.sku_id, '">\n                        <div class="img-wrap">\n                            <img class="img" src="').concat(t.sku_pic, '" alt="">\n                            <div class="mtar__pos-a mtar__flex mask">\n                                <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                            </div>\n                        </div>\n                        <div class="text mtar__elli-2">').concat(t.skuName, "</div>\n                    </div>\n                ") })) : (i.forEach(function(t) { a += '\n                    <div class="mtar__sku-list-li" data-id="'.concat(t.sku_id, '">\n                        <img class="img" src="').concat(t.sku_pic, '" alt="">\n                    </div>\n                ') }), this.proEls.proName.innerText = n), this.proEls.skuList.innerHTML = a, this.skuHandler = this._clickSku.bind(this), it.all(".mtar__sku-list-li".concat(tn, ":not(.product)")).forEach(function(t) { t.addEventListener("click", e.skuHandler) })
                        }
                    }
                }, {
                    key: "initProductLookDom",
                    value: function() {
                        var n = this;
                        if (this.showProduct) {
                            this._dealProductType("look"), ($e ? it.all(".mtar__look-list-li-mobile") : it.all(".mtar__tab-look-list .mtar__tab-item")).forEach(function(t) { t.removeEventListener("click", n.lookHandler) }), this.proEls.lookDetail.innerText = this.i18n.t("product_detail"), this.lookWrapEle = it.one($e ? ".mtar__look-wrap-mobile" : ".mtar__look-wrap");
                            var i = "";
                            $e ? (this.lookInfo.forEach(function(t) {
                                var e = n._name(t.lang_data);
                                i += '\n                    <div class="mtar__look-list-li-mobile" data-id="'.concat(t.makeup_id, '">\n                        <div class="img-wrap">\n                            <img class="img" src="').concat(t.makeup_pic, '" alt="">\n                            <div class="mtar__pos-a mtar__flex mask">\n                                <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                            </div>\n                        </div>\n                        <div class="text mtar__elli">').concat(e, "</div>\n                    </div>\n                ")
                            }), this.proEls.tabList.innerHTML = i, this.lookTabItemW = it.one(".mtar__look-list-li-mobile").clientWidth) : (this.lookInfo.forEach(function(t) {
                                var e = n._name(t.lang_data);
                                i += '\n                    <div class="mtar__tab-item" data-id="'.concat(t.makeup_id, '">\n                        <div class="mtar__tab-img-wrap mtar__img-wrap">\n                            <img class="mtar__tab-img" src="').concat(t.makeup_pic, '" alt="">\n                        </div>\n                        <div class="mtar__tab-text mtar__elli-2">').concat(e, "</div>\n                    </div>\n                ")
                            }), this.proEls.tabList.innerHTML = i, this.lookTabMR = 22, this.lookTabW = this.proEls.tabList.clientWidth, this.lookTabItemW = it.one(".mtar__tab-look-list .mtar__tab-item").clientWidth, this.lookTabScrW = this.lookInfo.length * (this.lookTabItemW + this.lookTabMR) - this.lookTabMR, It(this.proEls.tabIconL, "mtar__hide", this.lookInfo.length < 6), It(this.proEls.tabIconR, "mtar__hide", this.lookInfo.length < 6)), this.lookHandler = function(t) { n._clickLook(t, null, !0, !0) }, ($e ? it.all(".mtar__look-list-li-mobile") : it.all(".mtar__tab-look-list .mtar__tab-item")).forEach(function(t) { t.addEventListener("click", n.lookHandler) })
                        }
                    }
                }, { key: "_clickLookLR", value: function(t) { console.log("----dir", t, this.lookTabItemW, this.lookTabMR, this.lookTabScrW, this.lookTabW), wt({ dir: t, w: this.lookTabItemW + this.lookTabMR, scrW: this.lookTabScrW, tabW: this.lookTabW, tabList: this.proEls.tabList, tabIconL: this.proEls.tabIconL, tabIconR: this.proEls.tabIconR }) } }, {
                    key: "_clickLookTabItem",
                    value: function(t) {
                        if (this.showProduct)
                            if ($e) {
                                if (it.removeClass(it.one(".mtar__look-list-li-mobile.active"), "active"), !t) return;
                                it.addClass(t, "active");
                                var e = .04 * this.winW,
                                    n = t.getBoundingClientRect().left;
                                (n < e || n > this.winW - this.lookTabItemW - e) && _t(this.proEls.tabList, t.offsetLeft - e)
                            } else {
                                if (it.removeClass(it.one(".mtar__tab-look-list .active"), "active"), !t) return;
                                it.addClass(t, "active"), gt({ l: t.offsetLeft + this.lookTabItemW / 2 - this.lookTabW / 2, scrW: this.lookTabScrW, tabW: this.lookTabW, tabList: this.proEls.tabList, tabIconL: this.proEls.tabIconL, tabIconR: this.proEls.tabIconR })
                            }
                    }
                }, {
                    key: "_clickLook",
                    value: function(t, e, n, i) {
                        e = e || Je, this._dealProductType("look"), this.isCompare && this._setCompare(!1);
                        var r, a, o = qe();
                        if ("number" == typeof t || "string" == typeof t) r = t, a = $e ? it.one('.mtar__look-list-li-mobile[data-id="'.concat(r, '"]')) : it.one('.mtar__tab-look-list .mtar__tab-item[data-id="'.concat(r, '"]'));
                        else if (r = (a = t.currentTarget).dataset.id, this.nowLookId && this.nowLookId === r) return void e(this.returnData(o ? 1 : 0, { look_id: r }));
                        this._clickLookTabItem(a), this._selectLookById(r, e, n), i && !o && this._track("webar_makeup_use", { look_id: r, function: this.detectMode ? "ar_makeup" : "photo_makeup", way: "list_switch" })
                    }
                }, {
                    key: "_selectLookById",
                    value: function(p, v, _) {
                        var g = this;
                        this._dealProductType("look");
                        var w = this.returnData.bind(this);
                        ($e ? it.all(".mtar__look-sku-list-li-mobile") : it.all(".mtar__look-sku-tab-item")).forEach(function(t) { t.removeEventListener("click", g.lookSkuHandler) }), this.nowLookId = p, this.nowSkuId = null, this.proEls.lookBtnWrap.appendChild(this.btnEle), St({ url: "/web_sdk/makeup/detail", data: { makeup_id: p } }).then(function(t) {
                            if (0 === t.code) {
                                var r = t.data;
                                if (!r || !r.id) return Xe(g.proEls.skuTabIconL, g.proEls.skuTabIconR, !0), v && v(w(5, { look_id: p })), void(g.nowLookId = null);
                                var e = r.sku_list || [],
                                    a = [],
                                    o = {},
                                    s = {},
                                    l = {},
                                    n = !!b()(r.products || {}).length;
                                if (b()(r.products || {}).forEach(function(t) {
                                        var e = r.products[t],
                                            n = (e.lang_data || []).find(function(t) { return t.lang === g.lang });
                                        n = n || ((e.lang_data || []).find(function(t) { return t.lang === g.defaultLang }) || {}), o[e.id] = n.product_name, s[e.id] = n.brand_name
                                    }), e.forEach(function(t) {
                                        var e = t.sku_info || {},
                                            n = e.sku_id,
                                            i = t.category;
                                        l[n] = Ve({}, t, { releatedSkuId: n }), 1 == r.effect_from && a.push({ id: "".concat(n, "_").concat(i), key: Re[i], url: e.zip_url })
                                    }), g.lookSkuMap = l, 2 == r.effect_from && r.related_makeup_info) {
                                    var i = r.related_makeup_info.material_data || [],
                                        c = Me;
                                    i.forEach(function(t) {
                                        var e = c[t.position] || t.position;
                                        t.zip_url && Re[e] && a.push({ id: "".concat(t.material_id, "_").concat(e), key: Re[e], url: t.zip_url, filter: t.filter ? t.filter / 100 : t.filter })
                                    })
                                }
                                g.allZips = a;
                                var u = { look_id: r.makeup_id };
                                if (qe() && (v && v(w(1, u)), g.productCb = v = null), g.isInited ? g.resetEffect(function(t) { v && v(Ve({}, t || {}, { data: u })) }) : g.firstMakeupInfo = u, !g.showProduct) return;
                                if ($e || (g.proEls.lookName.innerText = g._name(r.lang_data)), !e.length || !n) return g.proEls.lookSkuList.innerHTML = '\n                        <div class="no-text">'.concat(g.i18n.t("no_product"), "</div>\n                    "), void Xe(g.proEls.skuTabIconL, g.proEls.skuTabIconR, !0);
                                var h, d = "";
                                if ($e) e.forEach(function(t) {
                                    var e = t.product_id,
                                        n = t.sku_info || {},
                                        i = g._name(n.lang_data);
                                    s[e] && o[e] && (d += '\n                                <div class="mtar__look-sku-list-li-mobile" data-id="'.concat(n.sku_id, '">\n                                    <div class="img-wrap">\n                                        <img class="img" src="').concat(n.sku_pic, '" alt="">\n                                        <div class="mtar__pos-a mtar__flex mask">\n                                            <svg class="i" width="17" height="11" viewBox="0 0 17 11" xmlns="http://www.w3.org/2000/svg"><path d="M15.527.904l-9.9 9.9L.745 5.92" fill-rule="nonzero" stroke="#FFF" fill="none"/></svg>\n                                        </div>\n                                    </div>\n                                    <div class="text-wrap">\n                                        <div class="text-1 mtar__elli">').concat(s[e], '</div>\n                                        <div class="text-2 mtar__elli-2">').concat(o[e], '</div>\n                                        <div class="text-3 mtar__elli-2">').concat(i, "</div>\n                                    </div>\n                                </div>\n                            "))
                                }), g.proEls.lookSkuList.innerHTML = d, h = it.all(".mtar__look-sku-list-li-mobile") || [];
                                else {
                                    e.forEach(function(t) {
                                        var e = t.product_id,
                                            n = t.sku_info || {},
                                            i = g._name(n.lang_data);
                                        s[e] && o[e] && (d += '\n                                <div class="mtar__look-sku-tab-item" data-id="'.concat(n.sku_id, '">\n                                    <div class="mtar__img-wrap mtar__look-sku-tab-img-wrap">\n                                        <img class="mtar__look-sku-tab-img" src="').concat(n.sku_pic, '" alt="">\n                                    </div>\n                                    <div class="mtar__look-sku-tab-text-wrap">\n                                        <div class="mtar__look-sku-tab-id">').concat(s[e], '</div>\n                                        <div class="mtar__look-sku-tab-tit mtar__elli-2">').concat(o[e], '</div>\n                                        <div class="mtar__look-sku-tab-text mtar__elli-2">').concat(i, "</div>\n                                    </div>\n                                </div>\n                            "))
                                    }), g.proEls.lookSkuList.innerHTML = d, h = it.all(".mtar__look-sku-tab-item") || [], g.lookSkuTabMR = 10, g.lookSkuTabW = g.proEls.lookSkuList.clientWidth, g.lookSkuTabItemW = (h[0] || {}).clientWidth, g.lookSkuTabScrW = e.length * (g.lookSkuTabItemW + g.lookSkuTabMR) - g.lookSkuTabMR;
                                    var f = h.length < 3;
                                    Xe(g.proEls.skuTabIconL, g.proEls.skuTabIconR, f)
                                }
                                if (!h.length) return g.proEls.lookSkuList.innerHTML = '\n                        <div class="no-text">'.concat(g.i18n.t("no_product"), "</div>\n                    "), void Xe(g.proEls.skuTabIconL, g.proEls.skuTabIconR, !0);
                                g.lookSkuHandler = g._clickLookSkuItem.bind(g), h.forEach(function(t) { t.addEventListener("click", g.lookSkuHandler) })
                            } else {
                                g.nowLookId = null;
                                var m = t && t.msg || g.i18n.t("server_error");
                                g.proEls.lookSkuList.innerHTML = "", Xe(g.proEls.skuTabIconL, g.proEls.skuTabIconR, !0), _ && q(m), v && v(w(1100412 === t.code ? 5 : 101, {}, m))
                            }
                        })
                    }
                }, { key: "_lookListSlide", value: function() { this.isLookUp ? (it.removeClass(this.lookWrapEle, "up"), it.addClass(this.lookWrapEle, "down"), it.removeClass(this.$els.mtarWrap, "is-up")) : (it.removeClass(this.lookWrapEle, "down"), it.addClass(this.lookWrapEle, "up"), it.addClass(this.$els.mtarWrap, "is-up")), this.isLookUp = !this.isLookUp } }, {
                    key: "_clickLookSkuItem",
                    value: function(t) {
                        var e = t.currentTarget;
                        it.hasClass(e, "active") ? it.removeClass(e, "active") : it.addClass(e, "active")
                    }
                }, { key: "_clickSkuLookLR", value: function(t) { wt({ dir: t, w: 2 * (this.lookSkuTabItemW + this.lookSkuTabMR), scrW: this.lookSkuTabScrW, tabW: this.lookSkuTabW, tabList: this.proEls.lookSkuList, tabIconL: this.proEls.skuTabIconL, tabIconR: this.proEls.skuTabIconR }) } }, {
                    key: "_clickSku",
                    value: function(t) {
                        this.isCompare && this._setCompare(!1);
                        var e = t.currentTarget.dataset.id;
                        this.nowSkuId !== e && (this._selectSkuById(e, null, !0), qe() || this._track("webar_products_use", { sku_id: e, function: this.detectMode ? "ar_makeup" : "photo_makeup", way: "list_switch" }))
                    }
                }, {
                    key: "_selectSkuById",
                    value: function(t, e, n) {
                        var i = this.returnData.bind(this);
                        this._dealProductType("sku"), this.nowSkuId = t, this.nowLookId = null, this.proEls.skuBtnWrap.appendChild(this.btnEle);
                        var r = ((this.skuInfo || {}).skuMap || {})[t],
                            a = i(5);
                        if (!r) return n && q(a.msg), void(e && e(a));
                        if (this.showProduct) {
                            var o = it.one(".mtar__sku-list-li".concat(tn, '[data-id="').concat(t, '"]'));
                            if (!o) return void(e && e(a));
                            var s = it.one(".mtar__sku-list-li".concat(tn, ".active"));
                            if (s && it.removeClass(s, "active"), it.addClass(o, "active"), $e) {
                                var l = it.one(".mtar__sku-list-li-mobile").clientWidth,
                                    c = this.proEls.skuList.getBoundingClientRect().left,
                                    u = o.getBoundingClientRect().left - c,
                                    h = this.winW - c - 1.5 * l;
                                (u < 0 || h < u) && _t(this.proEls.skuList, o.offsetLeft)
                            } else this.proEls.skuName.innerText = r.skuName
                        }
                        var d = r.categoryKey;
                        this.allZips = [{ skuId: t, trackKey: Ce[d], id: "".concat(r.id, "_").concat(d), key: d, url: r.zip_url }];
                        var f = { sku_id: r.releatedSkuId };
                        qe() && (e && e(i(1, f)), this.productCb = e = null), this.isInited ? this.resetEffect(function(t) { e && e(Ve({}, t || {}, { data: f })) }) : this.firstMakeupInfo = f
                    }
                }, { key: "_dealProductType", value: function(t) { this.productType = t; var e = this.$els && this.$els.wrap || it.one(".mtar"); "look" === t ? (it.removeClass(e, "mtar__no-sku-look"), it.removeClass(e, "mtar__in-sku"), it.addClass(e, "mtar__in-look")) : "sku" === t ? (it.removeClass(e, "mtar__no-sku-look"), it.addClass(e, "mtar__in-sku"), it.removeClass(e, "mtar__in-look")) : (this.nowSkuId = null, this.nowLookId = null, it.addClass(e, "mtar__no-sku-look"), it.removeClass(e, "mtar__in-sku"), it.removeClass(e, "mtar__in-look"), this.allZips = [], this.resetEffect()) } }, {
                    key: "applyMakeupBySkuId",
                    value: function(t, e, n) {
                        var i = 1 < arguments.length && void 0 !== e ? e : Je,
                            r = 2 < arguments.length ? n : void 0;
                        if (!t) return i(this.returnData(2)), void this._dealProductType("none");
                        this.isCompare && this._setCompare(!1);
                        var a = qe();
                        this.productCb = i;
                        var o = ((this.skuInfo || {}).skuMap || {})[t];
                        "sku" === this.productType && o ? this._selectSkuById(t, i, r) : this.initProductBySkuId(t, i, r), a || this._track("webar_products_use", { sku_id: t, function: this.detectMode ? "ar_makeup" : "photo_makeup", way: "call_api" })
                    }
                }, {
                    key: "applyMakeupByLookId",
                    value: function(t, e, n) {
                        var i = 1 < arguments.length && void 0 !== e ? e : Je,
                            r = 2 < arguments.length ? n : void 0;
                        if (t) {
                            this.isCompare && this._setCompare(!1);
                            var a = qe();
                            if (this.productCb = i, this.showProduct) {
                                var o = $e ? it.one('.mtar__look-list-li-mobile[data-id="'.concat(t, '"]')) : it.one('.mtar__tab-look-list .mtar__tab-item[data-id="'.concat(t, '"]'));
                                this.lookWrapEle && !o && (this.lookWrapEle = null, this.productType = "none"), "look" === this.productType ? this._clickLook(t, i, r) : this.initProductByLookId(t, i, r)
                            } else this._selectLookById(t, i, r);
                            a || this._track("webar_makeup_use", { look_id: t, function: this.detectMode ? "ar_makeup" : "photo_makeup", way: "call_api" })
                        } else i(this.returnData(3))
                    }
                }]), a
            }(Ke)
        },
        tjlA: function(t, W, e) {
            "use strict";
            (function(t) {
                /*!
                 * The buffer module from node.js, for the browser.
                 *
                 * @author   Feross Aboukhadijeh <http://feross.org>
                 * @license  MIT
                 */
                var i = e("H7XF"),
                    a = e("kVK+"),
                    o = e("49sm");

                function n() { return h.TYPED_ARRAY_SUPPORT ? 2147483647 : 1073741823 }

                function s(t, e) { if (n() < e) throw new RangeError("Invalid typed array length"); return h.TYPED_ARRAY_SUPPORT ? (t = new Uint8Array(e)).__proto__ = h.prototype : (null === t && (t = new h(e)), t.length = e), t }

                function h(t, e, n) { if (!(h.TYPED_ARRAY_SUPPORT || this instanceof h)) return new h(t, e, n); if ("number" != typeof t) return r(this, t, e, n); if ("string" == typeof e) throw new Error("If encoding is specified then the first argument must be a string"); return c(this, t) }

                function r(t, e, n, i) {
                    if ("number" == typeof e) throw new TypeError('"value" argument must not be a number');
                    return "undefined" != typeof ArrayBuffer && e instanceof ArrayBuffer ? function(t, e, n, i) {
                        if (e.byteLength, n < 0 || e.byteLength < n) throw new RangeError("'offset' is out of bounds");
                        if (e.byteLength < n + (i || 0)) throw new RangeError("'length' is out of bounds");
                        e = void 0 === n && void 0 === i ? new Uint8Array(e) : void 0 === i ? new Uint8Array(e, n) : new Uint8Array(e, n, i);
                        h.TYPED_ARRAY_SUPPORT ? (t = e).__proto__ = h.prototype : t = u(t, e);
                        return t
                    }(t, e, n, i) : "string" == typeof e ? function(t, e, n) {
                        "string" == typeof n && "" !== n || (n = "utf8");
                        if (!h.isEncoding(n)) throw new TypeError('"encoding" must be a valid string encoding');
                        var i = 0 | f(e, n),
                            r = (t = s(t, i)).write(e, n);
                        r !== i && (t = t.slice(0, r));
                        return t
                    }(t, e, n) : function(t, e) { if (h.isBuffer(e)) { var n = 0 | d(e.length); return 0 === (t = s(t, n)).length ? t : (e.copy(t, 0, 0, n), t) } if (e) { if ("undefined" != typeof ArrayBuffer && e.buffer instanceof ArrayBuffer || "length" in e) return "number" != typeof e.length || function(t) { return t != t }(e.length) ? s(t, 0) : u(t, e); if ("Buffer" === e.type && o(e.data)) return u(t, e.data) } throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.") }(t, e)
                }

                function l(t) { if ("number" != typeof t) throw new TypeError('"size" argument must be a number'); if (t < 0) throw new RangeError('"size" argument must not be negative') }

                function c(t, e) {
                    if (l(e), t = s(t, e < 0 ? 0 : 0 | d(e)), !h.TYPED_ARRAY_SUPPORT)
                        for (var n = 0; n < e; ++n) t[n] = 0;
                    return t
                }

                function u(t, e) {
                    var n = e.length < 0 ? 0 : 0 | d(e.length);
                    t = s(t, n);
                    for (var i = 0; i < n; i += 1) t[i] = 255 & e[i];
                    return t
                }

                function d(t) { if (t >= n()) throw new RangeError("Attempt to allocate Buffer larger than maximum size: 0x" + n().toString(16) + " bytes"); return 0 | t }

                function f(t, e) {
                    if (h.isBuffer(t)) return t.length;
                    if ("undefined" != typeof ArrayBuffer && "function" == typeof ArrayBuffer.isView && (ArrayBuffer.isView(t) || t instanceof ArrayBuffer)) return t.byteLength;
                    "string" != typeof t && (t = "" + t);
                    var n = t.length;
                    if (0 === n) return 0;
                    for (var i = !1;;) switch (e) {
                        case "ascii":
                        case "latin1":
                        case "binary":
                            return n;
                        case "utf8":
                        case "utf-8":
                        case void 0:
                            return L(t).length;
                        case "ucs2":
                        case "ucs-2":
                        case "utf16le":
                        case "utf-16le":
                            return 2 * n;
                        case "hex":
                            return n >>> 1;
                        case "base64":
                            return B(t).length;
                        default:
                            if (i) return L(t).length;
                            e = ("" + e).toLowerCase(), i = !0
                    }
                }

                function m(t, e, n) {
                    var i = t[e];
                    t[e] = t[n], t[n] = i
                }

                function p(t, e, n, i, r) {
                    if (0 === t.length) return -1;
                    if ("string" == typeof n ? (i = n, n = 0) : 2147483647 < n ? n = 2147483647 : n < -2147483648 && (n = -2147483648), n = +n, isNaN(n) && (n = r ? 0 : t.length - 1), n < 0 && (n = t.length + n), n >= t.length) {
                        if (r) return -1;
                        n = t.length - 1
                    } else if (n < 0) {
                        if (!r) return -1;
                        n = 0
                    }
                    if ("string" == typeof e && (e = h.from(e, i)), h.isBuffer(e)) return 0 === e.length ? -1 : v(t, e, n, i, r);
                    if ("number" == typeof e) return e &= 255, h.TYPED_ARRAY_SUPPORT && "function" == typeof Uint8Array.prototype.indexOf ? r ? Uint8Array.prototype.indexOf.call(t, e, n) : Uint8Array.prototype.lastIndexOf.call(t, e, n) : v(t, [e], n, i, r);
                    throw new TypeError("val must be string, number or Buffer")
                }

                function v(t, e, n, i, r) {
                    var a, o = 1,
                        s = t.length,
                        l = e.length;
                    if (void 0 !== i && ("ucs2" === (i = String(i).toLowerCase()) || "ucs-2" === i || "utf16le" === i || "utf-16le" === i)) {
                        if (t.length < 2 || e.length < 2) return -1;
                        s /= o = 2, l /= 2, n /= 2
                    }

                    function c(t, e) { return 1 === o ? t[e] : t.readUInt16BE(e * o) }
                    if (r) {
                        var u = -1;
                        for (a = n; a < s; a++)
                            if (c(t, a) === c(e, -1 === u ? 0 : a - u)) { if (-1 === u && (u = a), a - u + 1 === l) return u * o } else -1 !== u && (a -= a - u), u = -1
                    } else
                        for (s < n + l && (n = s - l), a = n; 0 <= a; a--) {
                            for (var h = !0, d = 0; d < l; d++)
                                if (c(t, a + d) !== c(e, d)) { h = !1; break }
                            if (h) return a
                        }
                    return -1
                }

                function _(t, e, n, i) {
                    n = Number(n) || 0;
                    var r = t.length - n;
                    (!i || r < (i = Number(i))) && (i = r);
                    var a = e.length;
                    if (a % 2 != 0) throw new TypeError("Invalid hex string");
                    a / 2 < i && (i = a / 2);
                    for (var o = 0; o < i; ++o) {
                        var s = parseInt(e.substr(2 * o, 2), 16);
                        if (isNaN(s)) return o;
                        t[n + o] = s
                    }
                    return o
                }

                function g(t, e, n, i) { return O(function(t) { for (var e = [], n = 0; n < t.length; ++n) e.push(255 & t.charCodeAt(n)); return e }(e), t, n, i) }

                function w(t, e, n, i) { return O(function(t, e) { for (var n, i, r, a = [], o = 0; o < t.length && !((e -= 2) < 0); ++o) n = t.charCodeAt(o), i = n >> 8, r = n % 256, a.push(r), a.push(i); return a }(e, t.length - n), t, n, i) }

                function b(t, e, n) { return 0 === e && n === t.length ? i.fromByteArray(t) : i.fromByteArray(t.slice(e, n)) }

                function y(t, e, n) {
                    n = Math.min(t.length, n);
                    for (var i = [], r = e; r < n;) {
                        var a, o, s, l, c = t[r],
                            u = null,
                            h = 239 < c ? 4 : 223 < c ? 3 : 191 < c ? 2 : 1;
                        if (r + h <= n) switch (h) {
                            case 1:
                                c < 128 && (u = c);
                                break;
                            case 2:
                                128 == (192 & (a = t[r + 1])) && 127 < (l = (31 & c) << 6 | 63 & a) && (u = l);
                                break;
                            case 3:
                                a = t[r + 1], o = t[r + 2], 128 == (192 & a) && 128 == (192 & o) && 2047 < (l = (15 & c) << 12 | (63 & a) << 6 | 63 & o) && (l < 55296 || 57343 < l) && (u = l);
                                break;
                            case 4:
                                a = t[r + 1], o = t[r + 2], s = t[r + 3], 128 == (192 & a) && 128 == (192 & o) && 128 == (192 & s) && 65535 < (l = (15 & c) << 18 | (63 & a) << 12 | (63 & o) << 6 | 63 & s) && l < 1114112 && (u = l)
                        }
                        null === u ? (u = 65533, h = 1) : 65535 < u && (u -= 65536, i.push(u >>> 10 & 1023 | 55296), u = 56320 | 1023 & u), i.push(u), r += h
                    }
                    return function(t) {
                        var e = t.length;
                        if (e <= k) return String.fromCharCode.apply(String, t);
                        var n = "",
                            i = 0;
                        for (; i < e;) n += String.fromCharCode.apply(String, t.slice(i, i += k));
                        return n
                    }(i)
                }
                W.Buffer = h, W.SlowBuffer = function(t) {+t != t && (t = 0); return h.alloc(+t) }, W.INSPECT_MAX_BYTES = 50, h.TYPED_ARRAY_SUPPORT = void 0 !== t.TYPED_ARRAY_SUPPORT ? t.TYPED_ARRAY_SUPPORT : function() { try { var t = new Uint8Array(1); return t.__proto__ = { __proto__: Uint8Array.prototype, foo: function() { return 42 } }, 42 === t.foo() && "function" == typeof t.subarray && 0 === t.subarray(1, 1).byteLength } catch (t) { return !1 } }(), W.kMaxLength = n(), h.poolSize = 8192, h._augment = function(t) { return t.__proto__ = h.prototype, t }, h.from = function(t, e, n) { return r(null, t, e, n) }, h.TYPED_ARRAY_SUPPORT && (h.prototype.__proto__ = Uint8Array.prototype, h.__proto__ = Uint8Array, "undefined" != typeof Symbol && Symbol.species && h[Symbol.species] === h && Object.defineProperty(h, Symbol.species, { value: null, configurable: !0 })), h.alloc = function(t, e, n) { return i = null, a = e, o = n, l(r = t), r <= 0 || void 0 === a ? s(i, r) : "string" == typeof o ? s(i, r).fill(a, o) : s(i, r).fill(a); var i, r, a, o }, h.allocUnsafe = function(t) { return c(null, t) }, h.allocUnsafeSlow = function(t) { return c(null, t) }, h.isBuffer = function(t) { return !(null == t || !t._isBuffer) }, h.compare = function(t, e) {
                    if (!h.isBuffer(t) || !h.isBuffer(e)) throw new TypeError("Arguments must be Buffers");
                    if (t === e) return 0;
                    for (var n = t.length, i = e.length, r = 0, a = Math.min(n, i); r < a; ++r)
                        if (t[r] !== e[r]) { n = t[r], i = e[r]; break }
                    return n < i ? -1 : i < n ? 1 : 0
                }, h.isEncoding = function(t) {
                    switch (String(t).toLowerCase()) {
                        case "hex":
                        case "utf8":
                        case "utf-8":
                        case "ascii":
                        case "latin1":
                        case "binary":
                        case "base64":
                        case "ucs2":
                        case "ucs-2":
                        case "utf16le":
                        case "utf-16le":
                            return !0;
                        default:
                            return !1
                    }
                }, h.concat = function(t, e) {
                    if (!o(t)) throw new TypeError('"list" argument must be an Array of Buffers');
                    if (0 === t.length) return h.alloc(0);
                    var n;
                    if (void 0 === e)
                        for (n = e = 0; n < t.length; ++n) e += t[n].length;
                    var i = h.allocUnsafe(e),
                        r = 0;
                    for (n = 0; n < t.length; ++n) {
                        var a = t[n];
                        if (!h.isBuffer(a)) throw new TypeError('"list" argument must be an Array of Buffers');
                        a.copy(i, r), r += a.length
                    }
                    return i
                }, h.byteLength = f, h.prototype._isBuffer = !0, h.prototype.swap16 = function() { var t = this.length; if (t % 2 != 0) throw new RangeError("Buffer size must be a multiple of 16-bits"); for (var e = 0; e < t; e += 2) m(this, e, e + 1); return this }, h.prototype.swap32 = function() { var t = this.length; if (t % 4 != 0) throw new RangeError("Buffer size must be a multiple of 32-bits"); for (var e = 0; e < t; e += 4) m(this, e, e + 3), m(this, e + 1, e + 2); return this }, h.prototype.swap64 = function() { var t = this.length; if (t % 8 != 0) throw new RangeError("Buffer size must be a multiple of 64-bits"); for (var e = 0; e < t; e += 8) m(this, e, e + 7), m(this, e + 1, e + 6), m(this, e + 2, e + 5), m(this, e + 3, e + 4); return this }, h.prototype.toString = function() {
                    var t = 0 | this.length;
                    return 0 == t ? "" : 0 === arguments.length ? y(this, 0, t) : function(t, e, n) {
                        var i = !1;
                        if ((void 0 === e || e < 0) && (e = 0), e > this.length) return "";
                        if ((void 0 === n || n > this.length) && (n = this.length), n <= 0) return "";
                        if ((n >>>= 0) <= (e >>>= 0)) return "";
                        for (t = t || "utf8";;) switch (t) {
                            case "hex":
                                return A(this, e, n);
                            case "utf8":
                            case "utf-8":
                                return y(this, e, n);
                            case "ascii":
                                return I(this, e, n);
                            case "latin1":
                            case "binary":
                                return x(this, e, n);
                            case "base64":
                                return b(this, e, n);
                            case "ucs2":
                            case "ucs-2":
                            case "utf16le":
                            case "utf-16le":
                                return E(this, e, n);
                            default:
                                if (i) throw new TypeError("Unknown encoding: " + t);
                                t = (t + "").toLowerCase(), i = !0
                        }
                    }.apply(this, arguments)
                }, h.prototype.equals = function(t) { if (!h.isBuffer(t)) throw new TypeError("Argument must be a Buffer"); return this === t || 0 === h.compare(this, t) }, h.prototype.inspect = function() {
                    var t = "",
                        e = W.INSPECT_MAX_BYTES;
                    return 0 < this.length && (t = this.toString("hex", 0, e).match(/.{2}/g).join(" "), this.length > e && (t += " ... ")), "<Buffer " + t + ">"
                }, h.prototype.compare = function(t, e, n, i, r) {
                    if (!h.isBuffer(t)) throw new TypeError("Argument must be a Buffer");
                    if (void 0 === e && (e = 0), void 0 === n && (n = t ? t.length : 0), void 0 === i && (i = 0), void 0 === r && (r = this.length), e < 0 || n > t.length || i < 0 || r > this.length) throw new RangeError("out of range index");
                    if (r <= i && n <= e) return 0;
                    if (r <= i) return -1;
                    if (n <= e) return 1;
                    if (this === t) return 0;
                    for (var a = (r >>>= 0) - (i >>>= 0), o = (n >>>= 0) - (e >>>= 0), s = Math.min(a, o), l = this.slice(i, r), c = t.slice(e, n), u = 0; u < s; ++u)
                        if (l[u] !== c[u]) { a = l[u], o = c[u]; break }
                    return a < o ? -1 : o < a ? 1 : 0
                }, h.prototype.includes = function(t, e, n) { return -1 !== this.indexOf(t, e, n) }, h.prototype.indexOf = function(t, e, n) { return p(this, t, e, n, !0) }, h.prototype.lastIndexOf = function(t, e, n) { return p(this, t, e, n, !1) }, h.prototype.write = function(t, e, n, i) {
                    if (void 0 === e) i = "utf8", n = this.length, e = 0;
                    else if (void 0 === n && "string" == typeof e) i = e, n = this.length, e = 0;
                    else {
                        if (!isFinite(e)) throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
                        e |= 0, isFinite(n) ? (n |= 0, void 0 === i && (i = "utf8")) : (i = n, n = void 0)
                    }
                    var r = this.length - e;
                    if ((void 0 === n || r < n) && (n = r), 0 < t.length && (n < 0 || e < 0) || e > this.length) throw new RangeError("Attempt to write outside buffer bounds");
                    i = i || "utf8";
                    for (var a, o, s, l, c, u, h = !1;;) switch (i) {
                        case "hex":
                            return _(this, t, e, n);
                        case "utf8":
                        case "utf-8":
                            return c = e, u = n, O(L(t, (l = this).length - c), l, c, u);
                        case "ascii":
                            return g(this, t, e, n);
                        case "latin1":
                        case "binary":
                            return g(this, t, e, n);
                        case "base64":
                            return a = this, o = e, s = n, O(B(t), a, o, s);
                        case "ucs2":
                        case "ucs-2":
                        case "utf16le":
                        case "utf-16le":
                            return w(this, t, e, n);
                        default:
                            if (h) throw new TypeError("Unknown encoding: " + i);
                            i = ("" + i).toLowerCase(), h = !0
                    }
                }, h.prototype.toJSON = function() { return { type: "Buffer", data: Array.prototype.slice.call(this._arr || this, 0) } };
                var k = 4096;

                function I(t, e, n) {
                    var i = "";
                    n = Math.min(t.length, n);
                    for (var r = e; r < n; ++r) i += String.fromCharCode(127 & t[r]);
                    return i
                }

                function x(t, e, n) {
                    var i = "";
                    n = Math.min(t.length, n);
                    for (var r = e; r < n; ++r) i += String.fromCharCode(t[r]);
                    return i
                }

                function A(t, e, n) {
                    var i = t.length;
                    (!e || e < 0) && (e = 0), (!n || n < 0 || i < n) && (n = i);
                    for (var r = "", a = e; a < n; ++a) r += z(t[a]);
                    return r
                }

                function E(t, e, n) { for (var i = t.slice(e, n), r = "", a = 0; a < i.length; a += 2) r += String.fromCharCode(i[a] + 256 * i[a + 1]); return r }

                function S(t, e, n) { if (t % 1 != 0 || t < 0) throw new RangeError("offset is not uint"); if (n < t + e) throw new RangeError("Trying to access beyond buffer length") }

                function F(t, e, n, i, r, a) { if (!h.isBuffer(t)) throw new TypeError('"buffer" argument must be a Buffer instance'); if (r < e || e < a) throw new RangeError('"value" argument is out of bounds'); if (n + i > t.length) throw new RangeError("Index out of range") }

                function R(t, e, n, i) { e < 0 && (e = 65535 + e + 1); for (var r = 0, a = Math.min(t.length - n, 2); r < a; ++r) t[n + r] = (e & 255 << 8 * (i ? r : 1 - r)) >>> 8 * (i ? r : 1 - r) }

                function M(t, e, n, i) { e < 0 && (e = 4294967295 + e + 1); for (var r = 0, a = Math.min(t.length - n, 4); r < a; ++r) t[n + r] = e >>> 8 * (i ? r : 3 - r) & 255 }

                function C(t, e, n, i) { if (n + i > t.length) throw new RangeError("Index out of range"); if (n < 0) throw new RangeError("Index out of range") }

                function P(t, e, n, i, r) { return r || C(t, 0, n, 4), a.write(t, e, n, i, 23, 4), n + 4 }

                function D(t, e, n, i, r) { return r || C(t, 0, n, 8), a.write(t, e, n, i, 52, 8), n + 8 }
                h.prototype.slice = function(t, e) {
                    var n, i = this.length;
                    if ((t = ~~t) < 0 ? (t += i) < 0 && (t = 0) : i < t && (t = i), (e = void 0 === e ? i : ~~e) < 0 ? (e += i) < 0 && (e = 0) : i < e && (e = i), e < t && (e = t), h.TYPED_ARRAY_SUPPORT)(n = this.subarray(t, e)).__proto__ = h.prototype;
                    else {
                        var r = e - t;
                        n = new h(r, void 0);
                        for (var a = 0; a < r; ++a) n[a] = this[a + t]
                    }
                    return n
                }, h.prototype.readUIntLE = function(t, e, n) { t |= 0, e |= 0, n || S(t, e, this.length); for (var i = this[t], r = 1, a = 0; ++a < e && (r *= 256);) i += this[t + a] * r; return i }, h.prototype.readUIntBE = function(t, e, n) { t |= 0, e |= 0, n || S(t, e, this.length); for (var i = this[t + --e], r = 1; 0 < e && (r *= 256);) i += this[t + --e] * r; return i }, h.prototype.readUInt8 = function(t, e) { return e || S(t, 1, this.length), this[t] }, h.prototype.readUInt16LE = function(t, e) { return e || S(t, 2, this.length), this[t] | this[t + 1] << 8 }, h.prototype.readUInt16BE = function(t, e) { return e || S(t, 2, this.length), this[t] << 8 | this[t + 1] }, h.prototype.readUInt32LE = function(t, e) { return e || S(t, 4, this.length), (this[t] | this[t + 1] << 8 | this[t + 2] << 16) + 16777216 * this[t + 3] }, h.prototype.readUInt32BE = function(t, e) { return e || S(t, 4, this.length), 16777216 * this[t] + (this[t + 1] << 16 | this[t + 2] << 8 | this[t + 3]) }, h.prototype.readIntLE = function(t, e, n) { t |= 0, e |= 0, n || S(t, e, this.length); for (var i = this[t], r = 1, a = 0; ++a < e && (r *= 256);) i += this[t + a] * r; return (r *= 128) <= i && (i -= Math.pow(2, 8 * e)), i }, h.prototype.readIntBE = function(t, e, n) { t |= 0, e |= 0, n || S(t, e, this.length); for (var i = e, r = 1, a = this[t + --i]; 0 < i && (r *= 256);) a += this[t + --i] * r; return (r *= 128) <= a && (a -= Math.pow(2, 8 * e)), a }, h.prototype.readInt8 = function(t, e) { return e || S(t, 1, this.length), 128 & this[t] ? -1 * (255 - this[t] + 1) : this[t] }, h.prototype.readInt16LE = function(t, e) { e || S(t, 2, this.length); var n = this[t] | this[t + 1] << 8; return 32768 & n ? 4294901760 | n : n }, h.prototype.readInt16BE = function(t, e) { e || S(t, 2, this.length); var n = this[t + 1] | this[t] << 8; return 32768 & n ? 4294901760 | n : n }, h.prototype.readInt32LE = function(t, e) { return e || S(t, 4, this.length), this[t] | this[t + 1] << 8 | this[t + 2] << 16 | this[t + 3] << 24 }, h.prototype.readInt32BE = function(t, e) { return e || S(t, 4, this.length), this[t] << 24 | this[t + 1] << 16 | this[t + 2] << 8 | this[t + 3] }, h.prototype.readFloatLE = function(t, e) { return e || S(t, 4, this.length), a.read(this, t, !0, 23, 4) }, h.prototype.readFloatBE = function(t, e) { return e || S(t, 4, this.length), a.read(this, t, !1, 23, 4) }, h.prototype.readDoubleLE = function(t, e) { return e || S(t, 8, this.length), a.read(this, t, !0, 52, 8) }, h.prototype.readDoubleBE = function(t, e) { return e || S(t, 8, this.length), a.read(this, t, !1, 52, 8) }, h.prototype.writeUIntLE = function(t, e, n, i) {
                    t = +t, e |= 0, n |= 0, i || F(this, t, e, n, Math.pow(2, 8 * n) - 1, 0);
                    var r = 1,
                        a = 0;
                    for (this[e] = 255 & t; ++a < n && (r *= 256);) this[e + a] = t / r & 255;
                    return e + n
                }, h.prototype.writeUIntBE = function(t, e, n, i) {
                    t = +t, e |= 0, n |= 0, i || F(this, t, e, n, Math.pow(2, 8 * n) - 1, 0);
                    var r = n - 1,
                        a = 1;
                    for (this[e + r] = 255 & t; 0 <= --r && (a *= 256);) this[e + r] = t / a & 255;
                    return e + n
                }, h.prototype.writeUInt8 = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 1, 255, 0), h.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)), this[e] = 255 & t, e + 1 }, h.prototype.writeUInt16LE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 2, 65535, 0), h.TYPED_ARRAY_SUPPORT ? (this[e] = 255 & t, this[e + 1] = t >>> 8) : R(this, t, e, !0), e + 2 }, h.prototype.writeUInt16BE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 2, 65535, 0), h.TYPED_ARRAY_SUPPORT ? (this[e] = t >>> 8, this[e + 1] = 255 & t) : R(this, t, e, !1), e + 2 }, h.prototype.writeUInt32LE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 4, 4294967295, 0), h.TYPED_ARRAY_SUPPORT ? (this[e + 3] = t >>> 24, this[e + 2] = t >>> 16, this[e + 1] = t >>> 8, this[e] = 255 & t) : M(this, t, e, !0), e + 4 }, h.prototype.writeUInt32BE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 4, 4294967295, 0), h.TYPED_ARRAY_SUPPORT ? (this[e] = t >>> 24, this[e + 1] = t >>> 16, this[e + 2] = t >>> 8, this[e + 3] = 255 & t) : M(this, t, e, !1), e + 4 }, h.prototype.writeIntLE = function(t, e, n, i) {
                    if (t = +t, e |= 0, !i) {
                        var r = Math.pow(2, 8 * n - 1);
                        F(this, t, e, n, r - 1, -r)
                    }
                    var a = 0,
                        o = 1,
                        s = 0;
                    for (this[e] = 255 & t; ++a < n && (o *= 256);) t < 0 && 0 === s && 0 !== this[e + a - 1] && (s = 1), this[e + a] = (t / o >> 0) - s & 255;
                    return e + n
                }, h.prototype.writeIntBE = function(t, e, n, i) {
                    if (t = +t, e |= 0, !i) {
                        var r = Math.pow(2, 8 * n - 1);
                        F(this, t, e, n, r - 1, -r)
                    }
                    var a = n - 1,
                        o = 1,
                        s = 0;
                    for (this[e + a] = 255 & t; 0 <= --a && (o *= 256);) t < 0 && 0 === s && 0 !== this[e + a + 1] && (s = 1), this[e + a] = (t / o >> 0) - s & 255;
                    return e + n
                }, h.prototype.writeInt8 = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 1, 127, -128), h.TYPED_ARRAY_SUPPORT || (t = Math.floor(t)), t < 0 && (t = 255 + t + 1), this[e] = 255 & t, e + 1 }, h.prototype.writeInt16LE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 2, 32767, -32768), h.TYPED_ARRAY_SUPPORT ? (this[e] = 255 & t, this[e + 1] = t >>> 8) : R(this, t, e, !0), e + 2 }, h.prototype.writeInt16BE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 2, 32767, -32768), h.TYPED_ARRAY_SUPPORT ? (this[e] = t >>> 8, this[e + 1] = 255 & t) : R(this, t, e, !1), e + 2 }, h.prototype.writeInt32LE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 4, 2147483647, -2147483648), h.TYPED_ARRAY_SUPPORT ? (this[e] = 255 & t, this[e + 1] = t >>> 8, this[e + 2] = t >>> 16, this[e + 3] = t >>> 24) : M(this, t, e, !0), e + 4 }, h.prototype.writeInt32BE = function(t, e, n) { return t = +t, e |= 0, n || F(this, t, e, 4, 2147483647, -2147483648), t < 0 && (t = 4294967295 + t + 1), h.TYPED_ARRAY_SUPPORT ? (this[e] = t >>> 24, this[e + 1] = t >>> 16, this[e + 2] = t >>> 8, this[e + 3] = 255 & t) : M(this, t, e, !1), e + 4 }, h.prototype.writeFloatLE = function(t, e, n) { return P(this, t, e, !0, n) }, h.prototype.writeFloatBE = function(t, e, n) { return P(this, t, e, !1, n) }, h.prototype.writeDoubleLE = function(t, e, n) { return D(this, t, e, !0, n) }, h.prototype.writeDoubleBE = function(t, e, n) { return D(this, t, e, !1, n) }, h.prototype.copy = function(t, e, n, i) {
                    if (n = n || 0, i || 0 === i || (i = this.length), e >= t.length && (e = t.length), e = e || 0, 0 < i && i < n && (i = n), i === n) return 0;
                    if (0 === t.length || 0 === this.length) return 0;
                    if (e < 0) throw new RangeError("targetStart out of bounds");
                    if (n < 0 || n >= this.length) throw new RangeError("sourceStart out of bounds");
                    if (i < 0) throw new RangeError("sourceEnd out of bounds");
                    i > this.length && (i = this.length), t.length - e < i - n && (i = t.length - e + n);
                    var r, a = i - n;
                    if (this === t && n < e && e < i)
                        for (r = a - 1; 0 <= r; --r) t[r + e] = this[r + n];
                    else if (a < 1e3 || !h.TYPED_ARRAY_SUPPORT)
                        for (r = 0; r < a; ++r) t[r + e] = this[r + n];
                    else Uint8Array.prototype.set.call(t, this.subarray(n, n + a), e);
                    return a
                }, h.prototype.fill = function(t, e, n, i) {
                    if ("string" == typeof t) {
                        if ("string" == typeof e ? (i = e, e = 0, n = this.length) : "string" == typeof n && (i = n, n = this.length), 1 === t.length) {
                            var r = t.charCodeAt(0);
                            r < 256 && (t = r)
                        }
                        if (void 0 !== i && "string" != typeof i) throw new TypeError("encoding must be a string");
                        if ("string" == typeof i && !h.isEncoding(i)) throw new TypeError("Unknown encoding: " + i)
                    } else "number" == typeof t && (t &= 255);
                    if (e < 0 || this.length < e || this.length < n) throw new RangeError("Out of range index");
                    if (n <= e) return this;
                    var a;
                    if (e >>>= 0, n = void 0 === n ? this.length : n >>> 0, "number" == typeof(t = t || 0))
                        for (a = e; a < n; ++a) this[a] = t;
                    else {
                        var o = h.isBuffer(t) ? t : L(new h(t, i).toString()),
                            s = o.length;
                        for (a = 0; a < n - e; ++a) this[a + e] = o[a % s]
                    }
                    return this
                };
                var T = /[^+\/0-9A-Za-z-_]/g;

                function z(t) { return t < 16 ? "0" + t.toString(16) : t.toString(16) }

                function L(t, e) {
                    var n;
                    e = e || 1 / 0;
                    for (var i = t.length, r = null, a = [], o = 0; o < i; ++o) {
                        if (55295 < (n = t.charCodeAt(o)) && n < 57344) {
                            if (!r) {
                                if (56319 < n) {-1 < (e -= 3) && a.push(239, 191, 189); continue }
                                if (o + 1 === i) {-1 < (e -= 3) && a.push(239, 191, 189); continue }
                                r = n;
                                continue
                            }
                            if (n < 56320) {-1 < (e -= 3) && a.push(239, 191, 189), r = n; continue }
                            n = 65536 + (r - 55296 << 10 | n - 56320)
                        } else r && -1 < (e -= 3) && a.push(239, 191, 189);
                        if (r = null, n < 128) {
                            if (--e < 0) break;
                            a.push(n)
                        } else if (n < 2048) {
                            if ((e -= 2) < 0) break;
                            a.push(n >> 6 | 192, 63 & n | 128)
                        } else if (n < 65536) {
                            if ((e -= 3) < 0) break;
                            a.push(n >> 12 | 224, n >> 6 & 63 | 128, 63 & n | 128)
                        } else {
                            if (!(n < 1114112)) throw new Error("Invalid code point");
                            if ((e -= 4) < 0) break;
                            a.push(n >> 18 | 240, n >> 12 & 63 | 128, n >> 6 & 63 | 128, 63 & n | 128)
                        }
                    }
                    return a
                }

                function B(t) { return i.toByteArray(function(t) { var e; if ((t = ((e = t).trim ? e.trim() : e.replace(/^\s+|\s+$/g, "")).replace(T, "")).length < 2) return ""; for (; t.length % 4 != 0;) t += "="; return t }(t)) }

                function O(t, e, n, i) { for (var r = 0; r < i && !(r + n >= e.length || r >= t.length); ++r) e[r + n] = t[r]; return r }
            }).call(this, e("yLpj"))
        },
        u2kb: function(t) { t.exports = JSON.parse('{"vertical_tip":"Please browse on a vertical screen for a better result.","detect":"Real-time trial","import_img":"Import","long_press_img":"Press and hold to save photo","loading_hard":"Loading","faceErrorText":{"1000":"No faces detected. Please upload a clear frontal photo.","1003":"No more than 2 faces."},"file_limit":"Only {0} format is supported.","save_image_tip":"Long press or screenshot to save the image","buy":"Purchase now","add_to_car":"Add to cart","no_sku_tip":"Product/Makeup does not exist","custome_btn_tip":"Operation function will be defined by the merchant","product_detail":"Buy","update_page":{"sorry":"Sorry,","tip":"this function is not available. Please update the system and try again.","advice":"Recommended system:","suggest_list":{"1":{"tag":"iPhone","text":"iOS 11 and above"},"2":{"tag":"Android","text":"Android OS 6.0, Chrome V60.0 and above"},"3":{"tag":"Windows","text":"Chrome V60.0, Firefox V70, QQ browser 10.5.1(3824), Opera V64, Edge V44 and above"},"4":{"tag":"macOS","text":"Chrome V60.0, Safari V11, Firefox V70, Opera V64 and above"}}},"no_action_tip":"Unable to carry out the operation","select_modal":{"select":"Choose a trial form","import":"Import","detect":"Real-time trial","tip":"Real-time trial unavailable. Please import a photo for trial."},"switch_select":"Switch trial forms","no_camera_tip":"The camera is not working. Please check your browser settings to make sure the camera is enabled.","no_sup_camera_tip":"设备浏览器不支持相机，请更换浏览器或设备","select_skus_tip":"Please choose a product","server_error":"Server error","horizontal_refresh":"Please do not edit camera access in landscape mode. If you want to continue experiencing, please refresh to try again.","error_tip":"An unexpected error has occurred, please check your network connection and refresh to try again.","err_page_tip":"Display error, please kill the Safari process and scan the QR code again.","no_product":"No products found.","browser_tip":{"1":"Please tap “…” on the upper right corner and select \\"Open in Safari\\".","2":"Please tap “…” on the upper right corner and select \\"Open in Chrome\\"."},"error_msg":{"1":"Currently in select mode. Unable to apply makeup effects.","2":"The SKU ID does not exist.","3":"The makeup look ID does not exist.","4":"Please cancel the comparison mode first","5":"Product/Makeup does not exist","6":"Unable to carry out the operation","7":"Authorization incomplete, please call it later.","100":"Unknown error","101":"Other errors","300":"License expired, please contact Meitu for assistance."}}') },
        uOPS: function(t, e) { t.exports = !0 },
        uekQ: function(t, e, n) { n("dEVD"), t.exports = n("WEpk").parseInt },
        uplh: function(t, e, n) {
            var i = n("ar/p"),
                r = n("mqlF"),
                a = n("5K7Z"),
                o = n("5T2Y").Reflect;
            t.exports = o && o.ownKeys || function(t) {
                var e = i.f(a(t)),
                    n = r.f;
                return n ? e.concat(n(t)) : e
            }
        },
        uxLE: function(t, e) { t.exports = '<div class="btn mtar__product-car">\n    <svg class="icon" width="16" height="14" viewBox="0 0 16 14" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path stroke="#000" d="M.788.867h1.707l2.167 8.089h8.448L15 2.922h-2.494"/><g fill-rule="nonzero"><path fill="#000" d="M8.613 0h1v5.2h-1z"/><path stroke="#000" d="M10.502 4.444l-1.415 1.4-1.414-1.4"/></g><ellipse stroke="#000" cx="5.172" cy="12.04" rx="1.05" ry="1.04"/><ellipse stroke="#000" cx="12.692" cy="12.04" rx="1.05" ry="1.04"/></g></svg>\n    <div class="text"></div>\n</div>\n<div class="btn mtar__product-buy">\n    <svg class="icon" width="17" height="15" viewBox="0 0 17 15" xmlns="http://www.w3.org/2000/svg"><g stroke="#FFF" fill="none" fill-rule="evenodd"><path d="M16.242 1.086L7.757 9.571 4.878 6.692M10.807 1.786a6.5 6.5 0 1 0 3.4 5.714"/></g></svg>\n    <div class="text"></div>\n</div>' },
        v5Dd: function(t, e, n) {
            var i = n("NsO/"),
                r = n("vwuL").f;
            n("zn7N")("getOwnPropertyDescriptor", function() { return function(t, e) { return r(i(t), e) } })
        },
        vBP9: function(t, e, n) {
            var i = n("5T2Y").navigator;
            t.exports = i && i.userAgent || ""
        },
        vDqi: function(t, e, n) { t.exports = n("zuR4") },
        vjea: function(n, t, e) {
            var i = e("TRZx");

            function r(t, e) { return n.exports = r = i || function(t, e) { return t.__proto__ = e, t }, r(t, e) }
            n.exports = r
        },
        vwuL: function(t, e, n) {
            var i = n("NV0k"),
                r = n("rr1i"),
                a = n("NsO/"),
                o = n("G8Mo"),
                s = n("B+OT"),
                l = n("eUtF"),
                c = Object.getOwnPropertyDescriptor;
            e.f = n("jmDH") ? c : function(t, e) {
                if (t = a(t), e = o(e, !0), l) try { return c(t, e) } catch (t) {}
                if (s(t, e)) return r(!i.f.call(t, e), t[e])
            }
        },
        w0Vi: function(t, e, n) {
            "use strict";
            var a = n("xTJ+"),
                o = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
            t.exports = function(t) {
                var e, n, i, r = {};
                return t && a.forEach(t.split("\n"), function(t) {
                    if (i = t.indexOf(":"), e = a.trim(t.substr(0, i)).toLowerCase(), n = a.trim(t.substr(i + 1)), e) {
                        if (r[e] && 0 <= o.indexOf(e)) return;
                        r[e] = "set-cookie" === e ? (r[e] ? r[e] : []).concat([n]) : r[e] ? r[e] + ", " + n : n
                    }
                }), r
            }
        },
        "w2d+": function(t, e, n) {
            "use strict";
            var i = n("hDam"),
                r = n("UO39"),
                a = n("SBuE"),
                o = n("NsO/");
            t.exports = n("MPFp")(Array, "Array", function(t, e) { this._t = o(t), this._i = 0, this._k = e }, function() {
                var t = this._t,
                    e = this._k,
                    n = this._i++;
                return !t || n >= t.length ? (this._t = void 0, r(1)) : r(0, "keys" == e ? n : "values" == e ? t[n] : [n, t[n]])
            }, "values"), a.Arguments = a.Array, i("keys"), i("values"), i("entries")
        },
        w6GO: function(t, e, n) {
            var i = n("5vMV"),
                r = n("FpHa");
            t.exports = Object.keys || function(t) { return i(t, r) }
        },
        wYmx: function(t, e, n) {
            "use strict";
            var a = n("eaoh"),
                o = n("93I4"),
                s = n("MCSJ"),
                l = [].slice,
                c = {};
            t.exports = Function.bind || function(e) {
                var n = a(this),
                    i = l.call(arguments, 1),
                    r = function() {
                        var t = i.concat(l.call(arguments));
                        return this instanceof r ? function(t, e, n) {
                            if (!(e in c)) {
                                for (var i = [], r = 0; r < e; r++) i[r] = "a[" + r + "]";
                                c[e] = Function("F,a", "return new F(" + i.join(",") + ")")
                            }
                            return c[e](t, n)
                        }(n, t.length, t) : s(n, t, e)
                    };
                return o(n.prototype) && (r.prototype = n.prototype), r
            }
        },
        wgeU: function(t, e) {},
        xAGQ: function(t, e, n) {
            "use strict";
            var i = n("xTJ+");
            t.exports = function(e, n, t) { return i.forEach(t, function(t) { e = t(e, n) }), e }
        },
        xHqa: function(t, e, n) {
            var i = n("hfKm");
            t.exports = function(t, e, n) { return e in t ? i(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : t[e] = n, t }
        },
        "xTJ+": function(t, e, n) {
            "use strict";
            var r = n("HSsa"),
                i = Object.prototype.toString;

            function a(t) { return "[object Array]" === i.call(t) }

            function o(t) { return void 0 === t }

            function s(t) { return null !== t && "object" == typeof t }

            function l(t) { return "[object Function]" === i.call(t) }

            function c(t, e) {
                if (null != t)
                    if ("object" != typeof t && (t = [t]), a(t))
                        for (var n = 0, i = t.length; n < i; n++) e.call(null, t[n], n, t);
                    else
                        for (var r in t) Object.prototype.hasOwnProperty.call(t, r) && e.call(null, t[r], r, t)
            }
            t.exports = {
                isArray: a,
                isArrayBuffer: function(t) { return "[object ArrayBuffer]" === i.call(t) },
                isBuffer: function(t) { return null !== t && !o(t) && null !== t.constructor && !o(t.constructor) && "function" == typeof t.constructor.isBuffer && t.constructor.isBuffer(t) },
                isFormData: function(t) { return "undefined" != typeof FormData && t instanceof FormData },
                isArrayBufferView: function(t) { return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(t) : t && t.buffer && t.buffer instanceof ArrayBuffer },
                isString: function(t) { return "string" == typeof t },
                isNumber: function(t) { return "number" == typeof t },
                isObject: s,
                isUndefined: o,
                isDate: function(t) { return "[object Date]" === i.call(t) },
                isFile: function(t) { return "[object File]" === i.call(t) },
                isBlob: function(t) { return "[object Blob]" === i.call(t) },
                isFunction: l,
                isStream: function(t) { return s(t) && l(t.pipe) },
                isURLSearchParams: function(t) { return "undefined" != typeof URLSearchParams && t instanceof URLSearchParams },
                isStandardBrowserEnv: function() { return ("undefined" == typeof navigator || "ReactNative" !== navigator.product && "NativeScript" !== navigator.product && "NS" !== navigator.product) && ("undefined" != typeof window && "undefined" != typeof document) },
                forEach: c,
                merge: function n() {
                    var i = {};

                    function t(t, e) { "object" == typeof i[e] && "object" == typeof t ? i[e] = n(i[e], t) : i[e] = t }
                    for (var e = 0, r = arguments.length; e < r; e++) c(arguments[e], t);
                    return i
                },
                deepMerge: function n() {
                    var i = {};

                    function t(t, e) { "object" == typeof i[e] && "object" == typeof t ? i[e] = n(i[e], t) : i[e] = "object" == typeof t ? n({}, t) : t }
                    for (var e = 0, r = arguments.length; e < r; e++) c(arguments[e], t);
                    return i
                },
                extend: function(n, t, i) { return c(t, function(t, e) { n[e] = i && "function" == typeof t ? r(t, i) : t }), n },
                trim: function(t) { return t.replace(/^\s*/, "").replace(/\s*$/, "") }
            }
        },
        yDJ3: function(G, t, e) {
            (function(t) {
                var e = "Expected a function",
                    i = "__lodash_hash_undefined__",
                    n = 1 / 0,
                    r = "[object Function]",
                    a = "[object GeneratorFunction]",
                    o = "[object Symbol]",
                    s = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
                    l = /^\w*$/,
                    c = /^\./,
                    u = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g,
                    h = /\\(\\)?/g,
                    d = /^\[object .+?Constructor\]$/,
                    f = "object" == typeof t && t && t.Object === Object && t,
                    m = "object" == typeof self && self && self.Object === Object && self,
                    p = f || m || Function("return this")();
                var v, _ = Array.prototype,
                    g = Function.prototype,
                    w = Object.prototype,
                    b = p["__core-js_shared__"],
                    y = (v = /[^.]+$/.exec(b && b.keys && b.keys.IE_PROTO || "")) ? "Symbol(src)_1." + v : "",
                    k = g.toString,
                    I = w.hasOwnProperty,
                    x = w.toString,
                    A = RegExp("^" + k.call(I).replace(/[\\^$.*+?()[\]{}|]/g, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$"),
                    E = p.Symbol,
                    S = _.splice,
                    F = W(p, "Map"),
                    R = W(Object, "create"),
                    M = E ? E.prototype : void 0,
                    C = M ? M.toString : void 0;

                function P(t) {
                    var e = -1,
                        n = t ? t.length : 0;
                    for (this.clear(); ++e < n;) {
                        var i = t[e];
                        this.set(i[0], i[1])
                    }
                }

                function D(t) {
                    var e = -1,
                        n = t ? t.length : 0;
                    for (this.clear(); ++e < n;) {
                        var i = t[e];
                        this.set(i[0], i[1])
                    }
                }

                function T(t) {
                    var e = -1,
                        n = t ? t.length : 0;
                    for (this.clear(); ++e < n;) {
                        var i = t[e];
                        this.set(i[0], i[1])
                    }
                }

                function z(t, e) {
                    for (var n, i, r = t.length; r--;)
                        if ((n = t[r][0]) === (i = e) || n != n && i != i) return r;
                    return -1
                }

                function L(t, e) { for (var n, i = 0, r = (e = function(t, e) { if (N(t)) return; var n = typeof t; if ("number" == n || "symbol" == n || "boolean" == n || null == t || Z(t)) return 1; return l.test(t) || !s.test(t) || null != e && t in Object(e) }(e, t) ? [e] : N(n = e) ? n : H(n)).length; null != t && i < r;) t = t[U(e[i++])]; return i && i == r ? t : void 0 }

                function B(t) {
                    var e, n;
                    return K(t) && !(y && y in t) && ((n = K(e = t) ? x.call(e) : "") == r || n == a || function(t) {
                        var e = !1;
                        if (null != t && "function" != typeof t.toString) try { e = !!(t + "") } catch (t) {}
                        return e
                    }(t) ? A : d).test(function(t) { if (null != t) { try { return k.call(t) } catch (t) {} try { return t + "" } catch (t) {} } return "" }(t))
                }

                function O(t, e) { var n, i, r = t.__data__; return ("string" == (i = typeof(n = e)) || "number" == i || "symbol" == i || "boolean" == i ? "__proto__" !== n : null === n) ? r["string" == typeof e ? "string" : "hash"] : r.map }

                function W(t, e) { var n, i, r = (i = e, null == (n = t) ? void 0 : n[i]); return B(r) ? r : void 0 }
                P.prototype.clear = function() { this.__data__ = R ? R(null) : {} }, P.prototype.delete = function(t) { return this.has(t) && delete this.__data__[t] }, P.prototype.get = function(t) { var e = this.__data__; if (R) { var n = e[t]; return n === i ? void 0 : n } return I.call(e, t) ? e[t] : void 0 }, P.prototype.has = function(t) { var e = this.__data__; return R ? void 0 !== e[t] : I.call(e, t) }, P.prototype.set = function(t, e) { return this.__data__[t] = R && void 0 === e ? i : e, this }, D.prototype.clear = function() { this.__data__ = [] }, D.prototype.delete = function(t) {
                    var e = this.__data__,
                        n = z(e, t);
                    return !(n < 0) && (n == e.length - 1 ? e.pop() : S.call(e, n, 1), !0)
                }, D.prototype.get = function(t) {
                    var e = this.__data__,
                        n = z(e, t);
                    return n < 0 ? void 0 : e[n][1]
                }, D.prototype.has = function(t) { return -1 < z(this.__data__, t) }, D.prototype.set = function(t, e) {
                    var n = this.__data__,
                        i = z(n, t);
                    return i < 0 ? n.push([t, e]) : n[i][1] = e, this
                }, T.prototype.clear = function() { this.__data__ = { hash: new P, map: new(F || D), string: new P } }, T.prototype.delete = function(t) { return O(this, t).delete(t) }, T.prototype.get = function(t) { return O(this, t).get(t) }, T.prototype.has = function(t) { return O(this, t).has(t) }, T.prototype.set = function(t, e) { return O(this, t).set(t, e), this };
                var H = j(function(t) {
                    var e;
                    t = null == (e = t) ? "" : function(t) { if ("string" == typeof t) return t; if (Z(t)) return C ? C.call(t) : ""; var e = t + ""; return "0" == e && 1 / t == -n ? "-0" : e }(e);
                    var r = [];
                    return c.test(t) && r.push(""), t.replace(u, function(t, e, n, i) { r.push(n ? i.replace(h, "$1") : e || t) }), r
                });

                function U(t) { if ("string" == typeof t || Z(t)) return t; var e = t + ""; return "0" == e && 1 / t == -n ? "-0" : e }

                function j(r, a) {
                    if ("function" != typeof r || a && "function" != typeof a) throw new TypeError(e);
                    var o = function() {
                        var t = arguments,
                            e = a ? a.apply(this, t) : t[0],
                            n = o.cache;
                        if (n.has(e)) return n.get(e);
                        var i = r.apply(this, t);
                        return o.cache = n.set(e, i), i
                    };
                    return o.cache = new(j.Cache || T), o
                }
                j.Cache = T;
                var N = Array.isArray;

                function K(t) { var e = typeof t; return t && ("object" == e || "function" == e) }

                function Z(t) { return "symbol" == typeof t || (e = t) && "object" == typeof e && x.call(t) == o; var e }
                G.exports = function(t, e, n) { var i = null == t ? void 0 : L(t, e); return void 0 === i ? n : i }
            }).call(this, e("yLpj"))
        },
        yK9s: function(t, e, n) {
            "use strict";
            var r = n("xTJ+");
            t.exports = function(n, i) { r.forEach(n, function(t, e) { e !== i && e.toUpperCase() === i.toUpperCase() && (n[i] = t, delete n[e]) }) }
        },
        yLpj: function(t, e) {
            var n;
            n = function() { return this }();
            try { n = n || new Function("return this")() } catch (t) { "object" == typeof window && (n = window) }
            t.exports = n
        },
        yLu3: function(t, e, n) { t.exports = n("VKFn") },
        zLkG: function(t, e, n) { e.f = n("UWiX") },
        zXhZ: function(t, e, n) {
            var i = n("5K7Z"),
                r = n("93I4"),
                a = n("ZW5q");
            t.exports = function(t, e) { if (i(t), r(e) && e.constructor === t) return e; var n = a.f(t); return (0, n.resolve)(e), n.promise }
        },
        zn7N: function(t, e, n) {
            var r = n("Y7ZC"),
                a = n("WEpk"),
                o = n("KUxP");
            t.exports = function(t, e) {
                var n = (a.Object || {})[t] || Object[t],
                    i = {};
                i[t] = e(n), r(r.S + r.F * o(function() { n(1) }), "Object", i)
            }
        },
        zuR4: function(t, e, n) {
            "use strict";
            var i = n("xTJ+"),
                r = n("HSsa"),
                a = n("CgaS"),
                o = n("SntB");

            function s(t) {
                var e = new a(t),
                    n = r(a.prototype.request, e);
                return i.extend(n, a.prototype, e), i.extend(n, e), n
            }
            var l = s(n("JEQr"));
            l.Axios = a, l.create = function(t) { return s(o(l.defaults, t)) }, l.Cancel = n("endd"), l.CancelToken = n("jfS+"), l.isCancel = n("Lmem"), l.all = function(t) { return Promise.all(t) }, l.spread = n("DfZB"), t.exports = l, t.exports.default = l
        }
    }, r.c = i, r.d = function(t, e, n) { r.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: n }) }, r.r = function(t) { "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }), Object.defineProperty(t, "__esModule", { value: !0 }) }, r.t = function(e, t) {
        if (1 & t && (e = r(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var n = Object.create(null);
        if (r.r(n), Object.defineProperty(n, "default", { enumerable: !0, value: e }), 2 & t && "string" != typeof e)
            for (var i in e) r.d(n, i, function(t) { return e[t] }.bind(null, i));
        return n
    }, r.n = function(t) { var e = t && t.__esModule ? function() { return t.default } : function() { return t }; return r.d(e, "a", e), e }, r.o = function(t, e) { return Object.prototype.hasOwnProperty.call(t, e) }, r.p = "", r(r.s = "tjUo").default;

    function r(t) { if (i[t]) return i[t].exports; var e = i[t] = { i: t, l: !1, exports: {} }; return n[t].call(e.exports, e, e.exports, r), e.l = !0, e.exports }
    var n, i
});