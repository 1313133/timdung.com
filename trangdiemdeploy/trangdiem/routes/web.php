<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@index')->name('homePage');
Route::get('/{slug}', 'HomePageController@index')->name('homePage');
Route::get('/makeup', 'HomePageController@redireHomePage')->name('redireHOmePage');
Route::get('/makeup/{slug}', 'HomePageController@redireHomePage');

Route::get('/trang-diem/{slug}', 'HomePageController@redireHomePage');


