<html lang="en"> 
@php
    $dataCompany = Session::get('dataCompany');   
@endphp
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=no" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <link rel="stylesheet" href="contain/css/style.css" />
    <title> Trang điểm</title>
    <script src="./resources/js/main.js"></script>
    <script src="./resources/js/callapi.js"></script>

    <link rel="stylesheet" href="contain/css/slick-theme.css" />
    <link rel="stylesheet" href="contain/css/slick.css" />

    <script type="text/javascript" src="contain/js/slick.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>

</head>

<body>
    <div id="b-placeholder"></div>


    <script>
        function getBaseUrl() {
            return '/';
        }
    </script>

    <script>
        function resolveImageSource() {
            var baseUrl = getBaseUrl();
            var images = document.querySelectorAll('img[data-src]');
            for (var i = 0; i < images.length; i++) {
                var img = images[i];
                img.src = baseUrl + img.getAttribute('data-src');
            }
        }
    </script>
    <script>
        function pathToRegex(path) {
            return new RegExp(
                '^' +
                path
                .replace(new RegExp('\/', 'g'), '\\/')
                .replace(new RegExp(':\w+', 'g'), '(.+)') +
                '$'
            );
        }

        function router() {
            const routes = [{
                path: '/',
                url: './trangdiem/contain/products.html'
            }, {
                path: 'welcome',
                url: './trangdiem/contain/welcome.html'
            }, {
                path: '/trangdiem/makeup',
                url: './trangdiem/contain/makeup.html'
            }, {
                path: 'products',
                url: './trangdiem/contain/products.html'
            }, {
                path: 'share',
                url: './trangdiem/contain/share.html'
            }, ];

            const potentialMatches = routes.map(route => {
                return {
                    route,
                    result: location.pathname.match(pathToRegex(route.path)),
                };
            });

            var match = potentialMatches.find(p => p.result !== null);

            if (!match) {
                match = {
                    route: routes[0],
                    result: [location.pathname],
                };
            }

            $(function() {
                $('#b-placeholder').load(getBaseUrl() + match.route.url, function() {
                    resolveImageSource();
                });
            });
        }

        function navigateTo(url) {
            var oldUrl = window.location.href.replace(
                window.location.protocol +
                '//' +
                window.location.hostname +
                ((window.location.port && window.location.port !== '80') ||
                    window.location.port !== '443' ?
                    ':' + window.location.port :
                    '')
            );
            window.dispatchEvent(new CustomEvent('router:beforeNavigation'), {
                oldUrl: oldUrl,
                newUrl: url,
            });
            history.pushState(null, null, url);
            router();
            window.dispatchEvent(new CustomEvent('router:afterNavigation'), {
                oldUrl: oldUrl,
                newUrl: url,
            });
        }

        window.addEventListener('popstate', router);
        var companyData = {!! json_encode($dataCompany) !!};
         document.addEventListener('DOMContentLoaded', function() {
            sessionStorage.setItem('companyData',  JSON.stringify(companyData));
           
            router();
        });
    </script>



</body>

</html>