function resultChoseImage(makeupId, colorHex) {

    uploadImage(makeupId, colorHex);
}

function makeRequest(url, method, headers, data) {
    var request = new XMLHttpRequest();

    return new Promise(function(resolve, reject) {
        request.onreadystatechange = function() {
            // Only run when request is completed
            if (request.readyState != 4) return;

            if (request.status >= 200 && request.status < 300) {
                resolve(request);
            } else {
                reject({
                    status: request.status,
                    statusText: request.statusText,
                });
            }
        };

        request.onerror = function(error) {
            reject(error);
        };

        request.open(method || "GET", url, true);

        if (headers) {
            for (var key in headers) {
                request.setRequestHeader(key, headers[key]);
            }
        }

        if (data) {
            request.send(data);
        } else {
            request.send();
        }
    });
}

var imatedataGloble;

function uploadImage(makeupId, colorHex) {

    $(".loading").addClass("loadingcameraShow");


    var config = {
        "configSkin": {
            "email": "haravandemo@gmail.com",
            "link": "https://csgadmin.com/api/userskin",
            "key": "NWY0N2FkMjg4ZjFiYmIwYWViZDBkNDdhXzU2Nzg5MTBfSG5mMlJRcDhMbkNuWWhBQw=="
        }
    };
    config = config.configSkin;

    imageProcess = document.getElementById("fileOpen");

    var imageProcess2 = document.getElementById("resultImage2");

    const postImageTomakeupAI = function(config) {
        const dataInput = imageProcess2.src;

        var jdata = {
            email: config.email,
            image: dataInput.substr(dataInput.indexOf('base64,') + 7) + '',
            image_type: config.image_type,
            makeupID: makeupId
        };

        var headers = {
            'Content-Type': 'application/json',
            // apikey: config.configmakeup.key,
        };

        return makeRequest(
            config.link,
            'POST',
            headers,
            JSON.stringify(jdata)
        );
    };

    uploading = true;
    var config = {
        link: 'https://csgvietnam.com/api/user-upload',
        image_type: 'jpg',
        email: 'test@gmail.com',
    };
    // makeupPageModule.setImageLoading(uuid, true);
    postImageTomakeupAI(config)
        .then(function(secondResponse) {

            // Store response before navigating to result page.
            const data = JSON.parse(secondResponse.responseText);
            const IMAGE_SELECTOR = '$.data.data.media_info_list[0].media_data';
            console.log(IMAGE_SELECTOR);

            var dataimage = 'data:image/jpg;base64,' + data.data.media_info_list[0].media_data;

            imageProcess.src = dataimage;



            setTimeout(() => {

                loadProductByColor(colorHex);

            }, 500);

        })
        .catch(function(err) {
            console.error(err);
            alert('Xin vui lòng thử lại');
        })
        .finally(function() {
            uploading = false;
        });
}


function setImageSrc(imageSrc) {
    // imageSrcs[uuid] = imageSrc;

    imageProcess = document.getElementById("fileOpen");
    // render(uuid);
}