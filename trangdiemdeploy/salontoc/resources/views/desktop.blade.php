<!DOCTYPE html>
<html>
@php
    $dataCompany = Session::get('dataCompany'); 
@endphp
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Trang điểm online</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="./resources/css/dist/styleNew.css" rel="stylesheet">
    <script src="./resources/js/main.js"></script>
    <script src="./resources/js/callapi.js"></script>
    <link rel="stylesheet" type="text/css" href="./resources/slick/slick-theme.css" />

    <script type="text/javascript" src="./resources/slick/slick.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./resources/slick/slick-theme.css" />

    <script type="text/javascript" src="./resources/slick/slick.min.js"></script>
</head>

<body>
    <div class="body-wrap is-pc is-desktop">
        <div class="header">
            <div class="header-con com-flex">
                <div class="header-cate com-flex hide">
                    <svg class="icon" width="22" height="22" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg"><path d="M20.75 13.25h-7.5v7.5h7.5v-7.5zm-12 0h-7.5v7.5h7.5v-7.5zm0-12h-7.5v7.5h7.5v-7.5zm12 0h-7.5v7.5h7.5v-7.5z" stroke="#000" stroke-width="1.5" fill="none" fill-rule="evenodd"></path></svg>
                    <svg class="icon hide" width="19" height="19" viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg"><path d="M17.678 0l1.06 1.06L10.43 9.37l8.31 8.309-1.061 1.06-8.309-8.309-8.308 8.31L0 17.677l8.308-8.309L0 1.061 1.06 0 9.37 8.308 17.678 0z" fill="#000" fill-rule="evenodd"></path></svg>
                    <div class="all-cate">Danh mục trải nghiệm</div>
                </div>
                <div class="titleHeader"> TIKITECH </div>
                <div class="header-car com-flex hide">
                    <svg class="icon" width="24" height="22" viewBox="0 0 24 22" xmlns="http://www.w3.org/2000/svg"><g transform="translate(0 1)" fill-rule="nonzero" stroke="#000" stroke-width="1.5" fill="none"><path d="M.4 0h2.73l3.468 13.333H19.28l3.025-9.945h-13.9"></path><ellipse cx="7.105" cy="18.467" rx="1.709" ry="1.667"></ellipse><ellipse cx="18.337" cy="18.467" rx="1.709" ry="1.667"></ellipse></g></svg>
                    <div class="com-elli car-num"></div>
                </div>
            </div>
        </div>

        <div class="category show">
            <div class="category-con">
                <div class="category-tit">
                    <div class="tit">Danh mục trải nghiệm</div>
                    <div class="des">Bạn chọn danh mục muốn thử</div>
                </div>
                <div class="category-list" id="listCategoryMobileNew">


                </div>
            </div>
        </div>
    </div>

    <script>
             var companyData = {!! json_encode($dataCompany) !!};
             document.addEventListener("DOMContentLoaded", function(event) {
                sessionStorage.setItem('companyData',  JSON.stringify(companyData));

                loadAllCategory();
            });


   </script>

</body>

</html>