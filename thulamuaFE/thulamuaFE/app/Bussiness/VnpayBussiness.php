<?php


namespace App\Bussiness;

// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
use Session;

use Response;

use Jenssegers\Agent\Agent as Agent;


class VnpayBussiness
{
    public function create(Request $request,$sumAmount = 10000 )
    {
        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = "TIKITE01"; //Mã website tại VNPAY 
        $vnp_HashSecret = "FGFKVQGAKBMGNFIHURNGAJEJXAOVDRDX"; //Chuỗi bí mật
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://localhost:8000/return-vnpay";
        $vnp_TxnRef = date("YmdHis"); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toán đơn  hàng ";
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $sumAmount * 100;
        $vnp_Locale = 'vn';
        $vnp_IpAddr = request()->ip();
        
        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
           // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
            $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
        //  return redirect($vnp_Url);
      
        return $vnp_Url;
    }

    public function updateStatus (  $vnp_ResponseCode)
    {
        // $dataOrder = Session::get('dataOrder')->data;

        // $dataOrder = Session::get('dataOrder');

        $dataOrder = Session::get('dataOrder')->data;

    
   
        $url = "https://api.thulamua.com/api/update_status_order";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/update_status_order', [
                'form_params' => [
                    'order_id' => $dataOrder->order_id,
                    'status'=> "1",
                    'vnpaystatus'=> $vnp_ResponseCode
                ]
        ]);
        
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
      
         if($reponse->status==200)
        {
        
            return Response::json([
                'sucesss'  =>true,
                'message'=> "Đã thanh toán thành công"
              ], 200); // Status code here

        }
        else 
        {
            return Response::json([
                'sucesss'  =>false,
                'message'=> "Thanh toán thất bại"
              ], 200); // Status code here
        }


    }


    public function VNpayHandller (Request $request)
    {

        $vnp_ResponseCode = $request->vnp_ResponseCode;
        $vnp_Amount = $request->vnp_Amount;
        $vnp_BankCode = $request->vnp_BankCode;
        $vnp_BankTranNo = $request->vnp_BankTranNo;
        $vnp_CardType =  $request->vnp_CardType;
        $vnp_OrderInfo=  $request->vnp_OrderInfo;

        if($vnp_ResponseCode == "00")
        {   
         
            return  $this->updateStatus( $vnp_ResponseCode);
        }
        else 
        {
            return Response::json([
                'sucesss'  =>false,
                'message'=> "Thanh toán thất bại"
              ], 200); // Status code here
           
        }

    }
}
