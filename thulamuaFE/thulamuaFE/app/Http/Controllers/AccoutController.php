<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Response;

use Session;
use Jenssegers\Agent\Agent as Agent;
use stdClass;

class AccoutController extends Controller
{

    public function update(Request $request)
    {
        if(!Session::has('dataLogin'))
        {
            //  return Response::json([
            //     'sucesss'  => false,
            //     'message'=> "Đã đăng nhập hệ thống"
            //   ], 200); // Status code here

        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/update_user_by_id";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/update_user_by_id', [
            'headers' => [
                'Authorization' => 'Bearer ' .Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ],
            'form_params' => [
             'name' => $request->input("name"),
             'email' =>  $request->input("email"),
             'phone' => $request->input("phone"),
             'address' =>  $request->input("address"),
        ]
        ]);

       
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
      
         if($reponse->status==200)
        {
             return Response::json([
                'sucesss'  => true,
                'message'=> "Cập nhật thành công"
              ], 200); // Status code here

        }

    }
        
    
    public function changePassword(Request $request)
    {
  
        if(!Session::has('dataLogin'))
        {
           
        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/reset_password";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/reset_password', [
            'headers' => [
                'Authorization' => 'Bearer ' .Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ],
            'form_params' => [
             'new_password' => $request->input("new_password"),
      
        ]
        ]);

       
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
    
         if($reponse->status==200)
        {
             return Response::json([
                'sucesss'  => true,
                'message'=> "Cập nhật thành công"
              ], 200); // Status code here

        }

    }
      


    
    public function loginPage(Request $request)
    {
      

        if(!Session::has('dataLogin'))
        {
            
            return view("login.login");
        }
        else 
        {
            return redirect('/');
        }

        
      

    }

    public function registerPage(Request $request)
    {
       
        if(!Session::has('dataLogin'))
        {
            return view("login.register");
        }
        else 
        {
            return redirect('/');
        }

    }
    public function Logout (Request $request)
    {
         $request->session()->forget('dataLogin');
         $request->session()->flush();

         return redirect("/");

    }

    public  function RegisterAccout (Request $request)
    {
            $client = new \GuzzleHttp\Client();
            $url = "https://api.thulamua.com/api/login_user";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST', 'https://api.thulamua.com/api/add_user', [
            'form_params' => [
                  
                    'username' =>  $request->input("userName"),
                    'password' =>  $request->input("password"),
            ]
            ]);


        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
       
        if($reponse->status==200)
        {
           
            return Response::json([
              'sucesss'  => true,
              'message'=> "thêm mới thành công" 
            ], 200); // Status code here


                    
        } 
        else 
        {
         
             return Response::json([
                'sucesss'  => $reponse->is_success,
                'message'=> $reponse->message
              ], 200); // Status code here

        }



    }

    public function Login (Request $request)
    {

        if(Session::has('dataLogin'))
        {
             return Response::json([
                'sucesss'  => false,
                'message'=> "Đã đăng nhập hệ thống"
              ], 200); // Status code here

        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/login_user";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/login_user', [
        'form_params' => [
             'username' => $request->input("userName"),
             'password' =>  $request->input("password"),
        ]
        ]);

       
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
      
         if($reponse->status==200)
        {
           
            $dataInfo = $reponse->data->data;

            $dataLogin = new stdClass();
            $dataLogin->userLogin = $dataInfo;
            $dataLogin->isAllow =  true;
            $dataLogin->token =  $reponse->data->token;
            session(['dataLogin' => $dataLogin]);
            return Response::json([
              'sucesss'  => true,
              'dataUser'=> $dataInfo
            ], 200); // Status code here
                    
        } 
        else 
        {
             return Response::json([
                'sucesss'  => false,
                'message'=> "Không tồn tại  thông tin người dùng"
              ], 200); // Status code here

        }
        
  
    }

    public  function AccountPage(Request $request)
    {

        if(Session::has('dataLogin'))
        {
         
        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/get_user_by_id";
       

       $response =  $client->request('POST',$url, [
            'headers' => [
                'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents()); 
        
        if($reponse->status==200)
        {
            $data =  $reponse->data;
            return view("account.account", compact("data"));

        }
        
        
    }

    public  function AddressPage(Request $request)
    {

        if(Session::has('dataLogin'))
        {
         
        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/get_user_by_id";
       

       $response =  $client->request('POST',$url, [
            'headers' => [
                'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents()); 
        
        if($reponse->status==200)
        {
            $data =  $reponse->data;
            return view("account.address", compact("data"));

        }
        
        
    }



    public  function AccountOrderPage(Request $request)
    {
        return view("account.orderCart");
    }

   

    public function OrderInfo(Request $request)
    {
         if(!Session::has('dataLogin'))
        {
            return redirect("/dang-nhap-he-thong");
        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/get_address";
       

       $response =  $client->request('POST',$url, [
            'headers' => [
                'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents()); 
        
        if($reponse->status==200)
        {
            $dataLocation =  $reponse->data;
             return view("productCheckout", compact("dataLocation"));
         
        }
       
    }

    public  function OrderPage( Request $request)
    {

        if(!Session::has('dataLogin'))
        {
         
            return redirect("/dang-nhap-he-thong");

        }
        return view("account.order");

    }

    public function PaymentPage(Request $request)

    {
         
        return view ("productPayment");

    }

    public function resultOrder (Request $request)
    {
        $dataOrder = Session::get('dataOrder');


       
        if($dataOrder)
        {
             $data = $dataOrder->data;
              Session()->forget('dataOrder');
            return view("resultOrder", compact("data"));
        }
    
     
    }


    public function updatePersonal(Request $request)
    {
        if(Session::has('dataLogin'))
        {
             return Response::json([
                'sucesss'  => false,
                'message'=> "Đã đăng nhập hệ thống"
              ], 200); // Status code here

        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/login_user";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/login_user', [
        'form_params' => [
             'username' => $request->input("userName"),
             'password' =>  $request->input("password"),
        ]
        ]);

       
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
      
         if($reponse->status==200)
        {

        }

            

    }


}
