<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Request;


use Session;

use Response;
use Illuminate\Http\Request;
use App\Bussiness\VnpayBussiness;
use Jenssegers\Agent\Agent as Agent;





class OrderController extends Controller
{
    
   

    public function create(Request $request)
    {
    
     
        
        if(!Session::has('dataLogin'))
        {
             return Response::json([
                'sucesss'  => false,
                'message'=> "Chưa đăng nhập"
              ], 200); // Status code here

        }
        $paymentMethod =  $request->input("payment_method");
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/add_order";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/add_order', [
            'headers' => [
                'Authorization' => 'Bearer ' .Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ],
            'form_params' => [
                'list_product' => $request->input("list_product"),
                'order_address_id' =>  $request->input("order_address_id"),
                'transport_method' => $request->input("transport_method"),
                'payment_method' =>  $request->input("payment_method"),
                'cost' =>  $request->input("cost"),
                'delivery' =>  $request->input("delivery"),
                'transport_cost'=>  $request->input("transport_cost")
             ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());
    
        if($reponse->status==200)
        {
            $data = $reponse;
            if($data->is_success == true)
            {
              
                session(['dataOrder' => $data]);
              
                if($paymentMethod*1 ==0)
                {
                    
                    $vnpayBussiness = new VnpayBussiness();
                    $urlvpn = $vnpayBussiness->create($request,$request->input("cost"));
                 
             
                    return Response::json([
                        'sucesss'  => true,
                        'linkhref' => $urlvpn,
                        'message'=> "Đang chuyển sang trang thanh toán"
                      ], 200);
                    
                }
                else 
                {

                    return Response::json([
                        'sucesss'  => true,
                        'linkhref' => "/ket-qua-don-hang",
                        'message'=> "Tạo đơn hàng thành công"
                      ], 200);
                }

               
             }           

        } 
          
       
    }

    public function quanlydonhang(Request $request)
    {


        if(!Session::has('dataLogin'))
        {
         
            return redirect("/dang-nhap-he-thong");

        }
    
       
        $url = "https://api.thulamua.com/api/manager_orde";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/manager_order', [
            'headers' => [
                'Authorization' => 'Bearer ' .Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());  
       
      
         if($reponse->status==200)
        {
            $data = $reponse;

            
           
         
        
            if($data->is_success == true)
            {
                $data = $data->data;
                if(count($data)>0)
                {
                    $data = $data[0];
                }
               
                return view("account.order",compact("data"));

            }           

        } 
          
       
    }

   
}
