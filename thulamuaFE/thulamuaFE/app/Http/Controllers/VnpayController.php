<?php

namespace App\Http\Controllers;

// use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;

use App\Bussiness\VnpayBussiness;
use Jenssegers\Agent\Agent as Agent;


class VnpayController extends Controller
{
    public function create(Request $request)
    {
        session(['cost_id' => $request->id]);
        session(['url_prev' => url()->previous()]);
        $vnp_TmnCode = "TIKITE01"; //Mã website tại VNPAY 
        $vnp_HashSecret = "FGFKVQGAKBMGNFIHURNGAJEJXAOVDRDX"; //Chuỗi bí mật
        $vnp_Url = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
        $vnp_Returnurl = "http://localhost:8000/return-vnpay";
        $vnp_TxnRef = date("YmdHis"); //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toán đơn  hàng ";
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = 10000 * 100;
        $vnp_Locale = 'vn';
        $vnp_IpAddr = request()->ip();
        
        $inputData = array(
            "vnp_Version" => "2.0.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . $key . "=" . $value;
            } else {
                $hashdata .= $key . "=" . $value;
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
           // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
            $vnpSecureHash = hash('sha256', $vnp_HashSecret . $hashdata);
            $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;
        }
         return redirect($vnp_Url);
    }

  
    public function reponseVNPay (Request $request)
    {

         $vnp_ResponseCode = $request->vnp_ResponseCode;

         if($vnp_ResponseCode =="24")
         { 
            $urlBreak=  Session()->get('url_prev');
            return  redirect($urlBreak);
         }
        // $vnp_Amount = $request->vnp_Amount;
        // $vnp_BankCode = $request->vnp_BankCode;
        // $vnp_BankTranNo = $request->vnp_BankTranNo;
        // $vnp_CardType =  $request->vnp_CardType;
        // $vnp_OrderInfo=  $request->vnp_OrderInfo;
        $vnpbusiness = new VnpayBussiness();
        $reponse = $vnpbusiness->VNpayHandller($request);



        $result = $reponse->getData();

     

        if($result->sucesss ==true)
        {
            
          return   redirect("/ket-qua-don-hang");
        }

    }
}
