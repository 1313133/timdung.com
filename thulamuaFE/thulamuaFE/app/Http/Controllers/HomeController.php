<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Request;

use Jenssegers\Agent\Agent as Agent;

class HomeController extends Controller
{
    
   

    public function  listProduct (Request $request, $slug = null) 
    {

        
        $url = "https://api.thulamua.com/api/get_deal_by_slug";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/get_deal_by_slug',[
        'form_params' => [
                   'slug' =>  $slug
                 ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents());
      
        $dataProductBanner = [];
        if($reponse->status==200)
        { 
             $dataReponse =  $reponse->data->product; 
             $dataDealName =  $reponse->data->deal->name;
             $dataDealSlug =  $slug;
             $voucher =  $reponse->data->deal->voucher;
             return view('productlist', compact("dataReponse","dataDealName","dataDealSlug","voucher"));
        }
        else 
        {


        }
     }

    
    public function index (Request $request, $slug = null) 
    {
        $url = "https://api.thulamua.com/api/list_deal";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/list_deal', [
     
        ]);

        $body = $response->getBody();
        $reponse = json_decode($body->getContents());
       
         $dataProductBanner = [];

        if($reponse->status==200)
        {
             $dataBaner = $reponse->data->dataWeb;


             $clientRequestBanner = new \GuzzleHttp\Client();

             $responseBanner = $clientRequestBanner->request('POST', 'https://api.thulamua.com/api/get_banner', [
     
            ]);
    
            $bodygetbanner = $responseBanner->getBody();
            $reponseBody = json_decode($bodygetbanner->getContents());
            $dataBannerHomepage =$reponseBody->data[0];
            return view('welcome', compact("dataBaner","dataBannerHomepage"));
        }
        else 
        {


        }

    }

    public function product(    Request $request, $slug =null)
    {
        $Agent = new Agent();

        $url = "https://api.thulamua.com/api/list_product_by_id";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://api.thulamua.com/api/list_product_by_id',[
            'form_params' => [
                       'slug' =>  $slug
                     ]
        ]);


        $body = $response->getBody();
        $reponse = json_decode($body->getContents());
     
        if($reponse->status==200)
        {
               $data = $reponse->data[0];
          

                
            
                if ($Agent->isMobile()) {
                         return view("productMobile",compact("data"));
                    
                }
                 else
                {
                    return  view("productdetail", compact("data")); 
                }   
                
                

        }


 
      

    }


}
