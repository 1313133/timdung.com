<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Response;


class LocationController extends Controller
{
    
    public  function Add (Request $request)
    {

                $client = new \GuzzleHttp\Client();
                $url = " https://api.thulamua.com/api/add_address";
                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', 'https://api.thulamua.com/api/add_address', [

                'headers' => [
                    'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                    'Accept'        => 'application/json',
                ],
                'form_params' => [
                    
                        'name' =>  $request->input("fullName"),
                        'surname' =>  $request->input("fullName"),
                        'address' =>  $request->input("addresss"),
                        'city' =>  $request->input("tinh"),
                        'distrist' =>  $request->input("quan"),
                        'ward' =>  $request->input("phuong"),
                        'phone' =>  $request->input("phoneNumber")
                ]
                ]);



            $body = $response->getBody();
            $reponse = json_decode($body->getContents());  
        
            if($reponse->status==200)
            {
            
                return Response::json([
                'sucesss'  => true,
                'message'=> "thêm mới thành công" 
                ], 200); // Status code here


                        
            } 
            else 
            {
            
                return Response::json([
                    'sucesss'  =>false,
                    'message'=> "Có lỗi khi thêm mới"
                ], 200); // Status code here

            }



    }


    public  function Update (Request $request)
    {
                $client = new \GuzzleHttp\Client();
                $url = "https://api.thulamua.com/api/update_address";
                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', 'https://api.thulamua.com/api/update_address', [

                'headers' => [
                    'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                    'Accept'        => 'application/json',
                ],
                'form_params' => [
                        'name' =>  $request->input("fullName"),
                        'surname' =>  $request->input("fullName"),
                        'address' =>  $request->input("addresss"),
                        'city' =>  $request->input("tinh"),
                        'distrist' =>  $request->input("quan"),
                        'ward' =>  $request->input("phuong"),
                        'phone' =>  $request->input("phoneNumber")
                ]
                ]);



            $body = $response->getBody();
            $reponse = json_decode($body->getContents());  
        
            if($reponse->status==200)
            {
            
                return Response::json([
                'sucesss'  => true,
                'message'=> "thêm mới thành công" 
                ], 200); // Status code here


                        
            } 
            else 
            {
            
                return Response::json([
                    'sucesss'  =>false,
                    'message'=> "Có lỗi khi thêm mới"
                ], 200); // Status code here

            }
     }

    public  function AddressPage(Request $request)
    {

        if(Session::has('dataLogin'))
        {
         
        }
        $client = new \GuzzleHttp\Client();
        $url = "https://api.thulamua.com/api/get_address";
       

       $response =  $client->request('POST',$url, [
            'headers' => [
                'Authorization' => 'Bearer ' . Session::get('dataLogin')->token,        
                'Accept'        => 'application/json',
            ]
        ]);
        $body = $response->getBody();
        $reponse = json_decode($body->getContents()); 
        
        if($reponse->status==200)
        {
            $data =  $reponse->data;
            return view("account.address",compact("data"));

        }
        
        
    }

   

  
}
