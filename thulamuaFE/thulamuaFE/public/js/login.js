function validateForm () 
{


    var userNameInput = document.getElementById("userNameId");
    var passwordInput = document.getElementById("passwordId");

    var  valueInput = userNameInput.value;
    var valuePassword = passwordInput.value;
    var index = 0;
    
    if(valueInput =="")
    {
        document.getElementById("userNameError").classList.add("d-inline-block");
        index ++;

    }
    else{
        document.getElementById("userNameError").classList.remove("d-inline-block");
    }

    if(valuePassword =="")
    {
        document.getElementById("passwordError").classList.add("d-inline-block");
        index ++;
    
    }
    else{
        document.getElementById("passwordError").classList.remove("d-inline-block");
    }

    return index  <1;


}

function validateFormRegister () 
{


    var userNameInput = document.getElementById("userNameId");
    var passwordInput = document.getElementById("passwordId");
    var repeatpassword = document.getElementById("repeatpassword");

    var  valueInput = userNameInput.value;
    var valuePassword = passwordInput.value;
    var valuerepeatpassword = repeatpassword.value;
    var index = 0;
    
    if(valueInput =="")
    {
        document.getElementById("userNameError").classList.add("d-inline-block");
        index ++;

    }
    else{
        document.getElementById("userNameError").classList.remove("d-inline-block");
    }

    if(valuePassword =="")
    {
        document.getElementById("passwordError").classList.add("d-inline-block");
        index ++;
    
    }
    else{
        document.getElementById("passwordError").classList.remove("d-inline-block");
    }

    if(valuerepeatpassword =="")
    {
        document.getElementById("repeatpasswordError").classList.add("d-inline-block");
        index ++;
    
    }
    else if(valuerepeatpassword != valuePassword)
    {
        document.getElementById("repeatpasswordError").textContent = "Hai mật khẩu không giống nhau";
        document.getElementById("repeatpasswordError").classList.add("d-inline-block");
        index ++;
    
    }
    else{
        document.getElementById("repeatpasswordError").classList.remove("invalid-feedback");
    }


    return index  <1;


}

function login() 
{
  
    document.getElementById("errorForm").classList.remove("d-inline-block");

   
   if(!validateForm())
   {
      return;
   }

   $("#btnLogin").hide();
   $("#btnLoginLoading").show();
  
   loginpost("formLogin",successLoginFunction, "/dang-nhap");

}

function successLoginFunction(data)
{
    
   
    if(data.sucesss)
    {

        swal({
            title: 'Đăng nhập thành công',
            text: 'Đang điều hướng...',
            icon: 'success',
            timer: 1000,
            buttons: false,
        })
        .then(() => {
            window.location.href = "/";
        })
    }
    else 
    {
       
        document.getElementById("errorForm").textContent = data.message;
        document.getElementById("errorForm").classList.add("d-inline-block");
        $("#btnLogin").show();
        $("#btnLoginLoading").hide();
        
    }
   
    // swal("Đăng nhập thành công!");


}
function registerUser() 
{
    if(!validateFormRegister())
    {
       return;
    }  
    $("#btnRegister").hide();
    $("#btnLoginLoading").show();
 

    document.getElementById("errorForm").classList.remove("d-inline-block");
    loginpost("formRegister",successRegisterFunction,"/dang-ky-nguoi-dung");
    

}


function successRegisterFunction(data)
{
    if(data.sucesss)
    {
        swal({
            title: 'Dăng ký thành công',
            text: 'Đăng nhập lại...',
            icon: 'success',
            timer: 1000,
            buttons: false,
        })
        .then(() => {
            window.location.href = "/dang-nhap-he-thong";
        })
    }
    else 
    {
       
        document.getElementById("errorForm").textContent = data.message;
        document.getElementById("errorForm").classList.add("d-inline-block");
        $("#btnRegister").show();
        $("#btnLoginLoading").hide();
        
    }
    
}



