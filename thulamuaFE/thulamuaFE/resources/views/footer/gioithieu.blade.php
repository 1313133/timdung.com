@extends('layout')

@section('jsAdditional')
   
@endsection
@section('seoBasic')
<title> Giới thiệu</title>
<meta property="og:title" content="Giới thiệu">
<meta property="og:description" content="TIKITECH cam đoan bảo vệ an toàn thông tin thẻ tín dụng của bạn. Chúng tôi không được phép tiết lộ thông tin cá nhân mà không có sự cho phép bằng văn bản. Tuy nhiên, một số thông tin thu thập từ bạn và về bạn được sẽ được sử dụng để hỗ trợ việc cung cấp dịch vụ của chúng tôi với bạn.">
<meta property="og:url" content="/gioi-thieu">
@endsection

@section('csspage')
<link rel="stylesheet" href="/css/homepage.css">

<link rel="stylesheet" href="/css/footerPage.css">
@endsection
@section('contentpage')
<div class="footer-page  ">
    

<div class="title-footer container-fluid"> 
<h2>Giới thiệu</h2>
</div>

 <div class="content-footer container hozi-row " >
    
    <div class="tikitech-2"> 

        <strong>Tikitech</strong> is a technology company focusing on research and development leading technology computer vision and machine learning. (in Artificial Intelligence and Computer Science): Online Skin Analysis, Virtual Makeup T & Hair Salon Online Virtual Makeup Empowered by leading facial recognition algorithm and practical experience, it is able to precisely locates key facial points to add makeup effects to lips, eyelashes, full face (blush and foundation), and eyes (eyebrows, eyeliners, cosmetic contacts, eye shadow and double-fold eyelids) and provides virtual hair dying service. AI Skin Analysis Medical-grade intelligent skin analysis that tests for a wide range of skin conditions. For more professional and comprehensive skin detection solutions. Let’s check your facial skin status and discover your skincare routine now!

    </div>
        
    <div class="tikitech-2">
        <strong>TIKITECH</strong> thu thập thông tin của người dùng gửi cho chúng tôi. Chúng tôi cũng thu thập thông tin thông qua những trang người dùng truy cập, thông tin được cung cấp thông qua việc khảo sát và đăng ký, API. Những thông tin này có thể chứa dữ liệu cá nhân của bạn bao gồm: , số điện thoại, bốn chữ số cuối thẻ tín dụng , vv... <strong>TIKITECH</strong> cam đoan bảo vệ an toàn thông tin thẻ tín dụng của bạn. Chúng tôi không được phép tiết lộ thông tin cá nhân mà không có sự cho phép bằng văn bản. Tuy nhiên, một số thông tin thu thập từ bạn và về bạn được sẽ được sử dụng để hỗ trợ việc cung cấp dịch vụ của chúng tôi với bạn. Những thông tin chúng tôi thu thập không được chia sẻ với bạn, bán hoặc cho thuê, ngoại trừ một số trường hợp sau đây: Bạn phải đọc, đồng ý và chấp nhận tất cả các điều khoản và điều kiện có trong chính sách bảo mật này, bao gồm Điều khoản Dịch vụ của <strong>TIKITECH</strong> và Chính sách bảo mật của <strong>TIKITECH</strong> trước khi bạn có thể sử dụng Dịch vụ.    
    </div>

</div>



</div>

@endsection