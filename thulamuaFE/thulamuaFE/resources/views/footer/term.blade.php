@extends('layout')

@section('jsAdditional')
   
@endsection

@section('seoBasic')
<title>Điều khoản và dịch vụ</title>
<meta property="og:title" content="Điều khoản và dịch vụ">
<meta property="og:description" content="TIKITECH cam đoan bảo vệ an toàn thông tin thẻ tín dụng của bạn. Chúng tôi không được phép tiết lộ thông tin cá nhân mà không có sự cho phép bằng văn bản. Tuy nhiên, một số thông tin thu thập từ bạn và về bạn được sẽ được sử dụng để hỗ trợ việc cung cấp dịch vụ của chúng tôi với bạn.">
<meta property="og:url" content="/gioi-thieu">
@endsection

@section('csspage')
<link rel="stylesheet" href="/css/homepage.css">

<link rel="stylesheet" href="/css/footerPage.css">
@endsection
@section('contentpage')
<div class="footer-page  ">
    

    <div class="title-footer container-fluid"> 
        <h2>Điều khoản và  dịch vụ </h2>
     </div>

    <div class="content-footer container">
       

 <p>Các điều khoản sau đây ("Điều khoản dịch vụ") mô tả các điều khoản và điều kiện áp dụng cho việc bạn truy cập và sử dụng ứng dụng, dịch vụ <strong>TIKITECH</strong> cũng như bất kỳ phần mềm và sản phẩm liên quan nào (gọi chung là "Dịch vụ"). Dịch vụ kết nối với cửa hàng trực tuyến <strong>TIKITECH</strong> của bạn ("Tài khoản <strong>TIKITECH</strong> ") và tập trung các cuộc trò chuyện của khách hàng, các cuộc trò chuyện giữa nhân viên và các hoạt động tiếp thị, bao gồm nhưng không giới hạn, các chiến dịch quảng cáo cho các nền tảng truyền thông xã hội, tiếp thị qua email và giao tiếp với khách hàng. Tài liệu này là một thỏa thuận ràng buộc về mặt pháp lý giữa bạn và Công ty TNHH Công Nghệ <strong>TIKITECH</strong> và các Chi nhánh của chúng tôi (được gọi là "chúng tôi", "của chúng tôi", hoặc " <strong>TIKITECH</strong> "), trong đó "Chi nhánh" có nghĩa là bất kỳ tổ chức nào kiểm soát trực tiếp hoặc gián tiếp, đều được kiểm soát bởi hoặc nằm dưới sự kiểm soát chung với Công ty TNHH Công Nghệ <strong>TIKITECH</strong>. <br> <p> Để truy cập và sử dụng Dịch vụ, bạn phải có cửa hàng trực tuyến <strong>TIKITECH</strong> đang hoạt động ("Tài khoản <strong>TIKITECH</strong> "). Khi chúng tôi viết "bạn" hoặc "của bạn", chúng có nghĩa là chủ sở hữu, đại lý hoặc nhân viên của Tài khoản Haravan đăng ký Dịch vụ. Các Điều khoản Dịch vụ này áp dụng đối với tất cả các Dịch vụ được sở hữu và cung cấp bởi <strong>TIKITECH</strong> hoặc các Chi nhánh của chúng tôi, bao gồm nhưng không giới hạn: applamdep.com, và các ứng dụng liên quan. Bằng cách đăng ký Dịch vụ, bạn đồng ý bị ràng buộc bởi Điều khoản Dịch vụ. Bất kỳ tính năng hoặc công cụ mới nào được thêm vào dịch vụ hiện tại cũng phải tuân theo <strong>ĐIỀU KHOẢN DỊCH VỤ</strong>. Chúng tôi có quyền cập nhật và thay đổi Điều khoản Dịch vụ bằng cách đăng các cập nhật và thay đổi. Bạn nên kiểm tra Điều khoản Dịch vụ thường xuyên để nhận biết bất kỳ cập nhật hoặc thay đổi nào có thể ảnh hưởng đến bạn. Bạn phải đọc, đồng ý và chấp nhận tất cả các điều khoản và điều kiện có trong thỏa thuận Điều khoản Dịch vụ này, trước khi bạn có thể sử dụng Dịch vụ. 1. Định nghĩa Nội dung của khách hàng có nghĩa là tất cả thông tin và dữ liệu (bao gồm văn bản, cuộc trò chuyện của khách hàng, cuộc trò chuyện của nhân viên, hình ảnh, ảnh, video, âm thanh và tài liệu) hoặc bất kỳ nội dung nào khác ở bất kỳ phương tiện hoặc định dạng nào do bạn cung cấp hoặc thay mặt bạn cung cấp liên quan đến việc bạn sử dụng Dịch vụ, bao gồm thông tin và dữ liệu có sẵn trong Tài khoản Tikitech của bạn, nó có thể bao gồm thông tin hoặc dữ liệu từ Nền Tảng Được Hỗ Trợ được cài đặt trên Tài khoản Tikitech của bạn. Nhân viên có nghĩa là một hoặc nhiều tài khoản nhân viên mà Chủ sở hữu cửa hàng (như được định nghĩa trong Điều khoản dịch vụ Tikitech) có thể tạo để cho phép người khác truy cập Tài khoản Ha Tikitech
 </p>
 <div class="title-paragrap">1. Định nghĩa </div>
<ul>
<li>
    Nội dung của khách hàng có nghĩa là tất cả thông tin và dữ liệu (bao gồm văn bản, cuộc trò chuyện của khách hàng, cuộc trò chuyện của nhân viên, hình ảnh, ảnh, video, âm thanh và tài liệu) hoặc bất kỳ nội dung nào khác ở bất kỳ phương tiện hoặc định dạng nào do bạn cung cấp hoặc thay mặt bạn cung cấp liên quan đến việc bạn sử dụng Dịch vụ, bao gồm thông tin và dữ liệu có sẵn trong Tài khoản <strong>TIKITECH</strong> của bạn, nó có thể bao gồm thông tin hoặc dữ liệu từ Nền Tảng Được Hỗ Trợ được cài đặt trên Tài khoản <strong>TIKITECH</strong> của bạn.    
</li>

<li>
    Nhân viên có nghĩa là một hoặc nhiều tài khoản nhân viên mà Chủ sở hữu cửa hàng (như được định nghĩa trong Điều khoản dịch vụ <strong>TIKITECH</strong>) có thể tạo để cho phép người khác truy cập Tài khoản Ha <strong>TIKITECH</strong>.
    
</li>

</ul>
<div class="title-paragrap">2. Điều khoản chung </div>
<ul>



<li>Hỗ trợ kỹ thuật có sẵn thông qua Trung tâm trợ giúp <strong>TIKITECH</strong>.

</li>

<li>Dịch vụ hiện chỉ được cung cấp bằng tiếng Anh và tiếng Việt. </li>
<li>Bạn không được sử dụng Dịch vụ cho bất kỳ mục đích bất hợp pháp hoặc trái phép nào cũng như khi sử dụng Dịch vụ, bạn không được vi phạm bất kỳ luật nào trong khu vực pháp lý mà bạn hoạt động.</li>
    <li> Bạn không được sử dụng Dịch vụ khi vi phạm các điều khoản và điều kiện áp dụng cho bất kỳ Nền tảng được Hỗ trợ nào mà bạn có thể sử dụng liên quan đến Dịch vụ. Nếu chúng tôi được thông báo về việc bạn vi phạm các điều khoản của Nền tảng được hỗ trợ, chúng tôi có thể, theo quyết định riêng và tuyệt đối của chúng tôi, chấm dứt quyền truy cập của bạn vào Dịch vụ. Để tránh nghi ngờ, <strong>TIKITECH</strong> không chịu trách nhiệm giám sát, thực thi hoặc đảm bảo bạn tuân thủ các điều khoản và điều kiện áp dụng cho Các Nền tảng được hỗ trợ hoặc Các Dịch vụ của Bên thứ ba khác, như được nêu chi tiết hơn trong Mục 6 và 7 dưới đây. </li>




<li>Bạn đồng ý không tái dựng, nhân bản, sao chép, mua bán, hoặc khai thác bất kỳ phần nào của Dịch vụ, việc sử dụng Dịch vụ hoặc truy cập vào Dịch vụ mà không có sự cho phép rõ ràng bằng văn bản của chúng tôi.</li>
<li> Bạn không được mua công cụ tìm kiếm hoặc từ khóa trả tiền cho mỗi nhấp chuột khác (chẳng hạn như Google Ads), nhãn hiệu hoặc tên miền sử dụng nhãn hiệu của <strong>TIKITECH</strong> hoặc biến thể hay lỗi chính tả của chúng.</li> 
</ul>

<div class="title-paragrap">3. Quyền hạn của chúng tôi </div>
<ul>
<li>Chúng tôi có quyền sửa đổi, bao gồm nhưng không giới hạn ở việc thêm hoặc bớt các tính năng, ngừng hoặc chấm dứt Dịch vụ hoặc bất kỳ phần nào trong đó, hoặc chấm dứt Tài khoản <strong>TIKITECH</strong> của bạn hay quyền truy cập của bạn vào Dịch vụ, vì bất kỳ lý do gì mà không cần thông báo bất cứ lúc nào. Chúng tôi sẽ không chịu trách nhiệm với bạn hoặc bất kỳ bên thứ ba nào về bất kỳ sửa đổi, ngừng hoặc chấm dứt Dịch vụ/ bất kỳ phần nào của dịch vụ hoặc chấm dứt Tài khoản <strong>TIKITECH</strong> của bạn hay quyền truy cập của bạn vào Dịch vụ.</li>
<li>Chúng tôi có quyền từ chối Dịch vụ cho bất kỳ ai vì bất kỳ lý do gì bất cứ lúc nào.
<li>Chúng tôi có thể, nhưng không có nghĩa vụ, xóa mà không cần thông báo bất kỳ Nội dung hoặc Nhận xét của Khách hàng nào (như được định nghĩa trong Mục 11) mà chúng tôi xác định là bất hợp pháp theo quyết định riêng của chúng tôi như xúc phạm, đe dọa, bôi nhọ, phỉ báng, khiêu dâm, tục tĩu, phản cảm, vi phạm tài sản trí tuệ của bất kỳ bên nào hoặc các Điều khoản Dịch vụ này.</li>
<li>Việc vi phạm bất kỳ điều khoản nào của Điều khoản Dịch vụ, được xác định theo quyết định riêng của chúng tôi, có thể dẫn đến việc chấm dứt ngay lập tức quyền truy cập của bạn vào Dịch vụ. Không giới hạn bất kỳ biện pháp khắc phục nào khác mà chúng tôi có, chúng tôi có thể đình chỉ hoặc chấm dứt quyền truy cập của bạn vào Dịch vụ nếu chúng tôi nghi ngờ rằng bạn đã tham gia vào hoạt động gian lận liên quan đến Dịch vụ.</li>
<li>Việc lạm dụng bằng lời nói hoặc bằng văn bản dưới bất kỳ hình thức nào (bao gồm cả các mối đe dọa lạm dụng hoặc trả thù) đối với bất kỳ nhân viên, thành viên <strong>TIKITECH</strong> nào sẽ dẫn đến việc chấm dứt dịch vụ ngay lập tức.</li>
<li>Chúng tôi có quyền, nhưng không bắt buộc, để giới hạn tính khả dụng của Dịch vụ, hoặc các sản phẩm, dịch vụ được cung cấp thông qua Dịch vụ, cho bất kỳ người nào, khu vực địa lý, phạm vi pháp lý nào. Chúng tôi có thể thực hiện quyền này trong từng trường hợp cụ thể.</li>
<li>Chúng tôi có quyền cung cấp Dịch vụ của chúng tôi cho các đối thủ cạnh tranh của bạn và không hứa hẹn độc quyền trong bất kỳ phân khúc thị trường cụ thể nào. Bạn cũng phải chấp nhận và đồng ý rằng nhân viên và nhà thầu của <strong>TIKITECH</strong> cũng có thể sử dụng Dịch vụ và họ có thể cạnh tranh với bạn, mặc dù họ có thể không sử dụng thông tin bí mật của bạn khi làm như vậy.</li>
  
</ul>
</div>



</div>

@endsection