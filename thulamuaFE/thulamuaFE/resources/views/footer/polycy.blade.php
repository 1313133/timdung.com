@extends('layout')

@section('jsAdditional')
   
@endsection
@section('seoBasic')
<title> Chính sách và bảo mật</title>
<meta property="og:title" content="Chính sách và bảo mật">
<meta property="og:description" content="TIKITECH cam đoan bảo vệ an toàn thông tin thẻ tín dụng của bạn. Chúng tôi không được phép tiết lộ thông tin cá nhân mà không có sự cho phép bằng văn bản. Tuy nhiên, một số thông tin thu thập từ bạn và về bạn được sẽ được sử dụng để hỗ trợ việc cung cấp dịch vụ của chúng tôi với bạn.">
<meta property="og:url" content="/gioi-thieu">
@endsection

@section('csspage')
<link rel="stylesheet" href="/css/homepage.css">

<link rel="stylesheet" href="/css/footerPage.css">
@endsection
@section('contentpage')
<div class="footer-page  ">
    

    <div class="title-footer container-fluid"> 
        <h2>Chính sách bảo mật </h2>
     </div>

    <div class="content-footer container">
        <p><strong>Tikitech </strong> thập thông tin của người dùng gửi cho chúng tôi. Chúng tôi cũng thu thập thông tin thông qua những trang người dùng truy cập, thông tin được cung cấp thông qua việc khảo sát và đăng ký, API. Những thông tin này có thể chứa dữ liệu cá nhân của bạn bao gồm: , số điện thoại, bốn chữ số cuối thẻ tín dụng , vv... </p>
          <p><strong>Tikitech </strong>  cam đoan bảo vệ an toàn thông tin thẻ tín dụng của bạn. Chúng tôi không được phép tiết lộ thông tin cá nhân mà không có sự cho phép bằng văn bản. Tuy nhiên, một số thông tin thu thập từ bạn và về bạn được sẽ được sử dụng để hỗ trợ việc cung cấp dịch vụ của chúng tôi với bạn. Những thông tin chúng tôi thu thập không được chia sẻ với bạn, bán hoặc cho thuê, ngoại trừ một số trường hợp sau đây: </p>
              
          <p> Bạn phải đọc, đồng ý và chấp nhận tất cả các điều khoản và điều kiện có trong chính sách bảo mật này, bao gồm Điều khoản Dịch vụ của <strong>Tikitech </strong>  và Chính sách bảo mật của <strong>Tikitech </strong>  trước khi bạn có thể sử dụng Dịch vụ.</p>
    </div>



</div>

@endsection