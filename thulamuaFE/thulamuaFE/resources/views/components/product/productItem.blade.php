<div class="card-product">
    <div class="card-product-item">
    <div class="voucher">
        <span>{{$voucher}}% </span>
        <span> OFF </span>
    </div>
    <div class="product-image">
        <a href="/thong-tin-san-pham/{{$dataProuct->slug}}">
            
        <img src ="https://api.thulamua.com/image_product/{{ $dataProuct->image }}">

        </a>
   </div>

    <div class="productInfo">
         @php
             $brand = $dataProuct->brand_id;
         @endphp
        <h4 class="brand">F2</h4>
        <h4 class="product-title"> <a href="/thong-tin-san-pham/{{$dataProuct->slug}}">{{$dataProuct->name }} </a></h4> 
        <div class="price">
            <span class="retail">{{  number_format($price_after_discount, 0, ",", ".") }}đ  </span>    
            <span class="sale"> {{  number_format($dataProuct->price, 0, ",", ".") }}đ </span>     
        </div>
    </div>
    </div>
 </div>