<div class="section-homepage">

    <div class="title-section">
        {{ $titile }}
    </div>

    <div class="row">
        @foreach ($dataItem as $item)
             <div class="col-md-6 banner-item">
            <a href="/sales/{{ $item->slug }}">
                <img src="https://api.thulamua.com/public/image_deal/{{$item->image}}">
            </a>
            <div class="banner-item-description">
                @php
                    $df =\Carbon\Carbon::parse($item->time_start);
                    $dt = \Carbon\Carbon::parse($item->time_finish);
                    $remainDate =  $dt->diffInDays($df);
                  
                @endphp
                <p>{{ $item->name }} </p>
                @if ($remainDate >0)
                       <p>Còn {{ $remainDate }} ngày </p>
                @else   
                         <p>Đã kết thúc </p>
                @endif
               

            </div>
        </div>
        @endforeach
    
       

       
    </div>



</div>