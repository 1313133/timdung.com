<script type="text/javascript" src="/js/cart.js"></script>

<div id="cart-order" class="cart-order">
    <div class="close-btn" onclick="closeCart()"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
            viewBox="0 0 24 24">
            <title>ic-cross-line</title>
            <path fill="#4e595d"
                d="M13.303 12L23.726 1.575A.923.923 0 0 0 22.421.27L11.998 10.695 1.575.27A.923.923 0 0 0 .27 1.575L10.693 12 .27 22.425a.923.923 0 1 0 1.308 1.305l10.423-10.425L22.424 23.73a.924.924 0 1 0 1.308-1.305L13.305 12z">
            </path>
        </svg>
    </div>
    <div class="header-nav">
        <div class="order-title">Giỏ hàng</div>
        <span id ="countOrder">( 4 kết quả) </span>
    </div>


    <div class="content-cart" id = "listCart">

        {{-- <div class="cart-item">
            <div class="product-image">
                  
                    <img src ="/images/cartImage.jpeg"/>
            </div>

            <div class="product-info">
                <div class="product-name">
                    Kính Mát Nữ Wayfarer Thanh Tay Họa Tiết
                </div>
                 <div class="product-details">
                    <div class="quantity-product">
                        <form >
                                <div class="form-group">
                                    
                                    <label for="selectQuantity"
                                    class="">Số lượng:</label>
                                    <select onchange="updateCart()"  name="selectQuantity"
                                    class=" custom-select">
                                    
                                    <option value="1"  >1</option>
                                    <option value="2" >2</option>
                                    <option value="3" >3</option>
                                    <option value="4"  >4</option>
                                    <option value="5"  >5</option>
                                     
                                    </select>

                                </div>
                             
                        </form>
                    </div>
                    <div class="price-wrap">
                        <div class=" price">
                            <span class="notranslate retail">1.800.000₫</span>
                            <input type = "hidden" class="salePrice" value="720000" name ="salePrice"/> 
                            <input type = "hidden" class="productCode" value="productCode" name ="productCode"/>
                            <input type = "hidden"  class="imageLink" value="images/cartImage.jpeg" name ="imageLink"/> 
                            <span class="notranslate sale">720.000₫</span>
                        </div
                        ><a class="remove" onclick="removeCartItem(this)">Bỏ
                            sản phẩm</a>
                    </div>
                </div>
            </div>

        </div> --}}
    </div>

    <div class="bottom-cart"> 
        <div class="summary-money">
              <h4 >
                    Thành tiền
              </h4> 

              <div class="summary-price">
                <p id ="sum-amount-text"> 0đ</p>  
                <p> Đã bao gồm VAT nếu có </p>    
              </div>
        </div> 

        {{-- <div class="short-description"> 
                
             <p>Bạn đã tiết kiệm </p>
             <p>4.600.000đ</p>

        </div> --}}

        <div class="btn-checkout">
            <a href="/thong-tin-don-hang">
                <button> Đặt hàng </button>
            </a>
               
        </div>

    </div>

    <!-- <div class="cart-empty">
        
            <img src="/images/cart-empty.jpeg" >
             <p> Giỏ hàng của bạn còn trống </p>
            <div class="btn-checkout">
                <button> Tiếp tục mua sản phẩm</button>
            </div>
    
    </div> -->

</div>


<script>

    $( document ).ready(function() {
        setTimeout(() => {
            updateCartWeb();
        }, 1000);
    });



</script>