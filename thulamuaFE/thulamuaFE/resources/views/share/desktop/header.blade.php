
<div class="header-menu">
    <div class="container ">
       

        <div class="logo">
            <a href="/"> 
            <img src="/logo.svg">
            </a>
        </div>
        <div class="nav-sub-menu-header">
            
            {{-- <a class="brand"  target="_blank" href="/danh-cho-nhan-hangßß" style="margin:auto">
                
            </a> --}}
           
            <div data-dd-menu-trigger="true" class="nav-menu-header-item dropdown-button">
                
               
                @if(session()->has('dataLogin'))
                <button class="btn">
                    Tài
                    khoản
                </button>
                <div class="dropdown-menu"> 
                    
                     <a  href="/thong-tin-tai-khoan"> 
                          <li class="active">  Thông tin tài khoản </li>
                     </a>
                     <a href="/quan-ly-don-hang"> 
                        <li>Quản lý đơn hàng</li>
                      </a>

                      <a href="/dia-chi-cua-toi"> 
                          <li>Địa chỉ của tôi</li>
                       </a>

                       <a href="/dang-xuat" onclick="Logout"> 
                        <li>Thoát</li>
                     </a>
                
                </div>
                @else
                <a target="_blank" href="/danh-cho-nhan-hang" class="btn" style="margin:auto">
                    Dành cho nhãn hàng
                </a> 
                
                <a href="/dang-nhap-he-thong" class="btn" style="margin:auto">
                    Đăng nhập
                </a>
               
                @endif
                
            </div>

            <div class="cart-btn-menu " onclick="openCart()">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>ic-bag</title><path d="M3.371 22.498L4.733 7.499h3.064v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h5.998v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h3.065l1.362 14.999H3.371zM9.3 4.501a3.003 3.003 0 0 1 2.999-2.999 3 3 0 0 1 2.999 2.999v1.497H9.3V4.501zm11.935 1.497h-4.436V4.501A4.504 4.504 0 0 0 12.298 0a4.507 4.507 0 0 0-4.501 4.501v1.497H3.378L1.745 24h21.136L21.235 5.998z"></path></svg>    
            </div>

           


         


        </div>
    </div>
</div>

<div class="nav-mobile">
    <div class="navbar-open-menu" onclick="openNav()">

        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
            <title>ic-menu</title>
            <path d="M0 4h24v2H0V4zm0 7h24v2H0v-2zm0 7h24v2H0v-2z"></path>
        </svg>

    </div>
    <div class="navbar-mobile-logo">
        <a href="/"> 
        <img src="/logo.svg">
        </a>
    </div>
    <div class="navbar-cart">

        <svg  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" onclick="opencartMobile()">
            <title>ic-bag</title>
            <path
                d="M3.371 22.498L4.733 7.499h3.064v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h5.998v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h3.065l1.362 14.999H3.371zM9.3 4.501a3.003 3.003 0 0 1 2.999-2.999 3 3 0 0 1 2.999 2.999v1.497H9.3V4.501zm11.935 1.497h-4.436V4.501A4.504 4.504 0 0 0 12.298 0a4.507 4.507 0 0 0-4.501 4.501v1.497H3.378L1.745 24h21.136L21.235 5.998z">
            </path>
        </svg>

    </div>


</div>

@include('share.desktop.toolbox')
@include('share.desktop.menubar')
@include('share.cart.minicart')

<script>
    function Logout() 
    {
      
        
    }
</script>