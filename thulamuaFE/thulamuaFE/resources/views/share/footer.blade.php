<div class="footer-cs">

        <div class="container">

            <div class="row mt-3" style="">

                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">

                    <div class="text-uppercase fw-bold">
                        <i class="fas fa-gem me-3"></i>
                        <img style="width:30px;height:30px;"
                            src="/logo.svg">Thử là mua
                    </div>
                    <a target="" href="/gioi-thieu" class="text-reset">Giới thiệu</a>

                </div>

                <div class="col-lg-5 col-md-4 col-sm-12 col-xs-12 footerColumn">

                    <div class="text-uppercase fw-bold">
                        Chính sách và điều khoản
                    </div>

                    <a  href="/chinh-sach-va-bao-mat" class="text-reset">Chính sách và bảo mật</a>


                    <a  href="/dieu-khoan-va-dich-vu" class="text-reset">Điều khoản và dịch vụ</a>


                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 footerColumn">

                    <div class="text-uppercase fw-bold ">
                        LIÊN HỆ
                    </div>
                    <div class="footer_soc_desc">785/12 Nguyễn Kiệm,
                        <br>P.3 Q. Gò vấp, TP. HCM
                    </div>

                    <a class="footer_soc_desc" href="mailto:hi@tikitech.vn">Email:
                        <strong>hi@tikitech.vn</strong></a>

                    <a class="footer_soc_desc" href="tel:0903969952">Liên hệ:<strong> 0903
                            969 952</strong>
                    </a>


                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 footerColumn ">

                    <h6 class="text-uppercase fw-bold mb-4">
                        Mạng xã hội
                    </h6>

                    <a href="https://www.facebook.com/Tinhnangchoshoponline/?ref=pages_you_manage" target="_blank"
                        class=" " title="Share link on Facebook">
                        <img src="/icon-fb.svg">
                    </a>


                </div>

            </div>

        </div>

    </div>