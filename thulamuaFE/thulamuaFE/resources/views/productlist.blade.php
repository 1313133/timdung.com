@extends('layout')


@section('contentpage')
    
<div class="content-page container ">
    <div class="row-bread-crumbs"> 
            <ul  class="bread-crumbs">
                <li > <a href="javascript:void(0)"> Trang chủ</a> </li> 
                <li > <a href="/sales/{{$dataDealSlug}}">{{ $dataDealName }}</a> </li>
          </ul>

    </div>

    <div class="row_filter">  


    </div>

    <div class="list-product"> 
        @foreach ($dataReponse as $itemProduct)
               
            
                @include('components.product.productItem', 
                                    ["dataProuct"=> $itemProduct->product_id,
                                     "price_after_discount" => $itemProduct->price_after_discount,
                                    "voucher"=>$voucher  ] )
        @endforeach
       
           
    </div>
</div>


@endsection



