@extends('account.layout')

@section('contentchildPage')
 
<div class="edit-address">
  <div class="title-page" id="titleTextAddress"> Sửa địa chỉ giao hàng </div>
  <form   id ="form-edit-account">
    @csrf
     <div class="form-group">
        <label for="exampleFormControlInput1">Họ và tên</label>
        <input type="text" value="" id ="txtName"  name="fullName" class="form-control"   placeholder="">
    </div>

    <div class="form-group">
      <label for="exampleFormControlInput1">Địa chỉ</label>
      <input type="text" value=""  id ="txtAddress"  name="addresss" class="form-control"   placeholder="">
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Tỉnh</label>
    <select class="form-control" name="tinh" id ="ddlTinh"> 
      <option  value="Thành Phố Hồ Chí Minh"> Thành phố Hồ Chí Minh </option>

    </select>
</div>

<div class="form-group">
  <label for="exampleFormControlInput1">Quận/Huyện</label>

  <select class="form-control" name="quan" id ="ddlQuan"> 
    <option  value="Quận Gò Vấp"> Quận Gò Vấp </option>
    <option  value="Quận 12"> Quận 12 </option>
    <option  value="Quận 11">Quận 11 </option>
    <option  value="Quận 10">Quận 10 </option>
    <option  value="Quận 9">Quận 09 </option>
    <option  value="Quận 6">Quận 6 </option>
  </select>

 </div>

<div class="form-group">
  <label for="exampleFormControlInput1">Phường/xã</label>
  <select class="form-control" name="phuong" id ="ddlphuong"> 
    <option  value="Phường 3"> Phường 3 </option>
    <option  value="Phường 2">  Phường 2</option>
    <option  value="Phường 1"> Phường 1 </option>
    <option  value="Phường 10"> Phường 10 </option>
    
  </select>
</div>

<div class="form-group">
  <label >Số điện thoại</label>
  <input type="text" value="" id ="txtphoneNumber" name="phoneNumber" class="form-control"   placeholder="">
</div>





 


</form>


<div class="bottomSave">
  <button  onclick="saveAddress()" id = "saveAddress" class="save">Lưu </button>  
  <button style="display:none" id = "updateAddress" onclick="updateAddress()" class="save">Lưu </button>  

  <button onclick="backtoAdreePage()" class="btnCancel">Huỷ </button>     

</div>
</div>
<div class="address-content">
<div class="title-page"> Địa chỉ </div>

<p class="title-child">Địa chỉ giao hàng </p>

 

    <div class="list-address">

      @foreach ($data as $item)

      @php
          $addressText = $item->address.", ".$item->ward.", ".$item->distrist.", ".$item->city;
      @endphp
      <div class="address-card">
        <div class="address-card-item"> 
        <div class="heading-card">
              <span >{{$item->name}} </span>
              <a onclick="editAdress({{ $loop->index }})"> <span>Sửa </span>  </a>
        </div> 
  
        <div class="description">   
            <p>{{ $addressText }}<p>
  
            <p>{{ $item->phone }} </p>
        </div>
  
        <div class="btnDelete">
        
              <a onclick ="deleteAddress(this)">Xoá </a>
        </div>
        </div>
    
    
  </div>

          
      @endforeach

    </div>

  <a class="btn-addNewAddress" href="javascript:void(0)" onclick="addAddress()">
    <div class="jsx-1918375055"><span class="jsx-1918375055">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>ic-plus</title>
                <path d="M12 11h7v1h-7v7h-1v-7H4v-1h7V4h1v7z"></path>
            </svg></span>Thêm địa chỉ giao hàng mới</div>
    </a>

</div>


<script> 
  var addressInfo = {!! json_encode($data) !!};

</script>
@endsection