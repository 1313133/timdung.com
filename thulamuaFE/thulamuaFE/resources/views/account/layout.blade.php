@extends('layout')

@section('jsAdditional')
    <script type="text/javascript" src="/js/address.js"></script>  
@endsection
@section('contentpage')
<div class="account-page container ">

    <div class="area-menu-account"> 

        <a href="/thong-tin-tai-khoan"> <li class="{{ request()->is('thong-tin-tai-khoan')  ? 'activepage' : '' }}"> Thông tin tài khoản </li> </a>

        <a href="/quan-ly-don-hang"> <li class="{{ request()->is('quan-ly-don-hang')  ? 'activepage' : '' }}"> Quản lý đơn hàng </li> </a>

        <a href="/dia-chi-cua-toi"> <li class="{{ request()->is('dia-chi-cua-toi')  ? 'activepage' : '' }}">Địa chỉ của tôi</li> </a>
    </div>

    <div class="content-account">
         @yield('contentchildPage')
    </div>

</div>

@endsection