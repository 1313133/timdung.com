@extends('account.layout')

@section('contentchildPage')

<div class="search-order"> 


  <div class="order-row-item"> 

      <div class="heading">
            <span class="order-code">
                #{{ $data->_id}}
            </span>
            

            <span>
                Đặt hàng thành công  <strong>{{ \Carbon\Carbon::parse($data->create_at)->format('d-m-Y')}}</strong>
            </span>
      </div>

      <div class="content-order">
        
            <div class="row-item">
                 @php
                     $dataProduct = $data->detail_id->list_product;
                     $dataAddress = $data->order_address_id;

                 @endphp

                @foreach ($dataProduct as $item)
                  @php
                    $productItem = $item->product_id;
                  @endphp
                <div class="image-product"> 
                 
                   <img src ="https://api.thulamua.com/image_product/{{$productItem->image}}"> 


                </div>

                <div class="descript-product-ordered"> 
                    <span class="title">F2 - {{ $productItem->name }} </span>
                    <span class="quatinity">Số lượng: {{ $item->quantity }} </span>
                    <span class="prices">Giá:  1.120.000đ </span>
                </div>
                    
                @endforeach
                 
          
            </div>
        
      </div>

  </div>

  <div class="order-infomation">

  <div class="desktop">  
    <div class="order-heading"> 

        <div class="colum1">
          <p class="bold-text"> Đặt hàng lúc </p>
        </div>


        <div class="colum2">
          <p class="bold-text"> Địa chỉ giao hàng </p>
        </div>

        <div class="colum3">
        <p class="bold-text"> Hình thức thanh toán </p>
        </div>

        <div class="colum4">
          <p class="bold-text"> Thành tiền </p>
        </div>
  
  
      </div>

      
    </div>

    <div class="order-info-content"> 

    <div class="colum1">
      <p>{{ \Carbon\Carbon::parse($data->create_at)->format('d-m-Y')}} </p>
    </div>


    <div class="colum2">
        <p> 
          @php
              $dataAddressText =  $dataAddress->name."  |  ".$dataAddress->phone." ".$dataAddress->address." ".$dataAddress->ward." ".$dataAddress->distrist." ".$dataAddress->city;
          @endphp
          {{ $dataAddressText }}
    
        </p>
          
    </div>

    <div class="colum3">
       @if ($data->payment_method == 1)
           <p class="bold-text"> Thanh toán bằng tiền mặt (COD) </p>  
       @else
          <p class="bold-text"> Thanh toán bằng thẻ ngân hàng </p>
       @endif
       
    </div>

    <div class="colum4">
        <div class="money-item-row">
            <p class="colum-left"> Tạm tính </p>
            <p class="colum-right priceCal"> {{ $data->total }} </p>
        </div>

        <div class="money-item-row">
          <p class="colum-left"> Phí giao hàng</p>
          <p class="colum-right priceCal"> {{ $data->transport_cost }}</p>
      </div>


      {{-- <div class="money-item-row">
        <p class="colum-left"> Mã giảm giá </p>
        <p class="colum-right"> 0 đ</p>
    </div> --}}

    {{-- <div class="money-item-row">
      <p class="colum-left">Số dư </p>
      <p class="colum-right"> 0 đ</p>
      </div> --}}

      <div class="money-item-row">
        <p class="colum-left">Thành tiền </p>
        <p class="colum-right priceCal"> {{ $data->total + $data->transport_cost  }}</p>
    </div>
    


    </div>



  </div>

  </div>

  <div class="mobile">
        <div class="rowItemViewMobile center-left-right">
            <p class="title colum-left"> Đặt hàng vào ngày: <p>
              <p class="colum-right" >{{ \Carbon\Carbon::parse($data->create_at)->format('d-m-Y')}} <p>
        </div>
        <div class="rowItemViewMobile">
          <p class="title">Địa chỉ giao hàng: <p>
            <p class="description" >
              @php
              $dataAddressText =  $dataAddress->name."  |  ".$dataAddress->phone." ".$dataAddress->address." ".$dataAddress->ward." ".$dataAddress->distrist." ".$dataAddress->city;
          @endphp
          {{ $dataAddressText }}  
            <p>
        </div>

        <div class="rowItemViewMobile">
          <p class="title">
            Hình thức thanh toán: <p>
              @if ($data->payment_method == 1)
              <p class="bold-text"> Thanh toán bằng tiền mặt (COD) </p>  
          @else
             <p class="bold-text"> Thanh toán bằng thẻ ngân hàng </p>
          @endif
        </div>

        <div class="rowItemViewMobile">
          <p class="title">
            Thành tiền: <p>

            <div class="money-item-row">
              <p class="colum-left"> Tạm tính </p>
              <p class="colum-right"> 3.619.0000 </p>
          </div>

          <div class="money-item-row">
            <p class="colum-left"> Giao hàng </p>
            <p class="colum-right"> 19.000</p>
        </div>


        <div class="money-item-row">
          <p class="colum-left"> Mã giảm giá </p>
          <p class="colum-right"> 0 đ</p>
      </div>

      <div class="money-item-row">
        <p class="colum-left">Số dư </p>
        <p class="colum-right"> 0 đ</p>
      </div>
          
        </div>
  </div>

  


</div>

@endsection