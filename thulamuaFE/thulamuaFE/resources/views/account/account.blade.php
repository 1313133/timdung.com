@extends('account.layout')



@section('contentchildPage')

<div class="title-page"> Tài khoản </div>

<p class="title-child">Thông tin cá nhân </p>

<form   id ="formAccount">
  @csrf
    <div class="form-group">
        <label for="exampleFormControlInput1">Tên đăng nhập</label>
        <input type="text" name ="userName" value="{{$data->username }}" class="form-control" readonly  placeholder="">

    </div>

    <div class="form-group">
      <label>Họ và tên</label>
      <input name ="fullName" type="text" name="name" value="{{$data->name }}" class="form-control"  placeholder="">
      
    </div>

    <div class="form-group">
        <label>Địa chỉ email</label>
        <input name ="email" type="email" class="form-control"   value="{{$data->email }}"  placeholder="Địa chỉ email">
     </div>

      <div class="form-group">
        <label>Số điện thoại</label>
        <input name ="phone" type="text" class="form-control" value="{{$data->phone }}"   placeholder="Số điện thoại">
      </div>

    
    
</form>
<div class="bottomSave">
  <button class="save" onclick="updateAccout()">Lưu </button>    
  <button class="btnCancel">Huỷ </button>  
  
  

</div>


<div class="changePasswordArea" id="changePassword">
  <p class="title-child"> Mật khẩu</p>
  {{-- <a class="changePassword" onclick="displayPassword()">Đổi mật khẩu </a> --}}
</div>
<form id ="changepassword">

     @csrf 
     <div class="form-group">
        <label>Mât khẩu hiện tại</label>
        <input type="password" class="form-control"   placeholder="Mật khẩu hiện tại">
      </div>

      <div class="form-group">
        <label>Nhập mật khẩu mới</label>
        <input type="password" class="form-control" name="new_password"   placeholder="Mật khẩu mới">
      </div>

      <div class="form-group">
        <label>Nhập lại mật khẩu mới </label>
        <input type="password" class="form-control"  name="new_passwordRepeat"  placeholder="Nhập lại mật khẩu mới">
      </div>


   
     
</form>
<div class="bottomSave">
  <button class="save" onclick="changePassword()">Đổi mật khẩu </button>    
  <button class="btnCancel">Huỷ </button>     

</div>
<script type="text/javascript" src="/js/account.js"></script>  
@endsection