@extends('layout')

@section('contentpage')
    
<div class="content-page container ">
    <div class="row-bread-crumbs"> 
                <ul  class="bread-crumbs">
                    <li > <a href="/"> Trang chủ</a> </li> 
                    <li > <a href="javascript:void(0)">{{ $data->category_id->name }}</a> </li>
                    <li > <a href="javascript:void(0)">{{ $data->name }}</a> </li>
                </ul>

    </div>
    <div class="product-detail-page">
            <div class="row">
                    
                <div class="col-6 "> 
                            <div class="slidebanner">
                               @for ($i = 0; $i < 5; $i++)
                                   <div class="imageitem">
                                      <img src="https://api.thulamua.com/image_product/{{$data->image}}">
                                    </div>

                               @endfor
                              

                            </div>
                         
                        

                 </div>
                <div class="col-6 productInfo">
                
                    <div class="product-info">
                        <a>
                            <h4>{{ $data->brand_id->name }}</h4>
                        </a>
                        <h1 class="titleProduction"> {{ $data->name }} </h1>
                        <div class="price">
                            <span class="notranslate retail">  {{  number_format($data->price_after_discount, 0, ",", ".") }} đ </span>
                            <span class="notranslate sale"> {{  number_format($data->price, 0, ",", ".") }} ₫</span>
                        </div>
                    </div>
                    <div class="navbar-checkout">
                        <a onclick ="addCart()"> Thêm vào giở hàng </a>
                    </div>

                    <div class="product-timer">

                        <p class="title-product">Ưu đãi này sẽ kết thúc trong:</p>
                        @php

                            $df =\Carbon\Carbon::parse($data->time_start);
                            $dt = \Carbon\Carbon::parse($data->time_finish);
                            $timeStartText = date_diff($dt, $df)->format('%H:%I:%S');
                        
                        @endphp

                       <div class="row-timer">
                           <div class="timer-item">
                                <div class="round-timer">21 </div>
                                <div class="timer-text">Giờ</div>
                            </div>

                            <div class="timer-item">
                                <div class="round-timer">54 </div>
                                <div class="timer-text">Phút</div>
                            </div>
        
                            <div class="timer-item">
                                <div class="round-timer">26 </div>
                                <div class="timer-text">Giây</div>
                            </div>
                        </div>
                    </div>

                    <div class="additional-info">

                        <div class="addition-nal-item ">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-guarantee</title>
                                    <path
                                        d="M20.473 6.262c0-.782-.47-1.31-1.26-1.41-.15-.021-.304-.037-.453-.054-.445-.054-.865-.1-1.268-.187-1.576-.345-3.102-1.056-4.79-2.237-.279-.195-.586-.374-.948-.374-.358 0-.665.179-.944.37-2.258 1.551-4.291 2.341-6.395 2.474-.911.058-1.389.574-1.389 1.493v1.655c-.004 1.526-.004 3.102.012 4.657.004.578.079 1.177.216 1.784.324 1.422 1.064 2.703 2.262 3.913 1.493 1.509 3.364 2.674 5.717 3.559.158.058.328.091.507.091.216 0 .432-.046.624-.129.129-.058.262-.112.391-.166l.012-.004c.57-.245 1.156-.499 1.73-.798 1.913-1.01 3.36-2.204 4.424-3.651.956-1.297 1.451-2.603 1.509-3.992.012-.266.021-.549.025-.869l.021-2.57V8.773c0-.84 0-1.676-.004-2.511zm-.998 3.505v.029l-.021 2.578a37.2 37.2 0 0 1-.021.836c-.05 1.185-.482 2.312-1.318 3.443-.973 1.322-2.312 2.42-4.087 3.36-.536.283-1.106.528-1.659.765l-.012.004-.395.17a.576.576 0 0 1-.225.046.46.46 0 0 1-.158-.025c-2.216-.836-3.971-1.925-5.36-3.326-1.056-1.077-1.709-2.2-1.992-3.435a7.18 7.18 0 0 1-.191-1.572c-.017-1.547-.012-3.123-.012-4.644V6.341c0-.191.029-.333.087-.391.054-.058.183-.096.37-.108 2.291-.15 4.482-.99 6.894-2.653.187-.129.312-.191.378-.191s.191.067.378.195c1.8 1.256 3.439 2.017 5.148 2.391.449.1.894.15 1.368.204.15.017.295.033.445.054.291.037.387.141.387.42v2.507l-.004.998zM12.024 14l-2.351 1.236.449-2.618-1.902-1.854 2.629-.382L12.025 8l1.176 2.382 2.629.382-1.902 1.854.449 2.618L12.026 14z">
                                    </path>
                                </svg>
        
                            </div>
                            <span>Cam kết 100% chính hãng</span>
                        </div>
        
                        <div class="addition-nal-item ">
                            <div class="icon"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                    viewBox="0 0 24 24">
                                    <title>ic-ship</title>
                                    <path
                                        d="M20.911 8.159c-.666-.721-1.613-1.115-2.665-1.115h-2.745V4.999H2v12.064h2.506c.163.93.972 1.638 1.948 1.638s1.785-.708 1.948-1.638h7.697c.163.93.972 1.638 1.948 1.638s1.785-.708 1.948-1.638h2.002l.001-5.699c-.004-1.328-.377-2.439-1.089-3.206zM3.006 6.006h11.49v10.057H8.315c-.272-.763-1.006-1.312-1.86-1.312s-1.588.549-1.86 1.312H3.007V6.006zm3.448 11.69a.973.973 0 0 1 0-1.944.973.973 0 0 1 0 1.944zm11.595 0a.973.973 0 0 1 0-1.944.973.973 0 0 1 0 1.944zm2.941-1.638h-1.081c-.272-.763-1.006-1.312-1.86-1.312s-1.588.549-1.86 1.312h-.683V8.05h2.745c1.693 0 2.745 1.27 2.745 3.314v4.693h-.004z">
                                    </path>
                                </svg></div>
                            <div class="jsx-657340265">Giao hàng dự kiến: <span class="date-delivery">Do tình hình dịch bệnh
                                    đang ngày một diễn biến phức tạp, các tỉnh / thành phố trên cả nước đã tiến hành thực hiện
                                    việc giãn cách xã hội, nên thời gian xử lý và giao hàng trong giai đoạn này sẽ chậm hơn so
                                    với thông thường.</span></div>
                        </div>
                        <div class="addition-nal-item ">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-guarantee</title>
                                    <path
                                        d="M20.473 6.262c0-.782-.47-1.31-1.26-1.41-.15-.021-.304-.037-.453-.054-.445-.054-.865-.1-1.268-.187-1.576-.345-3.102-1.056-4.79-2.237-.279-.195-.586-.374-.948-.374-.358 0-.665.179-.944.37-2.258 1.551-4.291 2.341-6.395 2.474-.911.058-1.389.574-1.389 1.493v1.655c-.004 1.526-.004 3.102.012 4.657.004.578.079 1.177.216 1.784.324 1.422 1.064 2.703 2.262 3.913 1.493 1.509 3.364 2.674 5.717 3.559.158.058.328.091.507.091.216 0 .432-.046.624-.129.129-.058.262-.112.391-.166l.012-.004c.57-.245 1.156-.499 1.73-.798 1.913-1.01 3.36-2.204 4.424-3.651.956-1.297 1.451-2.603 1.509-3.992.012-.266.021-.549.025-.869l.021-2.57V8.773c0-.84 0-1.676-.004-2.511zm-.998 3.505v.029l-.021 2.578a37.2 37.2 0 0 1-.021.836c-.05 1.185-.482 2.312-1.318 3.443-.973 1.322-2.312 2.42-4.087 3.36-.536.283-1.106.528-1.659.765l-.012.004-.395.17a.576.576 0 0 1-.225.046.46.46 0 0 1-.158-.025c-2.216-.836-3.971-1.925-5.36-3.326-1.056-1.077-1.709-2.2-1.992-3.435a7.18 7.18 0 0 1-.191-1.572c-.017-1.547-.012-3.123-.012-4.644V6.341c0-.191.029-.333.087-.391.054-.058.183-.096.37-.108 2.291-.15 4.482-.99 6.894-2.653.187-.129.312-.191.378-.191s.191.067.378.195c1.8 1.256 3.439 2.017 5.148 2.391.449.1.894.15 1.368.204.15.017.295.033.445.054.291.037.387.141.387.42v2.507l-.004.998zM12.024 14l-2.351 1.236.449-2.618-1.902-1.854 2.629-.382L12.025 8l1.176 2.382 2.629.382-1.902 1.854.449 2.618L12.026 14z">
                                    </path>
                                </svg>
        
                            </div>
                            <span>Đổi trả trong vòng 7 ngày</span>
                        </div>
                    </div>
                    
                    <div class="section-content">
                        <div class="titile-section">
                            <div class="title">Thông tin sản phẩm</div>
                            <div class="expaind">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-minus</title>
                                    <path d="M5 11h15v1H5v-1z"></path>
                                </svg>
                
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-plus</title>
                                    <path d="M12 11h7v1h-7v7h-1v-7H4v-1h7V4h1v7z"></path>
                                </svg>
                
                            </div>
                        </div>
                        <div class="content-section">
                          {!! $data->info_product !!}
                        </div>
                    </div>
                
                    <div class="section-content">
                        <div class="titile-section">
                            <div class="title">Chất liệu và cách sử dụng</div>
                            <div class="expaind">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-minus</title>
                                    <path d="M5 11h15v1H5v-1z"></path>
                                </svg>
                
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <title>ic-plus</title>
                                    <path d="M12 11h7v1h-7v7h-1v-7H4v-1h7V4h1v7z"></path>
                                </svg>
                
                            </div>
                        </div>
                        <div class="content-section">
                            {!! $data->how_to_use !!}
                         </div>
                    </div>
            
                
                </div>
            </div>
            <div class="row">
                <div class="col-6 brand-info " >
                
                        <a class="jsx-1806707264 text-no-underline d-inline-block" href="/vn/brands/esprit-56e238252cb09b0e000eacbc">
                            <img alt="leflair-logo" src="https://leflair-assets.storage.googleapis.com/5dc3ea03b888e0c257f76c62.png" class="jsx-1806707264">
                        </a>
                        <h3 class="heading">"Đơn giản nhưng vẫn rất hiện đại"</h3>
                        <div class= "desc">Thương hiệu Esprit được khai sinh vào năm 1968 tại Mỹ từ việc kinh doanh nhãn hiệu Esprit lưu động ở khoang sau xe ô-tô. Ngày nay, Esprit đã trở thành thương hiệu thời trang quốc tế đại diện cho giá trị "Chất lượng là trên hết" trong từng khâu sản xuất nhỏ nhất. Esprit nhấn mạnh phong cách sống trẻ trung lạc quan, mang màu sắc trang nhã và tính ứng dụng cao.

                        </div>
                
                </div>

                
            </div>
            <div class="row section-introduction">
                <div class="col-6">
        
                    <div class="info-image">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDIBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/CABEIAO4A7gMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABAMFBgECB//aAAgBAQAAAAD7+AAARI9Z8LTPgAAAAACUHrzN673xH49celAAAABRJtnxEBz3P4TitfQAAAIQzNr8Yk6BxeOb0gOMAAAqi+Rud4hBa1E9kHlJtNa46ABysdilnMbp8DPrKAbvKzQiTC46AC6D3ieXNWeQ09Q7NRuqMLt60TnS82XoCpmn6wnmqTYVev5znuGqx2zzGstUmqtxsCpslnQwnvbrxSSeY/LMGLvND74p7ieBPnWfYHEmJhf1MKir0wKz11h7grrVZ0ARc7zK56s2es6uT9AU6lbqqPkwC0ieQVuqOKxorTd9c6ApNV3HhX030BF7FN5jMw1TGvv3taq2AtNXWinOOgchnyKfz05Wc59J0mlTcAi8pWFY/G4BDyepyuDNBPivG6+juKtAeO1skTflwCGT1z5tU0b1KrrmfrQs0BH6rZ2IBwDizXMclg/NVJvNPqxJ0CHiVnGrI10BF08/NZ8RV/Q1fq54iZAVnrLZZGzjYAh8MiPz6wgpfqzQg90BJiruPNTZwOmQl1Ss0gj89T+otdTnlyeia8q+IrQgWnmq6hbUvKDQBxNiakznnTPNVtl6CpslWsP79bX14WA57ZM/mNqtcL8HQKvr0LdItUbNSzACvzFhJQ7tZurdbA8108nO42+oIrtHXZbWfNbLZYnedRbr5negBWzSxt1dAqJ7/BX1f4d1gm2g0yAAQ1jbajEyVchf5B65s/Svlmv8W4AAC8Eb3IPU0gchiGYFp2JOgAAEdVM0x5gDshAqWwAAAAC8QuSeueYpZuMyAAAAAAcSGvaErYAAAf/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgQFA//aAAgBAhAAAAAKpEBagWQNQsSrk1JpCwuWsqBLc6kX6fbo5vjiNIi9nq6+Hm8sVZF6fZ59eXzxbEXXqdvn+bIqyVPv1cnxsW51LALLlqKhYrJqFIrIWwVIBVXKD//EABoBAAMAAwEAAAAAAAAAAAAAAAABAgMEBQb/2gAIAQMQAAAAAJU1NOgAUqgCXTAxulNKiVkDFVQUiaqVkUW4YmK5m4qidfU0Oht56ITdE8jzUbnf6lEqbpHL8pv4vSdCiYp0Ti81y+z6OqJSqpK0uV1ttNwVFRYAJUrMbcttIBZAxtqWNpZAFKbBJ0wAlTU22B//xAAyEAACAgEDAwIFAwIHAQAAAAACAwEEBQAGERITIRAgBxQjMTIVIjBBQhYkMzQ1QFEl/9oACAEBAAEIAP43PFMaOy0p50u25c8wF8I0y/z4CXtn7oumueDEoMYIf+m65PMiv5l2puPmONQpzZ51GPdxyXyHH3+QHU4+OOdTQnjwdJwaEnInxNx86iy2J1VtCzwX89uxHErERI54FVKZjlgKUuOYO0kdTemfAd+zP27l3XduR9/mnj+Y3gn8luSwZiDqLZHMNqMX5gSkDgoTYFgcj/HJQIzMtuFM/Tl7T8SmoR+T4WgNHbI56UxWa3y0KiBjUBA+PaSgP8jpLKOR6bVePCbq5n97aynDzpim1y8jZdGkW4bMAyP4bp8LgYiJmeIRVFcdRutQuZEArsdPW5YCEcD6RmscWWPFzuPNjt7F/PEshaAsHB7gDLZTK1Jv5qpjshRp2PWxTU2JOOXVZ8g1T1lqxVlcSQRPExMLKDESj3m0QHmWsl7f2168JHmXWCOe0lNWFR1F6N3Fl8vl7WIwuOr2a1BSre6cRNz4jrUnI5huY2llMTktsWvm9r45uvh5w9OYvazX+Z+JeDrzv++VTbZ11Y2v+m4itWZt3ddbcNq4lGpGJHy2uap7qa1gHRwVyp2p6wqWIGIWfus2e1HSJGR/lVR2x6ysOlh9pdZEKH0y286GMuTSThc3+rY/vlvELOB3DVz2Ow2NsY0XWb+70NVuXbt9O69rhnqnerbPoXcTtxVG9Vx+4Np2rK8VgMNlH592ezOT/wDt/EehR1vfJuJdfAUNqY9GE3vkMevEZu9mt22/lPSwiQnuqTYF6uNWUdo+YW9i5jhLBcHV7Wz1OPVRPWfWVp0rGBGqjtD1FrLWGVMRdsJ2OhNXaUZFW5K2ct4Zmfv38f8A4k2p2G0tlG5KIz8REREakxH8hsoieSh6ZnUFE+RGlWXZO0urtxVfc1vNHu65bobzsLo4bG1tsbeBJzmNw7kvHewWBztfN0yYvVhRVX9wI6Ho50wJWchON5JhD7Z+o6YGOlCdVglzpcfoYwwZCQxO4No3G/o543P7rsJjMxEAMRDrYjPAQNl/3/T4GP3xSREammmY8fIlHJK71hHhing6OIdjKT76bzM6VrcG7l4G9h8NdwGZlVAVqWbCXowggmCqsKrYlc3UcrhkY8h7xQXrZsxxIBRXyUsm2cmQpFYQC4GPYRQIyREbLR9IJrgrzHpauV6aZdZr2k21C5HjXETp1Lx3FIszM9tubwdXOUuw+uokVlqP1uL5CDhT5dWgZKJQ/wAJeDh9LbJBPjSR7SIjVX6lg3T7TIrTugQAVhwOiIQHqLM74oUOpVROIzm73fM3zTuPYVqTVg9/YrKxC7EFBfbT68Njkaj5OJWfsKOoZjVM+zYJZZFMhIHoDlZwUCUSMTq+M9oDhI9bxHVouitOqg9FcZ9txvA9A10wpXGsvmKuEqxYtW/iG1s9rHfpe6dyTzdq7ewW20xav57fT7AGjF4L4ifsinnb2ycJn0TdwkI3ltGeFUPilHgcjicpVzGOXdq2lyoxcANg0jI+y5EKuiY2Ag65+i46ViOrI/5U5nH+LHVq9PMgECPSMR7V/XuSXp8Rv+Mqaq2KuB2hXyI39/ZKzEjVsWX22y2wX4F6U79vHu71PH/EzJ14gbu6xo5PYp5mPhvzG1Fzowg1FE0S4Ill7Lw8qgtJLrrjyoeLAxOrjYiO3FD+/T/N4I9rz4WRaxvC1mydfEFMswIMi0fzXwwAo9C/AtLWbWCtZCQHIHrcp/LfDHFp1sNXa2dRidR9PI+21EEg41TL/LcabyFkphTIaEFBl1mRaxsiK2STP+S9tn/bHqn4rj6bkqfPbfuIjaZfqe08jjNTHEzE6jbgXsAF3GU6dPZGOjIZC7ddkLrbdirXO3bTXX8SHRDsXiK+MqRQxdWrGrsQOQCI9jv9A9Yj/RPVn/cHqmfSRRpy5W0h1j+Og+WcRkY9tmOax6xgwaeZ0UcxMax0/wCGt8NqHuzGzjc86I1i8tYw1r5ivkcjZyt5lu3r4fYyLWaK+7CRO6viGd+eNTq5MFkx49j/APQPWIj6Rzqz5sHqkqZ6j1kVgQTIY6OTIdWf23Fl7WD1BMaolHSwC9N+YebdEcgnxvDa0cTEiUiRfgXohDbVhaEZ9qtqbVTgK2xMFOHwgsd/T0X9S+Re23PTXLVOOK46OO5ZmIAYAYGLX7apFOPOAtRq8P7ALSy6gEvaU9i71epDBhIler2Nl7iG1X3Lg1ZOrGcxJfgWoGSniMJjK+zcQedy208PZ3TnmZvJxHjj0eyFrkopL4XJz7LxcAI6D6deI0ouHCU6yQSuAHSy6GCWrAQdUo1RKCVwXpPpaV3V+KjoMegufTJ41GVonVs1bV/ZOWmtZzO1a2brzksHjMDjNmUv1bNV6+T+IOelzqVKvj6i61b0aU2XwsYiIGIj2Nnu3BHVz6VeedLPhATqynuV9TExMxNY+tEaT9C7K59Mhvpa75UcTjN7Bavhjsnq5WJDe6tFiHBx65PFVstUKtaejM7ItG1GNwOZ3tfjI5Shj62MqBWqelixJz2l10QkIj0oX7W4N0HYq1r9W5efRRphwtczNIZmSaV0+TgIQqWnxriPSyiGjJRTb0MkJvKiQFgIYLEwet0PbW2zkXI22pGD2Ou9V3ZZoZ7Yc5aviLDbWGo2Hf006qQzDFKt/wBreYmOY0QCY8FERH20bgXHJk5lguhSECmOfSM7Tu5xmFVe2zl9tObd2xtLcOGxmCtG/bNrKXMV81k7Z9RCkYGEI8gM2H6ABAeI1Hozw0uEMByeZHmpZkScpVlDEtTS3LtMjrYzamHzAXr7cjncvdyWcTt7ADyKhEtOQpupqOXyShsW0zr54445i63+zm23QUf6tBYjHEEQrGSJ+SDcWNyNXBbd3LS27iLGOt4V2SZixdl8ltTE5a8i7ZcyEhM6qLkilx3Xee3GOmIcUz7LASt5jNZ3Zb5eoXL8VXzEdhmt2ZJ+K21ct1tmYVOKwina3NkbOeysbYxNSuNKmqvGMz2MzFptWj7fGrGXpU8lWoO3BhNyZ5V4WbFyVW9gloXunawZpAXKlVm4N3BGMvpUqnVBS/Nt2nGNdWpmZLmaAcQRz7LSVu4GG1WKjmaVuQ+mVmv3R6wq2IIu225TTkKTatn/AA9u3FJLH4zbO3E7fpEEZ3L2tx5ItvYTZ2PRjd8ZevW3rmrWDw6nUrOc3LgVxZzFawm3TTZTuHfIYPNqx8xMGsTCjvLM4rNX0ZXepov4OhuHFUrS79CvbTn0M2nuVe4ai2C1QsApgYmZMytM6AEQQrTWE9vhdIp8sCIGIiPdaTCm+K1rmIWdmrDI6wVbMIlLomJ86z9DIX8Oyvj1bDHHIWeI2FFg8/uI7m/yfay2Dx9V+T3Du35vARj6YY7HV6YYzGhuq5ue4zYOWm7gppWc+EYT4iUr85zaFqiq3Y2/8PH2i29NW0YCY8GxoqDkuWWy4iBBC9PfLi4ioqACDn+C6girwfpXtyMdDWpU5Q6kH050u2tkRHpUxlOjYsvrXdvFa3hSzWr+Fv098VszjsudheItnUwPw/oFh655XH7WsYDdsWcXuDAVtw48K9gYIViEyUBHMtuf2qXVNhdbjYtAxpzycXkBkzgYEYiIH+JtMDmSAqTI+wNbXKR0m0pnHLaa2R1DC7KPIDe48MG2ktQ1ZRzGuYjU2Fj9zuKj7TacyOFRVa2eWgtaR5h12PssVtsF1aigfiSUgEx+3+S3W61ei7LF6XdWXgx7LQnR49PR1TNAOfE42Yjmf07iOZiiuJ8xWSH2Nylx5O/xHCzYbPyUuWsgYAYAeI/ndVBk9URQP7zNH/xijSX7he0J8fOv44kcg8ftN52puPKNEwy/JVU2RBT8h/5+nnH5KUCh4H/pkoTDpNlGQKYEKX9T+XSHiDxwGvnRUjifCqgjPUf8n//EAD8QAAEDAgQCCAIGCQQDAAAAAAEAAgMEESExQVEQEgUTICIyYXGBQqEUMDORscEjQENSYpKywtE1U6LigtLw/9oACAEBAAk/APq8ToE6w8k8keaaQU3709ys5qNx+qe5KenBNJ8yrNUt/QJzk8qQfcgCuZqcnruu/UM9bIElGw2TQPNOv6KMqP5HgCVGbJhantLjum8vohzNWiz+twG6eV3QrNCaSdyn2TeY+ZQAHZY0q7V3mJtj5nBGzt1ceYT1gfqtSgrF34LFycQNkAONU0V4FzDY33/BU7pxzhnK11ke44AhUxYKCUR8/NfrDcj8lz9bWvLI+UX2z+/sG3mu9GgDfAgrFn4cNR9RgAhhkFi45le5XefxpoqV9MSJaiqOI82tVUaqZos6YtDeb2U5glqYOsgmb8MjQfyYmdT0tRx88kejw0g84RxMDQfUCyznrXf5/uQwihdL/V/6q5mq3iBgCkJ6mIB73O1AxKiewUzsHk4PboeABHmrgDQLA6hYsOy9r9vxJxKHeKuTqVnqeEM9dWN8UNOzm5fVUM9Jd3K0TgAuUjIvpLfo873i7Qdz/wDfAul5ayWYDnL7NjZ6BRPcGz9XIWi+BI/7L9FXwtIZKPiGrXeSY1krHOsA7mwJJVFH0h0fPKZGs6wMfGUxkNQ6PqoadhuI2rGDo2Pr5PJ+Y/sRvW9IHlP8Ea+zbSRH1IAuVIB0PSN6o4faP3B43BQAdqEO4U4kbFZbdncrwj8Ue8V4jwF5IoHvb6hpIUTqiqn55JS3F8hDj3VO6iNO9j6WiZ8HeAu7+JAMlqIGvaT8L7AhdJT1whaGsp2uLYm2/FZJ4HqU8FPaiCFTxNqJBZ8jWAOcPMqofPJM0NiDx9kNgmF1TX0LKeMtzBc9PY1sTeeeU4Au+IodV0fRkmMSYfSnahMMU8Z5JoX+KJ2x4eFG4PAYZ9nVyyaFkDhxaC0ixBUDK/oyZ3OIC8B0ap2dH9FxODzTtfd8pQAAXeKPI1PKuT6rmHunoEtRsdiqdjqqEERykd4KQ0XRzf0gBONUpA/oWUFxje7vQO/h3BUbGGQ8zi0eI5XPy4a6LEaLRGwI7HuVkMAvdadkhYMWLtzxmZFGM3PdYKVkkbxdrmOuDx7pWDt0SyRneilZ4o3bhSvmcxoaZH5uO57GbVa2RC0Kwdrwzdw0GKFwMr9rwBZcCArVU/8ACe6PdSugpfhuP6WomWiJzzid6j4Sn/Q6n9yV3dPo5W4eMLBw1PZyKF0FotUMFuszgszj2c3LxHNOIaTytDRckqgJcfCZD+QUj4Kc/DJ3B/IFKySQZPm/tag6CK323xn02UYljI5evaP6gqlkLnf7XejJ9NF1s1KzRv6WO34tXR583wH8inExvvmLEFe61z7OW6z4aBarJo4ZWt2cm8Muu/IqkYXGCMuDAGlxICjjpW/zuUz5Xn4nuuVtwqJIJN43W+/dQQ1Y3HccqKFlRIyJ7Xlo523c3Vf7r7LULQ37OhWoxWjuGufDTs6ArF17Dh+zma4/ML4I2D+V4HHZMc97zyhrRiSmua9psWkZcP2rYAf5eZDFwe773k8Neztw0dfhqVvgPqRc9WXNHmMQsZGhwaDs4YfO/GY1E7Qeuh1vsAmtm6WlbaGD9xODpZXXdYWXjleGN9SQE37JnMG/Jv4FY9TE2O/oAOGWHZ/dW/DK3A4X7fw8e7TTnkGwDsWofopz1rPfhiLWex2Twn80r/uaNhwsKeiZzknLmKBNPC/rv/FuDOJ27P7q34ZZIYs7eoWd8OLf0tP47asX+pUn/I/9kCHDMFbcGF8sjg1rRqU8Gsqm81Q9uxz/AMJnLU1JEkm4GgQ4ZN7errLILXALULMFajs4NdxAIOaBdRTHLcatQEgkHNKxuvmtkCSUy9a8Wgg1b5eq78DJObHJ79B6Dj7LN3Z1K0GK346FYlHw9kd4YheJvFt2O11B3CDpaGU3wycNx5p8ZfILmMGzX/4KkZJWj7NgxsdmjUq8VDEdMo27DdyiEcMbbNaOJ7ozK07OTVm7DgMSFYG+CwKzGCwa48aCbpSoZ4+qOAVBN0ZVv8DJsn8NMSsDrxj5mnI6tO4KJmo3akd0+o0KkfHSaO8tmD81C2KFgwaOOazOJ4Tvj6IoLx3acKmQ/kFOx9RBbrWA4svw0Wq0Q7oz4izgsnLNqPeyIRIkbC6xGipjUSuh697WeKQoEviLHxOHijdzAOC+1lgje/1LQeFwdkOUhWPpwAIO/EhNs3dYu34QyVFmO+kSM8EfkSp3Ohd46N3e+7dTPZXtLpasS4SSPVgZ3mSGICxiYcgvdGwaFrieyfiKztYheA7pofHI0scNCDgVTM6T6NLiYmOdZ8So4KXo+suX0eYudhopTFJGQ6pqGfsgNE4kgYuOvBqfgmXO/KolHmhyhOJOyFgiABmVXtbVQgN60DC52KoXwdJwPxiaLuqHpsUVQ4l3IzDkboCqcddG8OJbh1nk7cL2C9kfXtbrwnNHHQrMYNvwwlaA1rtiSBdHrKmra2aWXe4uAn541k4yY3ZPe9sTAwPkddxtqSqpks0R77fIa9uflqaj7JnKe8qiOnp4r/R6WF1+vtq4qFlPNTHkmia21nbqQQdKQG8MgNvYqJ1DS055K2VuDpiPhC7sUbQ1ovkAr8gXsEcSh2RYjVC4RwOAKweM1cFM54pWFrgulYHUWUZmFpIgn9bUynmnm1eVJaPKsqhk1uoCBMNPCyMc3siG1Ek7Y23bdUNHVUbSBJLSPIc32KdzsmYHtPkVSiVlmulkD/swTsiCCLghMdWU1PJaVzGAOiF7B1hophK6imDw9uxP+Q1HuTRh49wmE0VS4MrI2o3DgHD0RFlgwLABD0CsBt9QO6dkcRkVgVh58KptLK82Mjm/DqBsuk6mjrGts+QYslP8TU9slQyVkcjmDAuBfdBj53SmQNf4SRa11SUlM+NwbVP58gHaIktgjbGCdbCytyTu+jwP2tiD8mK5qaB3Uuafkm2pekW/R5x8sf8AgjaGoYW1FA7wPG7fNQTRmnkLWGRhFwmh2N8UbLuxhWAGq8C8R+pGIOHAkjdYncLFi7p8+EDYpKlwfKW/Ed/mVUNMVNEWCIt1scfmohJDKwR1jQ8A7X/D3aonSVAid1bWDEutYKCb6W+5kjMpACjA6Lmi5JmOebtKldHySCRrmDEWTnO5Ra7kbBC5R9lgNAsGjILVZAfVO5UQV9xQAKHKdwE7mao7HcJ2PmntJRRCe1XKYnoAeZWPmsfMp4WJ3P1tusGIHA3HmrtRY4oFpTypPknouKaE8JvuU5e5Vh+oGzk4WT8fMIe6kIT7otVh6BSJ7iu6FJ94TgPRD9UCePdOw2CYFZr/ACTmo831v//EACQRAAICAQQBBQEBAAAAAAAAAAABAhEQAyAhMUESIjJRYTBi/9oACAECAQE/ANlFHBwcFFb0iqLxZebK3dYsooooovD52dZo8F4sX0VZ1hrY8XiznsvF4R+YXWzohoznykLS04fJ2ytWrcePoelCfwdE9KcOWPZI8DyyHu04rxZDRhD4okT0oT7RPjRr9FnwMfQ8s0HcHHz2ac1OKkS09Ry9LfBqSUIGvxGMBZ8Hg8bGac3CSkjR1FB/5ZI1NRTfqfxROTnK2LPgYs0xY0tX0+2XQ79PMvaaur6+F0LFZlh4/RosoplFiRf2fuxbK4KwlZ0P7Ozsb2LnFFiYi1ZZR2PcmVjgpHGKL/hZZwWjgsvb/8QAKhEAAgAGAgEEAQQDAAAAAAAAAAECAxESITEEEFEgIkFhMhMjYqEwQnH/2gAIAQMBAT8A9DiRd4KRHuKRIvFEn626F1dFqXVGWlGf9LfBd5E6+mlzPpCQnUuK6LtsqUN4ZDh+hurPpGi57M1KIoi0eMsrTZs2QvNOm6CwqiXVqNlCsNaFD6Yoc9Me69RbPnvZO5cmS6RPJHyZ81exWryxRca+ijd3kh5U6Uv3VVeUSuTKnYhYvHfxQgP9hb7RNdnImRLdpO5U6b+cRLySuTNlOkLJOeUn/EffyQi2Lp6Fo5sNk6GY9PDJ8typjgZLnyYYL4F7iRA501I4KvjjmkXfyLY/yFvtHIkqdLcDOVIinQ/zh2S0SJDlqxfnF/SJMtSoFAh77+SAi33ch+euVxXMamS8RIShvxL/AHP6ONxv0vdFmJ9IT6+CAZD4No+hMp4KlUVYkOIt+UfQ8unbTTPtG0Kui7OBxIbSHEkbwLKozQ8EMPoftYs6KluRp1Ik26FG0W5qV8FaEOc+locPgu8i+jJVmR0+S7wKH/BahwmSjKNlqEkvT//Z"
                            alt="Product extra guarantee" class="jsx-3141430331">
        
                    </div>
        
                    <div class="info-text">
                        <div class="title-text">Bảo đảm 100% chính hãng</div>
                        <ul class=" text-list-ul">
                            <li class=" text-list">"Thử là mua" chỉ làm việc trực tiếp với các thương hiệu và nhà
                                phân phối chính thức</li>
                            <li class="text-list">"Thử là mua" kiểm soát chặt chẽ quy trình hàng hóa được hợp pháp
                                giao dịch, nhập khẩu và khai thuế</li>
                            <li class="text-list">"Thử là mua" vận hành nghiêm ngặt quy trình kiểm soát chất lượng
                                của từng sản phẩm trước mỗi chương trình ưu đãi</li>
                        </ul>
                    </div>
                </div>
        
                <div class="col-6">
        
                    <div class="info-image">
                        <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDIBCQkJDAsMGA0NGDIhHCEyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMv/CABEIAO4A7gMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAABAMFBgECB//aAAgBAQAAAAD7+AAARI9Z8LTPgAAAAACUHrzN673xH49celAAAABRJtnxEBz3P4TitfQAAAIQzNr8Yk6BxeOb0gOMAAAqi+Rud4hBa1E9kHlJtNa46ABysdilnMbp8DPrKAbvKzQiTC46AC6D3ieXNWeQ09Q7NRuqMLt60TnS82XoCpmn6wnmqTYVev5znuGqx2zzGstUmqtxsCpslnQwnvbrxSSeY/LMGLvND74p7ieBPnWfYHEmJhf1MKir0wKz11h7grrVZ0ARc7zK56s2es6uT9AU6lbqqPkwC0ieQVuqOKxorTd9c6ApNV3HhX030BF7FN5jMw1TGvv3taq2AtNXWinOOgchnyKfz05Wc59J0mlTcAi8pWFY/G4BDyepyuDNBPivG6+juKtAeO1skTflwCGT1z5tU0b1KrrmfrQs0BH6rZ2IBwDizXMclg/NVJvNPqxJ0CHiVnGrI10BF08/NZ8RV/Q1fq54iZAVnrLZZGzjYAh8MiPz6wgpfqzQg90BJiruPNTZwOmQl1Ss0gj89T+otdTnlyeia8q+IrQgWnmq6hbUvKDQBxNiakznnTPNVtl6CpslWsP79bX14WA57ZM/mNqtcL8HQKvr0LdItUbNSzACvzFhJQ7tZurdbA8108nO42+oIrtHXZbWfNbLZYnedRbr5negBWzSxt1dAqJ7/BX1f4d1gm2g0yAAQ1jbajEyVchf5B65s/Svlmv8W4AAC8Eb3IPU0gchiGYFp2JOgAAEdVM0x5gDshAqWwAAAAC8QuSeueYpZuMyAAAAAAcSGvaErYAAAf/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgQFA//aAAgBAhAAAAAKpEBagWQNQsSrk1JpCwuWsqBLc6kX6fbo5vjiNIi9nq6+Hm8sVZF6fZ59eXzxbEXXqdvn+bIqyVPv1cnxsW51LALLlqKhYrJqFIrIWwVIBVXKD//EABoBAAMAAwEAAAAAAAAAAAAAAAABAgMEBQb/2gAIAQMQAAAAAJU1NOgAUqgCXTAxulNKiVkDFVQUiaqVkUW4YmK5m4qidfU0Oht56ITdE8jzUbnf6lEqbpHL8pv4vSdCiYp0Ti81y+z6OqJSqpK0uV1ttNwVFRYAJUrMbcttIBZAxtqWNpZAFKbBJ0wAlTU22B//xAAyEAACAgEDAwIFAwIHAQAAAAACAwEEBQAGERITIRAgBxQjMTIVIjBBQhYkMzQ1QFEl/9oACAEBAAEIAP43PFMaOy0p50u25c8wF8I0y/z4CXtn7oumueDEoMYIf+m65PMiv5l2puPmONQpzZ51GPdxyXyHH3+QHU4+OOdTQnjwdJwaEnInxNx86iy2J1VtCzwX89uxHErERI54FVKZjlgKUuOYO0kdTemfAd+zP27l3XduR9/mnj+Y3gn8luSwZiDqLZHMNqMX5gSkDgoTYFgcj/HJQIzMtuFM/Tl7T8SmoR+T4WgNHbI56UxWa3y0KiBjUBA+PaSgP8jpLKOR6bVePCbq5n97aynDzpim1y8jZdGkW4bMAyP4bp8LgYiJmeIRVFcdRutQuZEArsdPW5YCEcD6RmscWWPFzuPNjt7F/PEshaAsHB7gDLZTK1Jv5qpjshRp2PWxTU2JOOXVZ8g1T1lqxVlcSQRPExMLKDESj3m0QHmWsl7f2168JHmXWCOe0lNWFR1F6N3Fl8vl7WIwuOr2a1BSre6cRNz4jrUnI5huY2llMTktsWvm9r45uvh5w9OYvazX+Z+JeDrzv++VTbZ11Y2v+m4itWZt3ddbcNq4lGpGJHy2uap7qa1gHRwVyp2p6wqWIGIWfus2e1HSJGR/lVR2x6ysOlh9pdZEKH0y286GMuTSThc3+rY/vlvELOB3DVz2Ow2NsY0XWb+70NVuXbt9O69rhnqnerbPoXcTtxVG9Vx+4Np2rK8VgMNlH592ezOT/wDt/EehR1vfJuJdfAUNqY9GE3vkMevEZu9mt22/lPSwiQnuqTYF6uNWUdo+YW9i5jhLBcHV7Wz1OPVRPWfWVp0rGBGqjtD1FrLWGVMRdsJ2OhNXaUZFW5K2ct4Zmfv38f8A4k2p2G0tlG5KIz8REREakxH8hsoieSh6ZnUFE+RGlWXZO0urtxVfc1vNHu65bobzsLo4bG1tsbeBJzmNw7kvHewWBztfN0yYvVhRVX9wI6Ho50wJWchON5JhD7Z+o6YGOlCdVglzpcfoYwwZCQxO4No3G/o543P7rsJjMxEAMRDrYjPAQNl/3/T4GP3xSREammmY8fIlHJK71hHhing6OIdjKT76bzM6VrcG7l4G9h8NdwGZlVAVqWbCXowggmCqsKrYlc3UcrhkY8h7xQXrZsxxIBRXyUsm2cmQpFYQC4GPYRQIyREbLR9IJrgrzHpauV6aZdZr2k21C5HjXETp1Lx3FIszM9tubwdXOUuw+uokVlqP1uL5CDhT5dWgZKJQ/wAJeDh9LbJBPjSR7SIjVX6lg3T7TIrTugQAVhwOiIQHqLM74oUOpVROIzm73fM3zTuPYVqTVg9/YrKxC7EFBfbT68Njkaj5OJWfsKOoZjVM+zYJZZFMhIHoDlZwUCUSMTq+M9oDhI9bxHVouitOqg9FcZ9txvA9A10wpXGsvmKuEqxYtW/iG1s9rHfpe6dyTzdq7ewW20xav57fT7AGjF4L4ifsinnb2ycJn0TdwkI3ltGeFUPilHgcjicpVzGOXdq2lyoxcANg0jI+y5EKuiY2Ag65+i46ViOrI/5U5nH+LHVq9PMgECPSMR7V/XuSXp8Rv+Mqaq2KuB2hXyI39/ZKzEjVsWX22y2wX4F6U79vHu71PH/EzJ14gbu6xo5PYp5mPhvzG1Fzowg1FE0S4Ill7Lw8qgtJLrrjyoeLAxOrjYiO3FD+/T/N4I9rz4WRaxvC1mydfEFMswIMi0fzXwwAo9C/AtLWbWCtZCQHIHrcp/LfDHFp1sNXa2dRidR9PI+21EEg41TL/LcabyFkphTIaEFBl1mRaxsiK2STP+S9tn/bHqn4rj6bkqfPbfuIjaZfqe08jjNTHEzE6jbgXsAF3GU6dPZGOjIZC7ddkLrbdirXO3bTXX8SHRDsXiK+MqRQxdWrGrsQOQCI9jv9A9Yj/RPVn/cHqmfSRRpy5W0h1j+Og+WcRkY9tmOax6xgwaeZ0UcxMax0/wCGt8NqHuzGzjc86I1i8tYw1r5ivkcjZyt5lu3r4fYyLWaK+7CRO6viGd+eNTq5MFkx49j/APQPWIj6Rzqz5sHqkqZ6j1kVgQTIY6OTIdWf23Fl7WD1BMaolHSwC9N+YebdEcgnxvDa0cTEiUiRfgXohDbVhaEZ9qtqbVTgK2xMFOHwgsd/T0X9S+Re23PTXLVOOK46OO5ZmIAYAYGLX7apFOPOAtRq8P7ALSy6gEvaU9i71epDBhIler2Nl7iG1X3Lg1ZOrGcxJfgWoGSniMJjK+zcQedy208PZ3TnmZvJxHjj0eyFrkopL4XJz7LxcAI6D6deI0ouHCU6yQSuAHSy6GCWrAQdUo1RKCVwXpPpaV3V+KjoMegufTJ41GVonVs1bV/ZOWmtZzO1a2brzksHjMDjNmUv1bNV6+T+IOelzqVKvj6i61b0aU2XwsYiIGIj2Nnu3BHVz6VeedLPhATqynuV9TExMxNY+tEaT9C7K59Mhvpa75UcTjN7Bavhjsnq5WJDe6tFiHBx65PFVstUKtaejM7ItG1GNwOZ3tfjI5Shj62MqBWqelixJz2l10QkIj0oX7W4N0HYq1r9W5efRRphwtczNIZmSaV0+TgIQqWnxriPSyiGjJRTb0MkJvKiQFgIYLEwet0PbW2zkXI22pGD2Ou9V3ZZoZ7Yc5aviLDbWGo2Hf006qQzDFKt/wBreYmOY0QCY8FERH20bgXHJk5lguhSECmOfSM7Tu5xmFVe2zl9tObd2xtLcOGxmCtG/bNrKXMV81k7Z9RCkYGEI8gM2H6ABAeI1Hozw0uEMByeZHmpZkScpVlDEtTS3LtMjrYzamHzAXr7cjncvdyWcTt7ADyKhEtOQpupqOXyShsW0zr54445i63+zm23QUf6tBYjHEEQrGSJ+SDcWNyNXBbd3LS27iLGOt4V2SZixdl8ltTE5a8i7ZcyEhM6qLkilx3Xee3GOmIcUz7LASt5jNZ3Zb5eoXL8VXzEdhmt2ZJ+K21ct1tmYVOKwina3NkbOeysbYxNSuNKmqvGMz2MzFptWj7fGrGXpU8lWoO3BhNyZ5V4WbFyVW9gloXunawZpAXKlVm4N3BGMvpUqnVBS/Nt2nGNdWpmZLmaAcQRz7LSVu4GG1WKjmaVuQ+mVmv3R6wq2IIu225TTkKTatn/AA9u3FJLH4zbO3E7fpEEZ3L2tx5ItvYTZ2PRjd8ZevW3rmrWDw6nUrOc3LgVxZzFawm3TTZTuHfIYPNqx8xMGsTCjvLM4rNX0ZXepov4OhuHFUrS79CvbTn0M2nuVe4ai2C1QsApgYmZMytM6AEQQrTWE9vhdIp8sCIGIiPdaTCm+K1rmIWdmrDI6wVbMIlLomJ86z9DIX8Oyvj1bDHHIWeI2FFg8/uI7m/yfay2Dx9V+T3Du35vARj6YY7HV6YYzGhuq5ue4zYOWm7gppWc+EYT4iUr85zaFqiq3Y2/8PH2i29NW0YCY8GxoqDkuWWy4iBBC9PfLi4ioqACDn+C6girwfpXtyMdDWpU5Q6kH050u2tkRHpUxlOjYsvrXdvFa3hSzWr+Fv098VszjsudheItnUwPw/oFh655XH7WsYDdsWcXuDAVtw48K9gYIViEyUBHMtuf2qXVNhdbjYtAxpzycXkBkzgYEYiIH+JtMDmSAqTI+wNbXKR0m0pnHLaa2R1DC7KPIDe48MG2ktQ1ZRzGuYjU2Fj9zuKj7TacyOFRVa2eWgtaR5h12PssVtsF1aigfiSUgEx+3+S3W61ei7LF6XdWXgx7LQnR49PR1TNAOfE42Yjmf07iOZiiuJ8xWSH2Nylx5O/xHCzYbPyUuWsgYAYAeI/ndVBk9URQP7zNH/xijSX7he0J8fOv44kcg8ftN52puPKNEwy/JVU2RBT8h/5+nnH5KUCh4H/pkoTDpNlGQKYEKX9T+XSHiDxwGvnRUjifCqgjPUf8n//EAD8QAAEDAgQCCAIGCQQDAAAAAAEAAgMEESExQVEQEgUTICIyYXGBQqEUMDORscEjQENSYpKywtE1U6LigtLw/9oACAEBAAk/APq8ToE6w8k8keaaQU3709ys5qNx+qe5KenBNJ8yrNUt/QJzk8qQfcgCuZqcnruu/UM9bIElGw2TQPNOv6KMqP5HgCVGbJhantLjum8vohzNWiz+twG6eV3QrNCaSdyn2TeY+ZQAHZY0q7V3mJtj5nBGzt1ceYT1gfqtSgrF34LFycQNkAONU0V4FzDY33/BU7pxzhnK11ke44AhUxYKCUR8/NfrDcj8lz9bWvLI+UX2z+/sG3mu9GgDfAgrFn4cNR9RgAhhkFi45le5XefxpoqV9MSJaiqOI82tVUaqZos6YtDeb2U5glqYOsgmb8MjQfyYmdT0tRx88kejw0g84RxMDQfUCyznrXf5/uQwihdL/V/6q5mq3iBgCkJ6mIB73O1AxKiewUzsHk4PboeABHmrgDQLA6hYsOy9r9vxJxKHeKuTqVnqeEM9dWN8UNOzm5fVUM9Jd3K0TgAuUjIvpLfo873i7Qdz/wDfAul5ayWYDnL7NjZ6BRPcGz9XIWi+BI/7L9FXwtIZKPiGrXeSY1krHOsA7mwJJVFH0h0fPKZGs6wMfGUxkNQ6PqoadhuI2rGDo2Pr5PJ+Y/sRvW9IHlP8Ea+zbSRH1IAuVIB0PSN6o4faP3B43BQAdqEO4U4kbFZbdncrwj8Ue8V4jwF5IoHvb6hpIUTqiqn55JS3F8hDj3VO6iNO9j6WiZ8HeAu7+JAMlqIGvaT8L7AhdJT1whaGsp2uLYm2/FZJ4HqU8FPaiCFTxNqJBZ8jWAOcPMqofPJM0NiDx9kNgmF1TX0LKeMtzBc9PY1sTeeeU4Au+IodV0fRkmMSYfSnahMMU8Z5JoX+KJ2x4eFG4PAYZ9nVyyaFkDhxaC0ixBUDK/oyZ3OIC8B0ap2dH9FxODzTtfd8pQAAXeKPI1PKuT6rmHunoEtRsdiqdjqqEERykd4KQ0XRzf0gBONUpA/oWUFxje7vQO/h3BUbGGQ8zi0eI5XPy4a6LEaLRGwI7HuVkMAvdadkhYMWLtzxmZFGM3PdYKVkkbxdrmOuDx7pWDt0SyRneilZ4o3bhSvmcxoaZH5uO57GbVa2RC0Kwdrwzdw0GKFwMr9rwBZcCArVU/8ACe6PdSugpfhuP6WomWiJzzid6j4Sn/Q6n9yV3dPo5W4eMLBw1PZyKF0FotUMFuszgszj2c3LxHNOIaTytDRckqgJcfCZD+QUj4Kc/DJ3B/IFKySQZPm/tag6CK323xn02UYljI5evaP6gqlkLnf7XejJ9NF1s1KzRv6WO34tXR583wH8inExvvmLEFe61z7OW6z4aBarJo4ZWt2cm8Muu/IqkYXGCMuDAGlxICjjpW/zuUz5Xn4nuuVtwqJIJN43W+/dQQ1Y3HccqKFlRIyJ7Xlo523c3Vf7r7LULQ37OhWoxWjuGufDTs6ArF17Dh+zma4/ML4I2D+V4HHZMc97zyhrRiSmua9psWkZcP2rYAf5eZDFwe773k8Neztw0dfhqVvgPqRc9WXNHmMQsZGhwaDs4YfO/GY1E7Qeuh1vsAmtm6WlbaGD9xODpZXXdYWXjleGN9SQE37JnMG/Jv4FY9TE2O/oAOGWHZ/dW/DK3A4X7fw8e7TTnkGwDsWofopz1rPfhiLWex2Twn80r/uaNhwsKeiZzknLmKBNPC/rv/FuDOJ27P7q34ZZIYs7eoWd8OLf0tP47asX+pUn/I/9kCHDMFbcGF8sjg1rRqU8Gsqm81Q9uxz/AMJnLU1JEkm4GgQ4ZN7errLILXALULMFajs4NdxAIOaBdRTHLcatQEgkHNKxuvmtkCSUy9a8Wgg1b5eq78DJObHJ79B6Dj7LN3Z1K0GK346FYlHw9kd4YheJvFt2O11B3CDpaGU3wycNx5p8ZfILmMGzX/4KkZJWj7NgxsdmjUq8VDEdMo27DdyiEcMbbNaOJ7ozK07OTVm7DgMSFYG+CwKzGCwa48aCbpSoZ4+qOAVBN0ZVv8DJsn8NMSsDrxj5mnI6tO4KJmo3akd0+o0KkfHSaO8tmD81C2KFgwaOOazOJ4Tvj6IoLx3acKmQ/kFOx9RBbrWA4svw0Wq0Q7oz4izgsnLNqPeyIRIkbC6xGipjUSuh697WeKQoEviLHxOHijdzAOC+1lgje/1LQeFwdkOUhWPpwAIO/EhNs3dYu34QyVFmO+kSM8EfkSp3Ohd46N3e+7dTPZXtLpasS4SSPVgZ3mSGICxiYcgvdGwaFrieyfiKztYheA7pofHI0scNCDgVTM6T6NLiYmOdZ8So4KXo+suX0eYudhopTFJGQ6pqGfsgNE4kgYuOvBqfgmXO/KolHmhyhOJOyFgiABmVXtbVQgN60DC52KoXwdJwPxiaLuqHpsUVQ4l3IzDkboCqcddG8OJbh1nk7cL2C9kfXtbrwnNHHQrMYNvwwlaA1rtiSBdHrKmra2aWXe4uAn541k4yY3ZPe9sTAwPkddxtqSqpks0R77fIa9uflqaj7JnKe8qiOnp4r/R6WF1+vtq4qFlPNTHkmia21nbqQQdKQG8MgNvYqJ1DS055K2VuDpiPhC7sUbQ1ovkAr8gXsEcSh2RYjVC4RwOAKweM1cFM54pWFrgulYHUWUZmFpIgn9bUynmnm1eVJaPKsqhk1uoCBMNPCyMc3siG1Ek7Y23bdUNHVUbSBJLSPIc32KdzsmYHtPkVSiVlmulkD/swTsiCCLghMdWU1PJaVzGAOiF7B1hophK6imDw9uxP+Q1HuTRh49wmE0VS4MrI2o3DgHD0RFlgwLABD0CsBt9QO6dkcRkVgVh58KptLK82Mjm/DqBsuk6mjrGts+QYslP8TU9slQyVkcjmDAuBfdBj53SmQNf4SRa11SUlM+NwbVP58gHaIktgjbGCdbCytyTu+jwP2tiD8mK5qaB3Uuafkm2pekW/R5x8sf8AgjaGoYW1FA7wPG7fNQTRmnkLWGRhFwmh2N8UbLuxhWAGq8C8R+pGIOHAkjdYncLFi7p8+EDYpKlwfKW/Ed/mVUNMVNEWCIt1scfmohJDKwR1jQ8A7X/D3aonSVAid1bWDEutYKCb6W+5kjMpACjA6Lmi5JmOebtKldHySCRrmDEWTnO5Ra7kbBC5R9lgNAsGjILVZAfVO5UQV9xQAKHKdwE7mao7HcJ2PmntJRRCe1XKYnoAeZWPmsfMp4WJ3P1tusGIHA3HmrtRY4oFpTypPknouKaE8JvuU5e5Vh+oGzk4WT8fMIe6kIT7otVh6BSJ7iu6FJ94TgPRD9UCePdOw2CYFZr/ACTmo831v//EACQRAAICAQQBBQEBAAAAAAAAAAABAhEQAyAhMUESIjJRYTBi/9oACAECAQE/ANlFHBwcFFb0iqLxZebK3dYsooooovD52dZo8F4sX0VZ1hrY8XiznsvF4R+YXWzohoznykLS04fJ2ytWrcePoelCfwdE9KcOWPZI8DyyHu04rxZDRhD4okT0oT7RPjRr9FnwMfQ8s0HcHHz2ac1OKkS09Ry9LfBqSUIGvxGMBZ8Hg8bGac3CSkjR1FB/5ZI1NRTfqfxROTnK2LPgYs0xY0tX0+2XQ79PMvaaur6+F0LFZlh4/RosoplFiRf2fuxbK4KwlZ0P7Ozsb2LnFFiYi1ZZR2PcmVjgpHGKL/hZZwWjgsvb/8QAKhEAAgAGAgEEAQQDAAAAAAAAAAECAxESITEEEFEgIkFhMhMjYqEwQnH/2gAIAQMBAT8A9DiRd4KRHuKRIvFEn626F1dFqXVGWlGf9LfBd5E6+mlzPpCQnUuK6LtsqUN4ZDh+hurPpGi57M1KIoi0eMsrTZs2QvNOm6CwqiXVqNlCsNaFD6Yoc9Me69RbPnvZO5cmS6RPJHyZ81exWryxRca+ijd3kh5U6Uv3VVeUSuTKnYhYvHfxQgP9hb7RNdnImRLdpO5U6b+cRLySuTNlOkLJOeUn/EffyQi2Lp6Fo5sNk6GY9PDJ8typjgZLnyYYL4F7iRA501I4KvjjmkXfyLY/yFvtHIkqdLcDOVIinQ/zh2S0SJDlqxfnF/SJMtSoFAh77+SAi33ch+euVxXMamS8RIShvxL/AHP6ONxv0vdFmJ9IT6+CAZD4No+hMp4KlUVYkOIt+UfQ8unbTTPtG0Kui7OBxIbSHEkbwLKozQ8EMPoftYs6KluRp1Ik26FG0W5qV8FaEOc+locPgu8i+jJVmR0+S7wKH/BahwmSjKNlqEkvT//Z"
                            alt="Product extra guarantee" class="jsx-3141430331">
        
                    </div>
        
                    <div class="info-text">
                        <div class="title-text">TRẢ HÀNG ĐƠN GIẢN THEO 4 BƯỚC</div>
                        <ul class=" text-list-ul">
                            <li class=" text-list">"Thử là mua" chỉ làm việc trực tiếp với các thương hiệu và nhà
                                phân phối chính thức</li>
                            <li class="text-list">"Thử là mua" kiểm soát chặt chẽ quy trình hàng hóa được hợp pháp
                                giao dịch, nhập khẩu và khai thuế</li>
                            <li class="text-list">"Thử là mua" vận hành nghiêm ngặt quy trình kiểm soát chất lượng
                                của từng sản phẩm trước mỗi chương trình ưu đãi</li>
                        </ul>
                    </div>
                    
                </div>
            </div>

    </div>
</div>

@endsection

<script> 
    var productInfo = {!! json_encode($data) !!};

</script>
@section('jsAdditional')
        <script type="text/javascript" src="/js/product-detail.js"></script>        
@endsection