@extends('layout')

@section('contentpage')
    

<div class="order-method-cart container ">
    <div class="titlechildren">
        <h1> 1.Hình thức giao hàng </h1>
    </div>
   
    <div class="method-giaohang"> 
        <ul class="list">
            <li class="row-item">
                  <input type="checkbox">
                    <span class="checkmark"></span>
                    <div class="method-content">
                      <div class="method-content__name">
                          <span>Giao hàng tiết kiệm</span>
                       </div>
                   </div>
                  </span>
                
            </li> 
            <li class="row-item">
                <input type="checkbox">
                  <span class="checkmark"></span>
                  <div class="method-content">
                    <div class="method-content__name">
                        <span>Giao hàng tiêu chuẩn</span>
                     </div>
                 </div>
                </span>
              
          </li> 
           


         
         
            
         </ul>  
     
    </div>

    
    <div class="titlechildren">
        <h1> 2.Hình thức thanh toán</h1>
    </div>
   
    <div class="method-giaohang"> 
        <ul class="list">
            <li class="row-item">
                  <input type="checkbox">
                    <span class="checkmark"></span>
                    <div class="method-content">
                      <div class="method-content__name">
                          <span>Thanh toán bằng tiền mặt khi nhận hàng</span>
                       </div>
                   </div>
                  </span>
                
            </li> 
            <li class="row-item">
                <input type="checkbox">
                  <span class="checkmark"></span>
                  <div class="method-content">
                    <div class="method-content__name">
                        <span>Thanh toán bằng VN-Pay</span>
                     </div>
                 </div>
                </span>
              
          </li> 
           


         
         
            
         </ul>  
     
    </div>

    <div class="method-checkout">
       
        <div class="btn-order">
            <button>Đặt mua </button>

        </div>

        <div class="btn-review">
           <a href="thong-tin-don-hang">
            <button>Kiểm tra đơn hàng </button>
           </a>

        </div>

    </div>

</div>


@endsection



