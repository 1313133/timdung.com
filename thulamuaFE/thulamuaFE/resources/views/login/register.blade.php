


<!DOCTYPE html>
<html lang="en">
<head>
        <title>Thử là mua</title>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1"
        name="viewport" />
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        @hasSection('csspage')
        
            @yield('csspage')
        @else 
        <link rel="stylesheet" href="/css/login.css">

        @endif 
       
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/vendor/slick/slick.css" />
        <link rel="stylesheet" type="text/css" href="/vendor/slick/slick-theme.css" />
</head>

<body>
    <div class="header-menu">
        <div class="container ">
            <div class="logo">
                <a href="/"> 
                <img src="/images/logo-mobile.png">
                </a>
            </div>
            <div class="nav-sub-menu-header">
                <div data-dd-menu-trigger="true" class="nav-menu-header-item dropdown-button">
                    <button class="  btn">
                        Vn
                    </button>
                </div>
    
             
                
            </div>
        </div>
    </div>
    
    <div class="nav-mobile">
        <div class="navbar-open-menu" onclick="openNav()">
    
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <title>ic-menu</title>
                <path d="M0 4h24v2H0V4zm0 7h24v2H0v-2zm0 7h24v2H0v-2z"></path>
            </svg>
    
        </div>
        <div class="navbar-mobile-logo">
            <a href="/"> 
            <img src="/images/logo-mobile.png">
            </a>
        </div>
        <div class="navbar-cart">
    
            <svg  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" onclick="opencartMobile()">
                <title>ic-bag</title>
                <path
                    d="M3.371 22.498L4.733 7.499h3.064v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h5.998v1.706a1.5 1.5 0 1 0 2.253 1.293c0-.553-.304-1.032-.751-1.293V7.499h3.065l1.362 14.999H3.371zM9.3 4.501a3.003 3.003 0 0 1 2.999-2.999 3 3 0 0 1 2.999 2.999v1.497H9.3V4.501zm11.935 1.497h-4.436V4.501A4.504 4.504 0 0 0 12.298 0a4.507 4.507 0 0 0-4.501 4.501v1.497H3.378L1.745 24h21.136L21.235 5.998z">
                </path>
            </svg>
    
        </div>
    
    
    </div>
    
    
    <div id="menuBar" class="menu-mobile">
        <div class="close-btn" onclick="closeNav()"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                viewBox="0 0 24 24">
                <title>ic-cross-line</title>
                <path fill="#4e595d"
                    d="M13.303 12L23.726 1.575A.923.923 0 0 0 22.421.27L11.998 10.695 1.575.27A.923.923 0 0 0 .27 1.575L10.693 12 .27 22.425a.923.923 0 1 0 1.308 1.305l10.423-10.425L22.424 23.73a.924.924 0 1 0 1.308-1.305L13.305 12z">
                </path>
            </svg></div>
        <div class="header-nav">
            <a href="/">
                <img src="/images/logo-mobile.png">
            </a>
    
            <div class="welcome-text">
                Chào mừng bạn quay trở lại
    
            </div>
        </div>
        <div class="content-menu">
            <a class="item">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <title>ic-new-user</title>
                    <path fill="#4e595d"
                        d="M12 13c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6zm0-10.909C9.293 2.091 7.091 4.294 7.091 7S9.294 11.909 12 11.909c2.707 0 4.909-2.203 4.909-4.909S14.706 2.091 12 2.091zM21.263 22H2.737C1.779 22 1 21.327 1 20.5c0-.068.016-1.685 1.418-3.3.816-.94 1.934-1.687 3.322-2.219C7.435 14.33 9.541 14 12 14s4.564.33 6.26.981c1.388.533 2.506 1.279 3.322 2.219C22.984 18.815 23 20.432 23 20.5c0 .827-.779 1.5-1.737 1.5zM12 15c-4.038 0-7.017.953-8.616 2.756-1.198 1.351-1.225 2.732-1.226 2.746 0 .274.259.498.579.498h18.526c.32 0 .579-.224.579-.5 0-.012-.027-1.393-1.226-2.744C19.016 15.953 16.037 15 12 15z">
                    </path>
                </svg>
                <div class="menu-text">TÀI KHOẢN</div>
            </a>
            <a class="item">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <title>ic-box</title>
                    <path
                        d="M3.669 7H20.34l-2.938-3.7H6.65L3.67 7zM20.7 8.3H3.3v12.4h17.4V8.3zM2 7l4.028-5H18.03L22 7v15H2V7zm8.236 4.76V18H9.34v-6.24h.896zm2.464 5.56c.251 0 .504-.088.76-.264l.408.664c-.181.112-.376.201-.584.268s-.408.1-.6.1c-.389 0-.727-.077-1.012-.232s-.504-.368-.656-.64a1.886 1.886 0 0 1-.228-.936c0-.331.085-.635.256-.912s.403-.497.696-.66c.293-.163.613-.244.96-.244.459 0 .84.135 1.144.404s.507.649.608 1.14l-2.608.88c.091.139.211.245.36.32s.315.112.496.112zm-.072-2.152c-.283 0-.513.099-.692.296s-.268.453-.268.768c0 .059.003.104.008.136l1.776-.632c-.069-.165-.171-.301-.304-.408s-.307-.16-.52-.16zm1.692 2.344c0-.133.049-.248.148-.344s.212-.144.34-.144c.117 0 .225.048.324.144s.148.211.148.344c0 .139-.049.255-.148.348a.465.465 0 0 1-.664.004.456.456 0 0 1-.148-.352z">
                    </path>
                </svg>
                <div class="menu-text">QUẢN LÝ ĐƠN HÀNG</div>
            </a>
            <a class="item">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <title>ic-bag-thin</title>
                    <path fill="#4e595d"
                        d="M3.5 22.2L4.797 7.5h2.918v1.672c-.425.255-.71.725-.71 1.267 0 .811.637 1.472 1.425 1.472s1.43-.661 1.43-1.472c0-.542-.289-1.012-.715-1.267V7.5h5.71v1.672c-.425.255-.71.725-.71 1.267 0 .811.637 1.472 1.425 1.472S17 11.25 17 10.439c0-.542-.289-1.012-.715-1.267V7.5h2.919l1.297 14.7h-17zM9 4.735C9 3.06 10.349 1.7 12 1.7c1.656 0 3 1.359 3 3.035V6.25H9V4.735zm11.442 1.513h-4.197V4.813c0-2.38-1.909-4.313-4.259-4.313-2.345 0-4.259 1.933-4.259 4.313v1.435H3.545L1.999 23.5h20L20.441 6.248z">
                    </path>
                </svg>
                <div class="menu-text">GIỎ HÀNG</div>
            </a>
            <a class="item">
                <svg xmlns="http://www.w3.org/2000/svg" width="35" height="24" viewBox="0 0 35 24">
                    <title>ico-promotions-vouchers</title>
                    <path fill="#4e595d"
                        d="M32.933 1.467H2.266a.8.8 0 0 0-.8.8v6a.8.8 0 0 0 .8.8 3.201 3.201 0 0 1 0 6.4.8.8 0 0 0-.8.8v6a.8.8 0 0 0 .8.8h30.667a.8.8 0 0 0 .8-.8v-6a.8.8 0 0 0-.8-.8 3.2 3.2 0 1 1 0-6.4.8.8 0 0 0 .8-.8v-6a.8.8 0 0 0-.8-.8zm-.8 1.599v4.467l-.032.006a4.802 4.802 0 0 0-3.967 4.728l.008.282a4.801 4.801 0 0 0 3.68 4.388l.312.061v4.467H3.067v-4.467l.034-.004a4.8 4.8 0 0 0 3.967-4.728l-.008-.282a4.805 4.805 0 0 0-3.68-4.389l-.313-.063V3.065h29.067z">
                    </path>
                    <path fill="#4e595d"
                        d="M25.6 5.6h1.333V4.267H25.6zM25.6 8.267h1.333V6.934H25.6zM25.6 10.933h1.333V9.6H25.6zM25.6 14.933h1.333V13.6H25.6zM25.6 17.6h1.333v-1.333H25.6zM25.6 20.267h1.333v-1.333H25.6zM21.034 6.368a.8.8 0 0 1 1.224 1.021l-.093.111-10.667 10.667a.8.8 0 0 1-1.224-1.021l.093-.111L21.034 6.368zM12.267 5.467a2.8 2.8 0 1 0 .001 5.601 2.8 2.8 0 0 0-.001-5.601zm0 1.6a1.201 1.201 0 1 1-.002 2.402 1.201 1.201 0 0 1 .002-2.402zM20.267 13.467a2.8 2.8 0 1 0 .001 5.601 2.8 2.8 0 0 0-.001-5.601zm0 1.6a1.201 1.201 0 1 1-.002 2.402 1.201 1.201 0 0 1 .002-2.402z">
                    </path>
                </svg>
                <div class="menu-text">ƯU ĐÃI</div>
            </a>
            <a class="item">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <title>ic-logout</title>
                    <path fill="#4e595d"
                        d="M12 13.941a.658.658 0 0 1-.667-.647V1.647c0-.357.299-.647.667-.647s.667.29.667.647v11.647a.658.658 0 0 1-.667.647z">
                    </path>
                    <path fill="#4e595d"
                        d="M12 23c-2.671 0-5.183-1.009-7.071-2.843S2 15.886 2 13.294c0-2.133.7-4.157 2.023-5.853a10.086 10.086 0 0 1 5.12-3.45c.353-.102.724.093.829.436s-.096.703-.449.805c-3.644 1.053-6.191 4.369-6.191 8.064 0 4.638 3.888 8.412 8.667 8.412s8.667-3.774 8.667-8.412c0-3.695-2.545-7.01-6.191-8.064-.353-.102-.553-.462-.449-.805s.476-.537.829-.436a10.078 10.078 0 0 1 5.12 3.45 9.435 9.435 0 0 1 2.023 5.853c0 2.592-1.04 5.03-2.929 6.863S14.669 23 11.998 23h.003z">
                    </path>
                </svg>
                <div class="menu-text">THOÁT</div>
            </a>
    
            <div class="bottom-mobile-menu">
            </div>
    
        </div>
    
    </div>

    <div class="content-page container ">
        
        <div class="frm-login"> 
                <div class="title-form"> 
                       Tạo tài khoản
                 </div>

                 <div class="frm-social">
                    
                    <button> Tạo mới bằng tài khoản Facebook </button>

                <div>
                <div class="title-break"> 
                   Hoặc
                </div>
               

                 <div class ="frm-input">  

                    <form id ="formRegister">
                    @csrf
                     <div class="form-group">
                            <label for="exampleInputEmail1">Tên đăng nhập</label>
                            <input type="text" name ="userName" id ="userNameId"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Địa chỉ email hoặc số điện thoại">
                            <div id ="userNameError"  class="  invalid-feedback"> Bạn chưa nhập tên đăng nhập.</div>
                    </div>
                    <div class="form-group">
                 
                            <label >Mật khẩu</label>
                                
                            
                            <input type="password" name="password" id ="passwordId" class=" form-control" placeholder="Mật khẩu">
                            <div id ="passwordError"  class="invalid-feedback"> Mật khẩu bạn chưa nhập.</div>
                        
                    </div>

                    <div class="form-group">
                 
                        <label >Nhập lại mật khẩu của bạn</label>
                            
                         
                         <input type="password" name="repeatpassword" id ="repeatpassword" class=" form-control" placeholder="Mật khẩu">
                         <div id ="repeatpasswordError"  class="  invalid-feedback"> Bạn nhập lại mật khẩu vừa nhập.</div>
                     
                       </div>
                       <button style="display:none" id ="btnLoginLoading" disabled  class="btn btn-primary">
                        <span class="spinner-border spinner-border-sm"></span>
                        Đang tạo tài khoản
                         </button>

                         <div class="form-group">
                            <div id ="errorForm"  class=" invalid-feedback"> Hệ thống có lỗi</div> 
                          </div>
                        <button type="button"  onclick="registerUser()" id ="btnRegister" class="btn btn-primary">Tạo tài khoản</button>

                        <div class="tip-text">
                            <p>Bằng việc tiếp tục, bạn đồng ý với <a href="https://tikitech.vn/quy-dinh/" target="_blank"> <strong>điều khoản sử dụng </strong></a> của chúng tôi.
                        </div>
                        <div class="tip-text">

                            <p> Bạn đã là thành viên?  <a href="dang-nhap-he-thong"><strong>Đăng nhập</strong> </a> </p>
                        </div>
                     </form>
                      
                 </div>


        </div>

     



    

    </div>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="/js/service.js"></script>
    <script type="text/javascript" src="/js/login.js"></script>
</body>

</html>

