@extends('layout')

@section('jsAdditional')

<script type="text/javascript" src="/js/processCheckout.js"></script>

@endsection
@section('contentpage')
    
<div class="order-page container ">
    <div class="titlePage">
        <h1> Giỏ hàng </h1>
    </div>
    <div class="checkout-cart">
        <div class="table-order">
            <div class="row product-heading">
                <div class="col-5"><label class="styles__StyledCheckbox-sc-kvz5pc-0 hNjxWW"><span class="checkbox-fake"></span><span class="label" id ="sumQuatinity"> Thông tin sản phẩm</span></label></div>
                <div class="col-2">Đơn giá</div>
                <div class="col-2">Số lượng</div>
                <div class="col-2">Thành tiền</div>
                <div class="col-1"><span class="productsV2__remove-all"><img
                            src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/trash.svg"
                            alt="deleted"></span></div>
            </div>
            <div id ="dataListCard"> 
               


            {{-- <div class="infomation-page">
              
                <div class="col-5 recordInformation">
                  
                        <div class="intended__checkbox"><label
                                class="styles__StyledCheckbox-sc-kvz5pc-0 hNjxWW"><input type="checkbox"
                                    data-view-id="" data-view-index="fc7165aa-2013-11ec-89a9-5286c27b7e80"><span
                                    class="checkbox-fake"></span></label></div>
                                    
                                    <a class="intended__img">
                            
                            <img
                                src="https://salt.tikicdn.com/cache/200x200/ts/product/b3/2d/72/3bdc77a5a1d9f6f2db25a4b9070c2765.jpg"
                                alt="Chuột Không Dây Logitech M221 - Hàng Chính Hãng - Màu Hồng"></a>
                                <div class="intended__content">
                                 
                                        <a class="intended__name">Chuột Không Dây
                                        Logitech M221 - Hàng Chính Hãng - Màu Hồng
                                    
                                        </a>
                                    
                                 </div>
                  
                </div>

                <div class="col-2 priceProduct"> 
                    <span class="intended__real-prices">239.000đ</span>
                </div>
                <div class="col-2">

                    <div class="intended-qty">
                        <div class="quantity-order">
                            <span class="qty-decrease ">
                                <img src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/decrease.svg" alt="decrease">
                            </span>
                            <input type="tel" class="qty-input" value="1">
                            <span class="qty-increase ">
                                <img src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/increase.svg" alt="increase">
                            </span>
                         </div>
                     </div>
                </div> 

                <div class="col-2">
                    <span class="intended__final-prices">239.000đ</span>
                </div>
            
                <div class="col-1" style="text-align: center;">
                    <span class="intended__delete"  >
                        <img src="https://frontend.tikicdn.com/_desktop-next/static/img/icons/trash.svg" alt="deleted">
                    </span>    
                </div>
        
           
            </div> --}}
           
            </div>

          
        </div>

        <div class="checkout-received">
            <div class="section-panel">
              

                @if (count($dataLocation) <1)
                        <div class="menu-bar-received">
                            <p>Giao tới</p>
                             <a href="/dia-chi-cua-toi"> Thêm mới địa chỉ</a>
                        </div>

                        <p><strong>Chưa có thông tin địa chỉ nhận hàng.</strong></p>

                @else
                    
                        @php
                        $dataLocationItem = $dataLocation[0];
                        $addressText = $dataLocationItem->address.", ".$dataLocationItem->ward.", ".$dataLocationItem->distrist.", ".$dataLocationItem->city;
                        @endphp

                        
                        <div class="menu-bar-received">
                            <p>Giao tới</p>

                            <a href="javascript:void(0)"> Thay đổi</a>
                        </div>
                    
                @endif
            </div>

                
             


            <div class="section-panel">
                <div class="row-item">
                    <p>Tạm tính</p>
                    <p id ="sum-amount-temp">0đ</p>
                </div>

                <div class="row-item">
                    <p>Giảm giá</p>
                    <p>0đ</p>
                </div>

                <div class="row-item">
                    <p>Phí vận chuyển</p>
                    <p >20.0000đ</p>
                    
                </div>
                
                 <div class="row-item ">
                   <p>Tổng cộng </p>
                   
                   <p class="sum-amount" ><strong id ="sum-money" class="priceCal">239.000đ </strong> <br> (Đã bao gồm phí VAT) </p> 
                </div>

                <input id ="fee-trans"  value="20000" type ="hidden" id = "row-item-fee">

                <div class="btn-order">
                    <a href="javascript:void(0)" onclick="checkout()">
                        <button>Mua hàng </button>
                    </a>
                  

                </div>

            </div>
           
        </div>


    </div>


</div>


<script> 
    var dataLocation = {!! json_encode($dataLocation) !!};

</script>
@endsection



