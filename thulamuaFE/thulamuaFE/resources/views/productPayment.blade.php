@extends('layout')

@section('jsAdditional')

<script type="text/javascript" src="/js/checkout.js"></script>

@endsection

@section('contentpage')
    

<div class="order-method-cart container ">
    <div class="titlechildren">
        <h1> 1.Hình thức giao hàng </h1>
    </div>
   
    <div class="method-giaohang"> 
        <ul class="list">
            <li class="row-item">
                  <input disabled checked  value="0" name ="transport_method" type="checkbox">
                    <span class="checkmarck"></span>
                    <div class="method-content">
                      <div class="method-content__name">
                          <span>Giao hàng tiết kiệm</span>
                       </div>
                   </div>
                  </span>
                
            </li> 

          
      </ul>  
     
    </div>

    
    <div class="titlechildren">
        <h1> 2.Hình thức thanh toán</h1>
    </div>
   
    <div class="method-giaohang"> 
        <ul class="list">
            <li class="row-item">
                  <input type="radio" value="1" name="payment_method">
                    <span class="checkmark"></span>
                    <div class="method-content">
                      <div class="method-content__name">
                          <span>Thanh toán tiền khi nhận hàng (COD)</span>
                       </div>
                   </div>
                  </span>
                
            </li> 
            <li class="row-item">
                <input type="radio" value="0"  name="payment_method">
                  <span class="checkmark"></span>
                  <div class="method-content">
                    <div class="method-content__name">
                        <span>Thanh toán trực tuyến (Internet Banking, VNPAY-QR, Ví điện tử VNPAY, Visa - Miễn phí thanh toán)</span>
                     </div>
                 </div>
                </span>
              
          </li> 
           


         
         
            
         </ul>  
     
    </div>

    <div class="method-checkout">
       
        <div class="btn-order">
            <a href="javascript:void(0)" onclick="checkout()">
            <button>Thanh toán và đặt hàng </button>

            </a>
        </div>

        <div class="btn-review">
           <a href="thong-tin-don-hang">
            <button>Kiểm tra đơn hàng </button>
           </a>

        </div>

    </div>

</div>


<script> 

    var _token = {!! json_encode(csrf_token()) !!};

</script>

@endsection



