<!DOCTYPE html>
<html lang="en">
<head>

      <meta charset="utf-8">

      @hasSection('seoBasic')

      @yield('seoBasic')
      @else 
           <title>Thử là mua</title>

      @endif 
   

        <link rel='shortcut icon' type='image/x-icon' href='/favicion.ico' />

        <meta content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5, user-scalable=1"
        name="viewport" />
        <meta http-equiv="X-UA-Compatible" content="IE=100" />
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        @hasSection('csspage')
        
            @yield('csspage')
        @else 
          <link rel="stylesheet" href="/css/homepage.css">

        @endif 
        <link rel="stylesheet" href="/css/toolbox.css">
        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/vendor/slick/slick.css" />
        <link rel="stylesheet" type="text/css" href="/vendor/slick/slick-theme.css" />
</head>

<body>
    @include('share.desktop.header')
    @yield('contentpage')
    @include('share.footer')
    <script type="text/javascript" src="/vendor//slick/slick.min.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>

    @hasSection('jsAdditional')
          
      @yield('jsAdditional')
      @else 
    

      @endif 
      <script type="text/javascript" src="/js/cart.js"></script>

      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>

</html>