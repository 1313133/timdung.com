<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

// Route::get('sales/{slug}', 'HomeController@listProduct');
Route::get('sales/{slug}', 'HomeController@listProduct');

Route::get('thong-tin-san-pham', 'HomeController@product');
Route::get('thong-tin-san-pham/{slug}', 'HomeController@product');

Route::get('dang-nhap-he-thong', 'AccoutController@loginPage');

Route::get('thong-tin-tai-khoan', 'AccoutController@AccountPage');

Route::get('mo-tai-khoan', 'AccoutController@registerPage');

Route::post('dang-nhap', 'AccoutController@Login');

Route::get('chinh-sach-va-bao-mat', 'PolycyController@Polycy');

Route::get('dieu-khoan-va-dich-vu', 'PolycyController@Term');
Route::get('gioi-thieu', 'PolycyController@gioithieu');

Route::get('dang-ky-nguoi-dung', 'AccoutController@RegisterAccout');
Route::post('dang-ky-nguoi-dung', 'AccoutController@RegisterAccout');

Route::get('dang-xuat', 'AccoutController@Logout');
Route::post('dang-xuat', 'AccoutController@Logout');




Route::get('dia-chi-cua-toi', 'LocationController@AddressPage');

Route::get('thong-tin-don-hang', 'AccoutController@OrderInfo');
Route::get('quan-ly-don-hang', 'OrderController@quanlydonhang');
Route::get('thanh-toan-va-van-chuyen', 'AccoutController@PaymentPage');
Route::get('get-all-deal', 'DealController@GetAll');

Route::get('ket-qua-don-hang', 'AccoutController@resultOrder');


Route::get('cong-thanh-toan', 'VnpayController@create');

Route::get('return-vnpay','VnpayController@reponseVNPay');
Route::post('tai-khoan/cap-nhat-thong-tin', 'AccoutController@update');
Route::post('/tai-khoan/thay-doi-mat-khau', 'AccoutController@changePassword');

Route::post('dia-chi/them-moi-dia-chi', 'LocationController@Add');
Route::post('dia-chi/cap-nhat-dia-chi', 'LocationController@Update');


Route::post('/don-hang/them-moi-don-hang', 'OrderController@create');

Route::get('/don-hang/getALL', 'OrderController@getAll');