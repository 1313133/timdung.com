<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Jenssegers\Agent\Agent;

class HomePageController extends Controller
{

   
    public function redireHomePage(Request $request, $slug =null )
    {
        return redirect('/');
    }
   
    public function CheckUrl ($slug) 
    {
        $checkacssSlugUrl ="https://api-soida.applamdep.com/api/check-access-slug";
        $client = new Client();

        $res = $client->request('post', 'https://api-soida.applamdep.com/api/check-access-slug', [
            'json' => [

                'slug'=> $slug
              ]
        ]);
        
     
        if($res->getStatusCode() ==200)
        {
            $checkresult = $res->getBody()->getContents();
            $checkresult = json_decode($checkresult);
    
           
            
            if($checkresult->is_success)
            {
                $result  = $checkresult->data;
                if($result == null)
                {
                    return  false;
                }
                else 
                {
                    session(['dataCompany' => $checkresult->data->company_data]);
                }
            
                return $result->isAccess;
            }
             return  false;
           
         }
        return false;

    }

    public function index (Request $request, $slug =null) 
    {
        
        $isCheck  = true;

        if($slug == "" ||$slug ==null)
        {

        }
        else 
        {
            $isCheck = $this->CheckUrl($slug);
        }  
        if(!$isCheck)
        {
        return view("notfound");
        return;
        }
        return view("welcome");
       

    }

}
