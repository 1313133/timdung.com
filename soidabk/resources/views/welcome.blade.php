<html lang="en">
@php
 $data = Session::get('dataCompany');
 
@endphp

<head>
    <meta charset="UTF-8" />
    <meta name="theme-color" content="#d47690">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=no" />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  
    <script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.5/dist/html2canvas.min.js"></script>
    <!-- <link rel="stylesheet" href="http://localhost:5000/contain/css/style.css" /> -->
    <link rel="stylesheet" href="./contain/css/style.css" />
    <!-- <link rel="stylesheet" href="https://ungdungsoida.netlify.app/contain/css/style.css" /> -->

    <title>Ứng dụng soi da</title>
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="./assets/aos/aos.css" />
    <link rel="stylesheet" href="./styles/skin_screening/index.css" />
    <link rel="stylesheet" href="./styles/skin_screening/responsive.css" />
    <link rel="stylesheet" href="./styles/global/index.css" />
    <link rel="stylesheet" href="./styles/global/global_responsive.css" />
    <!-- ASSETS CDN SLICK -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <style>
  
    </style>
<body>

    
    <div id="b-placeholder"></div>
    <script>
        var dataCompany = {!! json_encode($data) !!};
    </script>
    <script>
        function getBaseUrl() {
            // return "https://ungdungsoida.netlify.app/";
            return "./";
        }
    </script>
    <script>
        function resolveImageSource() {
            var baseUrl = getBaseUrl();
            var images = document.querySelectorAll("img[data-src]");
            for (var i = 0; i < images.length; i++) {
                var img = images[i];
                img.src = baseUrl + img.getAttribute("data-src");
            }
        }
    </script>
    <script>
        function pathToRegex(path) {
            return new RegExp(
                "^" +
                path
                .replace(new RegExp("\/", "g"), "\\/")
                .replace(new RegExp(":\w+", "g"), "(.+)") +
                "$"
            );
        }

        function router() {
            const routes = [{
                path: "/",
                url: "contain/skin.html",
            }, {
                path: "/welcome",
                url: "contain/welcome.html",
            }, {
                path: "/soida/skin",
                url: "contain/skin.html",
            }, {
                path: "/soida/result",
                url: "contain/result.html",
            }, ];


            const potentialMatches = routes.map((route) => {
                return {
                    route,
                    result: location.pathname.match(pathToRegex(route.path)),
                };
            });

            var match = potentialMatches.find((p) => p.result !== null);

            if (!match) {
                match = {
                    route: routes[0],
                    result: [location.pathname],
                };
            } else {

            }

            $(function() {
                $("#b-placeholder").load(getBaseUrl() + match.route.url, function() {
                    resolveImageSource();
                });
            });
        }


        function navigateTo(url) {
            history.pushState(null, null, url);
            router();
        }
         

        window.addEventListener("popstate", router);

        document.addEventListener("DOMContentLoaded", function() {
            router();
            sessionStorage.setItem('dataCompany', JSON.stringify(dataCompany));
        
        });

    </script>

    <script>
        
        function opencamera() {
             $("#takeFile").click();
        }

        function choseImage() {
            $("#choseImageFile").click();
        }
        
        function sendreward() {
            $("#btnreward").hide();
            $("#btnrewardLoading").show();
            $.ajax({
                type: "PUT",
                url: "https://api-soida.applamdep.com/api/add-customer-request",
                data: JSON.stringify({
                    UserName: "TIKITECH",
                    Phone: $("#mobilePhone").val(),
                    Type: 0,
                }),
                contentType: "application/json",
                dataType: "json",
                complete: function(data) {
                    $("#btnreward").show();
                    $("#btnrewardLoading").hide();
                    $("#formContact").hide();
                    $("#result").show();

                },
            });
        }

        function sendreward() {
            $("#btnreward").hide();
            $("#btnrewardLoading").show();
            $.ajax({
                type: "PUT",
                url: "https://api-soida.applamdep.com/api/add-customer-request",
                data: JSON.stringify({
                    UserName: "TIKITECH",
                    Phone: $("#mobilePhone").val(),
                    Type: 0,
                }),
                contentType: "application/json",
                dataType: "json",
                complete: function(data) {
                    $("#btnreward").show();
                    $("#btnrewardLoading").hide();
                    $("#formContact").hide();
                    $("#result").show();

                },
            });
        }

        function closePopup(elementclose)
        {
          
            var elementRemove = elementclose.closest("div");
            var elementParrent = elementclose.closest(".ai-skin__container");
            elementParrent.removeChild(elementRemove); 
          
            
        }
    </script>
</body>

</html>