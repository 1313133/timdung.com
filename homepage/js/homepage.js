function searchText(valueSearch = null) {

    var txtSearch = document.getElementById("txtSearch").value;

    if (valueSearch != null) {
        txtSearch = valueSearch;
    }

    var url = 'result.html?keysearch=' + txtSearch;
    window.location.href = url;
}




document.addEventListener('DOMContentLoaded', function() {

    var input = document.getElementById("txtSearch");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {

            event.preventDefault();
            searchText();
        }
    });


    var input = document.getElementById("txtSearchMobile");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {

            event.preventDefault();
            searchText(document.getElementById("txtSearchMobile").value);
        }
    });

}, false);


function openNav() {
    document.getElementById("mySidenav").style.width = "80%";
    document.getElementById("txtSearchMobile")
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function openSearchbar() {
    document.getElementById("navsearchmobinle").style.display = "block";
    document.getElementById("txtSearchMobile").focus();
}

function closeSearchbar() {
    document.getElementById("navsearchmobinle").style.display = "none";
}