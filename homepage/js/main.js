function getUrlVars() {

    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
        function(m, key, value) {
            vars[key] = value;
        });
    return vars;
}


function searchText(inputSearch = null) {
    var txtSearch = document.getElementById("txtSearch").value;

    if (inputSearch != null) {
        txtSearch = inputSearch;
    }
    var url = '/result.html?keysearch=' + txtSearch;
    window.location.href = url;
}


$(document).ready(function() {

    var input = document.getElementById("txtSearch");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            searchText();

        }
    });

    var input = document.getElementById("txtinputMobile");
    input.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            var valueSearch = document.getElementById("txtinputMobile").value;
            searchText(valueSearch);

        }
    });

    var baseUrl = "https://api-soida.applamdep.com/";
    var urlSearch = "api/search-plugin";

    var currenturl = (window.location.href);
    var url = new URL(currenturl);
    var keySearch = url.searchParams.get("keysearch");
    $("#txtSearch").val(keySearch);
    $("#txtinputMobile").val(keySearch);

    var bodyData = {
        "key": keySearch

    };
    $.ajax({
        url: baseUrl + urlSearch,
        type: 'POST',
        dataType: "json",
        data: {
            key: keySearch,
        }
    }).done(function(data) {

        var totalCompany = data.total;
        if (totalCompany.total_company > 0) {

            $("#numberCount").html(totalCompany.total_company);
            $("#recordedCount").show();
        }
        var allData = data.data;
        var htmlTemplate = "";
        for (let index = 0; index < allData.length; index++) {
            var itemSearch = allData[index];


            var dataSliderBanner = itemSearch.data;
            htmlTemplate += '<div class="searchItem">\
           <h5> ' + itemSearch.company_name + ' </h5>';

            for (let i = 0; i < dataSliderBanner.length; i++) {
                var slideItem = dataSliderBanner[i];

                htmlTemplate += '<p class="titleType">' + slideItem.type + ' </p>\
                   <div class="slide-product">';
                for (let z = 0; z < slideItem.data.length; z++) {
                    var itemProduct = slideItem.data[z];
                    var imagelink = "https://i.postimg.cc/W3WBCxvs/KEM-D-NG-BAN-M-Q10.jpg";


                    if (itemProduct.image_link) {
                        if (itemProduct.image_link != "") {
                            imagelink = "https://api-soida.applamdep.com/public/image_plugin/" + itemProduct.image_link;
                        }
                    }


                    htmlTemplate += '    <div class="productItem">\
                    <a target ="_blank" href="' + itemProduct.linkdetail + '" ><img src="' + imagelink + '">\
                    <div>' + itemProduct.title + '</div></a>\
                    </div>';
                }

                htmlTemplate += ' </div>';
            }

            htmlTemplate += '</div>';
        }


        var clientWidth = document.body.clientWidth;
        var numberSlider = 3;
        if (clientWidth <= 992) {
            $("#bodyResult-mobile").html(htmlTemplate);
            numberSlider = 2;
        } else {
            $("#bodyResult").html(htmlTemplate);
        }


        $('.slide-product').slick({
            slidesToShow: numberSlider,
            slidesToScroll: numberSlider,

            arrows: true,
            prevArrow: '<div class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
            nextArrow: '<div class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'
        });
    });

});