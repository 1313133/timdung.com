class locationMapSwitcher{
    constructor(switcher) {
        this.switcher = $(switcher);
        if(!this.switcher){
            return;
        }
        this.highlight = this.switcher.find('[data-switcher-highlight]');
        this.item = this.switcher.find('[data-switcher-item]');
        this.location = $('body').find('[data-mode-switch]');
        this.counter = this.location.find('[data-animation-counter]');
        this.pins = this.location.find('[data-location-pin]');
        this.do_pins = this.location.find('[data-location-pin].locations-map__pin--do');
        this.not_do_pins = this.location.find('[data-location-pin]:not(.locations-map__pin--do)');
        this.bindEvents();
    }
    bindEvents(){
        this.highlight.on('click',(event)=>{
            this.clickButton(event);
        });
        this.item.on('click',(event)=>{
            this.switch(event, this);
        });
    }
    clickButton(event){
        let that = this;
        this.highlight.siblings('.switcher__items').find('.switcher__item').not('.is-active').trigger('click');
    }
    switch(event){
        if (!$(event.currentTarget).hasClass('is-active')){
            this.switchMap(event);
        }
    }
    switchMap(event){
        this.location[0].classList.toggle('locations-map--vultr');
        this.location[0].classList.toggle('locations-map--do');
        let type = this.location[0].classList[1];
        this.mapAnimation(type);
    }
    mapAnimation(type){
        let pinDelay = 80,
            counterAnimation = {},
            counterAnimationDuration = (this.pins.length - this.do_pins.length) * 80;;

        if (type == "locations-map--vultr"){
            counterAnimation = {
                counterStartValue: this.pins.length,
                counterValue: this.do_pins.length
            };
            $('#moveDot').css({'left':'961px','top':'203px'});
            $('#moveDotName').text('Tokyo');
            $('#caliNameSwitch').text('Silicon Valley');
            $('#switchBox').css('background-color','#007bfc');
            $('#logoVultr').addClass('remove_filter');
            $('#toggleDot').css('transform','translateX(3px)');
            $('#VultrToggle').addClass('is-active');
            $('#doToggle').removeClass('is-active');
            for (let pin of Array.from(this.not_do_pins)) {
                anime({
                    targets: pin,
                    scale: [4.2, 1],
                    opacity: [0, 1],
                    delay: pinDelay,
                    duration: 480,
                    easing: 'cubicBezier(.16,0,0,1)',
                    begin: function(anim) {
                        $(pin).removeClass('is-hidden');
                    },
                });
                pinDelay = pinDelay + 80
    
            }
        }
        else{
            counterAnimation = {
                counterStartValue: this.do_pins.length,
                counterValue: this.pins.length 
            };
            $('#moveDot').css({'left':'762px','top':'295px'});
            $('#moveDotName').text('Bangalore');
            $('#caliNameSwitch').text('San Francisco');
            $('#switchBox').css('background-color','#798ea2');
            $('#logoVultr').removeClass('remove_filter');
            $('#toggleDot').css('transform','translateX(39px)');
            $('#vultrToggle').removeClass('is-active');
            $('#doToggle').addClass('is-active');
            for (let pin of Array.from(this.not_do_pins)) {
                anime({
                    targets: pin,
                    scale: [1, 4.2],
                    opacity: [1, 0],
                    delay: pinDelay,
                    duration: 480,
                    easing: 'cubicBezier(.16,0,0,1)',
                    complete: function(anim) {
                        $(pin).addClass('is-hidden');
                    }
                });
                pinDelay = pinDelay + 80
            }
        }
        
        anime({
            targets: counterAnimation,
            counterValue: counterAnimation.counterStartValue,
            round: 1,
            easing: 'linear',
            duration: counterAnimationDuration,
            update: () => {
                this.counter.text(counterAnimation.counterValue)
            },
            complete: () => {
                setTimeout(() => {

                    //locationsMap.init();

                }, 500)
            }
        });
    }
}

$('[data-switcher]').each(function () {
	new locationMapSwitcher(this)
});
