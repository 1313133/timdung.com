function openNav() {
 
    document.getElementById("menuBar").style.width = "70%";
  
    document.body.style.backgroundColor = "#ffffff";
  }
  
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
  function closeNav() {
    
    document.getElementById("menuBar").style.width = "0";
    // document.getElementById("main").style.marginLeft = "0";
    // document.body.style.backgroundColor = "white";
  }

  function openCart() 
  {
    document.getElementById("cart-order").style.width = "30%";
  }

  function closeCart() 
  {
    document.getElementById("cart-order").style.width = "0%";
  }


  function openCartMobile() 
  {
    document.getElementById("cart-order").style.width = "30%";
  }

  function closeCartMobile() 
  {
    document.getElementById("cart-order").style.width = "0%";
  }

 
  $(document).ready(function(){
    
    $('.slidebanner').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: false
    });
   
 

//      // Remove active class from all thumbnail slides
//  $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

//  // Set active class to first thumbnail slides
//  $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

//  // On before slide change match active thumbnail to current slide
//  $('.slidebanner').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
//  	var mySlideNumber = nextSlide;
//  	$('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
//  	$('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
// });
    // $('.slidebanner').slick({
    //     dots: true,
    //     centerMode: true,
    //     focusOnSelect: true,
    //     arrows: false,
    //     customPaging: function(slider, i) {
    //         return '<div class="thumbnails">' +$(slider.$slides[i]).find('img').prop('outerHTML')+ '</div>';
    //     }
    // });





    var coll = document.getElementsByClassName("titile-section");
    var i;

    for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
    this.classList.toggle("open");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
    content.style.display = "none";
    } else {
    content.style.display = "block";
    }
    });
    }
  });
