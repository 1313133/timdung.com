function openNav() {
 
    document.getElementById("menuBar").style.width = "70%";
  
    document.body.style.backgroundColor = "#ffffff";
  }
  
  /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
  function closeNav() {
    
    document.getElementById("menuBar").style.width = "0";
    // document.getElementById("main").style.marginLeft = "0";
    // document.body.style.backgroundColor = "white";
  }

  function openCart() 
  {
    document.getElementById("cart-order").style.width = "30%";
  }

  function closeCart() 
  {
    document.getElementById("cart-order").style.width = "0%";
  }


  function openCartMobile() 
  {
    document.getElementById("cart-order").style.width = "30%";
  }

  function closeCartMobile() 
  {
    document.getElementById("cart-order").style.width = "0%";
  }

  $(document).ready(function(){
    $('.mobile-slidebanner').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });


    var coll = document.getElementsByClassName("titile-section");
    var i;

    for (i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
    this.classList.toggle("open");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
    content.style.display = "none";
    } else {
    content.style.display = "block";
    }
    });
    }
  });
